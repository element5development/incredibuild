<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Incredibuild
 */

get_header(); ?>

	<section class="sub-banner curve bottom-left-top-right">
		<div class="gradient inverse overlay"></div>
		<div class="container text-center aos-item" data-aos="zoom-in">
			<h2><?php printf( esc_html__( 'Search Results for: %s', 'incredibuild' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
		</div>
	</section>
	<section class="awards">
		<div class="container">
			<?php
		if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

						<div class="row aos-item" data-aos="fade-up">
							<div class="col-md-12">
								<h5><?php the_title(); ?></h5>
								<p><?php the_excerpt(); ?></p>
								<p><a href="<?php the_permalink(); ?>" class="button inverse orange-btn">continue reading</a></p>
							</div>
						</div>
<?php			endwhile;

			echo '<div class="row">';
			echo '<div class="col-md-12 pt-50">';
			the_posts_navigation();
			echo '</div>';
			echo '</div>';

		else :
			echo '<div class="row">';
			echo '<div class="col-md-12 pt-50">';
			?>

	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'incredibuild' ); ?></h1>
	</header><!-- .page-header -->
	<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'incredibuild' ); ?></p>
			<?php

			echo '</div>';
			echo '</div>';

		endif; ?>
			</div>
	</section>
<?php
get_footer();
