<?php
/**
 * Template Name: Pricing Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	<style type="text/css">
		.list-style-tick ul {
			padding: 0;
		}
		.list-style-tick li {
			position: relative;
			margin-bottom: 20px;
			line-height: 28px;
			font-size: 16px;
			padding-left: 32px;
		}
		.list-style-tick li:before {
			content: '';
			position: absolute;
			top: 8px;
			left: 0px;
			background: url(<?php bloginfo('template_url'); ?>/assets/images/tick-pink.png);
			width: 16px;
			height: 13px;
		}
		.tick-point-3,
		.tick-point-2,
		.tick-point-1 {
			position: relative;
			padding-left: 60px;
		}
		.tick-point-1:before {
			content: '';
			position: absolute;
			top: 8px;
			left: 0px;
			background: url(<?php bloginfo('template_url'); ?>/assets/images/One.png) no-repeat;
			width: 40px;
			height: 37px;
		}
		.tick-point-2:before {
			content: '';
			position: absolute;
			top: 8px;
			left: 0px;
			background: url(<?php bloginfo('template_url'); ?>/assets/images/Two.png) no-repeat;
			width: 40px;
			height: 38px;
		}
		.tick-point-3:before {
			content: '';
			position: absolute;
			top: 8px;
			left: 0px;
			background: url(<?php bloginfo('template_url'); ?>/assets/images/Three.png) no-repeat;
			width: 40px;
			height: 38px;
		}
		.price-data input[type="number"] {
			height: 60px;
			border: 2px solid rgba(45, 49, 53, 0.25);
			background-color: #ffffff;
			border-radius: 50px;
			margin-bottom: 80px;
			padding-left: 15px;
		}
		input[type=number]::-webkit-inner-spin-button, 
		input[type=number]::-webkit-outer-spin-button { 
			-webkit-appearance: none; 
			margin: 0; 
		}
		.plus-minus .btn-number {
		    width: 40px;
		    height: 40px;
		}
		.plus-minus .btn-number.btn .glyphicon {
			font-size: 14px;
		}
	</style>
	<section class="pricing-box pt-50">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 style="text-transform: initial;">IncrediBuild accelerates your development by transforming the various machines on your network into a virtual multi-core supercomputer.</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<img src="<?php echo site_url(); ?>/wp-content/uploads/2017/10/graphic.png" class="img-responsive">
				</div>
				<div class="col-md-6 list-style-tick" style="padding-top: 10px;">
					<p>Here’s what you’ll need to let our parallel computing acceleration magic take place:</p>
					<ul>
						<li>An IncrediBuild Agent on each machine (including the developer’s machine).</li>
						<li>The relevant acceleration solution for developers who initiate the accelerated build from their machine and for a CI server from which the build starts.</li>
					</ul>
				</div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-md-12 price-data">
					<h6>IncrediBuild practically pays for itself.</h6>
					<p>Answer these three questions to see IncrediBuild’s pricing and ROI, tailor-made to your needs:</p>
				</div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-md-12">
					<h4 class="tick-point-1" style="color: #2d3135;">First, pick how many developers are in your team, and how many cores their machines have.</h4>
				</div>
			</div>
			<div class="row price-data">
				<div class="col-xs-5 col-should-extend">
					<h6>Number of Developer Machines</h6>
				</div>
				<div class="col-xs-4 col-should-extend">
					<h6>Number of Cores</h6>
				</div>
				<div class="col-xs-2"></div>
			</div>
			<div class="row price-data machineBlock">
				<div class="col-xs-5 col-should-extend">
					<input type="number" id="numDev1" name="numDev1" style="margin: 0px; text-align: center; padding: 0; font-size: 20px; width: 160px;" disabled="disabled">
				</div>
				<div class="col-xs-4 col-should-extend">
					<select class="aos-item" data-aos="fade-down" style="margin: 0px; min-width: 100%;" id="numDevCore1" name="numDevCore1">
						<option value="">Add a number of cores</option>
						<option value="4">Up to 4 cores</option>
						<option value="8">Up to 8 cores</option>
						<option value="16">Up to 16 cores</option>
						<option value="28">Up to 28 cores</option>
						<option value="32">Up to 32 cores</option>
						<option value="48">Up to 48 cores</option>
						<option value="64">Up to 64 cores</option>
					</select>
				</div>
				<div class="col-xs-2"></div>
			</div>
			<input type="hidden" id="machineBlockCount" name="machineBlockCount" value="1">
			<div class="row price-data">
				<div class="col-md-12">
					<div class="border-top" style="margin-top: 40px;"></div>
				</div>
			</div>
			<center><a href="javascript:;" class="button inverse orange-btn add-more-machine-btn">ADD MORE MACHINES</a></center>
			<br>
			<br>
			<br>
			<div class="row">
				<div class="col-md-12">
					<h4 class="tick-point-2" style="color: #2d3135;">Now, add the IncrediBuild solutions best fitting your acceleration needs, and how many developers will need to initiate them from their machine.</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 price-data">
					<div class="input-sec mt-30 aos-item" data-aos="fade-down">
						<div class="input-group plus-minus">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[3]">
									<span class="glyphicon glyphicon-minus"></span>
								</button>
							</span>
							<input type="text" name="quant[3]" class="form-control input-number" readonly="readonly" value="0" min="0" max="10">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[3]">
									<span class="glyphicon glyphicon-plus"></span>
								</button>
							</span>
						</div>
						<div class="input-text">
							<p><font class="secName">Visual Studio C/C++</font> <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Accelerates runtime for various in-house and commercial development tools used during the development process"></i>
							<input type="hidden" name="sectionPriceField" value="99">
							<input type="hidden" name="sectionPriceFieldOriginal" value="99">
							<span class="pull-right"><strong>$<font class="sectionPrice">0</font></strong></span></p>
						</div>
					</div>
					<div class="input-sec aos-item" data-aos="fade-down">
						<div class="input-group plus-minus">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
									<span class="glyphicon glyphicon-minus"></span>
								</button>
							</span>
							<input type="text" name="quant[1]" class="form-control input-number" readonly="readonly" value="0" min="0" max="10">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
									<span class="glyphicon glyphicon-plus"></span>
								</button>
							</span>
						</div>
						<div class="input-text">
							<p><font class="secName">Make and Build Tools</font> <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Accelerates runtime for various in-house and commercial development tools used during the development process"></i>
							<input type="hidden" name="sectionPriceField" value="99">
							<input type="hidden" name="sectionPriceFieldOriginal" value="99">
							<span class="pull-right"><strong>$<font class="sectionPrice">0</font></strong></span></p>
						</div>
					</div>
					<div class="input-sec aos-item" data-aos="fade-down">
						<div class="input-group plus-minus">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quant[2]">
									<span class="glyphicon glyphicon-minus"></span>
								</button>
							</span>
							<input type="text" name="quant[2]" class="form-control input-number" readonly="readonly" value="0" min="0" max="10">
							<span class="input-group-btn">
								<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[2]">
									<span class="glyphicon glyphicon-plus"></span>
								</button>
							</span>
						</div>
						<div class="input-text">
							<p><font class="secName">IncrediBuild for Dev Tools</font> <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Accelerates runtime for various in-house and commercial development tools used during the development process"></i>
							<input type="hidden" name="sectionPriceField" value="125">
							<input type="hidden" name="sectionPriceFieldOriginal" value="125">
							<span class="pull-right"><strong>$<font class="sectionPrice">0</font></strong></span></p>
						</div>
					</div>
					<div class="addMoreSolutionHere aos-item" data-aos="fade-down"></div>
					<h4 class="mt-30 aos-item removeHeadingAsWell" data-aos="fade-down" style="color: #2d3135;">Add More IncrediBuild solutions</h4>
					<div id="nogo"></div>
					<select class="aos-item" data-aos="fade-down" id="addMoreSolution" style="margin-top: 0px; margin-bottom: 20px;">
						<option value="">Select</option>
						<option value="IncrediBuild for Wii U" data-quan="4" data-priceVal="99">IncrediBuild for Wii U</option>
						<option value="IncrediBuild for 3DS" data-quan="5" data-priceVal="99">IncrediBuild for 3DS</option>
						<option value="Sony Playstation4 and Vita" data-quan="6" data-priceVal="99">IncrediBuild for Sony Playstation4 and Vita</option>
						<option value="IncrediBuild for Xbox One" data-quan="7" data-priceVal="99">IncrediBuild for Xbox One</option>
						<option value="IncrediBuild for C#" data-quan="8" data-priceVal="75">IncrediBuild for C#</option>
					</select>
				</div>
				<div class="col-md-3"></div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-md-12">
					<h4 class="tick-point-3" style="color: #2d3135;">Last but not least, select your current average build/task time in minutes.</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 price-data">
					<div class="aos-item" data-aos="fade-down">
						<label>Build/Task Time in Minutes</label>
						<input type="range" id="buildMinutes" class="range2" value="10" step="1" min="1" max="100">
						<h2 class="text-center" id="js-rangeslider-2"><output id="js-rangeslider-2" style="display: inline-block">10</output></h2>
						<div class="text-center mt-50">
							<h4 style="color: #2d3135; text-transform: initial;">Before we calculate your ROI, would you like to add helper <br>machines to enjoy even more acceleration?</h4>

							<div class="helperBlock" style="display: none; text-align: left; margin-top: 30px;">
								<div class="row price-data">
									<div class="col-xs-5 col-should-extend">
										<h6>Number of Helper Machines</h6>
									</div>
									<div class="col-xs-4 col-should-extend">
										<h6>Number of Cores</h6>
									</div>
									<div class="col-xs-2"></div>
								</div>
								<div class="row price-data machineHelperBlock">
									<div class="col-xs-5 col-should-extend">
										<input type="number" id="numHelper1" name="numHelper1" style="margin: 0px; text-align: center; padding: 0; font-size: 20px; width: 160px;" disabled="disabled">
									</div>
									<div class="col-xs-4 col-should-extend">
										<select class="aos-item" data-aos="fade-down" style="margin: 0px; min-width: 100%;" id="numHelperCore1" name="numHelperCore1">
											<option value="">Add a number of cores</option>
											<option value="4">Up to 4 cores</option>
											<option value="8">Up to 8 cores</option>
											<option value="16">Up to 16 cores</option>
											<option value="28">Up to 28 cores</option>
											<option value="32">Up to 32 cores</option>
											<option value="48">Up to 48 cores</option>
											<option value="64">Up to 64 cores</option>
										</select>
									</div>
									<div class="col-xs-2"></div>
								</div>
								<input type="hidden" id="machineHelperBlockCount" name="machineHelperBlockCount" value="1">
								<div class="row price-data">
									<div class="col-md-12">
										<div class="border-top" style="margin-top: 50px;"></div>
									</div>
								</div>
								<center><a href="javascript:;" class="button inverse orange-btn add-more-machineHelper-btn">ADD MORE MACHINES</a></center>
							</div>

							<a href="javascript:;" class="button inverse orange-btn showHelperBlock" style="width: 160px;">YES</a>
							<br>
							<br>
							<a href="javascript:;" class="button orange-btn calculate-btn" style="font-size: 18px;">calculate price</a>
						</div>
					</div>

					<div id="hereIsCalculation"></div>

					<div class="total mt-50 calculated calculatedText" style="display: none;">
						<div class="col-md-1"></div>
						<div class="col-md-10">
							<div class="calculatedTextFigures">
							</div>
							<input type="hidden" name="totalCalAmountVal" value="0">
							<p class="sub-total">Total <strong class="pull-right">$<font class="totalCalAmountText">0</font></strong></p>
						</div>
						<div class="col-md-1"></div>
					</div>
				</div>
			</div>
			<div class="row overFlowHide" style="overflow: hidden;">
				<div class="with-inc aos-item" data-aos="fade-down">
					<div class="row">
						<div class="thirdDiv clearfix" id="counter">
							<div class="col-md-6 with-out calculated" style="display: none;">
								<p class="text-center out">Without IncrediBuild</p>
								<div class="progressbar withoutIBCounter">
									<div id="circle3">
										<div class="progress-text-top">Estimated Yearly Cost</div>
										<div class="circle-overlay"></div>
										<p class="counter-data">
											$<span class="counter-value circle-counter" data-count="14400">0</span>
										</p>
										<div class="progress-text">Dev cost: $40/hr</div>
									</div>
									<div id="circle2">
										<div class="progress-text-top">Yearly Time Spent</div>
										<div class="circle-overlay"></div>
										<p class="counter-data">
											<span class="counter-value circle-counter" data-count="360">0</span> hrs
										</p>
										<div class="progress-text">Average: 20 working days</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 calculated" style="display: none;">
								<p class="text-center with">Using IncrediBuild</p>
								<div class="progressbar withIBCounter">
									<div id="circle4">
										<div class="progress-text-top">Yearly Savings</div>
										<div class="circle-overlay"></div>
										<p class="counter-data">
											$<span class="counter-value circle-counter" data-count="12480">0</span>
										</p>
									</div>
									<div id="circle5">
										<div class="progress-text-top">ROI</div>
										<div class="circle-overlay"></div>
										<p class="counter-data">
											<span class="counter-value circle-counter" data-count="2">0</span>
										</p>
										<div class="progress-text">months</div>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
			<div class="row" style="padding-bottom: 0px;">
				<div class="col-md-12 price-data">
					<div id="counter" class="text-center calculated" style="display: none;">
						<p class="calculate-price">Calculated Price</p>
						<h2>$<font class="totalCalAmountText2">0</font></span></h2>
						<p>I want it on <strong><font class="numberOfAgentsText">0</font> agents</strong>, for <strong>$<font class="costPerAgentText">0</font></strong> per  <br>12-month Incredibuild license.</p>
						<div class="text-center mt-50">
							<a href="https://www.incredibuild.com/ibonlinestore" class="button inverse orange-btn hover">purchase</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="pricing-box pt-50" style="display: none;">
		<div class="make-container">
			<div class="row">
				<div class="col-md-3 aos-item stickyArea" data-aos="fade-down" style="width: 310px;">
					<div class="box">
						<p>What's your total build time?</p>
						<h2 class="buildTimeWithoutIncredibuild">0</h2>
						<h6>Hours per Year</h6>
						<p class="mb-40">Save <strong>$<font class="savingWithIncredibuild">0</font> a year</strong> with IncrediBuild!</p>
						<p><a href="#hereIsCalculation">View ROI Breakdown</a></p>
					</div>
					<?php the_field('left_side_content'); ?>
				</div>
				<div class="col-md-6 aos-item" data-aos="fade-down">
					<div class="price-data">
						<?php the_field('middle_section_content') ?>
						<form class="mt-50 aos-item" data-aos="fade-down">
							<label>Number of Agents</label>
							<input type="range" id="numberOfAgents" class="range2" value="10" step="1" min="1" max="10">
							<h2 class="text-center" id="js-rangeslider-0"><output id="js-rangeslider-0" style="display: inline-block">10</output></h2>
						</form>
						<form class=" aos-item" data-aos="fade-down">
							<br>
							<br>
							<label>Number of Cores (per Agent)</label>
							<input type="range" id="numberOfCoresPerAgents" class="range2" value="10" step="1" min="1" max="64">
							<h2 class="text-center" id="js-rangeslider-1"><output id="js-rangeslider-1" style="display: inline-block">10</output></h2>
						</form>
						<h4 class="mt-80 aos-item" data-aos="fade-down"><?php the_field('solutions_heading'); ?></h4>

						<h4 class="mt-30 aos-item" data-aos="fade-down"><?php the_field('build_minutes_heading'); ?></h4>

					</div>
				</div>
				<div class="col-md-3 aos-item stickyArea" data-aos="fade-down" style="width: 310px;">
					<div class="box">
						<p style="text-transform: uppercase;">Your Cost</p>
						<h2>$<font class="costWithIncredibuild">0</font></h2>
						<p style="margin-bottom: 75px;">I want it on <strong><font class="numberOfAgentsText">0</font> agents</strong>, for <strong>$<font class="costPerAgentText">0</font></strong> per Incredibuild license.</p>
						<p><a href="#hereIsCalculation">View Summary</a></p>
					</div>
					<?php the_field('right_side_content'); ?>
				</div>
			</div>
		</div>
	</section>

<style type="text/css">
	input[type="number"] {
		outline: 0px;
	}
	.input-number {
		background: #fff !important;
	}
	.stickyArea {
		width: 310px !important;
	}
	.is_stuck.stickyArea {
		margin-top: 30px;
	}
	@media (max-width: 1270px) {
		.stickyArea {
			width: 280px !important;
		}
	}
	@media (max-width: 1150px) {
		.stickyArea {
			width: 240px !important;
		}
	}
	@media (max-width: 991px) {
		.with-inc:after,
		.stickyArea {
			display: none !important;
			width: 240px !important;
		}
	}
	@media (max-width: 610px) {
		.plus-minus,
		.input-text {
			width: 100%;
		}
		#circle3, #circle2, #circle4, #circle5, #circlenewYC, #circlenewYT, #circlenewIBYC, #circlenewIBYT {
			display: inline-block;
			float: none;
		}
		.price-data select {
			width: 100%;
		}
		.progressbar,
		.price-data {
			margin-bottom: 0px;
		}
		h4 {
			font-size: 18px;
			line-height: 20px;
		}
	}
</style>

<?php endwhile; ?>

<?php
get_footer();
