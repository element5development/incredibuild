<?php
/*
 * Template Name: Contact Us
 */

get_header(); ?>
<?php while(have_posts()) : the_post();?>
    <section class="sub-banner curve bottom-left-top-right">
        <div class="gradient inverse overlay"></div>
        <div class="container text-center aos-item" data-aos="zoom-in">
            <h1><?php the_title(); ?></h1>
        </div>
    </section>
    <section class="login2">
        <div class="container">
            <div class="row">
                <div class="col-md-12 login-sec contactFormSection">
                    <?php the_content();?>
                    <?php if(have_rows('contact_coutrie')):?>
                        <div class="contact-table">
                        <?php while (have_rows('contact_coutrie')) : the_row();?>
                            <div class="contact-table--row">
                                <div class="contact-table--field">
                                    <span class="country-name"><?php the_sub_field('name')?></span>
                                </div>
                                <div class="contact-table--field">
                                    <?php the_sub_field('address')?>
                                </div>
                                <div class="contact-table--field">
                                    <?php the_sub_field('contacts')?>
                                </div>
                            </div>
                        <?php endwhile;?>
                        </div>
                    <?php endif;?>

                </div>
            </div>
        </div>
    </section>
<?php endwhile;?>
<?php
get_footer();
