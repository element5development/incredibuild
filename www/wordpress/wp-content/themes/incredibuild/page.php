<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<section class="sub-banner curve bottom-left-top-right">
		<div class="gradient inverse overlay"></div>
		<div class="container text-center aos-item" data-aos="zoom-in">
			<h2><?php the_title(); ?></h2>
		</div>
	</section>
	<section class="login2">
		<div class="container">
			<div class="row">
			<div class="col-md-12 pt-50">
				<?php the_content(); ?>
			</div>
			</div>
		</div>
	</section>


<?php endwhile; ?>
<?php
get_footer();
