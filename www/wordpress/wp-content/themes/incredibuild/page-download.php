<?php
/**
 * Template Name: Download Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>

	<?php $currentTab = 1; ?>
	<?php if (isset($_GET['tab'])): ?>
		<?php $currentTab = $_GET['tab']; ?>
	<?php endif ?>
	
	
	<section class="login-sec  login-tab aos-item pt-40" data-aos="fade-down">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div id="exTab1">	
						<ul class="nav nav-pills mb-50">
							<li <?php if ($currentTab == 1): ?> class="active" <?php endif ?>>
			        			<a  href="#1a" data-toggle="tab"><?php the_field('first_tab_title'); ?></a>
							</li>
							<li <?php if ($currentTab == 2): ?> class="active" <?php endif ?>>
								<a href="#2a" data-toggle="tab"><?php the_field('second_tab_title'); ?></a>
							</li>
							<li <?php if ($currentTab == 3): ?> class="active" <?php endif ?>>
								<a href="#3a" data-toggle="tab"><?php the_field('third_tab_title'); ?></a>
							</li>
						</ul>
						    <!-- <div class="buttons_download">
						        <div class="tab_button">
						            <p class="caption"><?php echo get_post_meta( 15, 'Title_button_1', true ); ?></p>
						            <a href="<?php echo get_post_meta( 15, 'Url_button_1', true ); ?>"><?php echo get_post_meta( 15, 'Text_button_1', true ); ?></a>
						        </div>
						        <div class="tab_button">
						            <p class="caption"><?php echo get_post_meta( 15, 'Title_button_2', true ); ?></p>
						            <a href="<?php echo get_post_meta( 15, 'Url_button_2', true ); ?>"><?php echo get_post_meta( 15, 'Text_button_2', true ); ?></a>
						        </div>
						        <div class="tab_button">
						            <p class="caption"><?php echo get_post_meta( 15, 'Title_button_3', true ); ?></p>
						            <a href="<?php echo get_post_meta( 15, 'Url_button_3', true ); ?>"><?php echo get_post_meta( 15, 'Text_button_3', true ); ?></a>
						        </div>
						    </div> -->
						<div class="tab-content clearfix">
							<div class="tab-pane <?php if ($currentTab == 1): ?> active <?php endif ?>" id="1a">
								<div class="row">
									<div class="col-md-6 freeFormDiv">
										<?php echo do_shortcode('[gravityform id="6" title="false" description="false" ajax="true"]'); ?>
									</div>
									<div class="col-md-6 freeFormLogin">
										<h5 class="acc" style="margin-top: 20px;">Already have an account?
										<a href="/login/">Log in</a></h5>
									</div>
								</div>
							</div>
							<div class="tab-pane <?php if ($currentTab == 2): ?> active <?php endif ?>" id="2a">
								<div class="row">
									<div class="col-md-6 linuxFormDiv">
										<?php echo do_shortcode('[gravityform id="7" title="false" description="false" ajax="true"]'); ?>
									</div>
									<div class="col-md-6 linuxFormLogin">
										<h5 class="acc" style="margin-top: 20px;">Already have an account?
										<a href="/login/">Log in</a></h5>
									</div>
								</div>
							</div>
							<div class="tab-pane <?php if ($currentTab == 3): ?> active <?php endif ?>" id="3a">
								<div class="row">
									<div class="col-md-6 enterFormDiv">
										<?php echo do_shortcode('[gravityform id="8" title="false" description="false" ajax="true"]'); ?>
									</div>
									<div class="col-md-6 enterFormLogin">
										<h5 class="acc" style="margin-top: 20px;">Already have an account?
										<a href="/login/">Log in</a></h5>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="testimonial pt-20 pb-20 text-center aos-item" data-aos="fade-in">
		<?php get_template_part( 'template-parts/template-part', 'testimonials' ); ?>
	</section>

<?php endwhile; ?>

<style type="text/css">
	body.page-template-page-download .gform_wrapper ul.gform_fields li.gfield select {
		background: url(/wordpress/wp-content/themes/incredibuild/assets/images/dropdown-tick.png);
		background-repeat: no-repeat;
		background-position: center right;
	}
	body.page-template-page-download .gform_wrapper ul.gform_fields li.gfield select,
	body.page-template-page-download .gform_wrapper .top_label input.medium {
		width: 438px;
		max-width: 100%;
	}
	body.page-template-page-download .gform_wrapper .gform_page_footer {
		padding-left: 10px;
	}
	body.page-template-page-download .testimonial,
	body.page-template-page-download .footer-widget {
	    /*padding: 30px 0px 50px;*/
	    opacity: 1 !important;
	    transition: none !important;
	    transform: none !important;
	}
	body.page-template-page-download .gform_wrapper .validation_message {
	    display: block !important;
	    padding-top: 0px !important;
	    color: red !important;
	}
	body.page-template-page-download .nav-pills>li.active>a:after {
		right: 0px;
		width: 95%;
		left: 0px;
		margin: 0 auto;
	}
	body.page-template-page-download .gform_wrapper .gform_page_footer .button.gform_previous_button {
		display: none !important;
	}
	body.page-template-page-download .gform_wrapper .gf_progressbar_wrapper {
	    display: none !important;
	}
	body.page-template-page-download .gform_wrapper .gform_page_footer {
	    border: 0px !important;
	}
	#field_8_13 > label,
	#field_8_22 > label,
	#field_8_17 > label,
	#field_7_13 > label,
	#field_7_22 > label,
	#field_7_17 > label,
	#field_6_13 > label,
	#field_6_17 > label {
	    visibility: hidden;
	    height: 0px;
	}
	#field_6_19 > label,
	#field_7_19 > label,
	#field_8_19 > label {
	    border: 1px solid #ccc;
	    visibility: visible;
	    overflow: hidden;
	    margin: 30px 0;
	    height: 0px;
	}
	@media (min-width: 1024px) {
		#input_7_22 {
		    -webkit-column-count: 3; /* Chrome, Safari, Opera */
		    -moz-column-count: 3; /* Firefox */
		    column-count: 3;
		}
		.nav-pills>li {
			width: 33%;
		}
	}
</style>
<script type="text/javascript">
	jQuery(function($){
		setInterval(function(){
			if ($('#1a').is(":visible")) {
				$('#firstNameHere1').html($('#input_6_1').val());
				$('#firstNameHere2').html($('#input_6_1').val());
				if ($('#gform_page_6_2').is(":visible") || $('.gform_confirmation_wrapper ').is(":visible")) {
					$('.freeFormDiv').removeClass('col-md-6');
					$('.freeFormDiv').addClass('col-md-12');
					$('.freeFormLogin').hide();
				} else {
					$('.freeFormDiv').removeClass('col-md-12');
					$('.freeFormDiv').addClass('col-md-6');
					$('.freeFormLogin').show();
				}
			}
			if ($('#2a').is(":visible")) {
				$('#firstNameHere11').html($('#input_7_1').val());
				$('#firstNameHere22').html($('#input_7_1').val());
				if ($('#gform_page_7_2').is(":visible") || $('.gform_confirmation_wrapper ').is(":visible")) {
					$('.linuxFormDiv').removeClass('col-md-6');
					$('.linuxFormDiv').addClass('col-md-12');
					$('.linuxFormLogin').hide();
				} else {
					$('.linuxFormDiv').removeClass('col-md-12');
					$('.linuxFormDiv').addClass('col-md-6');
					$('.linuxFormLogin').show();
				}
			}
			if ($('#3a').is(":visible")) {
				$('#firstNameHere111').html($('#input_8_1').val());
				$('#firstNameHere222').html($('#input_8_1').val());
				if ($('#gform_page_8_2').is(":visible") || $('.gform_confirmation_wrapper ').is(":visible")) {
					$('.enterFormDiv').removeClass('col-md-6');
					$('.enterFormDiv').addClass('col-md-12');
					$('.enterFormLogin').hide();
				} else {
					$('.enterFormDiv').removeClass('col-md-12');
					$('.enterFormDiv').addClass('col-md-6');
					$('.enterFormLogin').show();
				}
			}
		}, 1000);
	});
</script>

<?php
get_footer();
