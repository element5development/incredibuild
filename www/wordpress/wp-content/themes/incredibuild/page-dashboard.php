<?php
/**
 * Template Name: Dashboard Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */
if (!is_user_logged_in()) {
	wp_redirect('/');
}
get_header();
$current_user = wp_get_current_user();
?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="login2" style="margin-bottom: 55px;">
		<div class="container">
			<div class="row">
			<div class="col-md-12 pt-50" data-aos="fade-up">
				<h4>Hi there <span><?php echo $current_user->first_name; ?></span>, how can we help you today?</h4>
			</div>
			</div>
			<div class="row pt-50">
				<div class="col-md-4 aos-item" data-aos="fade-up">
					<div class="box">
						<figure class="icon">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/login-1.png">
						</figure>
						<p>Latest Version</p>
						<h6>IncrediBuild latest version</h6>
						<a href="javascript:;" class="button inverse orange-btn">Button text <i class="fa fa-external-link" aria-hidden="true"></i></a>
					</div>
				</div>
				<div class="col-md-4 aos-item" data-aos="fade-up">
					<div class="box">
						<figure class="icon">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/login-2.png">
						</figure>
						<p>Need more add-ons?</p>
						<h6>the incredibuild store</h6>
						<a href="javascript:;" class="button inverse orange-btn">Button text <i class="fa fa-external-link" aria-hidden="true"></i></a>
					</div>
				</div>
				<div class="col-md-4 aos-item" data-aos="fade-up">
					<div class="box">
						<figure class="icon">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/login-3.png">
						</figure>
						<p>Edit Your Profile</p>
						<h6>edit your profile</h6>
						<a href="javascript:;" class="button inverse orange-btn">Button text <i class="fa fa-external-link" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
	</section>


<?php endwhile; ?>

<?php
get_footer();
