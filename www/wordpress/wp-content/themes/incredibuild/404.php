<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Incredibuild
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<div class="page-content">
                    <div class="container">
                        <div class="content-404">
                            <span>Error</span>
                            <img src="<?php echo get_template_directory_uri()."/assets/images/monitor_icon.png"?>" alt="">
                            <img src="<?php echo get_template_directory_uri()."/assets/images/404.png"?>" alt="">
                        </div>
                        <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'incredibuild' ); ?></h1>
                    </div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
