<?php
/**
 * Template Name: Case Studies Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="studies">
		<div class="container">
			<div class="row">
				<div class="grid aos-item" data-aos="fade-in">

<?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array( 
        'posts_per_page' => 12, 
        'paged' => $paged, 
        'post_type' => 'case_study'
    );
    $caseStudies = new WP_Query($args);

  while ($caseStudies->have_posts()) : $caseStudies->the_post(); 
?>
								<div class="grid-item">
									<?php if (get_field('small_title')): ?>
									<div class="small-btn"><?php echo get_field('small_title'); ?></div>
									<?php else : ?>
									<div class="small-btn">game dev</div>
									<?php endif; ?>
									<figure>
										<?php if (get_the_post_thumbnail_url()) { ?>
											<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive">
										<?php } else { ?>
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/case1.png" class="img-responsive">
										<?php } ?>
										<div class="overlay">
											<div>
												<a href="<?php the_permalink(); ?>" class="button inverse">Read more</a>
											</div>
										</div>
									</figure>
									<div class="text-box">
										<h6><?php if (strlen(the_title('','',FALSE)) > 45) { //Character length
										          $title_short = substr(the_title('','',FALSE), 0, 45); // Character length
										          preg_match('/^(.*)\s/s', $title_short, $matches);
										      if ($matches[1]) $title_short = $matches[1];
										          $title_short = $title_short.' ...'; // Ellipsis
										      } else {
										          $title_short = the_title('','',FALSE);
										      } ?>

										<?php echo $title_short ?>
											
										</h6>
										<?php 
										$excerptNew = get_the_excerpt();
										$excerptNew = preg_replace(" ([.*?])",'',$excerptNew);
										$excerptNew = strip_shortcodes($excerptNew);
										$excerptNew = strip_tags($excerptNew);
										$excerptNew = substr($excerptNew, 0, 60);
										$excerptNew = substr($excerptNew, 0, strripos($excerptNew, " "));
										$excerptNew = trim(preg_replace( '/s+/', ' ', $excerptNew));
										$excerptNew = $excerptNew.'...';
										$excerptNew = $excerptNew.' <a href="'.get_the_permalink().'">continue reading</a>';
											// $excerptNew = explode(' ', get_the_excerpt(), 16);

											// if (count($excerptNew) >= 16) {
											//   array_pop($excerptNew);
											//   $excerptNew = implode(" ", $excerptNew) . '...';
											// } else {
											//   $excerptNew = implode(" ", $excerptNew);
											// }

											// $excerptNew = preg_replace('`\[[^\]]*\]`', '', $excerptNew);
										?>
										<p><?php echo $excerptNew; ?></p>
									</div>
								</div>
<?php endwhile; ?>

<div class="row aos-item" data-aos="fade-up">
<div class="col-md-12 text-center">
<nav style="padding-top: 20px; ">
	<?php previous_posts_link( '&laquo; PREV', $caseStudies->max_num_pages) ?>
	<?php next_posts_link( 'NEXT &raquo;', $caseStudies->max_num_pages) ?>
</nav>
</div>
</div>
						<?php	
				wp_reset_postdata();
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="our-client text-center">
		<div class="container aos-item" data-aos="fade-down">
			<?php the_field('case_study_logo_section_text'); ?>
			<p></p>
			<div class="client-img">
				<?php if( have_rows('case_study_logo') ): while ( have_rows('case_study_logo') ) : the_row(); ?>
                    <div class="client-img-box">
					    <img src="<?php the_sub_field('image'); ?>" class="img-responsive">
                    </div>
				<?php endwhile; else : ?>
                    <div class="client-img-box">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/microsoft.png">
                    </div>
                    <div class="client-img-box">
					    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ea.png">
                    </div>
                    <div class="client-img-box">
					    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/amazon.png">
                    </div>
                    <div class="client-img-box">
					    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/epic.png">
                    </div>
                    <div class="client-img-box">
					    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/citibank.png">
                    </div>
                    <div class="client-img-box">
					    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/nvidia.png">
                    </div>
                    <div class="client-img-box">
					    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/intel.png">
                    </div>
                    <div class="client-img-box">
					    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/samsung.png">
                    </div>
                    <div class="client-img-box">
					    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/adobe.png">
                    </div>
                    <div class="client-img-box">
					    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/disney.png">
                    </div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<section class="testimonial pb-40 text-center aos-item" data-aos="fade-in">
		<?php get_template_part( 'template-parts/template-part', 'testimonials' ); ?>
	</section>


<?php endwhile; ?>

<?php
get_footer();
