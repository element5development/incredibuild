(function($){
    $(window).on('resize load', function(){
       var winWindth = $(window).width() / 2;
       var parentHeight = $(separatorDegCount.selector).parent().height() / 2;
       separatorDegCount.init(winWindth, parentHeight);
       videoBg();
       setTongle.init();
       footerSticky.init();
    });
    var separatorDegCount = {
        selector: '.separator-line',
        init: function(winWidth, parentHeight) {
            this.bottomWidth = winWidth;
            this.leftWidth = parentHeight;
            this.calcGip();
            this.calcCos();
            this.calcDeg();
            this.setDeg();
        },
        calcGip: function() {
            var self = this;
            this.gip = Math.sqrt(Math.pow(self.bottomWidth, 2) + Math.pow(self.leftWidth, 2));
        },
        calcCos: function() {
            this.cosA = this.bottomWidth / this.gip;
        },
        calcDeg: function() {
            var self = this;
            this.deg = (Math.acos(self.cosA) * 180) / Math.PI;
        },
        setDeg: function() {
            var self = this;
            $(this.selector).each(function(i, item){
                if($(item).parent().hasClass('right-top')) var deg = -self.deg;
                else var deg = self.deg;
                $(item).css('transform', 'rotate('+deg+'deg)');
            });
        }
    }
    function videoBg() {
        var videoSelector = $('#banner-video');
        var winWidth = $(window).width();
        if(winWidth <= 990) videoSelector.removeAttr('autoplay');
        else videoSelector.attr('autoplay');
    }
    $('.modal').on('show.bs.modal', function(){$('html').css({'overflow': 'hidden'})});
    $('.modal').on('hide.bs.modal', function(){
        $('html').removeAttr('style');
        $('body').removeClass('modal-open');
    });
    var setTongle = {
        init: function() {
            if($('.top-before').length == 0) return false;
            this.getSides();
            this.getTongle();
            $('.top-before').css({transform: "rotate("+this.tongle+"deg)"});
            $('.bottom-after').css({transform: "rotate(-"+this.tongle+"deg)"});
        },
        getSides: function(){
            var self = this;
            this.getLeftSide();
            this.bottomSide = $(window).width();
            this.topSide = Math.sqrt(Math.pow(self.bottomSide, 2) + Math.pow(self.leftSide, 2));
        },
        getTongle: function() {
            var self = this;
            this.tongleCos = this.bottomSide/this.topSide;
            this.tongle = (Math.acos(self.tongleCos) * 180) / Math.PI;
        },
        getLeftSide: function() {
            var winWidth = $(window).width();
            this.leftSide = 120;
            if (winWidth < 1140) this.leftSide = 100;
            if (winWidth < 992) this.leftSide = 80;
            if (winWidth < 768) this.leftSide = 70;
        }
    }
    var triangleAsotiate = {
        game: {
           triangle: '#triangle-right',
           toHide: '.qa',
           'noAfter': '.qa, .game',
           'hideBorderTopFor': '.qa, .game, .code'
        },
        qa: {
            triangle: '#triangle-right',
            toHide: '.game',
            'noAfter': '.qa, .game',
            'hideBorderTopFor': '.qa, .game, .code'
        },
        code: {
            triangle: '#triangle-left',
            toHide: '.house',
            'noAfter': '.code, .house',
            'hideBorderTopFor': '.code, .house, .complex'
        },
        house: {
            triangle: '#triangle-left',
            toHide: '.code',
            'noAfter': '.house, .code',
            'hideBorderTopFor': '.code, .house, .complex'
        },
        complex: {
            triangle: '#triangle-top',
            toHide: '.slow',
            'noAfter': '.complex, .slow, .house',
            'hideBorderTopFor': '.slow, .complex'
        },
        slow: {
            triangle: '#triangle-top',
            toHide: '.complex',
            'noAfter': '.complex, .slow, .house',
            'hideBorderTopFor': '.slow, .complex'
        }
    }
    $('.js-show-triangle').mouseenter(function(e){

        var parent = $(this),
            me = parent.find('div.hideOnHover'),
            asociate = parent.data('triangle'),
            triangle = triangleAsotiate[asociate].triangle,
            associatToHide = parent.find(triangleAsotiate[asociate].toHide),
            group = me.closest('.js-show-triangle-group'),
            groupParent = group.closest('.dev-container'),
            hideOnHover = group.find('.hideOnHover'),
            hideBorderTop = groupParent.find(triangleAsotiate[asociate].hideBorderTopFor),
            noAfter = groupParent.find(triangleAsotiate[asociate].noAfter),
            showOnHover = group.find('.showOnHover');
        
        parent.addClass('no-pseudo');
        hideOnHover.css('opacity', 0);
        hideBorderTop.css({'border-top-color': 'transparent' });
        noAfter.addClass('noAfter noBefore');
        //associatToHide.css('opacity', 0);
        $(triangle+', .'+asociate+'-hover').show();

    });
    $('.js-show-triangle').mouseleave(function(e){

        var parent = $(this),
            me = parent.find('div.hideOnHover'),
            asociate = parent.data('triangle'),
            triangle = triangleAsotiate[asociate].triangle,
            associatToHide = parent.find(triangleAsotiate[asociate].toHide),
            group = me.closest('.js-show-triangle-group'),
            groupParent = group.closest('.dev-container'),
            hideOnHover = group.find('.hideOnHover'),
            hideBorderTop = groupParent.find(triangleAsotiate[asociate].hideBorderTopFor).removeClass('noAfter'),
            noAfter = groupParent.find(triangleAsotiate[asociate].noAfter),
            showOnHover = group.find('.showOnHover');

        parent.removeClass('no-pseudo');
        hideOnHover.css('opacity', "");
        hideBorderTop.css({'border-top-color': '' });
        noAfter.removeClass('noAfter noBefore');
        // associatToHide.css('opacity', "");
        $(triangle+', .'+asociate+'-hover').hide();
    });
    var footerSticky = {
        footerSelector: '.footer-sticky',
        mainContentSelector: 'section.main-content',
        init: function () {
            var footerHeight = $(this.footerSelector).height();
            $(this.mainContentSelector).css('padding-bottom', footerHeight);
        }
    }
		//Model Acordeon
		function Acordeon(enabled, article, parentBox) {
			this.selectors = {
				enabled: enabled,
				article: article,
				parentBox: parentBox
			}
		}
		Acordeon.prototype.init = function() {
			var selectors = this.selectors;
			$(this.selectors.enabled).click(function(){
				$(this).parents(selectors.parentBox).find(selectors.article).slideToggle();
				$(this).parents(selectors.parentBox).toggleClass('open');
			})
		}
		var faqAcordeon = new Acordeon('.faq-tabs h6.question', '.answer', '.faq-box');
		faqAcordeon.init();
		var versionAcordeon = new Acordeon('.version-title', '.version-desc', '.version-box');
	  versionAcordeon.init();
	//	$('#ytplayer')[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
		var ytbPreloader = {
			selectors: {
				play: 		 '.play-ytb-video',
				preloader: '.preload-box',
				video: 		 '.video-box',
				parent:    '.video-preload-box'
			},
			init: function() {
				var selectors = this.selectors;
				var self = this;
				$(selectors.play).click(function(){
					self.play(this);
				})
			},
			play: function(elm) {
				var selectors = this.selectors;
				$(elm).parents(selectors.parent).find(selectors.video).find('iframe')[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
				$(elm).parents(selectors.parent).find(selectors.preloader).css('display', 'none');
				$(elm).parents(selectors.parent).find(selectors.video).css('display', 'block');
			}
		}

        $('.make-container .box .button').on('click', function(){
            $(this).parent().parent(".box").addClass("active");
        });


        $('.make-container .box .close').on('click', function(){
            $(this).parent().parent(".box").removeClass("active");
        });


        $('.js-show-triangle-group .triangleBoxes:before').on('click', function(){
            // console.log($(this));
        });


		$(window).load(function(){
				ytbPreloader.init();	
		});


        $('#exTab1 a').on('click', function(){
             console.log('hello');
             AOS.refreshHard();
        });
    
    /*custom script Vova*/
    $(window).scroll(function() {
        if(jQuery(window).scrollTop() >= 1500 && location.pathname == '/frequently-asked-questions.html') {
            $('.footer-widget').css({
                'opacity': '1',
                'transform': 'translate(0)'
            });
        }
    });
    $('body').on('click',function(e){
       function timer() {
            var classNameis = e.target.className;
           //for PC
            if(classNameis.indexOf("fa-search") == -1 && classNameis.indexOf('fa-remove') == -1 &&  $('.search-section').css('display') == 'block' && classNameis.indexOf('search-field') == -1) {
                $('.search-section').css('display','none');
                $('.fa.fa-search.fa-remove').removeClass('fa-remove');
            }
            //for phone
            if(classNameis.indexOf("fa-search") == -1 && classNameis.indexOf('fa-remove') == -1 &&  $('.search-section-mob').css('display') == 'block' && classNameis.indexOf('search-field') == -1) {
                $('.search-section-mob').css('display','none');
                $('.fa.fa-search.fa-remove').removeClass('fa-remove');
            }
       }
        setTimeout(timer,100);
        
    });

})(jQuery)