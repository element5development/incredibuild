<?php
/**
 * Template Name: Maintenance Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */
?>
<html>
    <head>
        <style>
               #content {
                    margin-top: 15%;
                }
                body {
                    background: #EEEFF0!important;
                }   
        </style>
    </head>   
    <body>
       <div id="content">
            <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
              <?php
                the_content()  
              ?>
            <?php endwhile; ?>
            <?php endif; ?>           
       </div>
    </body>
</html>