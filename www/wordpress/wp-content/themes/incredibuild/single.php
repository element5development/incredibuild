<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Incredibuild
 */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<section class="sub-banner curve bottom-left-top-right">
		<div class="gradient inverse overlay"></div>
		<div class="container text-center aos-item" data-aos="zoom-in">
			<h1><?php the_title(); ?></h1>
		</div>
	</section>
	<section class="login2">
		<div class="container">
			<div class="row">
			<div class="col-md-12 pt-50">
				<?php the_content(); ?>
			</div>
			</div>
		</div>
	</section>


<?php endwhile; ?>
<?php
get_footer();

