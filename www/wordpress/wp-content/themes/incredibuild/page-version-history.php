<?php
/**
 * Template Name: Version History Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */
$version_list = array_reverse(get_field('version_list'));

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'template-parts/template-part', 'header' ); ?>
    <section class="login2">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-50">
                    <?php the_content();?>
                </div>
            </div>
        </div>
    </section>

    <section class="version-list">
        <div class="container">
            <div class="row">
            <?php if(have_rows('version_list')):?>
                <?php foreach ($version_list as $version):?>
                    <div class="version-box col-md-12">
                        <span class="version-title">Changes in Version <?php echo $version['version'];?></span>
                        <div class="version-desc">
                            <?php echo $version['version_description'];?>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            </div>
        </div>
    </section>

<?php endwhile; ?>

<?php
get_footer();
