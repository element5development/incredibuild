<?php
/**
 * Template Name: Hacks Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="awards">
		<div class="container">

<?php 
  $temp = $wp_query; 
  $wp_query = null; 
  $wp_query = new WP_Query(); 
  $wp_query->query('showposts=10&post_type=dev_hack'.'&paged='.$paged); 

  while ($wp_query->have_posts()) : $wp_query->the_post(); 
?>
						<div class="row aos-item" data-aos="fade-up">
							<div class="col-md-4">
								<figure class="bg-grey">
									<?php if (get_the_post_thumbnail_url()) { ?>
										<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive">
									<?php } else { ?>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/hacks.png" class="img-responsive">
									<?php } ?>
								</figure>
							</div>
							<div class="justify_custom col-md-8">
								<h5><?php the_title(); ?></h5>
								<p><?php the_content(); ?></p>
								<!-- <p><a href="<?php the_permalink(); ?>" class="button inverse orange-btn">continue reading</a></p> -->
							</div>
						</div>

<?php endwhile; ?>

<nav style="padding-top: 20px; text-align: center;">
    <?php previous_posts_link('&laquo; Newer') ?>
    <?php next_posts_link('Older &raquo;') ?>
</nav>

<?php 
  $wp_query = null; 
  $wp_query = $temp;  // Reset
?>
				<?php
				// Restore original Post Data
				wp_reset_postdata();
			?>
		</div>
	</section>
	<section class="gradient gradient-curve trust trust-texture text-center mt-100 aos-item" data-aos="zoom-out">

      <?php get_template_part( 'template-parts/template-part', 'gradient-section' ); ?>

	</section>



<?php endwhile; ?>

<?php
get_footer();
