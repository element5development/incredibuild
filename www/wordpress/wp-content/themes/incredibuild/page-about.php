<?php
/**
 * Template Name: About Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>

	<section class="visual-content about">
		<div class="container">
			<div class="col-md-12 pb-50 aos-item" data-aos="fade-up">
				<?php the_field('about_content'); ?>
			</div>
			<div class="row">
			    <div class="thirdDiv clearfix" id="counter">
					<div class="col-sm-6 text-center aos-item" data-aos="fade-right">
						<div class="progressbar">
							<div id="circle14">
								<div class="circle-overlay"></div>
								<p class="counter-data">
									<span class="counter-value circle-counter" data-count="150000">0</span>
									<!-- <span class=" circle-counter">150,000</span> -->
								</p>
								<div class="progress-text">IncrediBuild Users</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 text-center aos-item" data-aos="fade-left">
						<div class="progressbar">
						    <div id="circle15">
								<div class="circle-overlay"></div>
								<p class="counter-data">
									<span class="counter-value circle-counter" data-count="5000000">0</span>+
									<!-- <span class="circle-counter">5,000,000+</span> -->
								</p>
								<div class="progress-text">hours saved <br> using IncrediBuild</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row pt-50">
				<h4 class="text-center">Leadership</h4>
				<?php
				if( have_rows('team_members') ):
					$x=0;
				    while ( have_rows('team_members') ) : the_row(); ?>
				    <?php if ($x==2): ?>
				    	</div>
				    	<div class="row">
				    <?php endif ?>
					<div class="col-md-6 pt-50 aos-item" data-aos="fade-down-right">
						<div class="clearfix">
							<figure class="about-img">
								<img src="<?php the_sub_field('image'); ?>">
							</figure>
							<div class="about-team"><strong><?php the_sub_field('name'); ?> </strong><?php the_sub_field('title'); ?></div>
						</div>
						<div style="text-align: justify;">
							<p><?php echo get_sub_field('content'); ?></p>
						</div>
					</div>
				<?php $x++; endwhile;
				endif;
				?>
			</div>
		</div>
	</section>
	<section class="awards about-awards">
		<div class="container aos-item" data-aos="fade-down">
			<h4 class="text-center">Awards</h4>
			<div class="row">
				<?php 
					// WP_Query arguments
					$args = array(
						'post_type'              => array( 'award' ),
						'post_status'            => array( 'publish' ),
						'posts_per_page'		 => 3
					);

					// The Query
					$awards = new WP_Query( $args );

					// The Loop
					if ( $awards->have_posts() ) {
						while ( $awards->have_posts() ) {
							$awards->the_post(); ?>
							<div class="col-md-4">
								<a href="<?php the_permalink(); ?>"><figure class="bg-grey" style="min-height: 200px;">
																	<?php if (get_the_post_thumbnail_url()) { ?>
																		<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive">
																	<?php } else { ?>
																		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/award.png" class="img-responsive">
																	<?php } ?>
																</figure></a>
								<a href="<?php the_permalink(); ?>" style="text-decoration: none;"><h5><?php the_title(); ?></h5></a>
								<div class="justify_custom about-award-text">
										<?php 
										$excerptNew = get_the_excerpt();
										$excerptNew = preg_replace(" ([.*?])",'',$excerptNew);
										$excerptNew = strip_shortcodes($excerptNew);
										$excerptNew = strip_tags($excerptNew);
										$excerptNew = substr($excerptNew, 0, 150);
										$excerptNew = substr($excerptNew, 0, strripos($excerptNew, " "));
										$excerptNew = trim(preg_replace( '/s+/', ' ', $excerptNew));
										$excerptNew = $excerptNew.'...'; ?>
										<p><?php echo $excerptNew; ?></p>
								</div>
								<p><a href="<?php the_permalink(); ?>" class="button inverse orange-btn">continue reading</a></p>
							</div>
					<?php	}
					} else {
							echo '<div class="col-md-12">no award found</div>';
					}

					// Restore original Post Data
					wp_reset_postdata();
				?>
			</div>
		</div>
	</section>

<?php endwhile; ?>

<?php
get_footer();
