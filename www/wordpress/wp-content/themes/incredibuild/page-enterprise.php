<?php
/**
 * Template Name: Enterprise Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="enterprise-text pt-50">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="justify_custom col-md-10 aos-item" data-aos="fade-down">
					<?php the_field('first_fold_text'); ?>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</section>
	<section class="dev pt-50">
		<div class="container">
			<div class="row aos-item" data-aos="fade-up">
				<div class="col-md-4">
					<figure>
						<img src="<?php the_field('first_fold_image'); ?>" >
					</figure>
				</div>
				<div class="col-md-8">
					<h4><?php the_field('first_fold_title'); ?></h4>
					<div class="justify_custom"><?php the_field('first_fold_content'); ?></div>
				</div>
			</div>
		</div>
	</section>
	<section class="features pt-15 pb-0">
		<div class="container aos-item" data-aos="fade-in">
			<h4 class="text-center" style="margin-bottom: 35px;"><?php the_field('second_fold_title'); ?></h4>
				<?php
				if( have_rows('second_fold_content_box') ):
					$x=1;
				    $rows_count = count(get_field('second_fold_content_box'));
				    while ( have_rows('second_fold_content_box') ) : the_row(); ?>
                    <?php if($x%2 == 1):?>
                    <div class="row">
                            <?php endif;?>
					<div class="col-md-6 aos-item" data-aos="fade-down">
						<figure>
							<img src="<?php the_sub_field('image'); ?>">
						</figure>
						<h6><?php the_sub_field('title'); ?></h6>
						<p style="text-align: justify;"><?php the_sub_field('text'); ?></p>
					</div>
				    <?php if ($x%2 == 0) { ?>
						</div>
				    <?php } ?>
				<?php $x++; endwhile;
				if($rows_count%2 != 0) echo '</div>';
				endif;
				?>
		</div>
	</section>
	<section class="testimonial pb-40 text-center aos-item" data-aos="fade-in" style="padding-top: 0px;">
		<?php get_template_part( 'template-parts/template-part', 'testimonials' ); ?>
	</section>
	<section class="gradient gradient-inverse trust text-center save-time mt-100 aos-item" data-aos="zoom-out">
		<div class="container text-center">
			<div class="row no-flex">
				<div class="col-sm-12 col-md-6 pr-30 no-flex">
					<div class="text">
						<h3><?php the_field('last_fold_heading'); ?></h3>
						<p><?php the_field('last_fold_text'); ?></p>
					</div>
				</div>
				<div class="col-sm-12 col-md-5 pl-30">
					<!--<input type="email" name="email" placeholder="Email Address">
					<input style="width: 320px;" type="submit" class="button inverse" value="Request a demo">-->
					<div id="unik"><?php gravity_form(5, false, false, false, '', true, 12); ?></div>
				</div>
			</div>
		</div>
	</section>



<?php endwhile; ?>

<?php
get_footer();
