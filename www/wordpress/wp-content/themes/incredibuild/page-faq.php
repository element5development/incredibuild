<?php
/**
 * Template Name: FAQ Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="faq support">
		<div class="container">
			<div class="justify_custom col-md-12 pb-50 aos-item" data-aos="fade-up">
				<?php the_field('first_fold_content'); ?>
			</div>
		</div>
	</section>

	<section class="faq-tabs aos-item" data-aos="fade-up">
		<div id="exTab1" class="container">	
			<ul class="nav nav-pills">
				<?php if( have_rows('faq_tabs') ): ?>
					<?php $x=1; while( have_rows('faq_tabs') ): the_row(); ?>
						<li class="<?php if ($x==1): ?>
							active
						<?php endif ?>">
		        			<a href="#<?php echo $x; ?>a" data-toggle="tab"><?php the_sub_field('faq_tab_title'); ?></a>
						</li>
					<?php $x++; endwhile; ?>
				<?php endif; ?>
			</ul>
			<br>
			<div class="tab-content clearfix aos-item" data-aos="fade-up" style="max-width: 100%;">
				<?php if( have_rows('faq_tabs') ): ?>
					<?php $y=1; while( have_rows('faq_tabs') ): the_row(); ?>
						<div class="tab-pane <?php if ($y==1): ?> active <?php endif ?>" id="<?php echo $y; ?>a">
							<?php if (get_sub_field('faq_tab_description')): ?>
								<style type="text/css">.tabsDesc strong, .tabsDesc p { color: #000; }</style>
								<div class="clearfix tabsDesc">
                                    <?php the_sub_field('faq_tab_description'); ?>
                                    <br>
								</div>
							<?php endif ?>
							<?php if( have_rows('questionanswer') ): ?>
								<?php $quecount = 0; ?>
								<?php while( have_rows('questionanswer') ): the_row(); ?>
									<?php if (get_sub_field('answer')): ?>
	                                    <div class="faq-box">
	                                        <h6 class="question"><?php the_sub_field('question'); ?></h6>
	                                        <div class="justify_custom answer">
	                                            <?php the_sub_field('answer'); ?>
	                                        </div>
	                                    </div>
									<?php else : ?>
										<div class="clearfix">
	                                        <h2><?php the_sub_field('question'); ?></h2>
										</div>
									<?php endif ?>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					<?php $y++; endwhile; ?>
				<?php endif; ?>
			</div>
  		</div>
	</section>

<?php endwhile; ?>

<?php
get_footer();