<?php
/**
 * Template Name: Game Development Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="solution-text pt-40">
		<figure class="solution-texture">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/solution-texture.png">
		</figure>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="justify_custom col-md-10 aos-item" data-aos="fade-up">
					<?php the_field('first_fold_content'); ?>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</section>
	<section class="game-slider solution-curve">
		<ul class="full-width-slider">
			<li>
				<figure>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/unrealengine001(1).png">
				</figure>
			</li>
<!-- 			<li>
				<figure>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/unrealengine001(1).png">
				</figure>
			</li>
			<li>
				<figure>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/unrealengine001(1).png">
				</figure>
			</li> -->
		</ul>
	</section>
	<section class="solution-text pt-55">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="justify_custom col-md-10 aos-item" data-aos="fade-up">
					<?php the_field('second_fold_content'); ?>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row pt-45 aos-item" data-aos="fade-up">
				<h6 class="text-center mb-0">INCREDIBUILD WITH THESE GAMING PLATFORMS:</h6>
				<div class="text-center solution-images pt-45">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/x-box.png">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/image.png">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/nin.png">
				</div>
				<div class="col-md-6 pt-65">
					<ul>
						<li>Dramatically faster gaming builds</li>
						<li>Complete Integration with Microsoft Visual Studio environments</li>
					</ul>
				</div>
				<div class="col-md-6 pt-65">
					<ul>
						<li>Complete Integration with Make environments</li>
						<li>Integrated build monitor visualization</li>
						<li>Optimized performance for all gaming build tool sets</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section class="sub-banner curve bottom-left-top-right benefits benefits" <?php if (get_field('benefits_background_image')): ?> style="background-image: url(<?php echo get_field('benefits_background_image'); ?>)" <?php endif ?>>
			<div class="gradient inverse overlay"></div>
			<div class="container">
				<div class="row">
					<h3 class="text-center mb-20">Top 3 Benefits of using IncrediBuild</h3>
                    <?php if( have_rows('benefits_content') ): while ( have_rows('benefits_content') ) : the_row(); ?>
                        <div class="col-md-4 aos-item" data-aos="fade-up" data-aos-duration="1500">
                            <h4><?php the_sub_field('title'); ?></h4>
                            <p class="justify_custom"><?php the_sub_field('text'); ?></p>
                        </div>
                    <?php endwhile; endif; ?>
                </div>
			</div>
		</section>


	<section class="testimonial pb-40 pt-5 text-center aos-item" data-aos="fade-in">
		<?php get_template_part( 'template-parts/template-part', 'testimonials' ); ?>
	</section>

	<section class="gradient gradient-curve trust trust-texture text-center mt-100 aos-item" data-aos="zoom-out">
      <?php get_template_part( 'template-parts/template-part', 'gradient-section' ); ?>
	</section>
<style type="text/css">
	.faster:after {
		content: '';
		background-color: #fff;
		height: 150px;
		width: 102%;
		position: absolute;
		top: 2px;
		transform-origin: 100% 30%;
		transform: rotate(3.1deg);
		z-index: 9;
		left: -4px;
		z-index: 999999;
	}
</style>

<?php endwhile; ?>

<?php
get_footer();
