<?php
/**
 * Template Name: Register Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="login-sec pt-50">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10 aos-item" data-aos="fade-down">
					<p>Welcome to IncrediBuild! It's good to have you on board. Please complete your profile to help us configure the IncrediBuild download for you.</p>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row pt-50">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<h4>Complete your Account</h4>
					<form>
						<p class=" aos-item" data-aos="fade-down">
							<label>Company</label>
							<input type="text" name="company" placeholder="company name">
						</p>
						<p class=" aos-item" data-aos="fade-down">
							<label>State</label>
							<select>
								<option>state 1</option>
								<option>state 2</option>
								<option>state 3</option>
							</select>
						</p>
						<p class=" aos-item" data-aos="fade-down">
							<label>City</label>
							<input type="city" name="city" placeholder="city">
						</p>
						<p class=" aos-item" data-aos="fade-down">
							<label>How did you hear about IncrediBuild?</label>
							<textarea placeholder="Enter comment"></textarea>
						</p>
						<p class=" aos-item" data-aos="fade-down">
							<label>Job title</label>
							<input type="text" name="job-title" placeholder="Enter job title">
						</p>
						<h4 class="pt-20">Which type of build with you be working on?</h4>
						<div class="check-box aos-item" data-aos="fade-down">
							<div class="styled-checkbox">
								<input type="checkbox" name="" value="C++ Build" id="build">
								<label for="build"></label>
							</div>
							<label>C++ Build</label>
						</div>
						<div class="check-box aos-item" data-aos="fade-down">
							<div class="styled-checkbox">
								<input type="checkbox" name="" value="C#(<5min complation time)" id="complation">
								<label for="complation"></label>
							</div>
							<label>C#(5min complation time)</label>
						</div>
						<div class="check-box aos-item" data-aos="fade-down">
							<div class="styled-checkbox">
								<input type="checkbox" name="" value="C#(<long complation time)" id="long">
								<label for="long"></label>
							</div>
							<label>C#(long complation time)</label>
						</div>
						<p class=" aos-item" data-aos="fade-down">
							<input type="submit" class="button inverse hover" value="get incredibuild">
						</p>
					</form>
				</div>
				<div class="col-md-5">
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-11">
						<form>
							<div class="check-box">
								<div class="styled-checkbox">
									<input id="yes" type="checkbox" name="" value="Yes, I want to receive exclusive Speed strategies, White papers and Offers from IncrediBuild my email.">
									<label for="yes"></label>
								<label>Yes, I want to receive exclusive Speed strategies, White papers and Offers from IncrediBuild my email.</label>
							</div>
						</form>
				</div>
		</div>
	</section>



<?php endwhile; ?>

<?php
get_footer();
