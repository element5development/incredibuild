<?php
/**
 * Template Name: Home Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php if( have_rows('slides') ): while ( have_rows('slides') ) : the_row(); ?>
	<section class="banner gradient curve top-left-bottom-right" <?php if (get_sub_field('background_image')): ?> style="background-image: url(<?php echo get_sub_field('background_image'); ?>)" <?php endif ?>>
		<?php if (get_sub_field('background_video_webm') || get_sub_field('background_video_mp4')): ?>
			<video class="video-bg" width="100%" autoplay loop id="banner-video">
				<source src="<?php echo get_sub_field('background_video_webm') ? get_sub_field('background_video_webm') : '/wordpress/wp-content/themes/incredibuild/assets/Main_Page_Header_1.webm'; ?>" type="video/webm">
				<source src="<?php echo get_sub_field('background_video_mp4'); ?>" type="video/mp4">
				Your browser does not support HTML5 video.
			</video>
		<?php endif; ?>
		<div class="container text-center aos-item" data-aos="zoom-in">
			<h2><?php the_sub_field('title'); ?></h2>
			<p><?php the_sub_field('content'); ?></p>
			<a href="<?php the_sub_field('button_link'); ?>" class="button inverse"><?php the_sub_field('button_text'); ?></a>
		</div>
	</section>
	<?php endwhile; endif; ?>

	<section class="our-client text-center aos-item" data-aos="fade-down">
		<div class="container">
			<p><?php the_field('first_fold_title'); ?></p>
			<div class="client-img js-slider">
				<?php if( have_rows('first_fold_logo') ): while ( have_rows('first_fold_logo') ) : the_row(); ?>
                    <div class="img-wrapper">
					    <img src="<?php the_sub_field('image'); ?>" class="img-responsive">
                    </div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
	<section class="gradient your-choice text-center">
		<div class="container aos-item" data-aos="fade-up">
			<ul class="home-gradient-slider">
				<li>
					<div>
						<p>1 / 4</p>
						<h4>What would you like to accelerate today?</h4>
						<a href="javascript:;" data-name="Continuous Integration" data-anchor="continuous-delivery.html" class="button inverse goTo2ndSlide">Continuous Integration</a>
						<a href="javascript:;" data-name="GENERAL C++" data-anchor="accelerate-visual-studio-cc-builds.html" class="button inverse goTo2ndSlide">GENERAL C++</a>
						<a href="javascript:;" data-name="GAMING DEVELOPMENT" data-anchor="accelerate-game-development.html" class="button inverse goTo2ndSlide">GAMING DEVELOPMENT</a>
						<a href="javascript:;" data-name="IMAGE PROCESSING" data-anchor="image-processing" class="button inverse goTo2ndSlide">IMAGE processing</a>
						<a href="javascript:;" data-name="IOT/EMBEDDED DEVELOPMENT" data-anchor="iot-embedded-development" class="button inverse goTo2ndSlide desktop">iot/embedded development</a>
						<!-- <a href="javascript:;" data-anchor="iotembedded-development" class="button inverse goTo2ndSlide mobile">iot/embedded dev</a> -->
					</div>
				</li>
				<li>
					<div>
						<a href="javascript:;" class="gotoBackSlide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/erow-left.png"></a>
						<p>2 / 4: <br><span style="font-weight: 400;">GREAT, SO</span> <font class="2ndSlideText" style="text-transform: uppercase;">GAMING DEVELOPMENT</font></p>
						<h4>How many developers are on your team?</h4>
						<div style="max-width: 596px; margin: 50px auto 0px;">
							<input type="range" class="range" name="goto3rdSlideValue" value="5" step="1" min="0" max="10">
						</div>
						<h2 class="text-center" id="js-rangeslider-1"><output class="js-rangeslider-1" id="js-rangeslider-1" style="display: inline-block">5</output></h2>
						<a href="javascript:;" class="button goto3rdSlide inverse mobile" style="min-width: 305px;">Next</a>
					</div>
				</li>
				<li>
					<div>
						<a href="javascript:;" class="gotoBackSlide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/erow-left.png"></a>
						<p>3 / 4: <br><span style="font-weight: 400;">GREAT, SO</span> <font class="2ndSlideText" style="text-transform: uppercase;">GAMING DEVELOPMENT</font> <span style="font-weight: 400;">WITH</span> <font class="3rdSlideText">5</font> DEVELOPERS</p>
						<h4>What is your average build time (in minutes)?</h4>
						<div style="max-width: 596px; margin: 50px auto 0px;">
							<input type="range" class="range" name="goto4thSlideValue" value="10" step="1" min="1" max="45">
						</div>
						<h2 class="text-center" id="js-rangeslider-1"><output class="js-rangeslider-2" id="js-rangeslider-1" style="display: inline-block">10</output></h2>
						<a href="javascript:;" class="button goto4thSlide inverse mobile" style="min-width: 305px;">Next</a>
					</div>
				</li>
				<li>
					<div>
						<a href="javascript:;" class="gotoBackSlide"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/erow-left.png"></a>
						<p>4 / 4: <br><span style="font-weight: 400;">GREAT, SO</span> <font class="2ndSlideText" style="text-transform: uppercase;">GAMING DEVELOPMENT</font> <span style="font-weight: 400;">WITH</span> <font class="3rdSlideText">5</font> DEVELOPERS <span style="font-weight: 400;">and</span> <font class="4thSlideText">30</font> MIN BUILD TIME</p>
						<h4>We have the perfect solution for you.</h4>
						<a href="javascript:;" class="button gotoPageLink inverse mobile" style="min-width: 305px;">Accelerate my build</a>
					</div>
				</li>
			</ul>
			<div style="display: none;"><form><input type="hidden" name="2ndSlideValue"><input type="hidden" name="3rdSlideValue"><input type="hidden" name="4thSlideValue"></form></div>
		</div>
	</section>
	<section class="transform-sec">

		<div class="container">
			<div class="row">
				<div class="col-md-6 aos-item" data-aos="fade-right" data-aos-anchor=".transform-sec" data-aos-offset="0">
					<h4><?php the_field('third_fold_title') ?></h4>
					<?php the_field('third_fold_content'); ?>
				</div>
				<div class="col-md-6 aos-item" data-aos="fade-left" data-aos-anchor=".transform-sec" data-aos-offset="0">
					<div style="cursor: pointer;" class="video-sec text-center" data-toggle="modal" data-target="#myModal">
						<img class="video" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/video.png">
						<div>
							<a href="javascript:;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play.png"></a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="transform-content text-center">
					<?php if( have_rows('third_fold_content_box') ): while ( have_rows('third_fold_content_box') ) : the_row(); ?>
						<div class="col-md-4 aos-item" data-aos="fade-down">
							<div class="icon">
								<figure>
									<img src="<?php the_sub_field('image'); ?>">
								</figure>
							</div>
							<h5><?php the_sub_field('title'); ?></h5>
							<p class="justify_custom"><?php the_sub_field('text'); ?></p>
						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
		<div id="latest-version-download">
			<a href="/trial_download?upgrade=1" target="_blank" class="button">Update to Latest Version</a>
		</div>
	</section>
	<section class="separator mt-50">
        <div class="separator-line"></div>
    </section>
	<section class="performance-sec">
		<div class="container">
			<?php if( have_rows('fourth_fold_content_row') ): $x=1; while ( have_rows('fourth_fold_content_row') ) : the_row(); ?>
				<?php if ($x%2) { ?>
					<div class="row aos-item" data-aos="fade-right">
						<div class="col-md-6">
							<figure>
								<img class="img-responsive" src="<?php the_sub_field('image'); ?>">
							</figure>
						</div>
						<div class="col-md-6">
							<h4><?php the_sub_field('title'); ?></h4>
							<p class="justify_custom"><?php the_sub_field('text'); ?></p>
							<a href="<?php the_sub_field('button_link'); ?>" class="button inverse fixed-button orange-btn"><?php the_sub_field('button_text'); ?></a>
						</div>
					</div>
				<?php } else { ?>
					<div class="row aos-item" data-aos="fade-left">
						<div class="col-md-6 mobile">
							<figure>
								<img class="img-responsive" src="<?php the_sub_field('image'); ?>">
							</figure>
						</div>
						<div class="col-md-6">
							<h4><?php the_sub_field('title'); ?></h4>
							<p class="justify_custom"><?php the_sub_field('text'); ?></p>
							<a href="<?php the_sub_field('button_link'); ?>" class="button inverse fixed-button orange-btn"><?php the_sub_field('button_text'); ?></a>
						</div>
						<div class="col-md-6 desktop">
							<figure>
								<img class="img-responsive" src="<?php the_sub_field('image'); ?>">
							</figure>
						</div>
					</div>
				<?php } ?>
			<?php $x++; endwhile; endif; ?>
		</div>
	</section>
	<section class="separator right-top mb-50">
        <div class="separator-line"></div>
    </section>
	<section class="our-customer text-center">
		<div class="container aos-item" data-aos="zoom-in">
			<h3><?php the_field('fifth_fold_title'); ?></h3>
			<p><?php the_field('fifth_fold_text'); ?></p>
			<div class="row">
				<?php if( have_rows('fifth_fold_content_box') ): while ( have_rows('fifth_fold_content_box') ) : the_row(); ?>
					<div class="col-md-4 border-box-parent aos-item" data-aos="fade-up">
						<div class="border-box" style="cursor: pointer;" onclick="location.href='<?php the_sub_field('link'); ?>';">
							<div class="icon">
								<figure>
									<img src="<?php the_sub_field('image'); ?>">
								</figure>
							</div>
							<p><?php the_sub_field('title'); ?></p>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
	<section class="separator">
        <div class="separator-line"></div>
    </section>
	<section class="testimonial text-center aos-item" data-aos="fade-in">
		<div class="container">
			<h4><?php the_field('testimonial_fold_title'); ?></h4>
		</div>
		<?php get_template_part( 'template-parts/template-part', 'testimonials' ); ?>
		<br>
		<a href="<?php the_field('after_testimonial_button_link'); ?>" class="button inverse fixed-button orange-btn"><?php the_field('after_testimonial_button_text'); ?></a>
	</section>
	<section class="gradient trust text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-6 aos-item aligment-button" data-aos="fade-down">
					<div class="text">
						<h3><?php the_field('heading_1') ?></h3>
						<p><?php the_field('text_1') ?></p>
                        <div class="aligment-elm">
						    <a href="<?php the_field('button_link_1') ?>" class="button inverse"><?php the_field('button_text_1') ?></a>
                        </div>
					</div>
				</div>
				<div class="col-md-6 aos-item aligment-button" data-aos="fade-up">
					<div class="text">
						<h3><?php the_field('heading_2') ?></h3>
						<p><?php the_field('text_2') ?></p>
                        <div class="aligment-elm">
						    <a href="<?php the_field('button_link_2') ?>" class="button inverse hover"><?php the_field('button_text_2') ?></a>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
                <div style="position:relative">
                    <button id="close_iframe">X</button>
                    <?php the_field('third_fold_video')?>
                </div>
			</div>
		</div>
	</div>

<?php endwhile; ?>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/rangeslider.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($){

		$('input[type=range]').rangeslider({
			polyfill: false
		});

		$('input[type=range]').change(function(){
			$(this).parent().parent().find('output').html($(this).val());
		});

	    var homeSlider = $('.home-gradient-slider').bxSlider({
	        pager: false,
	        auto: false,
	        touchEnabled: false,
            responsive: true,
	    });
	    jQuery('.goTo2ndSlide').click(function(){
	        $('input[name="2ndSlideValue"]').val($(this).attr('data-anchor'));
	        $('.2ndSlideText').html($(this).attr('data-anchor').replace('-', ' '));
	        homeSlider.goToNextSlide();
	    });
	    jQuery('.gotoBackSlide').click(function(){
	        homeSlider.goToPrevSlide();
	    });
	    jQuery('.goto3rdSlide').click(function(){
	    	console.log($('#js-rangeslider-1').text());
	        $('input[name="3rdSlideValue"]').val($('input[name="goto3rdSlideValue"]').val());
	        $('.3rdSlideText').html($('.js-rangeslider-1').text());
	        // $('.3rdSlideText').html($(this).val($('input[name="goto3rdSlideValue"]').val()));
	        homeSlider.goToNextSlide();
	    });
	    jQuery('.goto4thSlide').click(function(){
	        $('input[name="4thSlideValue"]').val($('input[name="goto4thSlideValue"]').val());
	        $('.4thSlideText').html($('.js-rangeslider-2').text());
	        // $('.4thSlideText').html($(this).val($('input[name="goto4thSlideValue"]').val()));
	        $('.gotoPageLink').attr('href', '<?php echo get_bloginfo("url"); ?>/'+$('input[name="2ndSlideValue"]').val());
	        homeSlider.goToNextSlide();
	    });

	    jQuery('#latest-version-download .close').click(function(){
	        jQuery('#latest-version-download').hide();
	    });

	    $(window).resize(function(){
            homeSlider.reloadSlider();
        })
	});

	
    //custom for video on homepage
    jQuery('#close_iframe').on('click',function() {
        jQuery('#myModal.in').click()
        jQuery('#ytplayer').remove();
        jQuery(this).parent().append('<iframe id="ytplayer" src="https://www.youtube.com/embed/d66gKA0AsvM" width="100%" height="100%" frameborder="0" allowfullscreen="allowfullscreen"></iframe>');
    });
    jQuery('div[aria-labelledby="exampleModalLabel"]').on('click',function() {
        jQuery('#ytplayer').remove();
        jQuery('#close_iframe').parent().append('<iframe id="ytplayer" src="https://www.youtube.com/embed/d66gKA0AsvM" width="100%" height="100%" frameborder="0" allowfullscreen="allowfullscreen"></iframe>'); 
    });
</script>

<?php
get_footer();
