<?php
/**
 * Template Name: Awards Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>

	<section class="awards">
		<div class="container">

<?php 
  $temp = $wp_query; 
  $wp_query = null; 
  $wp_query = new WP_Query(); 
  $wp_query->query('showposts=10&post_type=award'.'&paged='.$paged); 

  while ($wp_query->have_posts()) : $wp_query->the_post(); 
?>
						<div class="row aos-item" data-aos="fade-up">
							<div class="col-md-4">
								<figure class="bg-grey">
									<?php if (get_the_post_thumbnail_url()) { ?>
										<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive">
									<?php } else { ?>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/award.png" class="img-responsive">
									<?php } ?>
								</figure>
							</div>
							<div class="justify_custom col-md-8">
								<h5><?php the_title(); ?></h5>
								<p><?php the_excerpt(); ?></p>
								<p><a href="<?php the_permalink(); ?>" class="button inverse orange-btn">continue reading</a></p>
							</div>
						</div>

<?php endwhile; ?>
<div class="row aos-item" data-aos="fade-up">
<div class="col-md-4">
</div>
<div class="col-md-8">
<nav >
    <?php previous_posts_link('&laquo; Newer') ?>
    <?php next_posts_link('Older &raquo;') ?>
</nav>
</div>
</div>

<?php 
  $wp_query = null; 
  $wp_query = $temp;  // Reset
				wp_reset_postdata();
?>
		</div>
	</section>


<?php endwhile; ?>

<?php
get_footer();
