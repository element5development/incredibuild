<?php
/**
 * Template Name: Build Monitor Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'template-parts/template-part', 'header' ); ?>
    
	<section class="solution-text pt-50 make mb-100">
		<div class="container">
			<div class="row aos-item" data-aos="fade-down">
				<div class="col-md-12 mb-50">
					<?php the_field('first_fold_content'); ?>
				</div>
			</div>
			<div class="row mb-50 desktop">
				<div class="col-md-6 text-center">
					<h6><?php the_field('first_fold_title_1'); ?></h6>
				</div>
				<div class="col-md-6 text-center">
					<h6><?php the_field('first_fold_title_2'); ?></h6>
				</div>
				<div class="col-md-12">
					<img src="<?php the_field('first_fold_image_1'); ?>" class="img-responsive">
				</div>
			</div>
			<div class="row aos-item" data-aos="fade-down">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<img src="<?php the_field('first_fold_image_2'); ?>" class="img-responsive">
					<?php the_field('first_fold_bottom_content'); ?>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row mt-50 display-items aos-item" data-aos="fade-down">
				<div class="col-md-12">
					<h4 class="text-center"><?php the_field('first_fold_tabs_title'); ?></h4>
					<p class="text-center"><?php the_field('first_fold_tabs_text'); ?></p>
					<div id="exTab1" class="container">	
						<ul class="nav nav-pills">
							<?php if( have_rows('first_fold_tabs') ): ?>
								<?php $x=1; while( have_rows('first_fold_tabs') ): the_row(); ?>
									<li class="<?php if ($x==1): ?>
										active
									<?php endif ?>">
					        			<a href="#<?php echo $x; ?>a" data-toggle="tab"><?php the_sub_field('title'); ?></a>
									</li>
								<?php $x++; endwhile; ?>
							<?php endif; ?>
						</ul>
						<div class="tab-content clearfix">
						<?php if( have_rows('first_fold_tabs') ): ?>
							<?php $y=1; while( have_rows('first_fold_tabs') ): the_row(); ?>
								<div class="tab-pane <?php if ($y==1): ?> active <?php endif ?>" id="<?php echo $y; ?>a">
									<div class="row mt-50" style="min-height: 240px;">
										<div class="col-md-4">
											<?php the_sub_field('content'); ?>
										</div>
										<div class="col-md-8">
											<img src="<?php the_sub_field('image'); ?>" class="img-responsive">
										</div>
									</div>
								</div>
							<?php $y++; endwhile; ?>
						<?php endif; ?>
						</div>
			  		</div>
				</div>
			</div>
		</div>
	</section>
	<section class="monitor-text make mb-50">
		<div class="container">
			<div class="row">
			<?php if( have_rows('second_fold_content') ): ?>
				<?php $x=1; while( have_rows('second_fold_content') ): the_row(); ?>
					<?php if ($x!=3): ?>
						<div class="col-md-6 aos-item" data-aos="fade-right">
							<figure>
								<img src="<?php the_sub_field('image'); ?>" class="img-responsive">
							</figure>
							<p class="mt-30"><strong><?php the_sub_field('title'); ?></strong> <br><?php the_sub_field('content'); ?></p>
						</div>
					<?php else : ?>
						</div>
						<div class="row mt-50">
						<div class="col-md-6 aos-item" data-aos="fade-right">
							<figure>
								<img src="<?php the_sub_field('image'); ?>" class="img-responsive">
							</figure>
						</div>
						<div class="col-md-6 aos-item" data-aos="fade-left">
							<div class="flex" style="min-height: 275px;">
								<div>
									<p>	<strong><?php the_sub_field('title'); ?> </strong><br><?php the_sub_field('content'); ?></p>
								</div>
							</div>
						</div>
					<?php endif ?>
				<?php $x++; endwhile; ?>
			<?php endif; ?>
			</div>
		</div>
	</section>
	<section class="solution-text pt-50 monitor-detail">
		<div class="container">
			<div class="row">
                <?php if( have_rows('third_fold_content') ):?>
                    <?php $j = 1; while(have_rows('third_fold_content')) : the_row();?>
                            <div class="col-md-4 aos-item" data-aos="fade-down">
                                <figure class="icon">
                                    <img src="<?php the_sub_field('image'); ?>" class="img-responsive">
                                </figure>
                                <h6 class="text-center"><?php the_sub_field('title'); ?></h6>
                                <p><?php the_sub_field('content'); ?></p>
                            </div>
                    <?php if($j == 3):?>
                            </div><div class="row mt-50">
                            <div class="col-md-2"></div>
                    <?php endif;?>
                    <?php if($j == 5):?>
                            <div class="col-md-2"></div></div>
                    <?php endif;?>
                    <?php $j++; endwhile;?>
                <?php endif;?>
		</div>
	</section>
	<section class="monitor-video">
		<div class="container">
			<div class="row mt-50">
				<div class="col-md-12 text-center aos-item" data-aos="zoom-in">
					<h6 class="text-center">Learn how to use IncrediBuild's build monitor to visualize your build like never before and optimize it with ease.</h6>
					<div class="video-preload-box">
                        <div class="preload-box">
                            <img style="width:100%;" src="<?php the_field('third_fold_bottom_image')?>" alt="">
                            <button class="play-ytb-video"></button>
                        </div>
                        <div class="video-box">
                            <?php the_field('third_fold_bottom_video')?>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</section>
	<section class="testimonial pb-40 text-center aos-item" data-aos="fade-in">
		<?php get_template_part( 'template-parts/template-part', 'testimonials' ); ?>
	</section>
	<section class="gradient gradient-curve trust trust-texture text-center mt-100 aos-item" data-aos="zoom-in" style="padding: 60px 0px;">
		<div class="container">
			<figure class="texture">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/texture.png" class="img-responsive">
			</figure>
			<div class="row">
				<div class="col-md-12">
					<div class="text">
						<h3 class="mb-50"><?php the_field('last_fold_title'); ?></h3>
						<a href="<?php the_field('last_fold_button_link'); ?>" class="button inverse"><?php the_field('last_fold_button_text'); ?></a>
					</div>
				</div>
			</div>
		</div>
	</section>

<!--	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--		<div class="modal-dialog" role="document">-->
<!--			<div class="modal-content">-->
<!--				<div class="">-->
<!--					<video style="margin-bottom: -5px;" width="100%" autoplay>-->
<!--						<source src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/assets/images/Build_Monitor_Video.webm" type="video/webm">-->
<!--						<source src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/assets/images/Build_Monitor_Video.mov" type="video/mov">-->
<!--						Your browser does not support HTML5 video.-->
<!--					</video>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->

<?php endwhile; ?>

<?php
get_footer();
