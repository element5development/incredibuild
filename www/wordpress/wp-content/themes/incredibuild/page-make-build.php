<?php
/**
 * Template Name: Make And Build Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'template-parts/template-part', 'header' ); ?>
    
	<section class="solution-text pt-50 make mb-100">
		<figure class="solution-texture">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/make-texture.png">
		</figure>
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="justify_custom col-md-10 aos-item" data-aos="fade-down">
					<?php the_field('first_fold_content'); ?>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</section>
	<section class="make-box">
		<div class="make-container">
			<h4 class="text-center mb-50"><?php the_field('second_fold_title'); ?></h4>
			<div class="row">
				<?php if( have_rows('second_fold_boxes') ): while ( have_rows('second_fold_boxes') ) : the_row(); ?>
					<div class="col-sm-6 col-md-3  aos-item" data-aos="fade-up">
						<div class="box">
							<?php if(get_sub_field('image')) { ?>
							<figure>
								<img src="<?php the_sub_field('image'); ?>">
								<span class="button">Learn More</span>
							</figure>
							<?php } else { ?>
							<figure>
								<h2><?php the_sub_field('title'); ?></h2>
								<span class="button">Learn More</span>
							</figure>
							<?php } ?>

							<div class="overlay">
								<span class="close">x</span>
								<div>
									<h2><?php the_sub_field('title'); ?></h2>
									<p><?php the_sub_field('content'); ?></p>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
	<section class="solution-text pt-50 pb-40">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="justify_custom col-md-10 aos-item" data-aos="zoom-in">
					<?php the_field('second_fold_content'); ?>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</section>
	<section class="gradient gradient-curve trust trust-texture text-center mt-100 aos-item" data-aos="zoom-out">

      <?php get_template_part( 'template-parts/template-part', 'gradient-section' ); ?>

	</section>

<style type="text/css">
	@media (min-width: 768px) {
		.make-box .box:hover .overlay {
			top: 0px;
		}
	}
	.make-box .box {
		border: 1px solid #ccc;
	}
	.make-box .box .overlay .close,
	.make-box .box figure .button {
		display: none;
	}
	@media (max-width: 768px) {
		.make-box .box .overlay .close,
		.make-box .box figure .button {
			display: inline-block;
		}
	}
</style>

<script type="text/javascript">

var x = document.getElementById("bgVideo"); 

function playVid() { 
	alert();
    x.play(); 
} 

function pauseVid() { 
    x.pause(); 
} 
	// jQuery(document).ready(function(){
	// 	jQuery('#playVideo').click(function(){
	// 		var myVideo = document.getElementsByTagName('video')[0];
	// 		myVideo.play();
	// 	});
	// });
</script>

<?php endwhile; ?>

<?php
get_footer();
