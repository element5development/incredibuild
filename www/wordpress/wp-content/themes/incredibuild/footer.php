<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Incredibuild
 */

?>  </section>
    <section class="footer-sticky">
        <section class="footer-widget aos-item" data-aos="fade-up" data-aos-duration="1000">
        <!-- <section class="footer-widget" > -->
            <div class="container">
                <div class="row">
            <?php dynamic_sidebar('footer-1'); ?>
                </div>
            </div>
        </section>
        <footer>
            <div class="footer-container">
                <div class="row">
                    <div class="col-md-3 social mobile">
              <?php if (ot_get_option( 'facebook_link' )) { ?>
                <a href="<?php echo ot_get_option( 'facebook_link' ); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
              <?php } ?>
              <?php if (ot_get_option( 'twitter_link' )) { ?>
                <a href="<?php echo ot_get_option( 'twitter_link' ); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
              <?php } ?>
              <?php if (ot_get_option( 'linkedin_link' )) { ?>
                <a href="<?php echo ot_get_option( 'linkedin_link' ); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
              <?php } ?>
              <?php if (ot_get_option( 'google_plus_link' )) { ?>
                <a href="<?php echo ot_get_option( 'google_plus_link' ); ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
              <?php } ?>
                    </div>
                    <div class="col-md-4">
                        <h5>IncrediBuild</h5>
                        <p><?php echo ot_get_option( 'about_text' ); ?> <br><?php echo ot_get_option( 'copyrights_text' ); ?></p>
                    </div>
                    <div class="col-md-5">
                        <h5></h5>
                        <p><a href="mailto:<?php echo ot_get_option( 'email' ); ?>"><strong><?php echo ot_get_option( 'email' ); ?></strong></a><br>
                        <a href="tel:<?php echo ot_get_option( 'contact_number' ); ?>"><?php echo ot_get_option( 'contact_number' ); ?></a><br>
                        <a href="https://www.google.com/maps?q=<?php echo ot_get_option( 'address' ); ?>" target="_blank"><?php echo ot_get_option( 'address' ); ?></a></p>
                    </div>
                    <div class="col-md-3 social desktop">
              <?php if (ot_get_option( 'facebook_link' )) { ?>
                <a href="<?php echo ot_get_option( 'facebook_link' ); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
              <?php } ?>
              <?php if (ot_get_option( 'twitter_link' )) { ?>
                <a href="<?php echo ot_get_option( 'twitter_link' ); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
              <?php } ?>
              <?php if (ot_get_option( 'linkedin_link' )) { ?>
                <a href="<?php echo ot_get_option( 'linkedin_link' ); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
              <?php } ?>
              <?php if (ot_get_option( 'google_plus_link' )) { ?>
                <a href="<?php echo ot_get_option( 'google_plus_link' ); ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
              <?php } ?>
                    </div>
                </div>
            </div>
        </footer>
    </section>
  </div>

<?php wp_footer(); ?>

<!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.js"></script> -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.bxslider.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($){
        AOS.init({
            duration: 600,
            offset: 50,
            easing: 'ease-in-out-sine'
        });
// setInterval(function(){
//   $(".gradient, .separator, .separator-line, .gradient-curve, .gradient-curve:before, .gradient-inverse, .gradient-inverse:before, .solution-curve, .solution-curve:before, .make:before, .dev-gradient:after, .dev-gradient:before ").animate({background: "linear-gradient(270deg, #e4177b, #fede11)" }, 3000);
//   $(".gradient, .separator, .separator-line, .gradient-curve, .gradient-curve:before, .gradient-inverse, .gradient-inverse:before, .solution-curve, .solution-curve:before, .make:before, .dev-gradient:after, .dev-gradient:before ").animate({background: "linear-gradient(0deg, #e4177b, #fede11)" }, 3000);
// }, 6000);
//        $(window).on('load', function() {
//            AOS.refresh();
//        });
        window.addEventListener('load', AOS.refresh);
        jQuery(".search-btn a").click(function(){
        jQuery(".search-section").fadeToggle(),jQuery(".search-btn a i").toggleClass("fa-remove")});
        jQuery(".search-mobile").click(function(){
        jQuery(".search-section-mob").fadeToggle(),jQuery(".search-mobile i").toggleClass("fa-remove")});

        var slider = $('.testi-slider').bxSlider();
        var widthMatch = matchMedia("all and (max-width: 768px)");
        var widthHandler = function(matchList) {
            if (matchList.matches) {
                slider.reloadSlider({
                    minSlides: 1,
                    maxSlides: 1,
                    slideWidth: 500,
                    slideMargin: 10,
                    moveSlides: 1,
                    pager: false,
                    auto: true
                })
            } else {
                slider.reloadSlider({
                    minSlides: 1,
                    maxSlides: 4,
                    slideWidth: 460,
                    slideMargin: 10,
                    moveSlides: 1,
                    pager: false,
                    auto: true,
                    responsive: true
                })
            }
        }

        var testiSlider = $('.inner-testi-slider').bxSlider({
            auto: true,
            pager: false,
            controls: (jQuery(".inner-testi-slider>li").length > 1) ? true: false,
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            slideWidth: 780,
            responsive: true,
            infiniteLoop: true,
            autoHover: true,
            adaptiveHeight: false,
            onSliderLoad: function(){
                $(window).resize(function(){testiSlider.reloadSlider()});
            }
        });

        setTimeout(function(){
            if($('.inner-testi-slider').length > 0) {
                testiSlider.redrawSlider();   
            }
        },100);
    });
    function menuBarShowHide() {
        var x = document.getElementById("myDIV");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script>
<?php if (is_page_template('page-home.php')):?>
    <script>
          var winWidth = jQuery(window).width();
          var countSlides = 3;
          if(winWidth <= 768) countSlides = 1;
          var clientImgSlider = jQuery('.js-slider.client-img').bxSlider({
            auto: true,
            pager: false,
            controls: (jQuery('.js-slider.client-img > .img-wrapper').length > 1) ? true : false,
            minSlides: countSlides,
            maxSlides: countSlides,
            responsive: true,
            slideWidth: 200,
            adaptiveHeight: false, 
            onSliderLoad: function(){
               jQuery(window).resize(function(){
                   var winWidth = jQuery(window).width();
                   if (winWidth <= 768) countSlides = 1;
                   clientImgSlider.reloadSlider()
               });
            }
        });

    </script>
<?php endif;?>
<?php if (is_front_page()): ?>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/rangeslider.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('input[type=range]').rangeslider({
                polyfill: false
            });

            $('input[type=range]').change(function(){
                var sliderVal = $(this).val();
                if (sliderVal == 45) {
                    var sliderVal = $(this).val()+'+';
                }
                if(this.name = "goto3rdSlideValue" && sliderVal == 10) sliderVal = sliderVal+"+";
                $(this).parent().parent().find('output').html(sliderVal);
            });

            var homeSlider = $('.home-gradient-slider').bxSlider({
                pager: false,
                auto: false,
                touchEnabled: false
            });
            jQuery('.goTo2ndSlide').click(function(){
                $('input[name="2ndSlideValue"]').val($(this).attr('data-anchor'));
                $('.2ndSlideText').html($(this).attr('data-name'));
                //homeSlider.goToNextSlide();
            });
            jQuery('.gotoBackSlide').click(function(){
                //homeSlider.goToPrevSlide();
            });
            jQuery('.goto3rdSlide').click(function(){
                $('input[name="3rdSlideValue"]').val($('input[name="goto3rdSlideValue"]').val());
                $('.3rdSlideText').html($('input[name="goto3rdSlideValue"]').val());
                //homeSlider.goToNextSlide();
            });
            jQuery('.goto4thSlide').click(function(){
                $('input[name="4thSlideValue"]').val($('input[name="goto4thSlideValue"]').val());
                var sliderVal2 = $('input[name="goto4thSlideValue"]').val();
                if (sliderVal2 == 45) {
                    var sliderVal2 = $('input[name="goto4thSlideValue"]').val()+'+';
                }
                $('.4thSlideText').html(sliderVal2);
                $('.gotoPageLink').attr('href', '<?php echo get_bloginfo("url"); ?>/'+$('input[name="2ndSlideValue"]').val());
                //homeSlider.goToNextSlide();
            });
        });
    </script>
<?php endif ?>

<?php if (is_page_template('page-solution-single.php')): ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.full-width-slider').bxSlider({
                mode: 'fade',
                pager: false
            });
        });
    </script>
<?php endif ?>

<?php if (is_page_template('page-career.php')): ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('#input_1_2').change(function(){
                $('#gform_submit_button_1').click();
            });
        });
    </script>
    <style type="text/css">.gform_footer.top_label #gform_submit_button_1 {display: none !important;} #input_1_2 {/*margin-bottom: 40px !important;*/} #gform_confirmation_message_1 {text-align: center; font-size: 18px; font-weight: bold; margin-bottom: 40px !important;} body .gform_wrapper .validation_message {display: block !important;}</style>
<?php endif ?>

<?php if (is_page_template('page-about.php') || is_page_template('page-visual-studio.php') || is_page_template('page-continuous-integration.php') || is_page_template('page-pricing.php')): ?>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/circle-progress.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/appear.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            var el = $('.thirdDiv'),
            inited = false;
            el.appear({ force_process: true });
            el.on('appear', function() {
                if (!inited) {
                    $("#circle1").circleProgress({
                        value: 0.7,
                        size: 280,
                        fill: {
                          gradient: [ "#e4177b" , "#fede11"]
                          }
                    });

                    $("#circle1-mob").circleProgress({
                        value: 0.7,
                        size: 280,
                        fill: {
                          gradient: [ "#e4177b" , "#fede11"]
                          }
                    });

                    $("#circle3").circleProgress({
                        value: 0.75,
                        size: 240,
                        fill: {
                          gradient: [ "#a4a6a8"]
                          }
                    });

                    $("#circle2").circleProgress({
                        value: 0.75,
                        size: 240,
                        fill: {
                          gradient: [ "#a4a6a8"]
                          }
                    });

                    $("#circle4").circleProgress({
                        value: 0.8,
                        size: 240,
                        fill: {
                          gradient: [ "#e4177b" , "#fede11"]
                          }
                    });

                    $("#circle5").circleProgress({
                        value: 0.5,
                        size: 240,
                        fill: {
                          gradient: [ "#e4177b" , "#fede11"]
                          }
                    });
                    $("#circle6").circleProgress({
                        value: 0.65,
                        size: 115,
                        fill: {
                          gradient: [ "#a4a6a8"]
                          }
                    });
                    
                    
                    $("#circle7").circleProgress({
                        value: 0.65,
                        size: 115,
                        fill: {
                          gradient: [ "#a4a6a8"]
                          }
                    });
                    
                    
                    $("#circle8").circleProgress({
                        value: 0.55,
                        size: 115,
                        fill: {
                          gradient: [ "#a4a6a8"]
                          }
                    });
                    
                    
                    $("#circle9").circleProgress({
                        value: 0.8,
                        size: 115,
                        fill: {
                          gradient: [ "#a4a6a8"]
                          }
                    });

                    $("#circle10").circleProgress({
                        value: 0.2,
                        size: 115,
                        fill: {
                          gradient: [ "#e4177b" , "#fede11"]
                          }
                    });

                    $("#circle11").circleProgress({
                        value: 0.2,
                        size: 115,
                        fill: {
                          gradient: [ "#e4177b" , "#fede11"]
                          }
                    });

                    $("#circle12").circleProgress({
                        value: 0.1,
                        size: 115,
                        fill: {
                          gradient: [ "#e4177b" , "#fede11"]
                          }
                    });

                    $("#circle13").circleProgress({
                        value: 0.25,
                        size: 115,
                        fill: {
                          gradient: [ "#e4177b" , "#fede11"]
                          }
                    });

                    $("#circle14").circleProgress({
                        value: 0.75,
                        size: 280,
                        fill: {
                          gradient: [ "#e4177b" , "#fede11"]
                          }
                    });

                    $("#circle15").circleProgress({
                        value: 0.9,
                        size: 280,
                        fill: {
                          gradient: [ "#e4177b" , "#fede11"]
                          }
                    });
                    inited = true;
                }
            });
            var a = 0;
            jQuery(window).scroll(function(){
              if($('#counter').length) {
              var oTop = $('#counter').offset().top - window.innerHeight;
              if (a == 0 && $(window).scrollTop() > oTop) {
                $('.counter-value').each(function() {
                  var $this = $(this),
                    countTo = $this.attr('data-count');
                  $({
                    countNum: $this.text()
                  }).animate({
                      countNum: countTo
                    },

                    {

                      duration: 2000,
                      easing: 'swing',
                      step: function() {
                        $this.text(Math.floor(this.countNum));
                      },
                      complete: function() {
                        $this.text(this.countNum);
                        //alert('finished');
                      }

                    });
                });
                a = 1;
              }
            }

            });
        });
    </script>
<?php endif ?>

<?php if (is_page_template('page-pricing.php')): ?>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/rangeslider.js"></script>
    <script src="https://cdn.rawgit.com/leafo/sticky-kit/v1.1.2/jquery.sticky-kit.min.js"></script>
    <script type="text/javascript">
        function calculationInit(display) {
            jQuery('.calculatedTextFigures').html('');

            var totalCalAmount = 0;
            var quantity = '';
            jQuery('.input-sec').each(function(){
                if (jQuery(this).find('.input-number').val() > 0) {
                    var quantity = jQuery(this).find('.input-number').val();
                    jQuery('.calculatedTextFigures').append('<p class="clearfix">'+jQuery(this).find('.secName').html()+' (x'+quantity+') <span class="pull-right">$'+jQuery(this).find('input[name="sectionPriceField"]').val()+'</span></p>');
                    // var totalCalAmount = parseInt(totalCalAmount)+parseInt(jQuery(this).find('input[name="sectionPriceField"]').val());
                    // alert(jQuery(this).find('input[name="sectionPriceField"]').val());
                    var abcTotal = jQuery('.totalCalAmountText').html();
                    jQuery('.totalCalAmountText').html(parseInt(abcTotal)+parseInt(jQuery(this).find('input[name="sectionPriceField"]').val()));

                    var abcTotal2 = jQuery('input[name="totalCalAmountVal"]').val();
                    jQuery('input[name="totalCalAmountVal"]').val(parseInt(abcTotal2)+parseInt(jQuery(this).find('input[name="sectionPriceField"]').val()));
                }
            });


            var buildMinutes = jQuery('#buildMinutes').val();
            var numberOfCoresPerAgents = jQuery('#numberOfCoresPerAgents').val();
            var numberOfAgents = jQuery('#numberOfAgents').val();
            var totalSolutionsCost = jQuery('input[name="totalCalAmountVal"]').val();

            var estimtedYearlyCost = parseInt(numberOfAgents)*parseInt(buildMinutes)*parseInt(20)*12/60*40;
            var estimtedYearlyTime = parseInt(numberOfAgents)*parseInt(buildMinutes)*parseInt(20)*12/60;

            var coreCost = 365;
            var uptoCore = 4;
            if (numberOfCoresPerAgents > 48) {
                var coreCost = 1495;
                var uptoCore = 64;
            } else if (numberOfCoresPerAgents > 32) {
                var coreCost = 995;
                var uptoCore = 48;
            } else if (numberOfCoresPerAgents > 24) {
                var coreCost = 795;
                var uptoCore = 32;
            } else if (numberOfCoresPerAgents > 16) {
                var coreCost = 695;
                var uptoCore = 24;
            } else if (numberOfCoresPerAgents > 8) {
                var coreCost = 595;
                var uptoCore = 16;
            } else if (numberOfCoresPerAgents > 4) {
                var coreCost = 495;
                var uptoCore = 8;
            } else {
                var coreCost = 395;
                var uptoCore = 4;
            }

            // var estimtedYearlyCostIB = parseInt(numberOfAgents)*(395+140);
            var estimtedYearlyCostIB = parseInt(numberOfAgents)*parseInt(coreCost)+parseInt(totalSolutionsCost);
            // var estimtedYearlyCostIB = (parseInt(numberOfAgents)*394)+parseInt(jQuery('.totalCalAmountText').html());
            jQuery('.totalCalAmountText2').html(estimtedYearlyCostIB);
            jQuery('.totalCalAmountText').html(estimtedYearlyCostIB);
            var estimtedYearlySavedIB = parseInt(estimtedYearlyCost)-parseInt(estimtedYearlyCostIB);

            jQuery('.calculatedTextFigures').prepend('<p class="clearfix">Incredibuild for '+numberOfAgents+' agents with upto '+uptoCore+' cores each.<span class="pull-right">$'+parseInt(numberOfAgents)*parseInt(coreCost)+'</span></p>');

            var returnOfInv = Math.round((estimtedYearlyCostIB/estimtedYearlyCost) * 12);
            var costIBPerAgent = parseInt(estimtedYearlyCostIB)/parseInt(numberOfAgents);
            
            jQuery('.withoutIBCounter').html('<div class="progressbar withoutIBCounter"> <div id="circlenewYC"> <div class="progress-text-top">Estimated Yearly Cost</div> <div class="circle-overlay"></div> <p class="counter-data"> $<span class="counter-value circle-counter" data-count="'+estimtedYearlyCost+'">0</span> </p> <div class="progress-text">Dev cost: $40/hr</div> </div> <div id="circlenewYT"> <div class="progress-text-top">Yearly Time Spent</div> <div class="circle-overlay"></div> <p class="counter-data"> <span class="counter-value circle-counter" data-count="'+estimtedYearlyTime+'">0</span> hrs </p> <div class="progress-text">Average: 20 working days</div> </div> </div>');

            jQuery('.withIBCounter').html('<div class="progressbar withIBCounter"> <div id="circlenewIBYC"> <div class="progress-text-top">Yearly Savings</div> <div class="circle-overlay"></div> <p class="counter-data"> $<span class="counter-value circle-counter" data-count="'+estimtedYearlySavedIB+'">0</span> </p> </div> <div id="circlenewIBYT"> <div class="progress-text-top">ROI</div> <div class="circle-overlay"></div> <p class="counter-data"> <span class="counter-value circle-counter" data-count="'+returnOfInv+'">0</span> </p> <div class="progress-text">months</div> </div> </div>');

            jQuery('.buildTimeWithoutIncredibuild').html(estimtedYearlyTime);
            jQuery('.savingWithIncredibuild').html(estimtedYearlySavedIB);
            jQuery('.costWithIncredibuild').html(estimtedYearlyCostIB);
            jQuery('.numberOfAgentsText').html(numberOfAgents);
            jQuery('.costPerAgentText').html(costIBPerAgent);

            setTimeout(function(){
                if (display == true) {
                    jQuery('.calculated').show();
                }

                jQuery("#circlenewYC").circleProgress({
                    value: 0.75,
                    size: 240,
                    fill: {
                      gradient: [ "#a4a6a8"]
                      }
                });

                jQuery("#circlenewYT").circleProgress({
                    value: 0.75,
                    size: 240,
                    fill: {
                      gradient: [ "#a4a6a8"]
                      }
                });

                jQuery("#circlenewIBYC").circleProgress({
                    value: 0.8,
                    size: 240,
                    fill: {
                      gradient: [ "#e4177b" , "#fede11"]
                      }
                });

                jQuery("#circlenewIBYT").circleProgress({
                    value: 0.8,
                    size: 240,
                    fill: {
                      gradient: [ "#e4177b" , "#fede11"]
                      }
                });


                var a = 0;
                jQuery(window).scroll(function() {
                  var oTop = jQuery('#counter').offset().top - window.innerHeight;
                  if (a == 0 && jQuery(window).scrollTop() > oTop) {
                    jQuery('.counter-value').each(function() {
                      var $this = jQuery(this),
                        countTo = $this.attr('data-count');
                      jQuery({
                        countNum: $this.text()
                      }).animate({
                          countNum: countTo
                        },

                        {

                          duration: 2000,
                          easing: 'swing',
                          step: function() {
                            $this.text(Math.floor(this.countNum));
                          },
                          complete: function() {
                            $this.text(this.countNum);
                            //alert('finished');
                          }

                        });
                    });
                    a = 1;
                  }
                });
            }, 500);
        } 

        jQuery(document).ready(function($){
            $(".stickyArea").stick_in_parent();

            $('[data-toggle="tooltip"]').tooltip();

            jQuery('.calculate-btn').click(function(){
                calculationInit(true);
            });

            // Dev Machine Sec
            jQuery(document).on('click', '.add-more-machine-btn', function(){
                $('#machineBlockCount').val(parseInt($('#machineBlockCount').val())+1);
                $('#machineBlockCount').before('<div class="row price-data machineBlock"><div class="col-xs-5 col-should-extend"><input type="number" id="numDev'+$('#machineBlockCount').val()+'" name="numDev'+$('#machineBlockCount').val()+'" style="margin: 30px 0 0 0px; text-align: center; padding: 0; font-size: 20px; width: 160px;" disabled="disabled"></div><div class="col-xs-4 col-should-extend"><select class="aos-item" data-aos="fade-down" style="margin: 30px 0 0 0px; min-width: 100%;" id="numDevCore'+$('#machineBlockCount').val()+'" name="numDevCore'+$('#machineBlockCount').val()+'"><option value="">Add a number of cores</option><option value="4">Up to 4 cores</option><option value="8">Up to 8 cores</option><option value="16">Up to 16 cores</option><option value="28">Up to 28 cores</option><option value="32">Up to 32 cores</option><option value="48">Up to 48 cores</option><option value="64">Up to 64 cores</option></select></div><div class="col-xs-2 col-should-extend"><a href="#" class="removeIt" style="display: inline-block; color: #999; padding: 12px; margin-top: 33px;">Remove</a></div></div>');
            });

            jQuery(document).on('click', '.removeIt', function(){
                $('#machineBlockCount').val(parseInt($('#machineBlockCount').val())-1);
                $(this).parent().parent().remove();
                return false;
            });

            jQuery(document).on('change', '.machineBlock select', function(){
                if ($(this).val()) {
                  $(this).parent().parent().find('input').removeAttr('disabled');
                } else {
                  $(this).parent().parent().find('input').val('');
                  $(this).parent().parent().find('input').attr('disabled', 'disabled');
                }
            });

            // Helper Machine Sec
            $('.showHelperBlock').click(function(){
              $(this).hide();
              $('.helperBlock').show();
            })

            jQuery(document).on('click', '.add-more-machineHelper-btn', function(){
                $('#machineHelperBlockCount').val(parseInt($('#machineHelperBlockCount').val())+1);
                $('#machineHelperBlockCount').before('<div class="row price-data machineHelperBlock"><div class="col-xs-5 col-should-extend"><input type="number" id="numHelper'+$('#machineHelperBlockCount').val()+'" name="numHelper'+$('#machineHelperBlockCount').val()+'" style="margin: 30px 0 0 0px; text-align: center; padding: 0; font-size: 20px; width: 160px;" disabled="disabled"></div><div class="col-xs-4 col-should-extend"><select class="aos-item" data-aos="fade-down" style="margin: 30px 0 0 0px; min-width: 100%;" id="numHelperCore'+$('#machineHelperBlockCount').val()+'" name="numHelperCore'+$('#machineHelperBlockCount').val()+'"><option value="">Add a number of cores</option><option value="4">Up to 4 cores</option><option value="8">Up to 8 cores</option><option value="16">Up to 16 cores</option><option value="28">Up to 28 cores</option><option value="32">Up to 32 cores</option><option value="48">Up to 48 cores</option><option value="64">Up to 64 cores</option></select></div><div class="col-xs-2 col-should-extend"><a href="#" class="removeItHelper" style="display: inline-block; color: #999; padding: 12px; margin-top: 33px;">Remove</a></div></div>');
            });

            jQuery(document).on('click', '.removeItHelper', function(){
                $('#machineHelperBlockCount').val(parseInt($('#machineHelperBlockCount').val())-1);
                $(this).parent().parent().remove();
                return false;
            });

            jQuery(document).on('change', '.machineHelperBlock select', function(){
                if ($(this).val()) {
                  $(this).parent().parent().find('input').removeAttr('disabled');
                } else {
                  $(this).parent().parent().find('input').val('');
                  $(this).parent().parent().find('input').attr('disabled', 'disabled');
                }
            });

            // Add Solution
            $(document).on('change', '#addMoreSolution', function(){
                var tobedeleted = $(this).val();
                var tobedeletedPrice = $('#addMoreSolution option[value="'+tobedeleted+'"]').attr('data-priceVal');
                var tobedeletedQuan = $('#addMoreSolution option[value="'+tobedeleted+'"]').attr('data-quan');
                if (tobedeleted != '') {
                    $('#addMoreSolution option[value="'+tobedeleted+'"]').remove();
                    $('.addMoreSolutionHere').append('<div class="input-sec mt-30 aos-item" data-aos="fade-down"><div class="input-group plus-minus"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number" data-type="minus" data-field="quant['+tobedeletedQuan +']"><span class="glyphicon glyphicon-minus"></span></button></span> <input type="text" name="quant['+tobedeletedQuan +']" class="form-control input-number" readonly="readonly" value="1" min="0" max="10"> <span class="input-group-btn"><button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant['+tobedeletedQuan +']"><span class="glyphicon glyphicon-plus"></span></button></span></div><div class="input-text"><p><font class="secName">'+tobedeleted+'</font> <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Accelerates runtime for various in-house and commercial development tools used during the development process"></i><input type="hidden" name="sectionPriceField" value="'+tobedeletedPrice+'"> <input type="hidden" name="sectionPriceFieldOriginal" value="'+tobedeletedPrice+'"> <span class="pull-right"><strong>$<font class="sectionPrice">'+tobedeletedPrice+'</font></strong></span></p></div></div>');
                    $(document.body).trigger("sticky_kit:recalc");
                }
                if($('#addMoreSolution option').length == 1) {
                    $('#addMoreSolution, .removeHeadingAsWell').remove();
                }
                calculationInit(false);
            });
        });

        jQuery(function($) {
            $(document).on('input', 'input[type="range"]', function(e) {
            });

            $('input[type=range]').rangeslider({
                polyfill: false
            });

            $('input[type=range]').change(function(){
                $(this).parent().find('output').html($(this).val());
                calculationInit(false);
                //$('.numberOfAgentsText').html($(this).val());
            });

            $('#numberOfAgents').change(function(){
                $('.numberOfAgentsText').html($(this).val());
            });
        });

        jQuery(document).ready(function($){
            $(document).on('click', '.btn-number', function(e) {
                e.preventDefault();
                fieldName = $(this).attr('data-field');
                type      = $(this).attr('data-type');
                var input = $("input[name='"+fieldName+"']");
                var currentVal = parseInt(input.val());
                if (!isNaN(currentVal)) {
                    if(type == 'minus') {
                        
                        if(currentVal > input.attr('min')) {
                            input.val(currentVal - 1).change();
                            $(this).parent().parent().parent().find('.sectionPrice').html($(this).parent().parent().parent().find('input[name="sectionPriceFieldOriginal"]').val()*(currentVal - 1));
                            $(this).parent().parent().parent().find('input[name="sectionPriceField"]').val($(this).parent().parent().parent().find('input[name="sectionPriceFieldOriginal"]').val()*(currentVal - 1));
                        } 
                        if(parseInt(input.val()) == input.attr('min')) {
                            $(this).attr('disabled', true);
                        }

                    } else if(type == 'plus') {

                        if(currentVal < input.attr('max')) {
                            input.val(currentVal + 1).change();
                            $(this).parent().parent().parent().find('.sectionPrice').html($(this).parent().parent().parent().find('input[name="sectionPriceFieldOriginal"]').val()*(currentVal + 1));
                            $(this).parent().parent().parent().find('input[name="sectionPriceField"]').val($(this).parent().parent().parent().find('input[name="sectionPriceFieldOriginal"]').val()*(currentVal + 1));
                        }
                        if(parseInt(input.val()) == input.attr('max')) {
                            $(this).attr('disabled', true);
                        }

                    }
                } else {
                    input.val(0);
                }
                calculationInit(false);
            });
            $(document).on('focusin', '.input-number', function(e) {
                $(this).data('oldValue', $(this).val());
            });
            $(document).on('change', '.input-number', function(e) {
                minValue =  parseInt($(this).attr('min'));
                maxValue =  parseInt($(this).attr('max'));
                valueCurrent = parseInt($(this).val());
                
                name = $(this).attr('name');
                if(valueCurrent >= minValue) {
                    $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
                } else {
                    alert('Sorry, the minimum value was reached');
                    $(this).val($(this).data('oldValue'));
                }
                if(valueCurrent <= maxValue) {
                    $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
                } else {
                    alert('Sorry, the maximum value was reached');
                    $(this).val($(this).data('oldValue'));
                }
            });
            $(document).on('keydown', '.input-number', function(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                     // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) || 
                     // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            $('a[href="#hereIsCalculation"]').click(function(){
                $('html, body').animate({
                    scrollTop: $( $(this).attr('href') ).offset().top
                }, 3000);
                return false;
            });
        });
    </script>
<?php endif ?>

<?php if (is_page_template('page-dev-tools.php')): ?>
<!--    <script type="text/javascript">-->
<!--        jQuery(document).ready(function($){-->
<!--            jQuery('.game').mouseover(function(){-->
<!--                jQuery('#triangle-right, .game-hover').show();-->
<!--            });-->
<!--            jQuery('.qa').mouseover(function(){-->
<!--                jQuery('#triangle-right, .qa-hover').show();-->
<!--            });-->
<!--            jQuery('.house').mouseover(function(){-->
<!--                jQuery('#triangle-left, .house-hover').show();-->
<!--            });-->
<!--            jQuery('.code').mouseover(function(){-->
<!--                jQuery('#triangle-left, .code-hover').show();-->
<!--            });-->
<!--            jQuery('.slow').mouseover(function(){-->
<!--                jQuery('#triangle-top, .slow-hover').show();-->
<!--            });-->
<!--            jQuery('.complex').mouseover(function(){-->
<!--                jQuery('#triangle-top, .complex-hover').show();-->
<!--            });-->
<!--            jQuery('.bottom-dev .col-md-3').mouseover(function(){-->
<!--                jQuery(".complex, .slow").css("opacity", "0.2");-->
<!--            });-->
<!--            jQuery('.bottom-dev .col-md-3').mouseleave(function(){-->
<!--                jQuery(".complex, .slow").css("opacity", "1");-->
<!--            });-->
<!---->
<!--            jQuery('.dev-text .col-md-6').mouseleave(function(){-->
<!--                jQuery('#triangle-right, .game-hover').hide();-->
<!--            });-->
<!--            jQuery('.dev-text .col-md-6').mouseleave(function(){-->
<!--                jQuery('#triangle-right, .qa-hover').hide();-->
<!--            });-->
<!--            jQuery('.dev-text .col-md-6').mouseleave(function(){-->
<!--                jQuery('#triangle-left, .code-hover').hide();-->
<!--            });-->
<!--            jQuery('.dev-text .col-md-6').mouseleave(function(){-->
<!--                jQuery('#triangle-left, .house-hover').hide();-->
<!--            });-->
<!--            jQuery('.dev-text .col-md-3').mouseleave(function(){-->
<!--                jQuery('#triangle-top, .slow-hover').hide();-->
<!--            });-->
<!--            jQuery('.dev-text .col-md-3').mouseleave(function(){-->
<!--                jQuery('#triangle-top, .complex-hover').hide();-->
<!--            });-->
<!--        });-->
<!--    </script>-->
<?php endif ?>

</body>
</html>
