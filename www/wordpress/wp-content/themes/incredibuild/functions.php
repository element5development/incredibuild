<?php
/**
 * Incredibuild functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Incredibuild
 */

if ( ! function_exists( 'incredibuild_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function incredibuild_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Incredibuild, use a find and replace
		 * to change 'incredibuild' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'incredibuild', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'incredibuild' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'incredibuild_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'incredibuild_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function incredibuild_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'incredibuild_content_width', 640 );
}
add_action( 'after_setup_theme', 'incredibuild_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function incredibuild_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'incredibuild' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'incredibuild' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widgets', 'incredibuild' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'incredibuild' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s col-md-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );
}
add_action( 'widgets_init', 'incredibuild_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function incredibuild_scripts() {
	wp_enqueue_style( 'incredibuild-style', get_stylesheet_uri(), array(), strtotime("now") );
	wp_enqueue_style('slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css', array(), false);
	wp_enqueue_script('jquery', false, array(), false, false);
	wp_enqueue_script('custom', get_template_directory_uri()."/assets/js/custom.js", array('jquery'), false, true);
    wp_enqueue_script('slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', array('jquery'), false, true);
	// wp_enqueue_script( 'incredibuild-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	// wp_enqueue_script( 'incredibuild-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'incredibuild_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}


// Register Custom Post Type Awards

add_shortcode('wpbsearch', 'get_search_form');

function custom_post_type_awards() {

	$labels = array(
		'name'                  => _x( 'Awards', 'Post Type General Name', 'incredibuild' ),
		'singular_name'         => _x( 'Award', 'Post Type Singular Name', 'incredibuild' ),
		'menu_name'             => __( 'Awards', 'incredibuild' ),
		'name_admin_bar'        => __( 'Award', 'incredibuild' ),
		'archives'              => __( 'Item Archives', 'incredibuild' ),
		'attributes'            => __( 'Item Attributes', 'incredibuild' ),
		'parent_item_colon'     => __( 'Parent Item:', 'incredibuild' ),
		'all_items'             => __( 'All Items', 'incredibuild' ),
		'add_new_item'          => __( 'Add New Item', 'incredibuild' ),
		'add_new'               => __( 'Add New', 'incredibuild' ),
		'new_item'              => __( 'New Item', 'incredibuild' ),
		'edit_item'             => __( 'Edit Item', 'incredibuild' ),
		'update_item'           => __( 'Update Item', 'incredibuild' ),
		'view_item'             => __( 'View Item', 'incredibuild' ),
		'view_items'            => __( 'View Items', 'incredibuild' ),
		'search_items'          => __( 'Search Item', 'incredibuild' ),
		'not_found'             => __( 'Not found', 'incredibuild' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'incredibuild' ),
		'featured_image'        => __( 'Featured Image', 'incredibuild' ),
		'set_featured_image'    => __( 'Set featured image', 'incredibuild' ),
		'remove_featured_image' => __( 'Remove featured image', 'incredibuild' ),
		'use_featured_image'    => __( 'Use as featured image', 'incredibuild' ),
		'insert_into_item'      => __( 'Insert into item', 'incredibuild' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'incredibuild' ),
		'items_list'            => __( 'Items list', 'incredibuild' ),
		'items_list_navigation' => __( 'Items list navigation', 'incredibuild' ),
		'filter_items_list'     => __( 'Filter items list', 'incredibuild' ),
	);
	$args = array(
		'label'                 => __( 'Award', 'incredibuild' ),
		'description'           => __( 'Post Type Description', 'incredibuild' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'award', $args );

}
add_action( 'init', 'custom_post_type_awards', 0 );


// Register Custom Post Type Dev Hacks
function custom_post_type_dev_hacks() {

	$labels = array(
		'name'                  => _x( 'Dev Hacks', 'Post Type General Name', 'incredibuild' ),
		'singular_name'         => _x( 'Dev Hack', 'Post Type Singular Name', 'incredibuild' ),
		'menu_name'             => __( 'Dev Hacks', 'incredibuild' ),
		'name_admin_bar'        => __( 'Dev Hack', 'incredibuild' ),
		'archives'              => __( 'Item Archives', 'incredibuild' ),
		'attributes'            => __( 'Item Attributes', 'incredibuild' ),
		'parent_item_colon'     => __( 'Parent Item:', 'incredibuild' ),
		'all_items'             => __( 'All Items', 'incredibuild' ),
		'add_new_item'          => __( 'Add New Item', 'incredibuild' ),
		'add_new'               => __( 'Add New', 'incredibuild' ),
		'new_item'              => __( 'New Item', 'incredibuild' ),
		'edit_item'             => __( 'Edit Item', 'incredibuild' ),
		'update_item'           => __( 'Update Item', 'incredibuild' ),
		'view_item'             => __( 'View Item', 'incredibuild' ),
		'view_items'            => __( 'View Items', 'incredibuild' ),
		'search_items'          => __( 'Search Item', 'incredibuild' ),
		'not_found'             => __( 'Not found', 'incredibuild' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'incredibuild' ),
		'featured_image'        => __( 'Featured Image', 'incredibuild' ),
		'set_featured_image'    => __( 'Set featured image', 'incredibuild' ),
		'remove_featured_image' => __( 'Remove featured image', 'incredibuild' ),
		'use_featured_image'    => __( 'Use as featured image', 'incredibuild' ),
		'insert_into_item'      => __( 'Insert into item', 'incredibuild' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'incredibuild' ),
		'items_list'            => __( 'Items list', 'incredibuild' ),
		'items_list_navigation' => __( 'Items list navigation', 'incredibuild' ),
		'filter_items_list'     => __( 'Filter items list', 'incredibuild' ),
	);
	$args = array(
		'label'                 => __( 'Dev Hack', 'incredibuild' ),
		'description'           => __( 'Post Type Description', 'incredibuild' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'dev_hack', $args );

}
add_action( 'init', 'custom_post_type_dev_hacks', 0 );


// Register Custom Post Type Testimonials
function custom_post_type_testimonials() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'incredibuild' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'incredibuild' ),
		'menu_name'             => __( 'Testimonials', 'incredibuild' ),
		'name_admin_bar'        => __( 'Testimonial', 'incredibuild' ),
		'archives'              => __( 'Item Archives', 'incredibuild' ),
		'attributes'            => __( 'Item Attributes', 'incredibuild' ),
		'parent_item_colon'     => __( 'Parent Item:', 'incredibuild' ),
		'all_items'             => __( 'All Items', 'incredibuild' ),
		'add_new_item'          => __( 'Add New Item', 'incredibuild' ),
		'add_new'               => __( 'Add New', 'incredibuild' ),
		'new_item'              => __( 'New Item', 'incredibuild' ),
		'edit_item'             => __( 'Edit Item', 'incredibuild' ),
		'update_item'           => __( 'Update Item', 'incredibuild' ),
		'view_item'             => __( 'View Item', 'incredibuild' ),
		'view_items'            => __( 'View Items', 'incredibuild' ),
		'search_items'          => __( 'Search Item', 'incredibuild' ),
		'not_found'             => __( 'Not found', 'incredibuild' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'incredibuild' ),
		'featured_image'        => __( 'Featured Image', 'incredibuild' ),
		'set_featured_image'    => __( 'Set featured image', 'incredibuild' ),
		'remove_featured_image' => __( 'Remove featured image', 'incredibuild' ),
		'use_featured_image'    => __( 'Use as featured image', 'incredibuild' ),
		'insert_into_item'      => __( 'Insert into item', 'incredibuild' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'incredibuild' ),
		'items_list'            => __( 'Items list', 'incredibuild' ),
		'items_list_navigation' => __( 'Items list navigation', 'incredibuild' ),
		'filter_items_list'     => __( 'Filter items list', 'incredibuild' ),
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'incredibuild' ),
		'description'           => __( 'Post Type Description', 'incredibuild' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'testimonial', $args );

}
add_action( 'init', 'custom_post_type_testimonials', 0 );


// Register Custom Post Type Case Studies
function custom_post_type_case_studies() {

	$labels = array(
		'name'                  => _x( 'Case Studies', 'Post Type General Name', 'incredibuild' ),
		'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'incredibuild' ),
		'menu_name'             => __( 'Case Studies', 'incredibuild' ),
		'name_admin_bar'        => __( 'Case Study', 'incredibuild' ),
		'archives'              => __( 'Item Archives', 'incredibuild' ),
		'attributes'            => __( 'Item Attributes', 'incredibuild' ),
		'parent_item_colon'     => __( 'Parent Item:', 'incredibuild' ),
		'all_items'             => __( 'All Items', 'incredibuild' ),
		'add_new_item'          => __( 'Add New Item', 'incredibuild' ),
		'add_new'               => __( 'Add New', 'incredibuild' ),
		'new_item'              => __( 'New Item', 'incredibuild' ),
		'edit_item'             => __( 'Edit Item', 'incredibuild' ),
		'update_item'           => __( 'Update Item', 'incredibuild' ),
		'view_item'             => __( 'View Item', 'incredibuild' ),
		'view_items'            => __( 'View Items', 'incredibuild' ),
		'search_items'          => __( 'Search Item', 'incredibuild' ),
		'not_found'             => __( 'Not found', 'incredibuild' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'incredibuild' ),
		'featured_image'        => __( 'Featured Image', 'incredibuild' ),
		'set_featured_image'    => __( 'Set featured image', 'incredibuild' ),
		'remove_featured_image' => __( 'Remove featured image', 'incredibuild' ),
		'use_featured_image'    => __( 'Use as featured image', 'incredibuild' ),
		'insert_into_item'      => __( 'Insert into item', 'incredibuild' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'incredibuild' ),
		'items_list'            => __( 'Items list', 'incredibuild' ),
		'items_list_navigation' => __( 'Items list navigation', 'incredibuild' ),
		'filter_items_list'     => __( 'Filter items list', 'incredibuild' ),
	);
	$args = array(
		'label'                 => __( 'Case Study', 'incredibuild' ),
		'description'           => __( 'Post Type Description', 'incredibuild' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'case_study', $args );

}
add_action( 'init', 'custom_post_type_case_studies', 0 );

/**
 * WP Bootstrap Navwalker
 *
 * @package WP-Bootstrap-Navwalker
 */

/**
 * Class Name: WP_Bootstrap_Navwalker
 * Plugin Name: WP Bootstrap Navwalker
 * Plugin URI:  https://github.com/wp-bootstrap/wp-bootstrap-navwalker
 * Description: A custom WordPress nav walker class to implement the Bootstrap 3 navigation style in a custom theme using the WordPress built in menu manager.
 * Author: Edward McIntyre - @twittem, WP Bootstrap
 * Version: 2.0.5
 * Author URI: https://github.com/wp-bootstrap
 * GitHub Plugin URI: https://github.com/wp-bootstrap/wp-bootstrap-navwalker
 * GitHub Branch: master
 * License: GPL-3.0+
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 */

/* Check if Class Exists. */
if ( ! class_exists( 'WP_Bootstrap_Navwalker' ) ) {
	/**
	 * WP_Bootstrap_Navwalker class.
	 *
	 * @extends Walker_Nav_Menu
	 */
	class WP_Bootstrap_Navwalker extends Walker_Nav_Menu {

		/**
		 * Start Level.
		 *
		 * @see Walker::start_lvl()
		 * @since 3.0.0
		 *
		 * @access public
		 * @param mixed $output Passed by reference. Used to append additional content.
		 * @param int   $depth (default: 0) Depth of page. Used for padding.
		 * @param array $args (default: array()) Arguments.
		 * @return void
		 */
		public function start_lvl( &$output, $depth = 0, $args = array() ) {
			$indent = str_repeat( "\t", $depth );
			$output .= "\n$indent<ul role=\"menu\" class=\" dropdown-menu\" >\n";
		}

		/**
		 * Start El.
		 *
		 * @see Walker::start_el()
		 * @since 3.0.0
		 *
		 * @access public
		 * @param mixed $output Passed by reference. Used to append additional content.
		 * @param mixed $item Menu item data object.
		 * @param int   $depth (default: 0) Depth of menu item. Used for padding.
		 * @param array $args (default: array()) Arguments.
		 * @param int   $id (default: 0) Menu item ID.
		 * @return void
		 */
		public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

			/**
			 * Dividers, Headers or Disabled
			 * =============================
			 * Determine whether the item is a Divider, Header, Disabled or regular
			 * menu item. To prevent errors we use the strcasecmp() function to so a
			 * comparison that is not case sensitive. The strcasecmp() function returns
			 * a 0 if the strings are equal.
			 */
			if ( 0 === strcasecmp( $item->attr_title, 'divider' ) && 1 === $depth ) {
				$output .= $indent . '<li role="presentation" class="divider">';
			} elseif ( 0 === strcasecmp( $item->title, 'divider' ) && 1 === $depth ) {
				$output .= $indent . '<li role="presentation" class="divider">';
			} elseif ( 0 === strcasecmp( $item->attr_title, 'dropdown-header' ) && 1 === $depth ) {
				$output .= $indent . '<li role="presentation" class="dropdown-header">' . esc_attr( $item->title );
			} elseif ( 0 === strcasecmp( $item->attr_title, 'disabled' ) ) {
				$output .= $indent . '<li role="presentation" class="disabled"><a href="#">' . esc_attr( $item->title ) . '</a>';
			} else {
				$value = '';
				$class_names = $value;
				$classes = empty( $item->classes ) ? array() : (array) $item->classes;
				$classes[] = 'menu-item-' . $item->ID;
				$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
				if ( $args->has_children ) {
					$class_names .= ' dropdown';
				}
				if ( in_array( 'current-menu-item', $classes, true ) ) {
					$class_names .= ' active';
				}
				$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
				$id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args );
				$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
				$output .= $indent . '<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement"' . $id . $value . $class_names . '>';
				$atts = array();

				if ( empty( $item->attr_title ) ) {
					$atts['title']  = ! empty( $item->title )   ? strip_tags( $item->title ) : '';
				} else {
					$atts['title'] = $item->attr_title;
				}

				$atts['target'] = ! empty( $item->target ) ? $item->target : '';
				$atts['rel']    = ! empty( $item->xfn )    ? $item->xfn    : '';
				// If item has_children add atts to a.
				if ( $args->has_children && 0 === $depth ) {
					$atts['href']           = '#';
					$atts['data-toggle']    = 'dropdown';
					$atts['class']          = 'dropdown-toggle';
					$atts['aria-haspopup']  = 'true';
				} else {
					$atts['href'] = ! empty( $item->url ) ? $item->url : '';
				}
				$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
				$attributes = '';
				foreach ( $atts as $attr => $value ) {
					if ( ! empty( $value ) ) {
						$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
						$attributes .= ' ' . $attr . '="' . $value . '"';
					}
				}
				$item_output = $args->before;

				/*
				 * Glyphicons/Font-Awesome
				 * ===========
				 * Since the the menu item is NOT a Divider or Header we check the see
				 * if there is a value in the attr_title property. If the attr_title
				 * property is NOT null we apply it as the class name for the glyphicon.
				 */
				if ( ! empty( $item->attr_title ) ) {
					$pos = strpos( esc_attr( $item->attr_title ), 'glyphicon' );
					if ( false !== $pos ) {
						$item_output .= '<a' . $attributes . '><span class="glyphicon ' . esc_attr( $item->attr_title ) . '" aria-hidden="true"></span>&nbsp;';
					} else {
						$item_output .= '<a' . $attributes . '><i class="fa ' . esc_attr( $item->attr_title ) . '" aria-hidden="true"></i>&nbsp;';
					}
				} else {
					$item_output .= '<a' . $attributes . '>';
				}
				$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
				$item_output .= ( $args->has_children && 0 === $depth ) ? ' <span class="caret"></span></a>' : '</a>';
				$item_output .= $args->after;
				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
			} // End if().
		}

		/**
		 * Traverse elements to create list from elements.
		 *
		 * Display one element if the element doesn't have any children otherwise,
		 * display the element and its children. Will only traverse up to the max
		 * depth and no ignore elements under that depth.
		 *
		 * This method shouldn't be called directly, use the walk() method instead.
		 *
		 * @see Walker::start_el()
		 * @since 2.5.0
		 *
		 * @access public
		 * @param mixed $element Data object.
		 * @param mixed $children_elements List of elements to continue traversing.
		 * @param mixed $max_depth Max depth to traverse.
		 * @param mixed $depth Depth of current element.
		 * @param mixed $args Arguments.
		 * @param mixed $output Passed by reference. Used to append additional content.
		 * @return null Null on failure with no changes to parameters.
		 */
		public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
			if ( ! $element ) {
				return; }
			$id_field = $this->db_fields['id'];
			// Display this element.
			if ( is_object( $args[0] ) ) {
				$args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] ); }
			parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
		}

		/**
		 * Menu Fallback
		 * =============
		 * If this function is assigned to the wp_nav_menu's fallback_cb variable
		 * and a menu has not been assigned to the theme location in the WordPress
		 * menu manager the function with display nothing to a non-logged in user,
		 * and will add a link to the WordPress menu manager if logged in as an admin.
		 *
		 * @param array $args passed from the wp_nav_menu function.
		 */
		public static function fallback( $args ) {
			if ( current_user_can( 'edit_theme_options' ) ) {

				/* Get Arguments. */
				$container = $args['container'];
				$container_id = $args['container_id'];
				$container_class = $args['container_class'];
				$menu_class = $args['menu_class'];
				$menu_id = $args['menu_id'];

				if ( $container ) {
					echo '<' . esc_attr( $container );
					if ( $container_id ) {
						echo ' id="' . esc_attr( $container_id ) . '"';
					}
					if ( $container_class ) {
						echo ' class="' . sanitize_html_class( $container_class ) . '"'; }
					echo '>';
				}
				echo '<ul';
				if ( $menu_id ) {
					echo ' id="' . esc_attr( $menu_id ) . '"'; }
				if ( $menu_class ) {
					echo ' class="' . esc_attr( $menu_class ) . '"'; }
				echo '>';
				echo '<li><a href="' . esc_url( admin_url( 'nav-menus.php' ) ) . '" title="">' . esc_attr( 'Add a menu', '' ) . '</a></li>';
				echo '</ul>';
				if ( $container ) {
					echo '</' . esc_attr( $container ) . '>'; }
			}
		}
	}
} // End if().

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="button inverse orange-btn"';
}

function get_custom_excerpt($limit, $source = null){
    if($source == "content" ? ($excerpt = get_the_content()) : ($excerpt = get_the_excerpt()));
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $limit);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt = $excerpt.'...';
    return $excerpt;
}