<?php
/**
 * Template Name: Visual Studio Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

    <style type="text/css">
        .curve.bottom-left-top-right:after {
            background-color: #eee;
        }
        .visual-content {
            padding-top: 0px;
        }
    </style>
<?php while ( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'template-parts/template-part', 'header' ); ?>
    
    <section class="visual-content">
        <div style="background-color: #eee;" class="pt-25">
            <div class="container">
                <div class="justify_custom col-md-12 pb-30 aos-item" data-aos="fade-up">
                  <?php echo get_field('first_fold_top_content'); ?>
                </div>
            </div>
        </div>
        <div class="container mt-50">
            <div class="row pt-30">
                <div class="col-md-6 aos-item" data-aos="fade-right">
                    <figure>
                        <img class="img-responsive" src="<?php the_field('first_fold_image'); ?>">
                    </figure>
                </div>
                <div class="justify_custom col-md-6 aos-item" data-aos="fade-left">
                    <h6><?php the_field('first_fold_title'); ?></h6>
                  <?php the_field('first_fold_content'); ?>
                </div>
            </div>

        </div>
    </section>
    <section class="parallax-curve" style="background-image: url(<?php the_field('second_fold_background'); ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="justify_custom col-md-10 aos-item" data-aos="fade-up">
                  <?php the_field('second_fold_content'); ?>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-8 col-md-offset-2">
                </div>
            </div>
        </div>
    </section>
                <?php if(is_page('testing')) { ?>
                 <div class="videoWrapper"> 
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/iOdw9wL8O2E" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
                <?php } ?>
  <?php if((get_field('third_fold_content') && get_field('third_fold_image')) || get_field('third_fold_stats_content')):?>
        <section class="our-work mb-20 mt-50"> 
            <div class="container">
              <?php if(get_field('third_fold_content') && get_field('third_fold_image')):?>
                  <div class="row">
                      <div class="justify_custom col-md-6 mt-50 aos-item" data-aos="fade-right">
                        <?php the_field('third_fold_content'); ?>
                      </div>
                      <div class="col-md-6 aos-item" data-aos="fade-left">
                          <figure>
                              <img src="<?php the_field('third_fold_image'); ?>" class="img-responsive">
                          </figure>
                      </div>
                  </div>
              <?php endif;?>
              <?php if(get_field('third_fold_stats_content')):?>
                  <div class="row mt-50">
                      <div class="col-md-4 pt-5">
                          <div class="progressbar aos-item" data-aos="fade-right">
                              <!-- <div class="progress-text-top">Reduce project <br>build time by up to</div>
                    <li data-name="" data-percent="90%">
                        <svg viewBox="-10 -10 220 220">
                        <g fill="none" stroke-width="8" transform="translate(100,100)">
                        <path d="M 0,-100 A 100,100 0 0,1 86.6,-50" stroke="url(#cl1)"/>
                        <path d="M 86.6,-50 A 100,100 0 0,1 86.6,50" stroke="url(#cl2)"/>
                        <path d="M 86.6,50 A 100,100 0 0,1 0,100" stroke="url(#cl3)"/>
                        <path d="M 0,100 A 100,100 0 0,1 -86.6,50" stroke="url(#cl4)"/>
                        <path d="M -86.6,50 A 100,100 0 0,1 -86.6,-50" stroke="url(#cl5)"/>
                        <path d="M -86.6,-50 A 100,100 0 0,1 0,-100" stroke="url(#cl6)"/>
                        </g>
                        </svg>
                        <svg viewBox="-10 -10 220 220">
                        <path d="M200,100 C200,44.771525 155.228475,0 100,0 C44.771525,0 0,44.771525 0,100 C0,155.228475 44.771525,200 100,200 C155.228475,200 200,155.228475 200,100 Z" stroke-dashoffset="550"></path>
                        </svg>
                    </li>
                      <div class="progress-text">or more</div> -->
                              <div class="thirdDiv" id="counter">
                                  <div id="circle1">
                                      <div class="progress-text-top">Reduce project <br>build time by up to</div>
                                      <div class="circle-overlay"></div>
                                      <p class="counter-data">
                                          <span class="counter-value circle-counter" data-count="90">0</span>%
                                      </p>
                                      <div class="progress-text">or more</div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="justify_custom col-md-8 mt-30 aos-item" data-aos="fade-left">
                        <?php the_field('third_fold_stats_content'); ?>
                      </div>
                  </div>
              <?php endif;?>
            </div>
        </section>
  <?php endif;?>
  <?php if(get_field('fourth_fold_content') && get_field('fourth_fold_image')):?>
        <section class="visual curve curve-left-bottom-right-top">
            <figure class="visual-texture">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/visual-texture.png">
            </figure>
            <div class="container">
                <div class="row">
                    <div class="justify_custom col-md-6 pr-30 aos-item" data-aos="fade-right">
                      <?php the_field('fourth_fold_content'); ?>
                    </div>
                    <div class="col-md-6 pl-30 aos-item" data-aos="fade-left">
                        <figure>
                            <img src="<?php the_field('fourth_fold_image'); ?>" class="img-responsive">
                        </figure>
                    </div>
                </div>
            </div>
        </section>
  <?php endif;?>

    <section class="testimonial pt-15 pb-40 text-center aos-item" data-aos="fade-in">
      <?php get_template_part( 'template-parts/template-part', 'testimonials' ); ?>
    </section>

    <section class="benefits mb-30" style="background-image: url(<?php the_field('fifth_fold_background'); ?>);">
        <div class="container">
            <h3 class="border-gradient aos-item" data-aos="fade-up"><?php the_field('fifth_fold_title'); ?></h3>
            <div class="row">
              <?php if( have_rows('fifth_fold_content') ): while ( have_rows('fifth_fold_content') ) : the_row(); ?>
                  <div class="col-md-4 aos-item" data-aos="fade-up" data-aos-duration="1500">
                      <h4><?php the_sub_field('title'); ?></h4>
                      <p class="justify_custom"><?php the_sub_field('text'); ?></p>
                  </div>
              <?php endwhile; endif; ?>
            </div>
        </div>
    </section>

    <section class="gradient gradient-curve trust trust-texture text-center aos-item" data-aos="zoom-out">
      <?php get_template_part( 'template-parts/template-part', 'gradient-section' ); ?>
    </section>

    <svg style="position:absolute;" width="0" height="0">
        <defs>
            <linearGradient id="cl1" gradientUnits="objectBoundingBox" x1="0" y1="0" x2="1" y2="1">
                <stop stop-color="#e4177b"/>
                <stop offset="100%" stop-color="#e4177b"/>
            </linearGradient>
            <linearGradient id="cl2" gradientUnits="objectBoundingBox" x1="0" y1="0" x2="0" y2="1">
                <stop stop-color="#e4177b"/>
                <stop offset="100%" stop-color="#e4177b"/>
            </linearGradient>
            <linearGradient id="cl3" gradientUnits="objectBoundingBox" x1="1" y1="0" x2="0" y2="1">
                <stop stop-color="#e4177b"/>
                <stop offset="100%" stop-color="#fede11"/>
            </linearGradient>
            <linearGradient id="cl4" gradientUnits="objectBoundingBox" x1="1" y1="1" x2="0" y2="0">
                <stop stop-color="#fede11"/>
                <stop offset="100%" stop-color="#fede11"/>
            </linearGradient>
            <linearGradient id="cl5" gradientUnits="objectBoundingBox" x1="0" y1="1" x2="0" y2="0">
                <stop stop-color="#fede11"/>
                <stop offset="100%" stop-color="#fede11"/>
            </linearGradient>
            <linearGradient id="cl6" gradientUnits="objectBoundingBox" x1="0" y1="1" x2="1" y2="0">
                <stop stop-color="#fede11"/>
                <stop offset="100%" stop-color="#e4177b"/>
            </linearGradient>
        </defs>
    </svg>


<?php endwhile; ?>

<?php
get_footer();
