<?php
/**
 * Template Name: Support Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="visual-content support">
		<div class="container aos-item" data-aos="fade-up">
			<div class="col-md-12 pb-50">
				<h4><?php the_field('first_fold_content'); ?></h4>
			</div>
			<div class="col-md-12 text-center">
				<h4><?php the_field('second_fold_title'); ?></h4>
				<?php the_field('second_fold_content'); ?>
			</div>
			<div class="row text-center">
				<?php if( have_rows('second_fold_boxes') ):
					$x=1;
				    while ( have_rows('second_fold_boxes') ) : the_row(); ?>
					<div class="col-md-4 aos-item" data-aos="fade-up">
						<div class="border-box">
							<?php if (get_sub_field('image')): ?>
								<div class="icon">
									<figure>
										<img src="<?php the_sub_field('image'); ?>">
									</figure>
								</div>
							<?php endif ?>
							<p><?php the_sub_field('title'); ?></p>
							<h6><?php the_sub_field('content'); ?></h6>
							<?php 
								$btnAc = ' target="_blank"';
								if (strpos(get_sub_field('button_link'), get_bloginfo('url')) !== false) {
								    $btnAc = '';
								}
							?>
							<a href="<?php the_sub_field('button_link'); ?>" <?php echo $btnAc; ?> class="button inverse orange-btn"><?php the_sub_field('button_text'); ?></a>
						</div>
					</div>
				    <?php if ($x == 3) { ?> 
					</div><div class="row text-center">
				    <?php } ?>
				<?php $x++; endwhile; endif; ?>
			</div>
			<?php the_field('second_fold_bottom_content'); ?>
		</div>
	</section>



<?php endwhile; ?>

<?php
get_footer();
