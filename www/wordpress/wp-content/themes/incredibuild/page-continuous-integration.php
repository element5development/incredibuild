<?php
/**
 * Template Name: Continuous Integration Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php get_template_part( 'template-parts/template-part', 'header' ); ?>
    
	<section class="integration text-center">
		<div class="container">
			<div class="col-md-12  aos-item" data-aos="fade-up">
				<h5><?php the_field('first_fold_title'); ?></h5>
			</div>
		</div>
	</section>
	<section class="integration-content">
		<figure class="int-texture">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/int-texture.png">
		</figure>
		<div class="container">
			<?php
			if( have_rows('first_fold_timeline') ):
				$x=1;
			    while ( have_rows('first_fold_timeline') ) : the_row(); ?>
			    <?php if ($x%2) { ?>
					<div class="row">
						<div class="col-sm-6 pr-70 border-right pt-65">
							<?php if (get_sub_field('image')): ?>
								<figure class="mt-30">
									<img src="<?php echo get_sub_field('image'); ?>" class="img-responsive">
								</figure>
							<?php endif ?>
						</div>
						<div class="col-sm-6 pl-70 border-left pt-65">
							<div class="aos-item" data-aos="fade-left">
								<h6 class="right"><?php echo get_sub_field('title'); ?></h6>
								<p class="justify_custom"><?php echo get_sub_field('content'); ?> </p>
							</div>
						</div>
					</div>
			    <?php } else { ?>
					<div class="row">
						<div class="col-sm-6 pr-70 border-right pt-65 text-right">
							<div class="aos-item" data-aos="fade-right">
								<h6 class="left"><?php echo get_sub_field('title'); ?></h6>
								<p><?php echo get_sub_field('content'); ?></p>
							</div>
						</div>
						<div class="col-sm-6 pl-70 border-left pt-65">
							<div class="aos-item" data-aos="fade-left">
								<?php if (get_sub_field('image')): ?>
									<figure class="mt-30">
										<img src="<?php echo get_sub_field('image'); ?>" class="img-responsive">
									</figure>
								<?php endif ?>
							</div>
						</div>
					</div>
			    <?php } ?>
			<?php $x++; endwhile;
			endif;
			?>
		</div>
	</section>
	<section class="delivery-time">
		<div class="container">
			<div class="thirdDiv clearfix" id="counter">
				<div class="row aos-item" data-aos="zoom-in">
					<div class="col-md-4 mobile">
						<p><strong>Traditional Continuous Delivery</strong></p>
						<h3><span class="counter-value" data-count="10" style="color: rgba(45, 49, 53, 0.4);">0</span>min</h3>
						<small>running a single unit</small>
					</div>
					<div class="col-md-8">
						<div class="progressbar-delivery">
						    <div id="circle6">
								<div class="circle-overlay-integration"></div>
								<p class="counter-data">
									<span class=" circle-counter">2:30</span>
								</p>
								<div class="circle-title">Compilation</div>
							</div>
						    <div id="circle7">
								<div class="circle-overlay-integration"></div>
								<p class="counter-data">
									<span class=" circle-counter">2:30</span>
								</p>
								<div class="circle-title">Testing</div>
							</div>
						    <div id="circle8">
								<div class="circle-overlay-integration"></div>
								<p class="counter-data">
									<span class=" circle-counter">2:10</span>
								</p>
								<div class="circle-title">Data Conversion</div>
							</div>
						    <div id="circle9">
								<div class="circle-overlay-integration"></div>
								<p class="counter-data">
									<span class=" circle-counter">2:50</span>
								</p>
								<div class="circle-title">Code Analysis</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 desktop">
						<p><strong>Traditional Continuous Delivery</strong></p>
						<h3><span class="counter-value" data-count="10" style="color: rgba(45, 49, 53, 0.4);">0</span>min</h3>
						<small>running a single unit</small>
					</div>
				</div>
				<div class="row aos-item" data-aos="zoom-in">
					<div class="col-md-4 mobile">
						<p><strong>Continuous Delivery with <span>IncrediBuild</span></strong></p>
						<h2><span class="counter-value" data-count="80">0</span>%</h2>
						<p>reduction in time</p>
					</div>
					<div class="col-md-8">
						<div class="progressbar-delivery reduce-time">
							<div id="circle10">
								<div class="circle-overlay-integration"></div>
								<p class="counter-data">
									<span class=" circle-counter">0:30</span>
								</p>
							</div>
						    <div id="circle11">
								<div class="circle-overlay-integration"></div>
								<p class="counter-data">
									<span class=" circle-counter">0:30</span>
								</p>
							</div>
						    <div id="circle12">
								<div class="circle-overlay-integration"></div>
								<p class="counter-data">
									<span class=" circle-counter">0:10</span>
								</p>
							</div>
						    <div id="circle13">
								<div class="circle-overlay-integration"></div>
								<p class="counter-data">
									<span class=" circle-counter">0:50</span>
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 desktop">
						<p><strong>Continuous Delivery with <span>IncrediBuild</span></strong></p>
						<h2><span class="counter-value" data-count="80">0</span>%</h2>
						<p>reduction in time</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="benefits-content">
		<div class="container">
			<h3 class="text-center"><?php the_field('third_fold_title'); ?></h3>
			<?php
			if( have_rows('third_fold_timeline') ):
				$x=1;
			    while ( have_rows('third_fold_timeline') ) : the_row(); ?>
			    <?php if ($x%2) { ?>
					<div class="row aos-item" data-aos="fade-down">
						<div class="col-md-5">
							<figure>
								<img src="<?php echo get_sub_field('image'); ?>" class="img-responsive">
							</figure>
						</div>
						<div class="col-md-7">
							<div class=" aos-item" data-aos="fade-right">
								<h5><?php echo get_sub_field('title'); ?></h5>
								<p class="justify_custom"><?php echo get_sub_field('content'); ?></p>
							</div>
						</div>
					</div>
			    <?php } else { ?>
					<div class="row aos-item" data-aos="fade-up">
						<div class="col-md-5 mobile">
							<figure>
								<img src="<?php echo get_sub_field('image'); ?>" class="img-responsive">
							</figure>
						</div>
						<div class="col-md-7">
							<div class=" aos-item" data-aos="fade-left">
								<h5><?php echo get_sub_field('title'); ?></h5>
								<p><?php echo get_sub_field('content'); ?></p>
							</div>
						</div>
						<div class="col-md-5 desktop">
							<figure>
								<img src="<?php echo get_sub_field('image'); ?>" class="img-responsive">
							</figure>
						</div>
					</div>
			    <?php } ?>
			<?php $x++; endwhile;
			endif;
			?>
		</div>
	</section>
	<?php
       if($_SERVER['REQUEST_URI'] != '/jenkins/' && $_SERVER['REQUEST_URI'] != '/jenkins' && $_SERVER['REQUEST_URI'] != '/tfs/' && $_SERVER['REQUEST_URI'] != '/tfs') { 
    ?>
	<section class="separator right-top">
        <div class="separator-line"></div>
    </section>
    <?php
       }
    ?>
	<section class="testimonial mb-50 pb-40 text-center aos-item" data-aos="fade-in">
		<?php get_template_part( 'template-parts/template-part', 'testimonials' ); ?>
	</section>
	<section class="gradient gradient-curve trust trust-texture text-center aos-item" data-aos="zoom-out">

      <?php get_template_part( 'template-parts/template-part', 'gradient-section' ); ?>

	</section>



<?php endwhile; ?>

<?php
get_footer();
