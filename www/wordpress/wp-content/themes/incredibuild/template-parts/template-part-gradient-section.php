<?php

/* Section with Gradient background and snail background image */

if( (get_field('last_fold_title')) ) { ?>
        <div class="container">
            <figure class="texture">
                <img src="<?php bloginfo('template_url'); ?>/assets/images/texture.png" class="img-responsive">
            </figure>
            <div class="row" style="z-index: 99999999; position: relative;">
                <div class="col-md-12">
                    <div class="text">
                        <h3><?php the_field('last_fold_title'); ?></h3>
                        <a href="<?php the_field('last_fold_button_link'); ?>" class="button inverse"><?php the_field('last_fold_button_text'); ?></a>
                    </div>
                </div>
            </div>
        </div>
<?php } ?>
