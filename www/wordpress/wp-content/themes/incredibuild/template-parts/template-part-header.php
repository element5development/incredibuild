
<?php if (get_field('header_background_video_mp4') || get_field('header_background_video_webm')): ?>
    <section class="sub-banner curve bottom-left-top-right">
        <video id="bgVideo" class="video-bg" width="100%" autoplay loop muted id="banner-video">
            <source src="<?php echo get_field('header_background_video_webm'); ?>" type="video/webm">
            <source src="<?php echo get_field('header_background_video_mp4'); ?>" type="video/mp4">
            Your browser does not support HTML5 video.
        </video>
        <div class="gradient inverse overlay"></div>
        <div class="container text-center aos-item" data-aos="zoom-in"  data-aos-anchor=".sub-banner">
            <h1><?php echo get_field('header_title'); ?></h1>
          <?php if (get_field('header_sub_title')): ?>
              <p><?php echo get_field('header_sub_title');?></p>
          <?php endif ?>
        </div>
    </section>
  <script type="text/javascript">
    jQuery(document).ready(function(){
      if (iOS()) {
        jQuery('#bgVideo').remove();
        jQuery('.sub-banner').css('background', 'url(/wordpress/wp-content/uploads/2017/10/product.jpg)');
      };
    });
    function iOS() {

      var iDevices = [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
      ];

      if (!!navigator.platform) {
        while (iDevices.length) {
          if (navigator.platform === iDevices.pop()){ return true; }
        }
      }

      return false;
    }
  </script>
<?php else : ?>
  <section class="sub-banner curve bottom-left-top-right" <?php if (get_field('header_background')): ?> style="background-image: url(<?php echo get_field('header_background'); ?>)" <?php endif ?>>
    <div class="gradient inverse overlay"></div>
    <div class="container text-center aos-item" data-aos="zoom-in"  data-aos-anchor=".sub-banner">
      <h1><?php echo get_field('header_title'); ?></h1>
      <?php if (get_field('header_sub_title')): ?>
        <p><?php echo get_field('header_sub_title'); ?></p>
      <?php endif ?>
    </div>
  </section>
<?php endif; ?>