<?php if(count(get_field('testimonials_to_show')) > 0) { ?>
	<div class="container">
		<ul class="inner-testi-slider">
			<?php foreach (get_field('testimonials_to_show') as $key => $testimonial): ?>
				<li>
					<div class="text">
						<h5><?php echo $testimonial->post_content; ?></h5>
					</div>
					<?php if (get_the_post_thumbnail_url($testimonial->ID)): ?>
						<figure>
							<img src="<?php echo get_the_post_thumbnail_url($testimonial->ID); ?>">
						</figure>
					<?php endif ?>
					<p><strong><?php echo $testimonial->post_title; ?></strong>, <?php echo get_field('title', $testimonial->ID); ?>, <br><?php echo get_field('company', $testimonial->ID); ?></p>
				</li>
			<?php endforeach ?>
		</ul>
	</div>
<?php } else { ?>
		<div class="container">
			<div class="text">
				<h5>"We may be saving nearly 100 man hours per day using IncrediBuild on Visual Studio for Programmers alone"</h5>
			</div>
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/oval4.png">
			</figure>
			<p><strong>Javier Olivares</strong>, Lead Programmer, <br>Obsidian</p>
		</div>
<?php } ?>
