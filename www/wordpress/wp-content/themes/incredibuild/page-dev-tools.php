<?php
/**
 * Template Name: Dev Tools Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<section class="dev-banner" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/images/dev-banner.png');">
		<div class="dev-container">
			<div class="row text-center">
				<div class="dev-text">
					<div class="row">
						<div class="dev-heading col-sm-12">
							<h1 class="unik"><?php the_field('learn_more_heading'); ?></h1>
							<a href="#" data-toggle="modal" data-target="#learnModal" style="position: relative; z-index: 99999999999999999999; outline: none;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/plus-copy.png">Learn more</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 js-show-triangle-group" data-groupid="1">
							<div class="game js-show-triangle" data-triangle="game">
								<div class="hideOnHover">
									<figure class="text-center">
										<img src="<?php the_field('first_left_box_icon'); ?>" width="104">
									</figure>
									<p><?php the_field('first_left_box_title'); ?></p>
								</div>
							</div>
							<div class="qa js-show-triangle" data-triangle="qa">
								<div class="hideOnHover">
									<figure class="text-center">
										<img src="<?php the_field('second_left_box_icon'); ?>" width="104">
									</figure>
									<p><?php the_field('second_left_box_title'); ?></p>
								</div>
							</div>
							<div id="triangle-right"></div>
							<div class="game-hover">
								<div class="text triangleBoxes">
									<div>
										<figure>
											<img src="<?php the_field('first_left_box_icon_white'); ?>" width="104">
										</figure>
										<h6><?php the_field('first_left_box_title'); ?></h6>
										<p><?php the_field('first_left_box_content'); ?></p>
									</div>
								</div>
							</div>
							<div class="qa-hover">
								<div class="text triangleBoxes">
									<div>
										<figure>
											<img src="<?php the_field('second_left_box_icon_white'); ?>" width="104">
										</figure>
										<h6><?php the_field('second_left_box_title'); ?></h6>
										<p><?php the_field('second_left_box_content'); ?></p>
									</div>
								</div>
							</div>
							<div class="none-hover"></div>
						</div>
						<div class="col-md-6 js-show-triangle-group"  data-groupid="2">
							<div class="code js-show-triangle " data-triangle="code">
								<div class="hideOnHover">
									<figure class="text-center">
										<img src="<?php the_field('first_right_box_icon'); ?>" width="104">
									</figure>
									<p><?php the_field('first_right_box_title'); ?></p>
								</div>
							</div>
							<div class="house js-show-triangle" data-triangle="house">
								<div class="hideOnHover">
									<figure class="text-center">
										<img src="<?php the_field('second_right_box_icon'); ?>" width="104">
									</figure>
									<p><?php the_field('second_right_box_title'); ?></p>
								</div>
							</div>
							<div id="triangle-left"></div>
							<div class="code-hover">
								<div class="text triangleBoxes">
									<div>
										<figure>
											<img src="<?php the_field('first_right_box_icon_white'); ?>" width="104">
										</figure>
										<h6><?php the_field('first_right_box_title'); ?></h6>
										<p><?php the_field('first_right_box_content'); ?></p>
									</div>
								</div>
							</div>
							<div class="house-hover">
								<div class="text triangleBoxes">
									<div>
										<figure>
											<img src="<?php the_field('second_right_box_icon_white'); ?>" width="104">
										</figure>
										<h6><?php the_field('second_right_box_title'); ?></h6>
										<p><?php the_field('second_right_box_content'); ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="clearfix bottom-dev js-show-triangle-group"  data-groupid="3">
							<div class="col-md-3"></div>
							<div class="col-md-3">
								<div class="complex js-show-triangle" data-triangle="complex">
									<div class="hideOnHover">
										<figure><img src="<?php the_field('first_bottom_box_icon'); ?>" width="104"></figure>
										<p><?php the_field('first_bottom_box_title'); ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="slow js-show-triangle" data-triangle="slow">
									<div class="hideOnHover">
										<figure><img src="<?php the_field('second_bottom_box_icon'); ?>" width="104"></figure>
										<p><?php the_field('second_bottom_box_title'); ?></p>
									</div>
								</div>
							</div>
							<div id="triangle-top"></div>
							<div class="complex-hover">
								<div class="text triangleBoxes">
									<div>
										<figure>
											<img src="<?php the_field('first_bottom_box_icon_white'); ?>" width="104">
										</figure>
										<h6><?php the_field('first_bottom_box_title'); ?></h6>
										<p><?php the_field('first_bottom_box_content'); ?></p>
									</div>
								</div>
							</div>
							<div class="slow-hover">
								<div class="text triangleBoxes">
									<div>
										<figure>
											<img src="<?php the_field('second_bottom_box_icon_white'); ?>" width="104">
										</figure>
										<h6><?php the_field('second_bottom_box_title'); ?></h6>
										<p><?php the_field('second_bottom_box_content'); ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-3"></div>
							<div class="none-hover-bottom"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="flex" style="width: 98%; height: 100%; position: absolute; top: 0px; max-width: 1395px;">
				<div class="dev-logo">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/DEV-Logo.png" width="100%">
				</div>
			</div>
		</div>
	</section>
	<section class="gradient dev-gradient text-center">
		<div class="container aos-item" data-aos="fade-up">
			<?php if (get_field('second_fold_heading')): ?>
				<h4><?php the_field('second_fold_heading'); ?></h4>
			<?php endif ?>
			<div class="row">
				<div class="col-sm-6">
					<div class="text whiteAnchorHover">
						<div class="justify_custom">
							<?php the_field('second_fold_left_content'); ?>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="text whiteAnchorHover">
						<div class="justify_custom">
							<?php the_field('second_fold_right_content'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="dev-section">
		<div class="container">
			<div class="row">
				<div class="justify_custom col-md-12 aos-item" data-aos="fade-up">
					<?php the_field('third_fold_top_content'); ?>
				</div>
			</div>
		</div>
	</section>
	<section class="special">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 text-right aos-item" data-aos="fade-right">
					<ul>
						<?php if( have_rows('third_fold_names') ): ?>
							<?php while( have_rows('third_fold_names') ): the_row(); ?>
								<li><?php the_sub_field('name'); ?></li>
							<?php endwhile; ?>
						<?php endif; ?>
					</ul>
				</div>
				<div class="col-sm-4">
					<div class="special-logo aos-item" data-aos="zoom-in">
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/dev-logo.png">
						</figure>
					</div>
				</div>
				<div class="col-sm-4 dev-logos aos-item" data-aos="fade-left">
					<?php if( have_rows('third_fold_logo') ): ?>
						<?php while( have_rows('third_fold_logo') ): the_row(); ?>
							<figure>
								<img src="<?php the_sub_field('image'); ?>">
							</figure>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	<section class="videos-sec">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php the_field('third_fold_bottom_content'); ?>
					<div class="aos-item" data-aos="fade-left">
						<div class="video-sec dev-video text-center">
							<img class="video" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/vide.png">
							<div>
								<a href="javascript:;" data-toggle="modal" data-target="#myModal"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play.png"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="gradient gradient-curve trust trust-texture text-center mt-100 aos-item" data-aos="zoom-in" style="padding: 60px 0px;">
		<?php get_template_part( 'template-parts/template-part', 'gradient-section' ); ?>
	</section>

  <div class="modal fade" id="learnModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
      	<button type="button" class="close" data-dismiss="modal" style="z-index: 999; position: relative;padding: 10px">&times;</button>
        <div class="modal-body">
          <p><?php the_field('learn_more_content'); ?></p>
        </div>
      </div>
      
    </div>
  </div>



	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
                <div style="position:relative">
                    <button id="close_iframe">X</button>
                    <?php the_field('third_fold_video')?>
                </div>
			</div>
		</div>
	</div>

<?php endwhile; ?>

<script>
//custom for video on homepage
    jQuery('#close_iframe').on('click',function() {
        jQuery('#myModal.in').click()
        jQuery('#ytplayer').remove();
        jQuery(this).parent().append('<iframe id="ytplayer" src="https://www.youtube.com/embed/d66gKA0AsvM" width="100%" height="100%" frameborder="0" allowfullscreen="allowfullscreen"></iframe>');
    });
    jQuery('div[aria-labelledby="exampleModalLabel"]').on('click',function() {
        jQuery('#ytplayer').remove();
        jQuery('#close_iframe').parent().append('<iframe id="ytplayer" src="https://www.youtube.com/embed/d66gKA0AsvM" width="100%" height="100%" frameborder="0" allowfullscreen="allowfullscreen"></iframe>'); 
    });
</script>
<?php
get_footer();