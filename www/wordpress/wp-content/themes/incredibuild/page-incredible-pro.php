<?php
/**
 * Template Name: Incredible Pro Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="dev pt-50">
		<div class="container" style="z-index: 0">
			<?php if( have_rows('first_fold_content') ): ?>
				<?php while( have_rows('first_fold_content') ): the_row(); ?>
					<!-- <div class="row mb-100 aos-item videoPop" data-aos="fade-up" data-toggle="modal" data-target="#myModal" data-video="<?php the_sub_field('youtube_link'); ?>"> -->
					<div class="row mb-100 aos-item">
						<div class="col-md-4">
							<figure>
								<img src="<?php the_sub_field('image'); ?>" >
							</figure>
						</div>
						<div class="col-md-8">
							<h4><?php the_sub_field('title'); ?></h4>
							<p class="justify_custom"><?php the_sub_field('content'); ?></p>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
			<div class="row">
				<div class="col-md-12">	
					<h3 class="text-center aos-item" data-aos="fade-in"><?php the_field('second_fold_title'); ?></h3>
				</div>
			</div>
			<?php
			if( have_rows('second_fold_content') ):
				$x=1;
			    while ( have_rows('second_fold_content') ) : the_row(); ?>
			    <?php if ($x%2) { ?>
					<div class="row">
						<div class="col-md-7 aos-item" data-aos="fade-up">
							<figure>
								<img src="<?php the_sub_field('image'); ?>" class="img-responsive">
							</figure>
						</div>
						<div class="col-md-5 flex aos-item" data-aos="fade-right" style="min-height: 380px;">
							<div class="justify_custom">
								<?php the_sub_field('content'); ?>
							</div>
						</div>
					</div>
			    <?php } else { ?>
					<div class="row">
						<div class="col-md-5 flex aos-item" data-aos="fade-left" style="min-height: 380px;">
							<div>
								<?php the_sub_field('content'); ?>
							</div>
						</div>
						<div class="col-md-7 aos-item" data-aos="fade-up">
							<figure>
								<img src="<?php the_sub_field('image'); ?>" class="img-responsive">
							</figure>
						</div>
					</div>
			    <?php } ?>
			<?php $x++; endwhile;
			endif;
			?>
		</div>
	</section>
	<section class="gradient gradient-curve trust trust-texture text-center aos-item" data-aos="zoom-out">
		<div class="container">
            <!-- <figure class="texture" style="background-image: url(<?php //echo get_stylesheet_directory_uri(); ?>/assets/images/texture.png");">
            </figure> -->
            <figure class="texture">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/texture.png" class="img-responsive">
			</figure>
			<div class="row" style="position: relative; z-index: 999999999;">
				<div class="col-md-12">
					<div class="text">
						<h3><?php the_field('third_fold_title'); ?></h3>
						<a href="<?php the_field('third_fold_button_link'); ?>" class="button inverse"><?php the_field('third_fold_button_text'); ?></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="features">
		<div class="container">
			<div class="row">
				<h4 class="text-center pt-50">IncrediBuild Features</h4>
			</div>
			<div class="row">
				<?php
				if( have_rows('fourth_fold_content') ):
					$x=1;
				    while ( have_rows('fourth_fold_content') ) : the_row(); ?>
					<div class="col-md-4 aos-item" data-aos="fade-up">
						<figure class="icon">
							<img src="<?php the_sub_field('image'); ?>">
						</figure>
						<h6><?php the_sub_field('title'); ?></h6>
						<div style="text-align: justify;">
						<?php the_sub_field('content'); ?>
						</div>
					</div>
				    <?php if ($x == 3) { ?> 
					</div><div class="row">
				    <?php } ?>
				<?php $x++; endwhile;
				endif;
				?>
			</div>
		</div>
	</section>
	<section class="gradient pb-15 pt-25 gradient-inverse trust text-center save-time aos-item" data-aos="zoom-out">
        <div class="top-before"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="grid aos-item" data-aos="fade-in">

<?php 
  $temp = $wp_query; 
  $wp_query = null; 
  $wp_query = new WP_Query(); 
  $wp_query->query('p=864&post_type=case_study'.'&paged='.$paged); 

  while ($wp_query->have_posts()) : $wp_query->the_post(); 
?>
								<div class="grid-item" style="width: 100% !important; max-width: 304px;">
									<?php if (get_field('small_title')): ?>
									<div class="small-btn"><?php echo get_field('small_title'); ?></div>
									<?php else : ?>
									<div class="small-btn">game dev</div>
									<?php endif; ?>
									<figure>
										<?php if (get_the_post_thumbnail_url()) { ?>
											<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive">
										<?php } else { ?>
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/case1.png" class="img-responsive">
										<?php } ?>
										<div class="overlay">
											<div>
												<a href="<?php the_permalink(); ?>" class="button inverse">Read more</a>
											</div>
										</div>
									</figure>
									<div class="text-box" style="text-align: left">
										<h6><?php if (strlen(the_title('','',FALSE)) > 45) { //Character length
										          $title_short = substr(the_title('','',FALSE), 0, 45); // Character length
										          preg_match('/^(.*)\s/s', $title_short, $matches);
										      if ($matches[1]) $title_short = $matches[1];
										          $title_short = $title_short.' ...'; // Ellipsis
										      } else {
										          $title_short = the_title('','',FALSE);
										      } ?>

										<?php echo $title_short ?>
											
										</h6>
										<?php 
										$excerptNew = get_the_excerpt();
										$excerptNew = preg_replace(" ([.*?])",'',$excerptNew);
										$excerptNew = strip_shortcodes($excerptNew);
										$excerptNew = strip_tags($excerptNew);
										$excerptNew = substr($excerptNew, 0, 60);
										$excerptNew = substr($excerptNew, 0, strripos($excerptNew, " "));
										$excerptNew = trim(preg_replace( '/s+/', ' ', $excerptNew));
										$excerptNew = $excerptNew.'...';
										$excerptNew = $excerptNew.' <a href="'.get_the_permalink().'">continue reading</a>';
											// $excerptNew = explode(' ', get_the_excerpt(), 16);

											// if (count($excerptNew) >= 16) {
											//   array_pop($excerptNew);
											//   $excerptNew = implode(" ", $excerptNew) . '...';
											// } else {
											//   $excerptNew = implode(" ", $excerptNew);
											// }

											// $excerptNew = preg_replace('`\[[^\]]*\]`', '', $excerptNew);
										?>
										<p style="color: #2d3135;font-size: 16px;font-weight: 400;"><?php echo $excerptNew; ?></p>
									</div>
								</div>
<?php endwhile; ?>

<!-- <div class="row aos-item" data-aos="fade-up">
<div class="col-md-12 text-center">
<nav style="padding-top: 20px; ">
    <?php //previous_posts_link('&laquo; Newer') ?>
    <?php //next_posts_link('Older &raquo;') ?>
</nav>
</div>
</div> -->
						<?php	
  $wp_query = null; 
  $wp_query = $temp;  // Reset
				wp_reset_postdata();
					?>

			</div>

					<!-- <div class="box">
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/time.png" class="img-responsive">
						</figure>
						<p>The Coalition Transforms Azure VMs into a 700-Core IncrediBuild "Virtual Supercomputer", Releases 2 AAA Games in 1 Year</p>
					</div> -->
				</div>
				<div class="col-md-8 ml-50">
					<div class="text">
						<h3><?php the_field('last_fold_title'); ?></h3>
						<a href="<?php the_field('last_fold_button_link'); ?>" class="button inverse"><?php the_field('last_fold_button_text'); ?></a>
					</div>
				</div>
			</div>
		</div>
        <div class="bottom-after"></div>
	</section>


	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="">
					modal
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function getId(url) {
		    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
		    var match = url.match(regExp);

		    if (match && match[2].length == 11) {
		        return match[2];
		    } else {
		        return 'error';
		    }
		}

		jQuery(function($){
			$('.videoPop').click(function(){
				var videoId = getId($(this).attr('data-video'));

				var iframeMarkup = '<iframe width="100%" height="315" src="//www.youtube.com/embed/' 
				    + videoId + '" frameborder="0" allowfullscreen></iframe>';
				$('#myModal .modal-content').html(iframeMarkup);	
			})
		});
	</script>

<?php endwhile; ?>

<?php
get_footer();
