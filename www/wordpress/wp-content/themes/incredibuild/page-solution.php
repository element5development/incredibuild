<?php
/**
 * Template Name: Acceleration Worlds Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="solution-texts pt-50">
		<div class="container">
			<div class="col-md-12">
				<?php the_field('first_fold_content'); ?>
			</div>
		</div>
	</section>
	<section class="login2 solution-box mb-20">
		<div class="container">
			<div class="row">
				<div class="col-md-12 pt-50">
					<h4 class="text-center" style="color: #e3147c;"><?php the_field('boxes_section_title'); ?></h4>
				</div>
			</div>
			<div class="row pt-50">
				<?php if( have_rows('solutions_boxes') ): while ( have_rows('solutions_boxes') ) : the_row(); ?>
					<div class="col-md-4 aos-item" data-aos="fade-down">
						<a class="box" href='<?php the_sub_field('link'); ?>';" style="text-decoration: none;">
							<figure class="icon">
								<img src="<?php the_sub_field('image'); ?>">
							</figure>

							<span class="h4"> <?php the_sub_field('title'); ?> </span>
						</a>
					</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
	<section class="separator">
        <div class="separator-line"></div>
    </section>
	<section class="studies solution-grid">
		<div class="container">
			<div class="row">
				<h3 class="text-center mb-50" style="color: #e3147c;"><?php the_field('third_fold_title') ?></h3>
				<div class="grid">
					<?php if( have_rows('content_boxes') ): while ( have_rows('content_boxes') ) : the_row(); ?>
						<div class="col-md-4">
							<div class="grid-item aos-item" data-aos="fade-up">
								<div class="small-btn"><?php the_sub_field('title'); ?></div>
								<figure>
									<img src="<?php the_sub_field('image'); ?>">
									<div class="overlay">
										<div>
											<a href="<?php the_sub_field('link'); ?>" class="button inverse">Read more</a>
										</div>
									</div>
								</figure>
								<div class="text-box">
									<h6 class="sub1">
										<?php
											$givchars = 80; 
											$postgiv = get_sub_field('text'); 
											$modgiv = substr($postgiv, 0, $givchars); 
											echo ' ' .$modgiv. '... ';
										?>
									</h6>
										
									<!-- <h6><?php //the_sub_field('text'); ?></h6> -->
								</div>
							</div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</section>
	<section class="gradient gradient-curve trust trust-texture text-center mt-100 aos-item" data-aos="zoom-out">

      <?php get_template_part( 'template-parts/template-part', 'gradient-section' ); ?>

	</section>


<?php endwhile; ?>

<?php
get_footer();
