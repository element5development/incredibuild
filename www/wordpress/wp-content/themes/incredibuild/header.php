<?php
// if ( !is_user_logged_in() ) {
//  	 header('Location:https://www.incredibuild.com/');
//  }
?>
<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Incredibuild
 */
// if (!is_user_logged_in()) {
// 	wp_safe_redirect('/maintenance');
// 	exit();
// }
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/style.css?version=<?php echo strtotime(" now "); ?>" />
	<!-- Hotjar Tracking Code for http://incredibuild.com/ -->
	<script>
	(function(h,o,t,j,a,r){
	h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	h._hjSettings={hjid:528819,hjsv:5};
	a=o.getElementsByTagName('head')[0];
	r=o.createElement('script');r.async=1;
	r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	a.appendChild(r);
	})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
</head>

<body <?php body_class(); ?>>
	<div class="main-wrapper">
	<header style="z-index: 999;">
		<div class="nav-container">
			<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
				<a class="mobile search-mobile" href="javascript:;"><i class="fa fa-search"></i></a>
				<div class="search-section-mob" style="display: none;">
					<?php get_search_form(); ?>
				</div>
				<a class="navbar-brand" href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png"></a>
				<button type="button" class="navbar-toggle collapsed" onclick="menuBarShowHide()">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>

				<div class="navbar-collapse navbar-right" id="myDIV">
					<ul class="navbar-nav mr-auto">
						<li class="mobile"><a class="button inverse hover" href="/download-incredibuild/">DOWNLOAD</a></li>
						<?php wp_nav_menu(array( 'theme_location'=> 'menu-1', 'container'=> '', 'items_wrap'=> '%3$s', 'walker' => new WP_Bootstrap_Navwalker() )); ?>
						<li class="desktop search-btn"><a href="javascript:;"><i class="fa fa-search fa-2x"></i></a>
							<div class="search-section" style="display: none;">
								<?php get_search_form(); ?>
							</div></li>
						<li class="desktop"><a class="button inverse hover" href="/download-incredibuild/">DOWNLOAD</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</header>
    <section class="main-content">

