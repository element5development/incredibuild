<?php
/**
 * Template Name: Login Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */
// wp_redirect('https://www.dev.incredibuild.archahosting.com/trial_download');
// exit();
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="login-sec pt-50">
		<div class="container">
			<div class="row aos-item" data-aos="fade-up">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<?php the_field('login_content'); ?>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="row pt-50" data-aos="fade-up">
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<h4>Existing User? Sign In</h4>
					<?php echo do_shortcode('[wppb-login redirect_url="/dashboard"]'); ?>
				</div>
				<div class="col-md-5">
					<h5>Don't have an account? <br>
					<a href="/download-incredibuild/">Sign Up</a></h5>
				</div>
			</div>
		</div>
	</section>


<?php endwhile; ?>

<style type="text/css">
	.wppb-form-field input, .wppb-form-field input[type="text"], .wppb-form-field input[type="number"], .wppb-form-field input[type="email"], .wppb-form-field input[type="url"], .wppb-form-field input[type="password"], .wppb-form-field input[type="search"], .wppb-form-field select, .wppb-form-field textarea, .wppb-checkboxes, .wppb-radios, #wppb-login-wrap .login-username input, #wppb-login-wrap .login-password input,
	.wppb-form-field label, #wppb-login-wrap .login-username label, #wppb-login-wrap .login-password label {
		width: 100%;
	}
	.login-sec input[type="submit"] {
		background: #e3147c;
		font-size: 18px;
		border: 2px solid #e3147c;
	}
	.login-sec input[type="submit"]:hover {
		background: #fff;
		color: #e3147c !important;
	}
	.wppb-user-forms .login-password input,
	.wppb-user-forms .login-username input {
		border-radius: 0px;
		color: #2d3135;
		font-family: "Open Sans";
		font-size: 16px;
		font-weight: 400;
		line-height: 28px;
		height: 60px;
		padding: 0px 15px;
		border: 2px solid #818386;
		background-color: #ffffff;
		width: 100%;
	}
	.login-remember {
		display: none !important;
	}
</style>

<?php
get_footer();
