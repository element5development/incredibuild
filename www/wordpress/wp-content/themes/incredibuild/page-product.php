<?php
/**
 * Template Name: Product Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	
	<section class="product-text pt-50">
		<div class="container">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="justify_custom col-md-10 aos-item" data-aos="fade-up">
					<?php the_field('first_fold_content'); ?>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</section>
	<section class="price">
		<div class="container">
			<div class="row pb-50">
				<?php if( have_rows('pricing_cards') ): ?>
					<?php while( have_rows('pricing_cards') ): the_row(); ?>
						<div class="col-md-4 aos-item" data-aos="fade-up">
							<div class="price-box">
								<div class="price-shadow"><?php the_sub_field('small_title'); ?></div>
								<div class="heading border-gradient">
									<h3 <?php if (get_sub_field('small_title') == 'PRO'): ?>
										style="margin-bottom: 52px;"
									<?php endif ?> <?php if (get_sub_field('small_title') == 'ENTERPRISE'): ?>
										style="margin-bottom: 2px;"
									<?php endif ?>><?php the_sub_field('title'); ?></h3>
									<?php if (get_sub_field('text')): ?>
										<p><?php the_sub_field('text'); ?></p>
									<?php endif ?>
								</div>
								<?php if( have_rows('features') ): ?>
									<ul>
										<?php while( have_rows('features') ): the_row(); ?>
											<li><?php the_sub_field('feature'); ?></li>
										<?php endwhile; ?>
									</ul>
								<?php endif; ?>
								<div class="text-center">
									<a href="<?php the_sub_field('button_link'); ?>" class="button inverse orange-btn"><?php the_sub_field('button_text'); ?></a>
									<?php the_sub_field('last_content'); ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="block-wrapper mb-50">
					<h4 class="text-center mb-40"><?php the_field('features_fold_title'); ?></h4>
					<div class="table-responsive">
						<table class="price-table">
							<thead>
								<tr class=" aos-item" data-aos="fade-up">
									<th width="58%">Feature</th>
									<th width="15%">FreeDev</th>
									<th width="12%" class="grey">Pro</th>
									<th width="15%">Enterprise</th>
								</tr>
							</thead>
							<tbody>
							<?php if( have_rows('features_list') ): ?>
								<?php while( have_rows('features_list') ): the_row(); ?>
									<tr class=" aos-item" data-aos="fade-up">
										<td><?php the_sub_field('title'); ?></td>
										<td><?php if (get_sub_field('free_dev')): ?>
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/check.png">
										<?php endif ?></td>
										<td class="grey"><?php if (get_sub_field('pro')): ?>
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/check.png">
										<?php endif ?></td>
										<td><?php if (get_sub_field('enterprise')): ?>
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/check.png">
										<?php endif ?></td>
									</tr>
								<?php endwhile; ?>
							<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="gradient gradient-curve trust trust-texture text-center mt-100 aos-item" data-aos="zoom-out">

      <?php get_template_part( 'template-parts/template-part', 'gradient-section' ); ?>

	</section>



<?php endwhile; ?>

<?php
get_footer();
