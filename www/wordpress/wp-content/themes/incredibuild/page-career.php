<?php
/**
 * Template Name: Careers Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Incredibuild
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/template-part', 'header' ); ?>
	<style>.careerForm #gform_fields_1 li {width: 100% !important;}</style>
	<section class="solution-texts pt-50">
		<div class="container">
			<div class="justify_custom col-md-12">
				<?php the_field('about_content'); ?>
			</div>
		</div>
	</section>
	<section class="login2 solution-box">
		<div class="container careersSection">
			<div class="panel-group" id="accordion">
            <script>
                var dataPosition = {}
            </script>
			<?php if( have_rows('jobs') ): $x=1; while ( have_rows('jobs') ) : the_row(); ?>
				<div class="panel panel-default">
					<div class="panel-heading <?php if ($x==1): ?>
						
					<?php endif ?>">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $x; ?>"><?php the_sub_field('title'); ?></a>
						</h4>
					</div>
					<div id="collapse<?php echo $x; ?>" data-position="<?php echo $x;?>" class="panel-collapse collapse <?php if ($x==1): ?>
						
					<?php endif ?>">
                        <script>
                            dataPosition[<?php echo $x?>] = "<?php echo get_sub_field('title');?>";
                        </script>
						<div class="panel-body">
                            <?php the_sub_field('description'); ?>
                        </div>
					</div>
				</div>
			<?php $x++;
			 endwhile; endif; ?>
			</div> 
		</div>
	</section>
    <div class="row hidden" id="js-append-form">
        <div class="col-md-3"></div>
        <div class="careerForm col-md-6">
            <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
        </div>
        <div class="col-md-3"></div>
    </div>
<script type="text/javascript">
	jQuery(document).ready(function($){
	    console.log(dataPosition);
		$(".panel-collapse").on("hide.bs.collapse", function(){
			$(this).parent().find('.panel-heading').removeClass('active');
		});
		$(".panel-collapse").on("show.bs.collapse", function(){
			$(this).parent().find('.panel-heading').addClass('active');
			$(this).parent().find('.panel-body').append($('#js-append-form'));
			$('#js-append-form').removeClass('hidden');
			var dataPositionNum = $(this).attr('data-position');
			$('#js-append-form').find('#input_1_3').val(dataPosition[dataPositionNum]);
		});
		// $('.panel-heading a').on('click',function(e){
		//     if($(this).parents('.panel').children('.panel-collapse').hasClass('in')){
		//         e.stopPropagation();
		//     }
		//     e.preventDefault();
		// });
	});
</script>
<?php endwhile; ?>

<?php
get_footer();
