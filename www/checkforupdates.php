<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
// log version info
function log_version_data()
{
  $cur_logfile_name = "/var/www/incredl3/public_html/ibusagelogs/" . date("F-j-Y");
  if ($_SERVER["QUERY_STRING"] != ""){
    $fp = fopen($cur_logfile_name,"a");
    fwrite($fp, date("h:i:s A") . "\t" . $_SERVER["REMOTE_ADDR"] . "\t" . $_SERVER["QUERY_STRING"] . "\n");
    fclose($fp);
  }
}

// - new code -
function _is_curl_installed() {
    if  (in_array  ('curl', get_loaded_extensions())) {
		return true;
    }
    else {

        return false;
    }
}
function log_version_to_db(){

	try {	

		$target_url = 'http://versionmanager.incredibuild.com/checkforupdatesnew?'.$_SERVER["QUERY_STRING"];

		$ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $target_url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
		curl_close($ch);

    } catch (Exception $e) {
    	echo $e->getMessage();
    }

}

// get querystring parameters -
/*$raw_context = (isset($_GET['context']))?pg_escape_string($_GET['context']):'';
if($raw_context){ 
	$context = explode(';', $raw_context);
    $arr_version = explode('_', $context[1]);
}*/

// - end new code function -


log_version_data();
if(_is_curl_installed()) log_version_to_db(); // new code function call

// retrun version data
echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n";
echo "<ib_versions>\n";
echo "<release build=\"1003140\" name=\"9.2\" />\n";
echo "<beta build=\"1002185\" name=\"3.60 Beta 2\" />\n";
echo "</ib_versions>";
?>


