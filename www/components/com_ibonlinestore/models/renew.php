<?php
/*------------------------------------------------------------------------
# ibonlinestore.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');
/**
 * Ibonlinestore Model
 */
class IbonlinestoreModelrenew extends JModelList
{
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select('*');
		// From the ibonlinestore_ibonlinestor table
		$query->from('#__ibonlinestore_ibonlinestor');

		return $query;
	}
	
	public function getSteps()
	{
				$db = JFactory::getDBO();
				$query = $db->getQuery(true);
				$query->select('*');
				$query->from('#__ibonlinestore_ibonlinestor');
				$query->where('`id` = 1');
				$db->setQuery($query);
				$data = $db->loadObject();
				
			return $data;
	}
				
	public function getRenew()
	{
	
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__ibonlinestore_car');
		$query->where('`type` = 2');
		$db->setQuery($query);
		$data = $db->loadObjectList();
		
		return $data;
	}
	

}
?>