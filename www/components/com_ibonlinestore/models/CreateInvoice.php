<?php

header("Content-Type: text/plain");

class Document
{
    public $ClientID = 0; //375612
    public $DocumentType = 1; // invoice
    public $Subject = '';
    //public $Currency = "ILS";
	public $Currency = "USD";
    public $Total = 0;
    public $TotalWithoutTax = 0;
    public $TotalTaxAmount = 0;
    public $TaxPercentage = 0;
    public $ExternalComments = "";
    public $InternalComments = "";
    public $BranchID = null;
    public $IssueDate;
    public $ConversionRate = 1;
    public $Discount;
    public $RoundAmount = 0;
    public $Language = 2; //1= 'heb' 2 = 'en'
    public $Items;
    public $AssociatedEmails;
    public $Payment;

    public function __construct(
							$clientID, 
							$documentType, 
							$subject, 
							$currency, 
							$total, 
							$totalWithoutTax, 
							$totalTaxAmount, 
							$taxPercentage, 
							$exComments, 
							$inComments, 
							$branchId, 
							$issueDate, 
							$conversionRate, 
							$discount, 
							$roundAmount, 
							$language, 
							$items = Array(), 
							$associatedEmails = Array(), 
							$payment = null
							)
    {
		$this->ClientID = $clientID;
		$this->DocumentType = $documentType;
		$this->Subject = $subject;
		$this->Currency = $currency;
		$this->Total = $total;
		$this->TotalWithoutTax = $totalWithoutTax;
		$this->totalTaxAmount = $totalTaxAmount;
		$this->TaxPercentage = $taxPercentage;
		$this->ExternalComments = $exComments;
		$this->InternalComments = $inComments;
		$this->BranchID = $branchId;
		
		
		$this->ConversionRate = $conversionRate;
		$this->RoundAmount = $roundAmount;
		$this->Language = $language;

		
        if (isset($issueDate))
			$this->IssueDate = $issueDate;		
		else
			$this->IssueDate = date("c", time()); // can be at the Past no erlier then last invoice
		
		$this->Discount = $discount;
        $this->Items = $items;
        $this->AssociatedEmails = $associatedEmails;
        $this->Payment = $payment;
    }
}

class Discount
{
    public $Value = 0;
    public $BeforeTax = true;// discount calculated before tax calculated
    public $IsNominal = true;// means that the discount is calculated according to currency type (in this case: 5 ILS), if it is false than discount calculated in percentages (in this case: 5%)
}

class Item
{
    public $Code = "001";
    public $Name = "first Item";
    public $Quantity = 1;
    public $Price = 0;
    public $Total = 0;
    public $TotalWithoutTax = 0;
    public $TaxPercentage = 0; // in case you want an item without tax
    public $Discount;

    public function __construct($code, $name, $quantity, $price, $total, $totalWithoutTax, $taxPercentage, $discount)
    {
		$this->Code = $code;
		$this->Name = $name;
		$this->Quantity = $quantity;
		$this->Price = $price;
		$this->Total = $total;
		$this->TotalWithoutTax = $totalWithoutTax;
		$this->TaxPercentage = $taxPercentage;
        $this->Discount = $discount;
    }
}

class Email
{	
    public $Mail = '';
    public $IsUserMail = true;// means that this is the user email, and this is the email that sends the document to all associated email
	
    public function __construct($mail= '', $isUserEmail = '')
    {
		$this->Mail = $mail;
		$this->IsUserMail = $isUserEmail;
	}
}

class Payment
{
    public $Date;
    public $Amount = 0;
    public $PaymentType = 4;

    public function __construct()
    {
        $this->Date = date("c", time());
    }
}

class Invoice4u
{
    public $token;
    public $result;

    private function requestWS($wsdl, $service, $params)
    {
        try
        {
            $options = array('trace' => true, 'exceptions' => true, 'cache_wsdl' => WSDL_CACHE_NONE, 'connection_timeout' => 10);
			$client = new SoapClient($wsdl, $options);
			//var_dump($client->__getTypes()); exit;
			$response = $client->$service($params);
			$service = $service . "Result";
            $this->result = $response->$service;
            return $this->result;
        } catch (SoapFault $f) {
            echo $f->faultstring;
            return $f->faultstring;
        } catch (Exception $e) {
            echo $e->getMessage();
            return $e->getMessage();
        }
    }

    public function Login()
    {
        $wsdl = "http://incredibuild.webmark.co.il/i4u/LoginService.svc?wsdl";
        $user = array('username' => 'test@test.com', 'password' => '123456', 'isPersistent' => false);

        $this->requestWS($wsdl, "VerifyLogin", $user);
        $this->token = $this->result;
    }

    // public function CreateDocument()
    // {
        // $wsdl = "http://incredibuild.webmark.co.il/i4u/DocumentService.svc?wsdl";
        // $doc = new Document();
        // $this->result = $this->requestWS($wsdl, "CreateDocument", array('doc' => $doc, 'token' => $this->token));
    // }

    public function CreateDocumentWith($doc)
    {
		//print_r($doc);
		
        $wsdl = "http://incredibuild.webmark.co.il/i4u/DocumentService.svc?wsdl";
        $this->result = $this->requestWS($wsdl, "CreateDocument", array('doc' => $doc, 'token' => $this->token));
		
		return $this->result;
    }
}

//if ($_REQUEST['create']) {
	// $invoice = new Invoice4u();
	// $invoice->Login();
	// $invoice->CreateDocument();

	// var_dump($invoice->result);

	// $str = $invoice->result;
	// $invoiceNumber = $str["DocumentNumber"];
	// $invoiceNumber = intval($invoiceNumber);//echo intval($invoiceNumber);
//}

?>