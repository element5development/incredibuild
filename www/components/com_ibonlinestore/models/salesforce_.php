<?php
/*------------------------------------------------------------------------
# ibonlinestor.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla modelitem library
jimport('joomla.application.component.modelitem');
//ini_set("soap.wsdl_cache_enabled", 0);

/**
 * Ibonlinestor Model for Ibonlinestore Component
 */
 
require_once 'salesforce_include/soapclient/SforceEnterpriseClient.php';
require_once 'salesforce_include/soapclient/SforceHeaderOptions.php';
		
class IbonlinestoreModelsalesforce extends JModelItem
{
	private static $acountSettings = array(
			"sforceUsername" 		=> 'danny@webmark.co.il',
			"sforcePassword" 		=> 'qwe%#@1qwR#@!!243',
			"sforceSecurityToken" 	=> 'QYGwMmfNbpznPfAK7jjF8FvO',
			"sforceWsdl"			=> '/salesforce_include/wsdl.jsp.xml'	);
			//"sforceWsdl"			=> '/salesforce_include/dev.sf.wsdl.jsp.xml'	);
	
	private function connect()
	{
		if (!$this->sforceClient)
		{
			$this->sforceClient = new SforceEnterpriseClient();
			$this->soapClient = $this->sforceClient->createConnection(JPATH_BASE.self::$acountSettings['sforceWsdl']);
			
			$sforceLogin = $this->sforceClient->login(
				self::$acountSettings['sforceUsername'],
				self::$acountSettings['sforcePassword'] . self::$acountSettings['sforceSecurityToken']
			);
		}

	}
	
	
	public function externalSendEmail($toAddress, $toName = '', $subject, $body, $from = '', $fromName = '', $reply = '', $replyName = '', $bcc = '', $secondaryAddress = '') {
			
		require_once (getcwd().'/templates/yoo_air/PHPMailerAutoload.php');

		$Mail = new PHPMailer();
		$Mail->IsSMTP(); // Use SMTP
		$Mail->Host        = "smtp.gmail.com"; // Sets SMTP server
		$Mail->SMTPDebug   = 0; // 2 to enable SMTP debug information
		$Mail->SMTPAuth    = TRUE; // enable SMTP authentication
		$Mail->SMTPSecure  = "tls"; //Secure conection
		$Mail->Port        = 587; // set the SMTP port
		$Mail->SMTPKeepAlive = FALSE;   
		$Mail->Username    = 'noreply@incredibuild.com'; // SMTP account username
		$Mail->Password    = 'xorPass77'; // SMTP account password
		$Mail->Priority    = 3; // Highest priority - Email priority (1 = High, 3 = Normal, 5 = low)
		$Mail->CharSet     = 'iso-8859-1';
		$Mail->Subject     = $subject;
		$Mail->ContentType = 'text/html; charset=utf-8\r\n';
		
		if ($bcc != '') {
			$Mail->AddBCC($bcc);
		}
		
		if ($from != '') {
			$Mail->From        = $from;
			if ($fromName != '') {
				$Mail->FromName        = $fromName;
			}
		}
		
		if ($reply != '') {
			$Mail->AddReplyTo($reply, $replyName);
		}
		
		$Mail->WordWrap    = 900; // RFC 2822 Compliant for Max 998 characters per line
		
		$Mail->AddAddress( $toAddress ); // To: //$toName
		
		
		// $Mail->AddCC('ron.ginton@incredibuild.com');
		// $Mail->AddCC( 'joel.blankchtein@incredibuild.com' ); 
		
		if ($secondaryAddress != '') {
			$Mail->AddAddress( $secondaryAddress ); 
		}
		
		$Mail->isHTML( TRUE );
		$Mail->Body = $body;
		
		//send the message, check for errors
		if (!$Mail->send()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return "1";
		}
		
		$Mail->SmtpClose();
	}
	
	public function trial_account($license_email,$opp_name, $landing = '')
	{
		//create opp
		
		$this->connect();
		
		$account_id = "001w0000018XJ0A";
		
		$items = array(
			"01uw000000aaRcjAAE",//Agent + Up to 8 Cores
			"01uw000000aaRcrAAE",//Visual Studio C/C++
			"01uw000000aaRcsAAE",//Visual Studio C#
			"01uw000000aaRcgAAE",//DEV tools
			"01uw000000aaRcyAAE",//Make & Builds tools
			"01uw000000aaRcwAAE",//Sony Playstation4 (subscription)
			"01uw000000aaRcxAAE",//Xbox1 (subscription)
		);
		
		
		$date  = date("Y-m-d",strtotime("+1 week"));
		$today_date  = date("d-m-Y");
		
		$fields = array ( "AccountId" => $account_id ,
							"ResellerLookup__c " => $account_id,
							"Name " => $opp_name." - ".$today_date,
							"StageName " => "open",
							"CloseDate " => $date,
							"Licensee_Email__c"	=> $license_email,
							"RenewalOpportunity__c"	=> "No"
							);

		$opp = $this->create_object("opportunity",$fields);
		
		$exp_date  = date("Y-m-d",strtotime("+1 month"));
		
		//create license
		$fields = array ( "Name" => $opp_name ,
							"Account__c " => $account_id,
//							"CreatedFromOpportunity__c" =>$opp[0]->id,
							"Trial__c" => "1",
							"ExpirationDate__c" => $exp_date,
//							"Draft__c" => "1"
							);

		$lic = $this->create_object("License__c",$fields);
		
	
		foreach ($items as $item)
		{
				$data[] = array(
							"OpportunityId" 	=> 	$opp[0]->id,
							"License__c"		=>  $lic[0]->id,
							"PricebookEntryId"	=>	$item,
							"Quantity"			=>	5,
							"UnitPrice"			=>  0
				);
		}
		
		$sf_info = $this->create_lineitems($data);
		
		$this->update_opportunity_status($opp[0]->id,"Closed Won");
	}

	
	public function update_joomla_id_from_file()
	{
	
	$reg = & JModel::getInstance('Register', 'IbonlinestoreModel');
	
	$file = fopen("joomlaid.csv", "r");
	while(!feof($file)){
		$line = explode(",",fgets($file));
		$line[0] = str_replace('"',"",$line[0]);
		echo $line[0];
		if (strlen($line[0]) == 15 )
			$reg->sf_sync($line[0]);
		
	}
	fclose($file);

	}
	
	public function get_contact_sync_info($sf_id)
	{
		
		$this->connect();	
		
		$Query = "Select FirstName,LastName,Email,Phone,MailingAddress,Account.Name From contact where id='".$sf_id."' and JoomlaID__c='' ";
		$Response = $this->sforceClient->query($Query);
		
		return $Response->records[0];
	}
	
	public function get_system_users($fname)
	{
		
		$this->connect();	
		//stops here
		$Query = "Select id From User where  FirstName ='".$fname."'";
		$Response = $this->sforceClient->query($Query);
		
		return $Response->records[0];
		
	}
	
	public function get_system_users_by_email($email)
	{
		$this->connect();	
		//stops here
		$Query = "Select id From User where  Email ='".$email."'";
		$Response = $this->sforceClient->query($Query);
		
		return $Response->records[0];
	}
	
	public function update_joomlaID($contact_id,$joomlaID)
	{
		$this->connect();
		
		$update = new stdClass;
		$update->type  ='contact';
		
		$update->fields = array ( "id" => $contact_id ,"JoomlaID__c" => $joomlaID);
		
		$Response = $this->sforceClient->update(array($update->fields),'contact');

		return $Response;
	}
	
	public function check_if_allow_bundle($accountId,$cart)
	{
			$Query = "Select type From account where id='".$accountId."'";
			$Response = $this->sforceClient->query($Query);
			
			$bundle = false;
			foreach ($cart as $c)
			{
				if ($c["type"] == 3) 
				{
					$bundle = true;
					break;
				}
			}

			if ($bundle && ($Response->records[0]->Type != "Prospect"))
			{
				return false;
			}
		
			return true;
	}
	
	public function get_sf_account_ids($sf_id,$joomla_id)
	{
		$this->connect();	
		$db = JFactory::getDbo();
		
		if ($joomla_id)
		{
			//check with joonlaID
			$Query = "Select AccountId,Approved_for_Online_Order__c From contact where JoomlaID__c='".$joomla_id."'";
			$Response = $this->sforceClient->query($Query);

			if ($Response->records[0])//contact
			{
				return array("AccountId" =>  $Response->records[0]->AccountId,"ContactId" => $joomla_id,"Approved_for_Online_Order" => $Response->records[0]->Approved_for_Online_Order__c);
			}
			else //lead
			{
				$Query = "Select ConvertedAccountId,ConvertedContactId,Exlusive_VS_Offer_Coupn_Usage__c, Id  From lead where JoomlaID__c='".$joomla_id."'";
				$Response = $this->sforceClient->query($Query);
				
				//print_r($Response);
				//print_r($Response->records[0]);
				// if ($Response->records[0] && (!empty($Response->records[0]->ConvertedAccountId)))//contact
				// {
					//print_r($Response);
					//print_r($Response);
					return array("AccountId" =>  $Response->records[0]->ConvertedAccountId,"ConvertedContactId" => $Response->records[0]->ConvertedContactId, "ExlusiveVSOfferCoupnUsage" => $Response->records[0]->Exlusive_VS_Offer_Coupn_Usage__c, "LeadID" => $Response->records[0]->Id);
				// }
				// else {
					
					// $Query = "Select Exlusive_VS_Offer_Coupn_Usage__c From lead where JoomlaID__c='".$joomla_id."'";
					// $Response = $this->sforceClient->query($Query);
					
					// print_r($Response);
					// return array("AccountId" => null,"ConvertedContactId" => null, "ExlusiveVSOfferCoupnUsage" => $Response->records[0]->Exlusive_VS_Offer_Coupn_Usage__c);					
				// }
			}
		}
		
		//check with sf_id
		$Query = "Select AccountId,Approved_for_Online_Order__c From contact where id='".$sf_id."'";
		$Response = $this->sforceClient->query($Query);

		if ($Response->records[0])//contact
		{
			return array("AccountId" =>  $Response->records[0]->AccountId,"ContactId" => $sf_id,"Approved_for_Online_Order" => $Response->records[0]->Approved_for_Online_Order__c);
		}
		else//lead
		{
			$Query = "Select ConvertedAccountId,ConvertedContactId From lead where id='".$sf_id."'";
			$Response = $this->sforceClient->query($Query);
			
			return array("AccountId" =>  $Response->records[0]->ConvertedAccountId,"ConvertedContactId" => $Response->records[0]->ConvertedContactId);
		}
	}
	
	public function mark_special_promotion_eligibility_redeemed($sf_id){
		$this->connect();
		$update = new stdClass;
		$update->type  ='contact';
		$update->fields = array ("id" => $sf_id, "Exlusive_VS_Offer_Coupon_Usage__c " => 'used');
		$Response = $this->sforceClient->update(array($update->fields),'contact');
	}
	
	public function check_special_promotion_eligibility($sf_id){
		$this->connect();	
		if (strpos($sf_id, 'AAC') !== false){
			$sf_id = str_replace('AAC', '', $sf_id);
		}
		$Query = "Select Exlusive_VS_Offer_Coupon_Usage__c From contact where id='".$sf_id."'";
		$Response = $this->sforceClient->query($Query);
		
		if ($Response->records[0])
		{
			//return print_r($Response->records[0], true);
			return $Response->records[0]->Exlusive_VS_Offer_Coupon_Usage__c;
		}
		return 'not_eligible';
	}

	public function create_opportunity($account_id,$opp_name,$license_email)
	{
		$this->connect();
		
		$date  = date("Y-m-d",strtotime("+1 week"));
		$today_date  = date("d-m-Y");
		
		$fields = array ( "AccountId" => $account_id ,
							"ResellerLookup__c " => $account_id,
							"Name " => $opp_name." - ".$today_date,
							"StageName " => "open",
							"CloseDate " => $date,
							"Licensee_Email__c"	=> $license_email,
							"RenewalOpportunity__c"	=> "No",
							);

		$Response = $this->create_object("opportunity",$fields);
		
		return $Response;
	}
	
	
	public function create_licence($account_id,$registered_name)
	{
		$this->connect();
		
		$date  = date("Y-m-d",strtotime("+1 week"));
		$today_date  = date("d-m-Y");
		
		$fields = array ( "Name" => $registered_name ,
							"Account__c " => $account_id);

		$Response = $this->create_object("License__c",$fields);
		
		return $Response;
	}
	
	public function create_contact_role($contact_id,$opp_id)
	{
		$this->connect();
		
		$fields = array ( "ContactId" => $contact_id ,
							"Role " => "other",
							"OpportunityId" => $opp_id);

		$Response = $this->create_object("OpportunityContactRole",$fields);
		
		return $Response;
	}
	
	public function craete_o_l_cr($account_id,$contact_id,$license_email,$registered_name,$licenace_type,$opp_name)
	{

	//create opportunity
	$opp_data = $this->create_opportunity($account_id,$opp_name,$license_email);
	$opp_id = $opp_data[0]->id;
	
	if ($licenace_type=="new")//create licence if new
	{
		$lic_data = $this->create_licence($account_id,$registered_name);
		
		$lic_id = $lic_data[0]->id."|".$registered_name;
		
	}
	else
		$lic_id = $registered_name;
	
	//create a contact role
	$cr_data = $this->create_contact_role($contact_id,$opp_id);
	$cr_id = $cr_data[0]->id;
	
	return array("opp_id" => $opp_id,"lic_id"=>$lic_id,"cr_id" =>$cr_id);
	
	}
	
	
	public function prepper_quote($contact_id,$email)
	{

	$quote_data = $this->get_quote(JFactory::getSession()->get('q'));
	$opp_id = $quote_data->records[0]->OpportunityId;
	
	$this->update_opportunity_email($opp_id,$email);
	
	//create a contact role
	$cr_data = $this->create_contact_role($contact_id,$opp_id);
	$cr_id = $cr_data[0]->id;
	
	return array("opp_id" => $opp_id,"lic_id"=>'|',"cr_id" =>$cr_id);
	
	}
	
	public function prepper_external_quote($q)
	{


	// get OpportunityId
	$quote_data = $this->get_quote($q);
	$opp_id = $quote_data->records[0]->OpportunityId;

	//check if quote not closed
	
	$Query = "select ID,Name,OpportunityId from Quote where ID='".$q."' and status<>'Approved by Customer'";
	$Response = $this->sforceClient->query($Query);
	
	if (count($Response->records) == 0)
	{
		die("Error - Quote already approved.");
	}
	
	$Query = "select ID,Name from opportunity where ID='".$opp_id."' and StageName<>'Closed Won'";
	$Response = $this->sforceClient->query($Query);
	
	if (count($Response->records) == 0)
	{
		die("Error - opportunity already closed-won.");
	}
	
	//check if contact role exists on opp
	$Query = "select Id, ContactId, OpportunityId from OpportunityContactRole where OpportunityId='".$opp_id."' ";
	$Response = $this->sforceClient->query($Query);
	
	if (count($Response->records) == 0)
	{
		die("Error - No conract roles in opportunity.");
	}
	
	return $opp_id;
	
	}
	
	
	public function update_standard_products()
	{
	$this->connect();	
	$db = JFactory::getDbo();
	
	//$contactQuery = "SELECT Id, Name,Family,Description,CurrencyIsoCode,ProductCode, IsActive FROM product2";
	$contactQuery = "Select Pricebook2Id,Id, Name, ProductCode, Product2Id, UnitPrice,UseStandardPrice,IsActive,Product2.Description,Product2.Family From PricebookEntry where IsActive = true and Pricebook2Id='01s200000000MUVAA2'";
	$contactResponse = $this->sforceClient->query($contactQuery);
	
	foreach ($contactResponse->records as $products)
	{
		if ($products->ProductCode)
		{
		
			if ($products->Product2->Family=="Agent")
				$type=0;
			elseif ($products->Product2->Family=="Solution")
				$type=1;
			else
				$type=2;
			
			$query = "update `bczfi_ibonlinestore_car` set price=".$products->UnitPrice.",`subtitle`='".$products->Name."', sf_prciebookid='".$products->Id."',description='".$products->Product2->Description."',type=".$type." where title='".$products->ProductCode."'";
			
			echo $query."<br>";
			$db->setQuery($query);
			$db->query(); 	
		}
	}

	echo "done.";
	die();
	}
	
	public function get_license_allowed_products($lic_ids)
	{
		$this->connect();	
		$db = JFactory::getDbo();
		
		$Query = "Select License__c from LicenseItem__c where License__c in (".$lic_ids.") and Product__r.Block_Online_Order__c=true";

		$Response = $this->sforceClient->query($Query);
		
		return $Response;
	}
	
	public function sf_licenses($accountid)
	{
		$this->connect();	
		$db = JFactory::getDbo();
		
		//$contactQuery = "SELECT Id, Name,Family,Description,CurrencyIsoCode,ProductCode, IsActive FROM product2";
		$Query = "Select Name,id From License__c where Account__c='".$accountid."' and Archived__c=false";
		$Response = $this->sforceClient->query($Query);
		
		$lic_ids = array();
		
		//loop get only allowed licenses
		foreach ($Response->records as $entry)
		{
			$lic_ids[] = "'".$entry->Id."'";
		}
		
		if (count($lic_ids) > 0)
			$dont_allow= $this->get_license_allowed_products(implode(",",$lic_ids));
	

		foreach ($dont_allow->records as $entry)
		{
			
			for ($i=0; $i<$Response->size; $i++)
			{
				if ($Response->records[$i])
				{
					if ($Response->records[$i]->Id == $entry->License__c)
					{
						unset ($Response->records[$i]);
						
						
					}
				}
			}
	
		}
		return ($Response);
	}
	
	public function update_payment_opportunity($oppid,$paymentid)
	{
		$this->connect();
		
		$update = new stdClass;
		$update->type  ='opportunity';
		
		
		
		$update->fields = array ( "id" => $oppid ,"Invoice_Receipt__c " => $paymentid,"StageName" => "Closed Won",
		
		'Card_Type__c'	=>	JRequest::getVar("cred_type"),
		'Card_Number__c'	=>	JRequest::getVar("ccno"),
		'Money_In__c'	=>	date("Y-m-d")
		
		);
		
		$Response = $this->sforceClient->update(array($update->fields),'opportunity');
				
		//new code to create i4u invoice - requires all data from SF
		
				 $sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');

                $opp = $sf->get_opportunity_info($oppid);					
				$cart = $sf->get_newlineitems($oppid);
				$cart = $cart->records;
				$poNum = $_REQUEST['index'];
				$session = & JFactory::getSession();
				if (JFactory::getSession()->get('ponum') && JFactory::getSession()->get('ponum') != '') {
				$poNum = JFactory::getSession()->get('ponum');
				}
				elseif (JRequest::getVar('ponum') && JRequest::getVar('ponum') != '') {
							$poNum = JRequest::getVar('ponum');
							}

				//extract address in order to create user in i4u
				$country = $opp[3]->records[0]->BillingAddress->country;
				$city = $opp[3]->records[0]->BillingAddress->city;
				$postalCode = $opp[3]->records[0]->BillingAddress->postalCode;
				$state = $opp[3]->records[0]->BillingAddress->state;
				$street = $opp[3]->records[0]->BillingAddress->street;
				$name = $opp[3]->records[0]->Name;
				$amount = $opp[0]->records[0]->Amount;
				$AccountNumber = $opp[2]->AccountNumber;

				//create user in i4u
				
				$fields = array (
        'TransType' => 'C:INSERT',
        'Username' => '4xoreaxacc',        
        'CompanyCode' => $AccountNumber,
        'CompanyName' => $name,
        'CompanyAddress' => $country,
        'CompanyZipcode' => $postalCode,
        'CompanyWebsite' => 'http://www.www.com',
        'CompanyComments' => 'from website',
        'CompanyCity' => $city         
);

		$headers = array (
        "Connection: keep-alive",
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.162 Safari/535.19",
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language: en-US,en;q=0.8",
        "Accept-Charset: utf-8;q=0.7,*;q=0.3" 
);
			
		
		$fields_string = http_build_query ( $fields );
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, 'https://account.invoice4u.co.il/public/HttpPost.aspx' );
		curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 80 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields_string);
		 

		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );

		$str = curl_exec ( $ch );
		curl_close ( $ch );
		//end of create client, now create i4u invoice
		$PaymentName = $opp[1]->records[0]->Name;
		$invoiceSubject = 'IncrediBuild online order: '.$PaymentName;

		        $rate = @file_get_contents("http://dev.webmark.co.il/boi.php");
				if($rate == 'error' || $rate === false){
					$rate = file_get_contents('boi_backup.txt');
				} else {
					file_put_contents('boi_backup.txt', $rate);
				}
        		
				$originalRate = $rate;
				
				$intDollar = floatval ($_REQUEST['sum']);
                $rate = floatval ($rate);
        		$intNIS = $intDollar * $rate;


$fields = array (
        'CcAmount' => $intNIS,
        'CurrencyTarget' => '60',
        'InvoiceItemCode' => '999',
        'Key' => '',
        'InvoiceItemQuantity' => '1',
        'InvoiceTaxRate' => '0',
        'InvoicePayByDate' => '',
        'InvoiceDiscount' => '',
        'InvoiceDiscountRate' => '0',
        'Username' => '4xoreaxacc',        
        'CcDate' => date('d/m/Y',time()),
        'InvoiceSubject' => $invoiceSubject,
        'InvoiceDiscountRate' => 0,
        'InvoiceItemCode' => '999',
        'InvoiceItemDescription' => JRequest::getVar("notes"),
        'InvoiceItemQuantity' => 1,
        'InvoiceItemPrice' => $intNIS,
        'InvoiceTaxRate' => 0,
        'InvoiceComments' =>  '$'.$intDollar.' '.$opp[2]->Name,
        'CompanyCode' => $AccountNumber,
        'CcNumber' => JRequest::getVar("ccno"),
        'CcType' => 'type',
        'CcVerification' => '',
        'CcTicket' => '',
        'CcExpiration' => '',
        'CompanyInfo' => '',
        'mailto' => '',
        'IsItemPriceWithTax' => '',
        'Cash' => '',
        'CheckDate' => '',
        'CheckNumber' => '',
        'checkAccount' => '',
        'CheckBranch' => '',
        'CheckAmount' => '',
        'TransDate' => '',
        'TransBank' => '',
        'TransBranch' => '',
        'TransAccount' => '',
        'TransAmount' => '',
        'CheckBank' => ''
        
         
);


$headers = array (
        "Connection: keep-alive",
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.162 Safari/535.19",
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language: en-US,en;q=0.8",
        "Accept-Charset: utf-8;q=0.7,*;q=0.3" 
);


$fields_string = http_build_query ( $fields );
$ch = curl_init ();
curl_setopt ( $ch, CURLOPT_URL, 'https://account.invoice4u.co.il/Public/w_invoicereceipt.asmx/CreateWithPaymentMethods' );
curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, true );
curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 80 );
curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields_string);
 

curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );

$str = curl_exec ( $ch );


$array = explode('<Var>DocNumber</Var>',$str);
$invoiceNumber = substr($array[1],17,7);
/*
		if(JRequest::getVar('Response')=="1337" ){
			print_r($ch);
			echo "<br><br>";
			print_r($str);
			echo "<br><br>";
		}
/* ERROR: API ISSUE 2015-01-28		
$xml = new SimpleXMLElement($str);
$array = explode('<Var>DocNumber</Var>',$str);
$invoiceNumber = substr($array[1],17,7);
*/
// $invoiceNumber = "1"; // temp value
		//end of create invoice				
		//end of new code
		//start of custom invoice for client
		if(JRequest::getVar('Response')=="1337" ){
			echo "Invoice Number: ";
			print_r($invoiceNumber);
		}
      $info = array();
	  
		$productDiscount = $opp[2]->VolumeDiscountOverride__c;
		$ResellerDiscount = $opp[3]->records[0]->VolumeDiscountOverride__c;		
		if ($ResellerDiscount && $ResellerDiscount > 0){
			$ResellerDiscount = 'Reseller Discount: '.$ResellerDiscount.'%';
		}
		else{
			$ResellerDiscount = '';
		}
		$invoiceProductsHTML = '';
		$netSumBeforeReseller = 0;
        foreach ($cart as $item) {
		
            $info["itemDesc"][] = $item->PricebookEntry->Product2->ProductCode.' - '.$item->PricebookEntry->Product2->Name;
			$info["itemPrice"][] = $item->UnitPrice;
			$info["itemQty"][] = $item->Quantity;
			$discountPercentage = number_format(100-($item->UnitPrice / $item->PricebookEntry->UnitPrice * 100),2);
			$invoiceProductsHTML .= '<tr>
			<td>'.$item->PricebookEntry->Product2->ProductCode.'</td>
			<td><span class="bold">'.$item->PricebookEntry->Product2->Family.'</span><br/>
				'.$item->PricebookEntry->Product2->Name.'
			</td>
			<td>'.$item->Quantity.'</td>
			<td>$'.$item->PricebookEntry->UnitPrice.'</td>
			<td style="display: none;">'.$discountPercentage.'%</td>
			<td>$'.number_format($item->UnitPrice,2).'</td>
			<td>$'.number_format($item->TotalPrice,2).'</td>';
			$netSumBeforeReseller = $netSumBeforeReseller + $item->TotalPrice;			
        }   
		
		
			$country = $opp[3]->records[0]->BillingAddress->country;
			$city = $opp[3]->records[0]->BillingAddress->city;
			$postalCode = $opp[3]->records[0]->BillingAddress->postalCode;
			$state = $opp[3]->records[0]->BillingAddress->state;
			$street = $opp[3]->records[0]->BillingAddress->street;
			$name = $opp[3]->records[0]->Name;
			$amount = $opp[0]->records[0]->Amount;
			
			$intDollar = str_replace(',','',$amount);
			$intDollar = $amount;
			$creditCardNumber = $opp[1]->records[0]->Credit_Card_number__c;
			$creditCardCompany = $opp[1]->records[0]->Credit_Card_Company__c;                                
			$payment_due = $opp[0]->records[0]->Payment_Due_Date__c;
//                $payment_due = strtotime($payment_due);
  //              $payment_due = date('d-m-Y',$payment_due);
    //            echo '<br/>'.$payment_due;
      //          $payDate = $opp[1]->records[0]->Date__c;
        //        $payDate =  strtotime($payDate);
                //change date arrangment
                $payDate = date('Y-m-d',$payDate);

                $rate = @file_get_contents("http://dev.webmark.co.il/boi.php");
        		if($rate == 'error' || $rate === false){
					$rate = file_get_contents('boi_backup.txt');
				} else {
					file_put_contents('boi_backup.txt', $rate);
				}
        		$intDollar = floatval ($intDollar);
				$rate = floatval ($rate);

        		$intNIS = $intDollar * $rate;
                

                $ccdate = explode('/',$_REQUEST['CcDate']);
                //$ccdate[2] = '20'.$ccdate[2];
                $ccdate = array_reverse($ccdate);
                $ccdate = implode('-',$ccdate);
                $_REQUEST['CcDate'] = $ccdate;
                $_REQUEST['CcAmount'] = str_replace('$','',$_REQUEST['CcAmount']);
                
                $invoiceSubject = 'IncrediBuild online order: '.$_REQUEST['InvoiceSubject'];
                
                $_REQUEST['CcAmount'] = str_replace(',','',$_REQUEST['CcAmount']);
                $_REQUEST['InvoiceItemPrice'] =	str_replace(',','',$_REQUEST['InvoiceItemPrice']);
                
                ini_set('set_time_limit', '5');  
                ini_set('max_execution_time', '5');                  
				$intNIS = intval ($intNIS);
				$fields = array(
                'compID'=>'xoreax',  
				//'sendOrig' => 'sales@incredibuild.com',
				'sendOrig' => 'sales@incredibuild.com',
                'user'=>'sales',  
                'pass'=>'828d66',
                'maampercent'=>'0',  
                'dateissued'=>date('Ymd'),  
                'clientname'=>$name,  
                'docType'=>'deal',
                'currency'=>'2', 
                'rate' => $rate,
				'client_street' => $street,
				'client_country' => $country,
				'client_city' => $city,
                  'validity' => $payment_due,  
                'desc'=>$info['itemDesc'],
        		'unitprice'=>$info['itemPrice'],
        		'quantity'=>$info['itemQty'],
                'paid'=>$intNIS,
                'totalsum'=>$intNIS,
				
                'show_response'=>'1',
                'debug'=>'yes',
                'hidenis'=>'1'
                );
				$country = $opp[3]->records[0]->BillingAddress->country;
				$vatPercent = '0.00';
				
				
				if ($country == 'Israel'  || $country == 'ישראל') {
				
                    $vatPercent = 0.18;
                    $vat = 18;
                    $fields['maampercent'] = $vat;
                    $fields['paidincvat']= $intNIS * (1 + $vatPercent);   
                    $fields['totalvat'] = $intNIS * $vatPercent;
                    $fields['totalwithvat']= $intNIS * (1 + $vatPercent);   
                    $fields['taxexempt'] = '0';
					$fields['hwc'] = 'עבור הזמנה מספר:: '.$poNum;
					$fields['es'] = 'חשבון עסקה עבור הזמנה מספר: '.$poNum;
     			  
                }
                
                else {
					$fields['es'] = 'Invoice for Purchase Order number: '.$poNum;
				     $fields['hwc'] = 'FOR PURCHASE ORDER NUMBER: '.$poNum;
					$fields['lang'] = 'en';
					$fields['taxexempt'] = '1';
					$fields['totalpaid'] = $intNIS;
				};
				
				
		$CloseDate =  $opp[0]->records[0]->CloseDate;
		$CloseDate = strtotime($CloseDate);
		$resellerID = $opp[0]->records[0]->ResellerLookup__c;
		$oppAccountID = $opp[0]->records[0]->AccountId;
		$firstAccountName = $opp[2]->Name;
		$secondAccountName = $opp[3]->records[0]->Name;
		$AccountNumber = $opp[2]->records[0]->AccountNumber;
		$AccountNumber = $opp[2]->AccountNumber;

		if ($VolumeDiscountOverride__c && $VolumeDiscountOverride__c != 0) $VolumeDiscountOverride__c = '<div>'.$VolumeDiscountOverride__c.'% Reseller Discount:</div>';
		else $VolumeDiscountOverride__c = '';
		if ($resellerID == $oppAccountID || $firstAccountName == $secondAccountName) $clientTitle = '<div class="bold">'.$firstAccountName.'</div>';
		else $clientTitle = '<div class="bold">'.$secondAccountName.'</div>			<div>For end user: <span class="bold">'.$firstAccountName.'</span></div>';

		if ($VolumeDiscountOverride__c) $totalBeforeTax = $netSumBeforeReseller - $totalResellerDiscount;
		else $totalBeforeTax = $netSumBeforeReseller;

		$VAT = $opp[0]->records[0]->VAT__c;
		if ($VAT) {
			$VATPercentage = 1 + $VAT / 100;
			$vatPercent	= '<div>'.$VAT.'% Tax:</div>';
			$netSumBeforeResellerAfterVAT = $totalBeforeTax * $VAT / 100;
			$vatTotal = '<div>$'.$netSumBeforeResellerAfterVAT.'</div>';
			$total = $totalBeforeTax - $netSumBeforeResellerAfterVAT;
			}
		else {
		$vatPercent = '';
		$vatTotal = '';
		$total = $totalBeforeTax;
		}
		$total = number_format($total,2);
		if ($opp[2]->VolumeDiscountOverride__c && $opp[2]->VolumeDiscountOverride__c > 0) $opp[2]->Account_Volume_Discount__c = $opp[2]->VolumeDiscountOverride__c;
		$Account_Volume_Discount__c = $opp[2]->Account_Volume_Discount__c;
		if ($Account_Volume_Discount__c && $opp[2]->Account_Volume_Discount__c > 0) $Account_Volume_Discount__c = 'Discount: '.$opp[2]->Account_Volume_Discount__c.'%';
	else $Account_Volume_Discount__c = 0;
$CloseDate = date('F d, Y',$CloseDate);
$file = getcwd().'/invoiceLayout/ir.html';


if(JRequest::getVar('Response')=="1337" ){
	echo "<br>FILE: $file<br>";
}
		
$invoiceLayout = file_get_contents($file);
$Comments_for_customer_invoice__c = $opp[0]->records[0]->Comments_for_customer_invoice__c;
$invoiceLayout = str_replace('##Comments_for_customer_invoice__c##',$$Comments_for_customer_invoice__c,$invoiceLayout);
$invoiceLayout = str_replace('##lastDigits##',JRequest::getVar("ccno"),$invoiceLayout);
$invoiceLayout = str_replace('##street##',$street,$invoiceLayout);
$invoiceLayout = str_replace('##city##',$city,$invoiceLayout);
$invoiceLayout = str_replace('##state##',$state,$invoiceLayout);
$invoiceLayout = str_replace('##postalCode##',$postalCode,$invoiceLayout);
$invoiceLayout = str_replace('##country##',$country,$invoiceLayout);
$invoiceLayout = str_replace('##invoiceNumber##',intval($invoiceNumber),$invoiceLayout);
$invoiceLayout = str_replace('##CloseDate##',$CloseDate,$invoiceLayout);
$invoiceLayout = str_replace('##$vatPercent##',$$vatPercent,$invoiceLayout);
$invoiceLayout = str_replace('##intDollar##',$intDollar,$invoiceLayout);
$invoiceLayout = str_replace('##invoiceProductsHTML##',$invoiceProductsHTML,$invoiceLayout);
$invoiceLayout = str_replace('##clientTitle##',$clientTitle,$invoiceLayout);
$invoiceLayout = str_replace('##poNum##',$poNum,$invoiceLayout);
//$invoiceLayout = str_replace('##netSumBeforeReseller##',$netSumBeforeReseller,$invoiceLayout);
$invoiceLayout = str_replace('##VolumeDiscountOverride__c##',$VolumeDiscountOverride__c,$invoiceLayout);
$invoiceLayout = str_replace('##totalResellerDiscount##',$totalResellerDiscount,$invoiceLayout);
$invoiceLayout = str_replace('##VAT##',$VAT,$invoiceLayout);
$invoiceLayout = str_replace('##vatPercent##',$vatPercent,$invoiceLayout);
$invoiceLayout = str_replace('##vatTotal##',$vatTotal,$invoiceLayout);
$invoiceLayout = str_replace('##total##',$total,$invoiceLayout);
$invoiceLayout = str_replace('##Account_Volume_Discount__c##',$Account_Volume_Discount__c,$invoiceLayout);
if ($resellerID != $oppAccountID && $firstAccountName != $secondAccountName) $invoiceLayout = str_replace('##ResellerDiscount##',$ResellerDiscount,$invoiceLayout);
else $invoiceLayout = str_replace('##ResellerDiscount##','',$invoiceLayout);


$this->update_opportunity_invoice($oppid,$rate,$poNum,intval($invoiceNumber));
// To send HTML mail, the Content-type header must be set
// $headers  = 'MIME-Version: 1.0' . "\r\n";
// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
// $headers .= 'From:  <>' . "\r\n";


//require_once 'templates/yoo_air/PHPMailerAutoload.php';

//$success = externalSendEmail(JRequest::getVar("email"), $firstAccountName . ' ' . $secondAccountName , 'IncrediBuild - Invoice / receipt number: '.intval($invoiceNumber), $invoiceLayout, 'sales@incredibuild.com', 'IncrediBuild', '', '', 'sales@incredibuild.com', $opp[2]->Licensee_Email__c );
		
// TEMP
// if( ($invoiceNumber && $invoiceNumber !== 1 ) || JRequest::getVar('Response')=="1337" ) // END TEMP
// {
	// // $mail =& JFactory::getMailer();
	// // $config =& JFactory::getConfig(); 
	// // $mail ->isHTML(true); 
	// // $mail->setSender("sales@incredibuild.com");
	// // $mail->addRecipient(JRequest::getVar("email"));
	// // $mail->setSubject('IncrediBuild - Invoice / receipt number: '.intval($invoiceNumber)); 
	// // $mail->setBody( $invoiceLayout ); 
	// // $mail->Send();
	// //$success = externalSendEmail(JRequest::getVar("email"), $firstAccountName . ' ' . $secondAccountName , 'IncrediBuild - Invoice / receipt number: '.intval($invoiceNumber), $invoiceLayout, 'sales@incredibuild.com', 'IncrediBuild');
	// //$success = externalSendEmail('sales@incredibuild.com', $firstAccountName . ' ' . $secondAccountName , 'IncrediBuild - Invoice / receipt number: '.intval($invoiceNumber), $invoiceLayout, 'sales@incredibuild.com', 'IncrediBuild' );
// }

// if(JRequest::getVar('Response')=="1337" ){
	// echo "<br>Invoice Mail Finished.<br>";
	// die();
// }

// $headers  = 'MIME-Version: 1.0' . "\r\n";
// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
// $headers .= 'From: IncrediBuild <sales@incredibuild.com>' . "\r\n";
// $headers .= 'Bcc: sales@incredibuild.com' . "\r\n";

$Licensee_Email__c = $opp[0]->records[0]->Licensee_Email__c;		
// $to  = $opp[5]->records[0]->Email . ', '; // note the comma
// $to .=  $Licensee_Email__c;

// $mail =& JFactory::getMailer();
// $config =& JFactory::getConfig(); 
// $mail ->isHTML(true); 
// $mail->setSender("sales@incredibuild.com");
// $mail->addBCC("sales@incredibuild.com");
// $mail->addRecipient($Licensee_Email__c);
// $mail->addRecipient($opp[5]->records[0]->Email);
// $mail->setSubject('IncrediBuild - Invoice / receipt number: '.intval($invoiceNumber)); 
// $mail->setBody( $invoiceLayout ); 
// $mail->Send();

// $success = externalSendEmail(
							// 'joel.blankchtein@incredibuild.com', 
							// '' , 
							// 'IncrediBuild - Invoice / receipt number: '.intval($invoiceNumber) . ' - TEST ADDITION', 
							// $invoiceLayout . '<br/><br/><br/><pre>' . print_r($opp[5]->records[0]->Email , TRUE) . '</pre><br/><br/><pre>' . print_r($Licensee_Email__c , TRUE) . '</pre><br/><br/>' . JRequest::getVar("email"), 
							// 'sales@incredibuild.com', 
							// 'IncrediBuild'
							// );
					
		// sleep(5);
		
		
$success = externalSendEmail(
							$opp[5]->records[0]->Email, 
							$firstAccountName . ' ' . $secondAccountName , 
							'IncrediBuild - Invoice / receipt number: '.intval($invoiceNumber), 
							$invoiceLayout, 
							'sales@incredibuild.com', 
							'IncrediBuild'
							);

		sleep(5);
		
$additional_emails_list = explode(';', $Licensee_Email__c);

foreach ($additional_emails_list as $additional_email) {
    
	$success = externalSendEmail(
							$additional_email, 
							$firstAccountName . ' ' . $secondAccountName , 
							'IncrediBuild - Invoice / receipt number: '.intval($invoiceNumber), 
							$invoiceLayout, 
							'sales@incredibuild.com', 
							'IncrediBuild'
							);
					
		sleep(5);
}							

		

$success = externalSendEmail(
							'sales@incredibuild.com', 
							$firstAccountName . ' ' . $secondAccountName , 
							'IncrediBuild - Invoice / receipt number: '.intval($invoiceNumber), 
							$invoiceLayout, 
							'sales@incredibuild.com', 
							'IncrediBuild'
							);
	
//mail('sales@incredibuild.com','IncrediBuild - Invoice / receipt number: '.intval($invoiceNumber), $invoiceLayout,$headers);

require_once (getcwd().'/invoiceLayout/dompdf/dompdf_config.inc.php');

 // $dompdf = new DOMPDF();
// $dompdf->load_html($invoiceLayout);
// $dompdf->render();
// $dompdf->stream("sample.pdf");

//end of custom invoice for client		
  
		return $Response;
	}
	
	
	public function update_opportunity_status($oppid,$status)
	{
		$this->connect();
		
		$update = new stdClass;
		$update->type  ='opportunity';
		
		$update->fields = array ( "id" => $oppid ,"StageName" => $status);
		
		$Response = $this->sforceClient->update(array($update->fields),'opportunity');

		return $Response;
	}
	
	public function approveQuote($opt_id,$q,$ib_info)
	{
		
		if (!$opt_id)
		{
			$ib_info = $ib_info || explode("|",IbonlinestoreHelper:: decrypt(JFactory::getSession()->get('ib')));
			$opt_id = $ib_info[3];
			$q = JFactory::getSession()->get('q');
		}		

		$app_items = $this->get_newlineitems($opt_id);
			

		foreach ($app_items->records as $item)
		{
			$tmp[] = $item->Id;
		}
		$this->delete_objects($tmp);//remove old items from opportunity
				
		$qoute_items = $this->get_newQlineitems($q);


		//print_r($qoute_items);
		//echo "------<br>";
		
		foreach ($qoute_items->records as $item)
		{
				$data[] = array(
							"OpportunityId" 	=> 	$opt_id,
							"License__c"		=>  $item->License__c,
							"PricebookEntryId"	=>	$item->PricebookEntryId,
							"Quantity"			=>	$item->Quantity,
							"UnitPrice"			=>  $item->UnitPrice,
							"CreatedFromQuoteId__c" => $q
				);
		}

		$sf_info = $this->create_lineitems($data);
		
		////print_r($sf_info);
		//echo "------<br>";
		
	}
	
	
	public function request_new_quote($items)
	{
		$ib_info = explode("|",IbonlinestoreHelper:: decrypt(JFactory::getSession()->get('ib')));
				
		$app_items = $this->get_newlineitems($ib_info[3]);
				
		foreach ($app_items->records as $item)
		{
			$tmp[] = $item->Id;
		}
		$this->delete_objects($tmp);//remove old items from opportunity	

		
		$qoute_items = $this->get_newQlineitems(JFactory::getSession()->get('q'));

		//preper items ids
		$new_qoute = array();
		foreach ($items as $item)
		{
			$item_info = explode("|",IbonlinestoreHelper:: decrypt($item['details']));
			
			$new_qoute_items[$item_info[7]] = $item['qty'];
		}
		
		//check existings only
		$items_num = count($qoute_items->records);
		for ($i=0;$i<$items_num;$i++)//update qty
		{
			if (array_key_exists($qoute_items->records[$i]->Id, $new_qoute_items))
			{
				$qoute_items->records[$i]->Quantity = $new_qoute_items[$qoute_items->records[$i]->Id];
			}
			else
				unset($qoute_items->records[$i]);
		}
		
		

		
		foreach ($qoute_items->records as $item)
		{
				$data[] = array(
							"OpportunityId" 	=> 	$ib_info[3],
							"License__c"		=>  $item->License__c,
							"PricebookEntryId"	=>	$item->PricebookEntryId,
							"Quantity"			=>	$item->Quantity,
							"UnitPrice"			=>  $item->UnitPrice,
							"CreatedFromQuoteId__c" => JFactory::getSession()->get('q')
				);
				
				$Pricebook2ID = $item->Quote->Pricebook2Id;
		}

		$sf_info = $this->create_lineitems($data);
		
		

		$fields = array(
			"OpportunityId" => $ib_info[3],
			"Name" => "New Requested Quote",
			"Status" => "Requested by Customer",
			"Pricebook2ID"	=> $Pricebook2ID
		);
		
		$data = $this->create_object("Quote", $fields);

		
		$html = "<h1>Thank you for your request to update the IncrediBuild price offer.</h1>";
		 
		$html .= "We have received your request and a member of our sales team will get back to you on this matter.";
		$html .= "For inquiries, send mail to sales@incredibuild.com.<br>";

		$html .= "<br><br>Thank you for your business,";

		$html .= "<br>Xoreax / IncrediBuild ";
		$html .= "<br>www.incredibuild.com";
		
		$html1 = "<h1>A new quote has been requested.</h1>";
		 
		$html1 .= "<a href='https://cs18.salesforce.com/".$data[0]->id."'>https://cs18.salesforce.com/".$data[0]->id."</a><br>";

		$html1 .= "<br>Xoreax / IncrediBuild ";
		$html1 .= "<br>www.incredibuild.com";

		$info = IbonlinestoreHelper:: userInfo();
		
		$mail =& JFactory::getMailer();
		$config =& JFactory::getConfig(); 
		$mail ->isHTML(true); 
		$mail->addRecipient("sales@xoreax.com");
		$mail->setSubject( 'A new quote request' ); 
		$mail->setBody( $html1 ); 
		$mail->Send();	
		
		
		$mail2 =& JFactory::getMailer();
		$config2 =& JFactory::getConfig(); 
		$mail2->isHTML(true); 
		$mail2->addRecipient($info->email);
		$mail2->setSubject( 'A new quote request' ); 
		$mail2->setBody( $html ); 
		$mail2->Send();	
		
	}
	
	
	public function update_quote_status($quote_id,$status)
	{
		$this->connect();
		
		$update = new stdClass;
		$update->type  ='Quote';
		
		$update->fields = array ( "id" => $quote_id ,"Status " => $status);
		
		$Response = $this->sforceClient->update(array($update->fields),'Quote');
		
		//print_r($Response);
		//echo "------<br>";
		
		return $Response;
	}
	
	
	
	public function update_opportunity_invoice($oppid,$rate,$poNum,$invoiceNumber,$type = 'ir')
		{
		
							$session = & JFactory::getSession();
							if (JFactory::getSession()->get('ponum') && JFactory::getSession()->get('ponum') != '') {
							$poNum = JFactory::getSession()->get('ponum');
							}
							elseif (JRequest::getVar('ponum') && JRequest::getVar('ponum') != '') {
							$poNum = JRequest::getVar('ponum');
							}
			
	//			if ($type=='ir') $dueDate = date('n/d/Y',strtotime('last day of next month'));

		$this->connect();
		$update = new stdClass;
		$update->type  ='opportunity';


		if ($type == 'ir') $recieptNum = $invoiceNumber;
		else $recieptNum = '';
		if ($type == 'ir') $update->fields = array ( "id" => $oppid, "Invoice_Number__c" => $invoiceNumber ,"Receipt_Number__c" => $recieptNum, "PO_Confirmation_num__c" => $poNum, "USD_NIS_Conv_Rate_Invoice__c" => $rate);
		else	$update->fields = array ( "id" => $oppid, "Invoice_Number__c" => $invoiceNumber ,"Receipt_Number__c" => $recieptNum, "PO_Confirmation_num__c" => $poNum, "USD_NIS_Conv_Rate_Invoice__c" => $rate, "Payment_Due_Date__c" => date('Y-m-d',strtotime('last day of next month')));
	//the difference between IR and non IR is the payment due date
		$Response = $this->sforceClient->update(array($update->fields),'opportunity');

		return $Response;
	}
	
	
	
	public function update_opportunity_email($oppid,$email)
	{
		$this->connect();
		
		$update = new stdClass;
		$update->type  ='opportunity';
		
		$update->fields = array ( "id" => $oppid ,"Licensee_Email__c" => $email);
		
		$Response = $this->sforceClient->update(array($update->fields),'opportunity');

		return $Response;
	}
	
	public function update_lead_owner($leadid,$ownerid)
	{
		$this->connect();
		
		$update = new stdClass;
		$update->type  ='lead';
		
		$update->fields = array ( "id" => $leadid ,"OwnerId" => $ownerid);
		
		$Response = $this->sforceClient->update(array($update->fields),'lead');

		return $Response;
	}
	
	public function create_lineitems($data)
	{
		$this->connect();
		
		$info = array();
		foreach ($data as $d)
		{
				array_push($info,$d);
		}
		$result = $this->sforceClient->create($info, "OpportunityLineItem");

		return $result;
	}
	
	
	public function delete_objects($ids)
	{
		$this->connect();

		$result = $this->sforceClient->delete($ids);

		 return $result;
	}
	
	
	public function create_object($type,$fields = array(),$useDefaultRule = false)
	{
		$lead = new stdClass;
		 $lead->type = $type;
		 $lead->fields = $fields;
		 
		 
		 if (!$useDefaultRule)
			 $result = $this->sforceClient->create(array($lead->fields), $type);
		 else
		 {
			$header = new stdClass;
			$header->assignmentRuleId = NULL;
			$header->useDefaultRuleFlag = TRUE;
			$this->sforceClient->setAssignmentRuleHeader($header);
	  
			$result = $this->sforceClient->create(array($lead->fields), $type);
		}
		 
		 return $result;
	}
	
	
	public function get_opportunity_id($accountid)
	{
		$this->connect();
		
		$Query = "select id, Name,AccountId,Amount from opportunity ";
		$Response = $this->sforceClient->query($Query);
	
		
		die();
	}
    
	
	public function get_contact_by_email($acc_email)
    {
		$this->connect();
		$Query = "select Name,email, AccountId from contact where email='".$acc_email."'";
		
		$Response = $this->sforceClient->query($Query);
	    return $Response;
		
	}
    
	public function get_opportunity_info($opp_id)
    {
	   

		$this->connect();
		$Query = "select id,Comments_for_customer_invoice__c, Licensee_Email__c,  Billed_Entity__c, VAT__c, PO__c,  PO_Confirmation_Num__c, accountid, CloseDate, Invoice_Number__c, Payment_Due_Date__c, Name,ResellerLookup__c, Invoice_Receipt__c,Amount,Billed_Entity_Account__c from opportunity  where id='".$opp_id."'";
		$Response = $this->sforceClient->query($Query);
        $res[] = $Response;
        $accountId = $Response->records[0]->AccountId;
		$reseller_id = $Response->records[0]->ResellerLookup__c;
		
        $Query = "select Formatted_Date_checks__c,Check_Number__c, Check_Bank_name__c, Check_bank_branch_Snif__c,Check__c, type__c,id, USD_Amount__c, Name,Credit_Card_Company__c,Credit_Card_number__c,Date__c,Formatted_Amount__c from Incoming_Payment__c where id='".$Response->records[0]->Invoice_Receipt__c."'";
		$Response = $this->sforceClient->query($Query);
        $res[] = $Response;
		$paymentID = $Response->records[0]->Id;
		
        
        $Query = "select Name, Account_Volume_Discount__c, AccountNumber__c, billingaddress, AccountNumber, VolumeDiscountOverride__c from account where id='".$accountId."'";
		$Response = $this->sforceClient->query($Query);
		$Response->records[0]->BillingAddress->Name = $Response->records[0]->Name;
		$Response->records[0]->BillingAddress->Account_Volume_Discount__c = $Response->records[0]->Account_Volume_Discount__c;
		
		$Response->records[0]->BillingAddress->AccountNumber = $Response->records[0]->AccountNumber__c;

		
		$Response->records[0]->BillingAddress->VolumeDiscountOverride__c = $Response->records[0]->VolumeDiscountOverride__c;
		$res[] = $Response->records[0]->BillingAddress;	
        $reseller =  ($res[0]->records[0]->ResellerLookup__c);
		$Billed_Entity__c = $res[0]->records[0]->Billed_Entity__c;

		if 	($Billed_Entity__c && $Billed_Entity__c != '') $reseller = $Billed_Entity__c;
		$Query = "select Name, billingaddress, VolumeDiscountOverride__c from account where id='".$reseller."'";
		$Response = $this->sforceClient->query($Query);
		$Response->records[0]->BillingAddress->Name = $Response->records[0]->Name;
		
		$Response = $this->sforceClient->query($Query);
	
		$res[] = $Response;
		
		
		$Query = "select id,PO__c,accountid, Payment_Due_Date__c, Name,ResellerLookup__c, Invoice_Receipt__c,Amount from opportunity  where Invoice_Receipt__C='".$paymentID."'";
	 	$Response = $this->sforceClient->query($Query);
        $res[] = $Response;
		
		$Query = "select Id, IsPrimary, ContactId, OpportunityId from OpportunityContactRole where IsPrimary = true AND OpportunityId='".$opp_id."' ";
		$Response = $this->sforceClient->query($Query);
		
		$newContactID = $Response->records[0]->ContactId;
		$Query = "select Name,email, AccountId from contact where id='".$newContactID."'";
		$Response = $this->sforceClient->query($Query);
	    $res[] = $Response;
		
		$Query = "select Name, BillingAddress from account where id='".$reseller_id."'";
		$Response = $this->sforceClient->query($Query);
	    $res[] = $Response;
		
		return $res; 
    }
	
	public function get_quote($quoteid)
	{
		$this->connect();

		$Query = "select ID,Name,OpportunityId from Quote where ID='".$quoteid."' and Available_in_Online_Store__c=true";
		$Response = $this->sforceClient->query($Query);
		
		return ($Response);
	}

	
	public function get_newQlineitems($quoteid)
	{
		$this->connect();
		$Query = "select Id,License__c,PricebookEntryId,PricebookEntry.Product2.Name,PricebookEntry.Product2.ProductCode,PricebookEntry.Product2.Family,PricebookEntry.Product2.Description,PricebookEntry.UnitPrice,Quantity,UnitPrice,TotalPrice,quote.Pricebook2ID from quotelineitem where  QuoteId='".$quoteid."' ";
		$Response = $this->sforceClient->query($Query);
	
		return ($Response);	
	}
	
	public function get_newlineitems($oppid)
	{
		$this->connect();
		$Query = "select id , PricebookEntryId,PricebookEntry.Product2.Name,PricebookEntry.Product2.ProductCode,PricebookEntry.Product2.Family,PricebookEntry.Product2.Description,PricebookEntry.UnitPrice,Quantity,UnitPrice,TotalPrice,Maintenancefor__c from OpportunityLineItem where  OpportunityId='".$oppid."' ";
		$Response = $this->sforceClient->query($Query);		

		return ($Response);	
	}
	
	public function convert_lead($leadID)
	{
	
	$this->connect();
	
	$leadConvert = new stdClass;
	$leadConvert->convertedStatus='Priority 1';
	$leadConvert->doNotCreateOpportunity='true';
	$leadConvert->leadId=$leadID;
	$leadConvert->overwriteLeadSource='true';
	$leadConvert->sendNotificationEmail='false';

	$leadConvertArray = array($leadConvert);

	$leadConvertResponse = $this->sforceClient->convertLead($leadConvertArray);

	return $leadConvertResponse;
  
	}
	

	public function create_download($id,$type)
	{
		$this->connect();
		
		if ($type=="1")//lead
		{
			$fields = array("Lead__c" => $id);
		}
		else//contact
			$fields = array("Contact__c" => $id);
		
		$data = $this->create_object("Download__c",$fields);
		
		return $data;
	}
	
	public function create_lead($fields,$autoassign = true)
	{
		$this->connect();

		$data = $this->create_object("lead",$fields,$autoassign);
		
		return $data;
	}
	
	
}
?>