<?php

header("Content-Type: text/plain");

class Document
{
    public $ClientID = 71027;
    public $DocumentType = 3; // invoice receipt
    public $Subject = "Test123";
    public $Currency = "ILS";
    public $Total = 100;
    public $TotalWithoutTax = 100;
    public $TotalTaxAmount = 0;
    public $TaxPercentage = 0;
    public $TaxIncluded =0; // means that the item price allready include tax, for example: if item Price = 118 ,so item Total = 118 and item TotalWithoutTax = 100 (in case that TaxPercentage = 18)
    public $ExternalComments = "";
    public $InternalComments = "";
    public $BranchID = null;
    public $IssueDate;
    public $ConversionRate = 1;
    public $Discount;
    public $RoundAmount = 0;
    public $Language = 2;
    public $Items;
    public $AssociatedEmails;
    public $Payments;

	public function __construct(
							$clientID, 
							$documentType, 
							$subject, 
							$currency, 
							$total, 
							$totalWithoutTax, 
							$totalTaxAmount, 
							$taxPercentage, 
							$exComments, 
							$inComments, 
							$branchId, 
							$issueDate, 
							$conversionRate, 
							$discount, 
							$roundAmount, 
							$language, 
							$items = Array(), 
							$associatedEmails = Array(), 
							$payment = null
							)
    {
		$this->ClientID = $clientID;
		$this->DocumentType = $documentType;
		$this->Subject = $subject;
		$this->Currency = $currency;
		$this->Total = $total;
		$this->TotalWithoutTax = $totalWithoutTax;
		$this->totalTaxAmount = $totalTaxAmount;
		$this->TaxPercentage = $taxPercentage;
		$this->ExternalComments = $exComments;
		$this->InternalComments = $inComments;
		$this->BranchID = $branchId;
		
		$this->ConversionRate = $conversionRate;
		$this->RoundAmount = $roundAmount;
		$this->Language = $language;
		
        if (isset($issueDate))
			$this->IssueDate = $issueDate;		
		else
			$this->IssueDate = date("c", time()); // can be at the Past no earlier then last invoice
		
		$this->Discount = $discount;
        $this->Items = $items;
        $this->AssociatedEmails = $associatedEmails;
        $this->Payments = $payment;
    }
}

class Discount{
    public $Value = 0;
    public $BeforeTax = true;// discount calculated before tax calculated
    public $IsNominal = true;// means that the discount is calculated according to currency type (in this case: 5 ILS), if it is false than discount calculated in percentages (in this case: 5%)
}
class Item
{
    public $Code = "001";
    public $Name = "first Item";
    public $Quantity = 1;
    public $Price = 100;
    public $Total = 100;
    public $TotalWithoutTax = 100;
    public $TaxPercentage = 0; // in case you want an item without tax
    public $Discount;

    public function __construct($code, $name, $quantity, $price, $total, $totalWithoutTax, $taxPercentage, $discount)
    {
		$this->Code = $code;
		$this->Name = $name;
		$this->Quantity = $quantity;
		$this->Price = $price;
		$this->Total = $total;
		$this->TotalWithoutTax = $totalWithoutTax;
		$this->TaxPercentage = $taxPercentage;
        $this->Discount = $discount;
    }
	
}

class Payment
{
    public $Date;
    public $Amount = 0;
    public $PaymentType = 4; // Cash

    public function __construct($amount, $paymentType = 4)
    {
        $this->Date = date("c", time());
		$this->Amount = $amount;
		$this->PaymentType = $paymentType;
    }
}

class Email
{
    public $Mail = "test@test.com";
    public $IsUserMail = true;// means that this is the user email, and this is the email that sends the document to all associated email
	
	public function __construct($mail= '', $isUserEmail = '')
    {
		$this->Mail = $mail;
		$this->IsUserMail = $isUserEmail;
	}
}

class Invoice4u
{
    public $token;
    public $result;

    // set connection to the service and return result
    private function requestWS($wsdl, $service, $params){
        try{
            $options = array('trace' => true, 'exceptions' => true, 'cache_wsdl' => WSDL_CACHE_NONE, 'connection_timeout' => 10);
            $client = new SoapClient($wsdl, $options);
            //var_dump($client->__getTypes()); exit;
            $response = $client->$service($params);
            $service = $service . "Result";
            $this->result = $response->$service;
            return $this->result;
        } catch (SoapFault $f) {
            echo $f->faultstring;
            return $f->faultstring;
        } catch (Exception $e) {
            echo $e->getMessage();
            return $e->getMessage();
        }
    }

    // login to get token
    public function Login()
    {
        $wsdl = "http://incredibuild.webmark.co.il/i4u/LoginService.svc?wsdl";
        $user = array('username' => 'test@test.com', 'password' => '123456', 'isPersistent' => false);

        $this->requestWS($wsdl, "VerifyLogin", $user);
        $this->token = $this->result;
    }

    // send doc and token object to service to create document
    public function CreateDocument()
    {
        $wsdl = "http://incredibuild.webmark.co.il/i4u/DocumentService.svc?wsdl";

        $doc = new Document();

        $this->result = $this->requestWS($wsdl, "CreateDocument", array('doc' => $doc, 'token' => $this->token));
    }
	
	public function CreateDocumentWith($doc){
        print_r($doc);
		$wsdl = "http://incredibuild.webmark.co.il/i4u/DocumentService.svc?wsdl";
        $this->result = $this->requestWS($wsdl, "CreateDocument", array('doc' => $doc, 'token' => $this->token));
		
		print_r($this->result);
		return $this->result;
    }
}
/*
$invoice = new Invoice4u();
$invoice->Login();
$invoice->CreateDocument();

var_dump($invoice->result);
*/
?>