<?php
/*------------------------------------------------------------------------
# register.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');
jimport( 'joomla.database.database.mysqli' );
	require_once 'php_inc/MCAPI.class.php';
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
/**
 * Register Model
 */
 
class IbonlinestoreModelRegister extends JModelList
{
	
	public function externalSendEmail($toAddress, $toName = '', $subject, $body, $from = '', $fromName = '', $reply = '', $replyName = '', $bcc = '', $secondaryAddress = '') {
			
		require_once (getcwd().'/templates/yoo_air/PHPMailerAutoload.php');

		$Mail = new PHPMailer();
		$Mail->IsSMTP(); // Use SMTP
		$Mail->Host        = "smtp.gmail.com"; // Sets SMTP server
		$Mail->SMTPDebug   = 0; // 2 to enable SMTP debug information
		$Mail->SMTPAuth    = TRUE; // enable SMTP authentication
		$Mail->SMTPSecure  = "tls"; //Secure conection
		$Mail->Port        = 587; // set the SMTP port
		$Mail->SMTPKeepAlive = FALSE;   
		$Mail->Username    = 'noreply@incredibuild.com'; // SMTP account username
		$Mail->Password    = 'xorPass77'; // SMTP account password
		$Mail->Priority    = 3; // Highest priority - Email priority (1 = High, 3 = Normal, 5 = low)
		$Mail->CharSet     = 'iso-8859-1';
		$Mail->Subject     = $subject;
		$Mail->ContentType = 'text/html; charset=utf-8\r\n';
		
		if ($bcc != '') {
			$Mail->AddBCC($bcc);
		}
		
		if ($from != '') {
			$Mail->From        = $from;
			if ($fromName != '') {
				$Mail->FromName        = $fromName;
			}
		}
		
		if ($reply != '') {
			$Mail->AddReplyTo($reply, $replyName);
		}
		
		$Mail->WordWrap    = 900; // RFC 2822 Compliant for Max 998 characters per line
		
		$Mail->AddAddress( $toAddress ); // To: //$toName
		
		
		// $Mail->AddCC('ron.ginton@incredibuild.com');
		// $Mail->AddCC( 'joel.blankchtein@incredibuild.com' ); 
		
		if ($secondaryAddress != '') {
			$Mail->AddAddress( $secondaryAddress ); 
		}
		
		$Mail->isHTML( TRUE );
		$Mail->Body = $body;
		
		//send the message, check for errors
		if (!$Mail->send()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return "1";
		}
		
		$Mail->SmtpClose();
	}
	
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select('*');
		// From the ibonlinestore_registe table
		$query->from('#__ibonlinestore_registe');

		return $query;
	}
	
	
	private function check_email($e){
		return (bool)preg_match("`^[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$`i", trim($e));
	}
	
	private function remember($info)
	{
	
		setcookie("ib",$info,time()+36000000);
		$this->setUser($info);
	}
	
	
	private function setUser($info)
	{
	
		$session = JFactory::getSession();
		$session->set('ib', $info);
		
	}
	
	
	public function logOut()
	{
	
	setcookie("ib","",time()-360000);
	
	$session = JFactory::getSession();
	$session->clear('ib');
	
	JFactory::getApplication()->redirect($_SERVER[HTTP_REFERER]);
	
	}
	
	private function get_adWords_arr()
	{
		
		$tmp = explode ("?",$_COOKIE['adWords']);

		if ($tmp[1]!="")
		{

			$arr = explode ("&",$tmp[1]);
			
			foreach ($arr as $r)
			{
				$t = explode("=",$r);
				$return[$t[0]] = urldecode($t[1]);
			}
			
			return $return;

		}

		return array();
	}
	
	public function i4u_receipt_only($data){
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		
		$invoiceNumber = $data['InvoiceNumber'];
		$paymentType = $data['paymentType'];
		$name = $data['userName'];
		$invoiceSubject = 'IncrediBuild order receipt: '.$name;
		$notes = $data['InvoiceComments'];
		$CompanyCode = $data['CompanyCode'];

		//$rate = @file_get_contents("http://dev.webmark.co.il/boi.php");
		$rate = @file_get_contents("https://www.incredibuild.com/boi.php");
		if($rate == 'error' || $rate === false){
			$rate = file_get_contents('boi_backup.txt');
		} else {
			file_put_contents('boi_backup.txt', $rate);
		}

		$originalRate = $rate;		
		$intDollar = floatval ($data['InvoiceItemPrice']);
		$rate = floatval ($rate);
		$intNIS = $intDollar * $rate;
		
		$fields = array (
			'Key' => '',
			'ReceiptSubject' => $invoiceSubject,
			'InvoiceNumber' => $invoiceNumber,
			'InvoiceAmount' => $intDollar,
			'NonInvoiceAmount' => $intDollar,
			'ReceiptComments' => $notes,
			'CompanyCode'=> $CompanyCode ,
			'Username' => '4xoreaxacc',
			'Deduction' => '0',
			'IsForceInvoiceClose' => '1',
			'MailTo' => 'ron.ginton@incredibuild.com'
		);

		$headers = array (
			"Connection: keep-alive",
			"User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.162 Safari/535.19",
			"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Language: en-US,en;q=0.8",
			"Accept-Charset: utf-8;q=0.7,*;q=0.3" 
		);

		$fields_string = http_build_query ( $fields );
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, 'https://account.invoice4u.co.il/Public/w_receipt.asmx/CREATE' );
		curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 80 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		$str = curl_exec ( $ch );
		curl_close ( $ch );
		
		echo "</center>Created!<br>";
		echo "<button onclick='window.opener.location.href = window.opener.location ;window.close();'>Close</button>  </center>";
		
		exit;

	}
	
	public function appriveQuoteInvoice($opt_id)
	{
	 
		try {
		$size = ob_get_length();
		header("Content-Length: $size");
		header('Connection: close');
		ob_end_flush();
		ob_flush();
		flush();

		if (session_id()) 
			session_write_close();
					
		$sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');

		$opp = $sf->get_opportunity_info($opt_id);					

			// print_r($opp);
			// return;
					
		$cart = $sf->get_newlineitems($opt_id);
		$cart = $cart->records;
		$poNum = $_REQUEST['index'];
		$poNum = $opp[0]->records[0]->PO_Confirmation_num__c;
		if (JFactory::getSession()->get('ponum') && JFactory::getSession()->get('ponum') != '') {
			$poNum = JFactory::getSession()->get('ponum');
		}				
	
		//extract address in order to create user in i4u
		$country = $opp[3]->records[0]->BillingAddress->country;
		$city = $opp[3]->records[0]->BillingAddress->city;
		$postalCode = $opp[3]->records[0]->BillingAddress->postalCode;
		$state = $opp[3]->records[0]->BillingAddress->state;
		$street = $opp[3]->records[0]->BillingAddress->street;
		$name = $opp[3]->records[0]->Name;
		$amount = $opp[0]->records[0]->Amount;
		$AccountNumber = $opp[0]->records[0]->Billed_Entity_Account__c;

		$rate = @file_get_contents("http://dev.webmark.co.il/boi.php");
		if($rate == 'error' || $rate === false){
			$rate = file_get_contents('boi_backup.txt');
		} else {
			file_put_contents('boi_backup.txt', $rate);
		}
		//$rate = $rate->CURRENCY[0]->RATE;
		$originalRate = $rate;
		
		$intDollar = floatval ($amount);
		$rate = floatval ($rate);
		$intNIS = $intDollar * $rate;

		//create user in i4u
		
		$fields = array (
			'CcAmount' => '',
			'InvoiceInternalComment' => '',
			'TransType' => 'IR:CREATE',
			'CurrencyTarget' => '60',
			'Key' => '',
			'InvoiceItemCode' => '999',
			'InvoiceItemQuantity' => '1',
			'InvoiceTaxRate' => '0',
			'InvoicePayByDate' => '',
			'InvoiceDiscount' => '',
			'InvoiceDiscountRate' => '0',
			'Username' => '4xoreaxacc',        
			'CcDate' => '',
			'InvoiceSubject' => 'IncrediBuild online order: '.$PaymentName,        
			'InvoiceItemDescription' => $_GET["notes"].'00000',
			'InvoiceItemQuantity' => '1',
			'InvoiceItemPrice' => $intNIS,
			'InvoiceTaxRate' => '0',
			'InvoiceComments' => '$'.$intDollar.' '.$opp[2]->Name,
			'CompanyCode' => $AccountNumber,
			'CcNumber' => '',
			'CcType' => '',
			'CcVerification' => '',
			'CcTicket' => '',
			'CcExpiration' => '',
			'CompanyInfo' => '',
			'mailto' => '',
			'IsItemPriceWithTax' => '',
			'Cash' => '',
			'CheckDate' => '',
			'CheckNumber' => '',
			'checkAccount' => '',
			'CheckBranch' => '',
			'CheckAmount' => '',
			'TransDate' => '',
			'TransBank' => '',
			'TransBranch' => '',
			'TransAccount' => '',
			'TransAmount' => '',
			'CheckBank' => ''
			
		);

		$headers = array (
			"Connection: keep-alive",
			"User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.162 Safari/535.19",
			"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
			"Accept-Language: en-US,en;q=0.8",
			"Accept-Charset: utf-8;q=0.7,*;q=0.3" 
		);

		$fields_string = http_build_query ( $fields );
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, 'https://account.invoice4u.co.il/Public/w_invoice.asmx/CREATE' );
		curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 80 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		$str = curl_exec ( $ch );
		curl_close ( $ch );
		
		$xmlstr = str_replace('<?xml version="1.0" encoding="utf-8"?>',"",$str);
		$xml = simplexml_load_string($xmlstr);
		$xml_children = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children();
		$invoiceNumber_tmp = json_decode(json_encode($xml_children->I->I[2]->Value));
		$invoiceNumber = $invoiceNumber_tmp->{'0'};

		//end of create client, now create i4u invoice
		$PaymentName = $opp[1]->records[0]->Name;
		
		$invoiceSubject = 'IncrediBuild online order: '.$PaymentName;

		
		$productDiscount = $opp[2]->VolumeDiscountOverride__c;
		$ResellerDiscount = $opp[3]->records[0]->VolumeDiscountOverride__c;		
		if ($ResellerDiscount && $ResellerDiscount > 0){
			$ResellerDiscount = 'Reseller Discount: '.$ResellerDiscount.'%';
		} else {
			$ResellerDiscount = '';
		}
		
		$info = array();
		$productDiscount = $opp[2]->VolumeDiscountOverride__c;
		$ResellerDiscount = $opp[3]->records[0]->VolumeDiscountOverride__c;		
		$invoiceProductsHTML = '';
		$netSumBeforeReseller = 0;
        foreach ($cart as $item) {
		
            $info["itemDesc"][] = $item->PricebookEntry->Product2->ProductCode.' - '.$item->PricebookEntry->Product2->Name;
			$info["itemPrice"][] = $item->UnitPrice;
			$info["itemQty"][] = $item->Quantity;
			$discountPercentage = 100-($item->UnitPrice / $item->PricebookEntry->UnitPrice * 100);
			$invoiceProductsHTML .= '<tr>
			<td>'.$item->PricebookEntry->Product2->ProductCode.'</td>
			<td><span class="bold">'.$item->PricebookEntry->Product2->Family.'</span><br/>
				'.$item->PricebookEntry->Product2->Name.'
			</td>
			<td>'.$item->Quantity.'</td>
			<td>$'.$item->PricebookEntry->UnitPrice.'</td>
			<td>$'.$item->UnitPrice.'</td>
			<td>$'.number_format($item->TotalPrice,2).'</td>';
			$netSumBeforeReseller = $netSumBeforeReseller + $item->TotalPrice;			
        }   
		$country = $opp[6]->records[0]->BillingAddress->country;
		$city = $opp[6]->records[0]->BillingAddress->city;
		$postalCode = $opp[6]->records[0]->BillingAddress->postalCode;
		$state = $opp[6]->records[0]->BillingAddress->state;
		$street = $opp[6]->records[0]->BillingAddress->street;
		$name = $opp[6]->records[0]->Name;
		$amount = $opp[0]->records[0]->Amount;
		$intDollar = str_replace(',','',$amount);
		$intDollar = $amount;

		
		$creditCardNumber = $opp[1]->records[0]->Credit_Card_number__c;
		$creditCardCompany = $opp[1]->records[0]->Credit_Card_Company__c;                                
		$payment_due = $opp[0]->records[0]->Payment_Due_Date__c;
		//      $payment_due = strtotime($payment_due);
		//      $payment_due = date('d-m-Y',$payment_due);
		//      echo '<br/>'.$payment_due;
		//      $payDate = $opp[1]->records[0]->Date__c;
        //      $payDate =  strtotime($payDate);
                //change date arrangment
		$payDate = date('Y-m-d',$payDate);

		$rate = @file_get_contents("http://dev.webmark.co.il/boi.php");
		if($rate == 'error' || $rate === false){
			$rate = file_get_contents('boi_backup.txt');
		} else {
			file_put_contents('boi_backup.txt', $rate);
		}
		//$rate = $rate->CURRENCY[0]->RATE;
		
		$intDollar = floatval ($intDollar);
		$rate = floatval ($rate);

		$intNIS = $intDollar * $rate;

		$ccdate = explode('/',$_REQUEST['CcDate']);
		//$ccdate[2] = '20'.$ccdate[2];
		$ccdate = array_reverse($ccdate);
		$ccdate = implode('-',$ccdate);
		$_REQUEST['CcDate'] = $ccdate;
		$_REQUEST['CcAmount'] = str_replace('$','',$_REQUEST['CcAmount']);
		
		$invoiceSubject = 'IncrediBuild online order: '.$_REQUEST['InvoiceSubject'];
		
		$_REQUEST['CcAmount'] = str_replace(',','',$_REQUEST['CcAmount']);
		$_REQUEST['InvoiceItemPrice'] =	str_replace(',','',$_REQUEST['InvoiceItemPrice']);
		
		ini_set('set_time_limit', '5');  
		ini_set('max_execution_time', '5');                  
		$intNIS = intval ($intNIS);
		$fields = array(
		'compID'=>'xoreax',  
		//'sendOrig' => 'sales@incredibuild.com',
		'sendOrig' => 'sales@incredibuild.com',
		'user'=>'sales',  
		'pass'=>'828d66',
		'maampercent'=>'0',  
		'dateissued'=>date('Ymd'),  
		'clientname'=>$name,  
		'docType'=>'deal',
		'currency'=>'2', 
		'rate' => $rate,
		'client_street' => $street,
		'client_country' => $country,
		'client_city' => $city,
		  'validity' => $payment_due,  
		'desc'=>$info['itemDesc'],
		'unitprice'=>$info['itemPrice'],
		'quantity'=>$info['itemQty'],
		'paid'=>$intNIS,
		'totalsum'=>$intNIS,
		'show_response'=>'1',
		'debug'=>'yes',
		'hidenis'=>'1'
		);
		$country = $opp[3]->records[0]->BillingAddress->country;
		$vatPercent = '0.00';
				
		$productDiscount = $opp[2]->VolumeDiscountOverride__c;
		$ResellerDiscount = $opp[3]->records[0]->VolumeDiscountOverride__c;		
		//if ($ResellerDiscount && $ResellerDiscount > 0) $ResellerDiscount = 'Reseller Discount: '.$ResellerDiscount.'%';
		if ($ResellerDiscount && $ResellerDiscount > 0){
			$ResellerDiscount = 'Reseller Discount: '.$ResellerDiscount.'%';
		}
		else{
			$ResellerDiscount = '';
		}	
		
		if ($country == 'Israel'  || $country == 'ישראל') {
			$vatPercent = 0.18;
			$vat = 18;
			$fields['maampercent'] = $vat;
			$fields['paidincvat']= $intNIS * (1 + $vatPercent);   
			$fields['totalvat'] = $intNIS * $vatPercent;
			$fields['totalwithvat']= $intNIS * (1 + $vatPercent);   
			$fields['taxexempt'] = '0';
			$fields['hwc'] = 'עבור הזמנה מספר:: '.$poNum;
			$fields['es'] = 'חשבון עסקה עבור הזמנה מספר: '.$poNum;
		}
		else {
			$fields['es'] = 'Invoice for Purchase Order number: '.$poNum;
			 $fields['hwc'] = 'FOR PURCHASE ORDER NUMBER: '.$poNum;
			$fields['lang'] = 'en';
			$fields['taxexempt'] = '1';
			$fields['totalpaid'] = $intNIS;
		};
				
		$CloseDate =  $opp[0]->records[0]->CloseDate;
		$CloseDate = strtotime($CloseDate);
		$resellerID = $opp[0]->records[0]->ResellerLookup__c;
		$oppAccountID = $opp[0]->records[0]->AccountId;
		$Licensee_Email__c = $opp[0]->records[0]->Licensee_Email__c;
		$firstAccountName = $opp[2]->Name;
		$secondAccountName = $opp[3]->records[0]->Name;
		$AccountNumber = $opp[2]->records[0]->AccountNumber;
		$AccountNumber = $opp[2]->AccountNumber;

		if ($VolumeDiscountOverride__c && $VolumeDiscountOverride__c != 0) $VolumeDiscountOverride__c = '<div>'.$VolumeDiscountOverride__c.'% Reseller Discount:</div>';
		else $VolumeDiscountOverride__c = '';
		if ($resellerID == $oppAccountID) $clientTitle = '<div class="bold">'.$firstAccountName.'</div>';
		else $clientTitle = '<div class="bold">'.$secondAccountName.'</div>			<div>For end user: <span class="bold">'.$firstAccountName.'</span></div>';

		if ($VolumeDiscountOverride__c) $totalBeforeTax = $netSumBeforeReseller - $totalResellerDiscount;
		else $totalBeforeTax = $netSumBeforeReseller;

		$VAT = $opp[0]->records[0]->VAT__c;
		if ($VAT) {
			$VATPercentage = 1 + $VAT / 100;
			$vatPercent	= '<div>'.$VAT.'% Tax:</div>';
			$netSumBeforeResellerAfterVAT = $totalBeforeTax * $VAT / 100;
			$vatTotal = '<div>$'.$netSumBeforeResellerAfterVAT.'</div>';
			$total = $totalBeforeTax - $netSumBeforeResellerAfterVAT;
		}
		else {
			$vatPercent = '';
			$vatTotal = '';
			$total = $totalBeforeTax;
		}
		
		$total = number_format($total,2);

		if ($opp[2]->VolumeDiscountOverride__c && $opp[2]->VolumeDiscountOverride__c > 0) {
			$opp[2]->Account_Volume_Discount__c = $opp[2]->VolumeDiscountOverride__c;
		}
		
		$Account_Volume_Discount__c = $opp[2]->Account_Volume_Discount__c;
		
		if ($Account_Volume_Discount__c && $opp[2]->Account_Volume_Discount__c > 0) {
			$Account_Volume_Discount__c = 'Discount: '.$opp[2]->Account_Volume_Discount__c.'%';
		}

		$CloseDate = date('F d, Y',$CloseDate);
		$file = getcwd().'/invoiceLayout/invoice.html';
		$invoiceLayout = file_get_contents($file);
		$Comments_for_customer_invoice__c = $opp[0]->records[0]->Comments_for_customer_invoice__c;
		$invoiceLayout = str_replace('##Comments_for_customer_invoice__c##',$$Comments_for_customer_invoice__c,$invoiceLayout);
		$invoiceLayout = str_replace('##lastDigits##',JRequest::getVar("ccno"),$invoiceLayout);
		$invoiceLayout = str_replace('##street##',$street,$invoiceLayout);
		$invoiceLayout = str_replace('##city##',$city,$invoiceLayout);
		$invoiceLayout = str_replace('##state##',$state,$invoiceLayout);
		$invoiceLayout = str_replace('##postalCode##',$postalCode,$invoiceLayout);
		$invoiceLayout = str_replace('##country##',$country,$invoiceLayout);
		$invoiceLayout = str_replace('##invoiceNumber##',intval($invoiceNumber),$invoiceLayout);
		$invoiceLayout = str_replace('##CloseDate##',$CloseDate,$invoiceLayout);
		$invoiceLayout = str_replace('##$vatPercent##',$$vatPercent,$invoiceLayout);
		$invoiceLayout = str_replace('##intDollar##',$intDollar,$invoiceLayout);
		$invoiceLayout = str_replace('##invoiceProductsHTML##',$invoiceProductsHTML,$invoiceLayout);
		$invoiceLayout = str_replace('##clientTitle##',$clientTitle,$invoiceLayout);
		$invoiceLayout = str_replace('##poNum##',$poNum,$invoiceLayout);
		//$invoiceLayout = str_replace('##netSumBeforeReseller##',$netSumBeforeReseller,$invoiceLayout);
		$invoiceLayout = str_replace('##VolumeDiscountOverride__c##',$VolumeDiscountOverride__c,$invoiceLayout);
		$invoiceLayout = str_replace('##totalResellerDiscount##',$totalResellerDiscount,$invoiceLayout);
		$invoiceLayout = str_replace('##VAT##',$VAT,$invoiceLayout);
		$invoiceLayout = str_replace('##vatPercent##',$vatPercent,$invoiceLayout);
		$invoiceLayout = str_replace('##vatTotal##',$vatTotal,$invoiceLayout);
		$invoiceLayout = str_replace('##total##',$total,$invoiceLayout);
		$invoiceLayout = str_replace('##Account_Volume_Discount__c##',$Account_Volume_Discount__c,$invoiceLayout);
		
		if ($resellerID != $oppAccountID && $firstAccountName != $secondAccountName) {
			$invoiceLayout = str_replace('##ResellerDiscount##',$ResellerDiscount,$invoiceLayout);
		} else {
			$invoiceLayout = str_replace('##ResellerDiscount##','',$invoiceLayout);
		}

		$sf->update_opportunity_invoice($opt_id,$rate,$poNum,intval($invoiceNumber),'deal');
		// To send HTML mail, the Content-type header must be set
		// $headers  = 'MIME-Version: 1.0' . "\r\n";
		// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		// $headers .= 'From: IncrediBuild <sales@incredibuild.com>' . "\r\n";
//		$headers .= 'Bcc: sales@incredibuild.com' . "\r\n";

		// $to  = $opp[5]->records[0]->Email . ', '; // note the comma
		// $to .=  $	Licensee_Email__c;
		// $mail =& JFactory::getMailer();
		// $config =& JFactory::getConfig(); 
		// $mail ->isHTML(true); 
		// $mail->setSender(array("sales@incredibuild.com","IncrediBuild"));
		// $mail->addRecipient($opp[5]->records[0]->Email);
		// $mail->addRecipient($	Licensee_Email__c);
 // $mail->addBCC('sales@incredibuild.com');
		// $mail->setSubject( 'IncrediBuild - Invoice number: '.intval($invoiceNumber) ); 
		// $mail->setBody( $invoiceLayout ); 
		// $mail->Send();	
		
		$success = externalSendEmail(
					$opp[5]->records[0]->Email,
					$firstAccountName . ' ' . $secondAccountName , 
					'IncrediBuild - Invoice Number: '.intval($invoiceNumber), 
					$invoiceLayout,
					'sales@incredibuild.com',
					'IncrediBuild'
					);
					
		sleep(5);
		
		$additional_emails_list = explode(';', $Licensee_Email__c);
		
		foreach ($additional_emails_list as $additional_email) {
			$success = externalSendEmail(
						$additional_email,
						$firstAccountName . ' ' . $secondAccountName , 
						'IncrediBuild - Invoice Number: '.intval($invoiceNumber), 
						$invoiceLayout,
						'sales@incredibuild.com',
						'IncrediBuild'
						 );
						
			sleep(5);
		}							

		
		$success = externalSendEmail(
					'sales@incredibuild.com',
					$firstAccountName . ' ' . $secondAccountName , 
					'IncrediBuild - Invoice Number: '.intval($invoiceNumber), 
					$invoiceLayout,
					'sales@incredibuild.com',
					'IncrediBuild'
					 );
					 
		//$success = externalSendEmail('ron.ginton@incredibuild.com', 'Ron', 'IncrediBuild - Invoice number: '.intval($invoiceNumber), $invoiceLayout);
		//mail($to,'IncrediBuild - Invoice number: '.intval($invoiceNumber), $invoiceLayout,$headers);
		//print_r($success);
		$req_dump = print_r($opp, TRUE);
		//mail('yaniv@webmark.co.il','Incredibuild - test','test00000000'.$req_dump);
		require_once (getcwd().'/invoiceLayout/dompdf/dompdf_config.inc.php');


		// $dompdf = new DOMPDF();
		// $dompdf->load_html($invoiceLayout);
		// $dompdf->render();
		// $dompdf->stream("sample.pdf");

		//end of custom invoice for client		
						
		setcookie("ib",$info,time()-36000000);
		JFactory::getSession()->clear('ib');
		setcookie("cart",$info,time()-36000000);
		JFactory::getSession()->clear('cart');
		setcookie("sf_cart_items",$info,time()-36000000);
		JFactory::getSession()->clear('sf_cart_items');
		setcookie("q",$info,time()-36000000);
		JFactory::getSession()->clear('q');
		
		JFactory::getSession()->set('sent_invoice',"0");
		}
		catch (Exception $ex) {
		
				print_r($e);
		}
		
		return;
	 }
	 
	 private function ParseCookies()
	 {
		// Parse __utmz cookie
		list($domain_hash,$timestamp, $session_number, $campaign_numer, $campaign_data) = split('[\.]', $_COOKIE["__utmz"],5);

		// Parse the campaign data
		$campaign_data = parse_str(strtr($campaign_data, "|", "&"));

		$this->campaign_source = $utmcsr;
		$this->campaign_name = $utmccn;
		$this->campaign_medium = $utmcmd;
		if (isset($utmctr)) $this->campaign_term = $utmctr;
		if (isset($utmcct)) $this->campaign_content = $utmcct;

		// You should tag you campaigns manually to have a full view
		// of your adwords campaigns data.
		// The same happens with Urchin, tag manually to have your campaign data parsed properly.

		if (isset($utmgclid)) {
			$this->campaign_source = "google";
			$this->campaign_name = "";
			$this->campaign_medium = "cpc";
			$this->campaign_content = "";
			$this->campaign_term = $utmctr;
		}

		// Parse the __utma Cookie
		list($domain_hash,$random_id,$time_initial_visit,$time_beginning_previous_visit,$time_beginning_current_visit,$session_counter) = split('[\.]', $_COOKIE["__utma"]);

		$this->first_visit = date("d M Y - H:i",$time_initial_visit);
		$this->previous_visit = date("d M Y - H:i",$time_beginning_previous_visit);
		$this->current_visit_started = date("d M Y - H:i",$time_beginning_current_visit);
		$this->times_visited = $session_counter;

		// Parse the __utmb Cookie

		list($domain_hash,$pages_viewed,$garbage,$time_beginning_current_session) = split('[\.]', $_COOKIE["__utmb"]);
		$this->pages_viewed = $pages_viewed;

		// End ParseCookies
	 }
	
	public function sf_sync($sf_id)
	{
		if (!$sf_id)
			$sf_id = JRequest::getVar('sf_id');
			
		$sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');
		$data = $sf->get_contact_sync_info($sf_id);

		if (JRequest::getVar('win'))
		{
		?>
		<center>
		<script>
		window.resizeTo(400, 250);
		</script>
		<br><Br>
		<?php
		}
		if (!$data)
		{
			if (JRequest::getVar('sf_id'))
				die("JoomlaID already exists.");
			else
				echo "JoomlaID already exists ".$sf_id.".<br>";
		}
		
		unset ($data->type);
		unset ($data->fields);
		
		$flds = array(
			'first_name'=> 	$data->FirstName,
			'last_name'	=>	$data->LastName,
			'email'		=>	$data->Email,
			'telephone'	=>	$data->Phone,
			'user_name' => 	$data->FirstName."_".rand(111,11111),
			'password'	=> 	"IB".rand(11111,99999),
			'company' 	=> 	$data->Account->Name,
			//'street' 	=> 	$data->MailingAddress->street,
			'city'		=>	$data->MailingAddress->city,
			'state'		=>	$data->MailingAddress->state,
			'country'	=>	$data->MailingAddress->country	
		);
		
		//$landing_page = $_GET['landing'];
		$landing_page = JRequest::getVar('landing');
		//$response = $this->newUser($flds,true, true, $landing_page);
		
		$response = $this->newUser($flds,true, true);
		
		if (is_numeric($response))
		{
			$sf->update_joomlaID($sf_id,$response);
			
			if (JRequest::getVar('sf_id'))
			{
				$html = "Dear ".$flds['first_name']." ".$flds['last_name'].".<br>";
				$html .= "Please find below your login details to IncrediBuild site:<br>";

				$html .= "<br>Username: ".$flds['user_name'];
				$html .= "<br>Password: ".$flds['password'];
				$html .= "<br><br>Please use these when you are asked to login to our store to place an order. If you have received from us an offer with a customized link for the order, please be sure to use this link and not the general store page.<br>";
				$html .= "<br>If you wish to reset your password, you may do so on the login page, by clicking the “Forgot my password” line below the password.<br>";
				$html .= "<br>If you wish to change the email address you are using, then after logging in, please use the edit profile link at the very top of the page.<br>";
				$html .= "<br>For any questions please contact us at sales@incredibuild.com<br>";
				$html .= "<br>Regards,<br>";
				$html .= "<br>IncrediBuild Sales Team<br>";

				$mail =& JFactory::getMailer();
				$config =& JFactory::getConfig(); 
				$mail ->isHTML(true); 
				$mail->addRecipient("sales@xoreax.com");
				$mail->setSubject( 'Joomla registration (via SF)' ); 
				$mail->setBody( $html1 ); 
				$mail->Send();	
				
				
				$mail2 =& JFactory::getMailer();
				$config2 =& JFactory::getConfig(); 
				$mail2->isHTML(true); 
				$mail2->addRecipient($flds['email']);
				$mail2->setSubject( 'Incredibuild Registration' ); 
				$mail2->setBody( $html ); 
				$mail2->Send();
			}
			
			if (JRequest::getVar('sf_id'))			
				die ("updated");
			else
				echo "updated ".$sf_id.".<br>";
		}
		else
		{
			$response = explode ("|",$response);
			
			if (JRequest::getVar('sf_id'))	
				die ($response[1]." - ".$response[2]);
			else
				echo $response[1]." - ".$response[2]." ".$sf_id.".<br>";
		}
	}

	public function newUser($flds, $sf_sync = false, $from_sf = false)
	{
		if (!$flds)
			$flds = JRequest::get( 'post' );

		$solutionTypeArray = Jrequest::getVar('solution_type');
		$landing_page = Jrequest::getVar('landing');
		
		$sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');
			
		foreach ($flds as $key=>$val)
		{
			switch ($key)
			{
				case "first_name":
				case "last_name":
				if (strlen($val) < 2 || strtolower($val) == strtolower($key))
					return "-1|".$key."|".ucfirst (str_replace("_"," ",$key))." must not be empty and at least 2 characters";
				break;
				
				case "company":
				case "street":
				case "city":
				case "country":
				case "job_title":
					if (strlen($val) <= 2 || strtolower($val) == strtolower($key))
						return "-1|".$key."|".ucfirst (str_replace("_"," ",$key))." must not be empty and longer than 2 characters";
					break;
				
				case "email":
				case "license_email":
					if (!$this->check_email($val))
						return "-1|".$key."|".ucfirst (str_replace("_"," ",$key))." is not valid";
					break;
				
				case "user_name":
				case "registered_name":				
					if (strlen($val) <=4 )
						return "-1|".$key."|".ucfirst (str_replace("_"," ",$key))." must not be empty and longer than 4 characters";
					break;
				
				case "telephone":
					if (strlen($val) <=6 )
						return "-1|".$key."|"."Telephone number must be longer than 6 digits";
					break;
				
				case "password":
				if (strlen($val) <=6 )
					return "-1|".$key."|"."Password must not be empty and longer than 6 characters";
				break;
				
				case "re_password":
				if ($val != $flds['password'])
					return "-1|".$key."|"."Password and Re-Password do not match";
				break;
			}
		}
			
		if (((strtolower($flds['country']) === 'united states') || (strtolower($flds['country']) === 'canada')) && ($landing_page != 'nvidia') && ($landing_page != 'vs')) {
			if (strlen($flds['state']) <= 1 || strtolower($flds['state']) == 'state') {
				echo "-1|state|When in the US or Canada, state must not be empty and longer than 1 characters";
				die();
		}
		}
		
		$solution_type_cpp = false;
		$solution_type_css = false;
		$solution_type_csl = false;
		
		if (isset($solutionTypeArray) && (!empty($solutionTypeArray))){  
			foreach ($solutionTypeArray as $key=>$val) {
				//echo print_r($val, true) . '\r\n';
				
				if ($val == 'cpp') {
					$solution_type_cpp = true;
				}
				if ($val == 'css') {
					$solution_type_css = true;
				}
				if ($val == 'csl') {
					$solution_type_csl = true;
				}
			}
		}
		
		// check if already exists

		if (IbonlinestoreHelper::checkExists(" email = '".IbonlinestoreHelper::escape($flds['email'])."'")) {
			return "-1|email|"."Email address already exists";
		}
		
		if (IbonlinestoreHelper::checkExists(" user_name = '".IbonlinestoreHelper::escape($flds['user_name'])."'")) {
			return "-1|user_name|"."User name already exists";
		}
		
		if (isset($flds['remember'])) {
			$remember = $flds['remember'];
		}
		
		if (isset($flds['Heard_from']))
		$heard = $flds['Heard_from'];
		else
			$heard = array();
		
		$download = ($flds['download'] === 'true');
		
		$mlist = $flds['mlist'];
		
		if (isset($flds['page_referer'])) {
			$page_referer_param = $flds['page_referer'];
		}
		
		unset($flds['mlist']);
		unset($flds['option']);
		unset($flds['re_password']);
		unset($flds['remember']);
		unset($flds['Heard_from']);
		unset($flds['download']);
		unset($flds['address']);
		unset($flds['job_title']);
		unset($flds['view']);
		unset($flds['mailing_list']);
		unset($flds['solution_type']);
		unset($flds['page_referer']);
		
		unset($flds['eula']);
		
		//$flds['street'] = $flds['country'];
		//$flds['city'] = $flds['city'];
		//$flds['state'] = $flds['state'];
		
		$flds['password'] = md5($flds['password']);
		
		// all good - add customer
		$db = JFactory::getDbo();
 
		// Create a new query object.
		$query = $db->getQuery(true);
		 
		// Insert columns.
		$columns = array_keys($flds);
		 
		// Insert values.
		$values = array_values ($flds);
		
		foreach($values as $key=>$val) $values[$key] = "'".IbonlinestoreHelper::escape($val)."'";
		foreach($columns as $key=>$val) $columns[$key] = "`".IbonlinestoreHelper::escape($val)."`";
		
		$adWords = $this->get_adWords_arr();

		if (isset($_COOKIE["__utma"]) and isset($_COOKIE["__utmz"])) {
	       $this->ParseCookies();
        }
		
		$campaign = ($adWords["campaign"]=="") ? $this->campaign_name : $adWords["campaign"];
	   
	   // Prepare the insert query.
		$query = "INSERT INTO `bczfi_ibonlinestore_registe` (".implode(",",$columns).") VALUES (".implode(",",$values).")";
		
		// //echo $query;exit;
		$db->setQuery($query);
		$db->query(); 
		
		$user_id = $db->insertid();
		
		$user = $sf->get_system_users('WebDev');
		
		// if ($flds['country'] === 'Japan') {
			// //$sfUser = $sf->get_system_users('IncrediBuild Japan');
			// //005w0000004mHRs

			// // $user = new stdClass();
			// // $user->Id = '005w0000004mHRs';
			// // $user->Type = 'user';
			
			// //$sfUser = $sf->get_system_users_by_email('sales1@incredibuild.co.jp');
			
			// $sfUserId = '005w0000004mHRs';	
		// }
		// else {
			// //$sfUser = $sf->get_system_users('');
			// //005200000033Eyp
			
			// // $user = new stdClass();
			// // $user->Id = '005200000033Eyp';
			// // $user->Type = 'user';
			
			// //$sfUser = $sf->get_system_users_by_email('joel.blankchtein@incredibuild.com');
			
			// $sfUserId = '005200000033Eyp';
		// }
		
		if (isset($sf_sync) && $sf_sync == true)
		{
			
			$fields = array(
				'FirstName' => $flds["first_name"],
				'LastName' => $flds["last_name"],
				'Company' => $flds["company"],
				'Email' 	=> $flds["email"],
				'Street' => $flds["street"],
				'State' => $flds["state"],
				'City'	=>$flds["city"] . ', ' . $flds["state"],
				'Country'	=>$flds["country"],
				'Phone' => $flds["telephone"],
				
				// 'Adgroup__c' 	=>	$adWords["Adgroup"],
				// 'keyword__c' 	=>	$adWords["keyword"],
				// 'ad_number__c' 	=>	$adWords["ad-number"],
				// 'target__c' 	=>	$adWords["target"],
				
				// 'Campaign_Name__c' 	=>	$campaign,
				
				// 'Campaign_Medium__c' 	=>	$this->campaign_medium,
				// 'Campaign_Term__c' 	=>	$this->campaign_term,
				// 'Campaign_source__c' 	=>	$this->campaign_source,
				
				// 'network__c' 	=>	$adWords["network"],
				// 'placement__c' 	=>	$adWords["placement"],
				// 'matchtype__c' 	=>	$adWords["matchtype"],
				
				//'Heard_from__c' 	=>	implode(",",$heard),
				
				// 'Date_of_first_Visit__c' 	=>	$this->first_visit,
				// 'Date_of_Previous_Visit__c' 	=>	$this->previous_visit,
				// 'Date_of_Current_Visit__c' 	=>	$this->current_visit_started,
				// 'Times_Visited__c' 	=>	$this->times_visited,
				// 'Pages_Viewed_Current_Visit__c' 	=>	$this->pages_viewed,
				
				//'OwnerId' => $sfUser->Id,
				// 'OwnerId' => $sfUserId,
				'OwnerId' => $user->Id,
				"JoomlaID__c"		=> $user_id,
				"Solution_Type_Cpp__c" => ($solution_type_cpp) ? 1 : 0, 
				"Solution_Type_Css__c" => ($solution_type_css) ? 1 : 0,
				"Solution_Type_Csl__c" => ($solution_type_csl) ? 1 : 0,
			
				'Pages_Visit_Trail__c' => $page_referer_param,
			);
		
			if($landing_page == 'nvidia'){
				$fields['Title'] = $flds["job_title"];
				$fields['Campaign_Medium__c'] 	=	"NVIDIA TADP";
				$fields['Heard_from__c'] 	=	"NVIDIA";
				$fields['LeadSource'] = 'Download';
				
				$download = true;
			}
			
			if($landing_page == 'vs'){
				$fields['Title'] = $flds["job_title"];
				$fields['Campaign_Medium__c'] 	=	"Free Visual Studio License";
				$fields['Heard_from__c'] 	=	"Visual Studio";
				$fields['LeadSource'] = 'Download';
				
				$download = true;
			}
			
			if ($landing_page == '')
			{
				$fields['Heard_from__c'] = implode(",",$heard);
				
				// $fields['Campaign_Medium__c'] = $this->campaign_medium;
				// $fields['Campaign_Term__c'] = $this->campaign_term;
				// $fields['Campaign_source__c'] = $this->campaign_source;
			}
			
			//externalSendEmail ('joel.blankchtein@incredibuild.com', 'Joel', 'LP - ' . $landing_page, '<pre>'. print_r($data, true) . '</pre><br/><br/><pre>'. print_r($fields, true) . '</pre><br/><br/><pre>'. print_r($flds, true) . '</pre><br/><br/><pre>'. print_r($download, true) . '</pre><br/><br/>');
			
			
			//SUBSCRIBE TO MAILING LIST
			if ($mlist && $mlist == 'subscribe') {
				$ibfree_list_id = 'cb2dbf4390';
				$mailingList = '';
				$apikey = 'd0a0b9ba19fbd8b4d83a9fd2c11e4493-us1';
				
				if($landing_page == 'vs'){
					$ibfree_list_id = 'efc07ee28f';
					$mailingList = 'Visual Studio LP';
				}
				
				if($landing_page == 'nvidia'){
					$ibfree_list_id = 'efc07ee28f';
					$mailingList = 'NVIDIA LP';
				}
				
				// MailChimp API
				$api = new MCAPI($apikey);
				
				$merge_vars = array(
					"FNAME" => $flds['first_name'], 
					"LNAME" => $flds['last_name'],
					"MMERGE6" => $flds['Company'],
					"MMERGE7" => $flds['telephone'],
					"MMERGE5" => $flds['country'],
					//NUMBER_OF_DEVS
					//PROGRAMMING_LANGUAGE
					"GROUPINGS"=>array(
						array('name' => 'Leads', 'groups' => $mailingList)
					)
				);

				//$EMAIL = $flds['email'];
				$list_id = $ibfree_list_id;
				//$retval = $api->listSubscribe( $list_id, $flds['email'], '', 'important', false );
				$retval = $api->listSubscribe( $list_id, $flds['email'], $merge_vars, 'important', false );
				
				//print_r($retval);
				//die();
			}
			
			//$data = $sf->create_lead($fields, false);
			$data = $sf->create_lead($fields);
			$update = $sf->update_lead_owner($data[0]->id,$user->Id);
						
			// if ($flds['country'] === 'Japan') {
				// $update = $sf->update_lead_owner($data[0]->id,$sfUserId);
			// }
			
			// print_r($data);
			// die();
			
			if ($download != true)// store
			{
			
				$data = $sf->convert_lead($data[0]->id);
				$sf_id = "'".$data->result[0]->contactId."'";
			
				if (!JFactory::getSession()->get('q'))
				{
					//create opportunity/licence/contactroll
					$sf_data = $sf->craete_o_l_cr($data->result[0]->accountId,$data->result[0]->contactId,$flds['license_email'],$flds['registered_name'],'new',$flds["company"]);
				}
				else
				{
					//echo '<br>test2';
					$sf_data = $sf->prepper_quote($data->result[0]->contactId,$flds['license_email']);
				}

			}
			else //download object
			{
				$sf_id = "'".$data[0]->id."'";
			
				$download_date = $sf->create_download($data[0]->id,"1");
			}
			
			//end SF
			
			$query = "UPDATE `bczfi_ibonlinestore_registe` SET `sf_id`= ".$sf_id." where id=".$user_id;
		
			$db->setQuery($query);
			$db->query();
			if($from_sf)
				return $user_id;
		}
		else
		{
			$query = "UPDATE `bczfi_ibonlinestore_registe` SET `sf_id`= ".JRequest::getVar('sf_id')." where id=".$user_id;
		
			$db->setQuery($query);
			$db->query(); 
			
			return $user_id;
		}
		
		$encrypt_info = IbonlinestoreHelper:: encrypt($user_id."|".$flds[user_name]."|".$flds[email]."|".$sf_data[opp_id]."|".$sf_data[lic_id]."|".$flds[license_email]);
		
		//if (isset($remember))
			$this->remember($encrypt_info);
		/*else
			$this->setUser($encrypt_info);
	*/
		
		
		// if (isset($mlist))
			// $this->addMlist($flds);
		
		//check if no Agents
		$cart = JFactory::getSession()->get('cart');
		
		if (count($cart)!=0){
			JFactory::getSession()->set('new',"1");
			
			if ($heard)
				JFactory::getSession()->set('headrd_from',implode(",",$heard));
			
			foreach($cart as $c)
			{
				if ($c['type']==0 || $c['type']==3)
					return 1;
			}
		}
		return 2;
	
	}
	
	
	public function only_lead($flds){
		foreach($flds as $title => $value){
			$trimmed_value = trim($value);
			switch($title){
				case "name":
				case "company":
				case "email":
				case "job":
				case "Country":
				case "phone":
					if($trimmed_value == ""){
						return $title." is a required field.";
					}
					break;
			}
			
		}
	
		$full_name = explode(' ', $flds['name']);
		$first_name = $full_name[0];
		$last_name = $full_name[1];
		if($full_name[2] && $full_name[2] != ''){
			$first_name = $full_name[0].' '.$full_name[1];
			$last_name = $full_name[2];
		} elseif($last_name == ''){
			$last_name = 'N/A';
		}

		$sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');
		$user = $sf->get_system_users('Ilan');

	//	$user = new stdClass();
	//	$user->id = '00520000001Vawh';
	//	$user->Type = 'user';
	
		$adWords = $this->get_adWords_arr();
		if (isset($_COOKIE["__utma"]) and isset($_COOKIE["__utmz"])) {
	       $this->ParseCookies();
        }
		$campaign = ($adWords["campaign"]=="") ? $this->campaign_name : $adWords["campaign"];
		
		$fields = array(
				'FirstName' => $first_name,
				'LastName' => $last_name,
				'Company' => $flds["company"],
				'Email' 	=> $flds["email"],
				'Title' => $flds["job"],
				'OwnerId' => $user->Id,
				'Heard_from__c' => 'Linux Form',
				/*'Street' => $flds["street"],
				'City'	=>$flds["city"],
				'Country'	=>$flds["Country"],*/
				
				'Street' => $flds["Country"],
				'City'	=>$flds["Country"],
				'Country'	=>$flds["Country"],
				
				'Phone' => $flds["phone"],
				'Campaign_Medium__c' => 'Linux form',
				'Adgroup__c' 	=>	$adWords["Adgroup"],
				'keyword__c' 	=>	$adWords["keyword"],
				'ad_number__c' 	=>	$adWords["ad-number"],
				'target__c' 	=>	$adWords["target"],
				'Campaign_Name__c' 	=>	$campaign,
				'Campaign_Medium__c' 	=>	$this->campaign_medium,
				'Campaign_Term__c' 	=>	$this->campaign_term,
				'Campaign_source__c' 	=>	$this->campaign_source,
				'network__c' 	=>	$adWords["network"],
				'placement__c' 	=>	$adWords["placement"],
				'matchtype__c' 	=>	$adWords["matchtype"],
				//'Heard_from__c' 	=>	implode(",",$heard),
				'Date_of_first_Visit__c' 	=>	$this->first_visit,
				'Date_of_Previous_Visit__c' 	=>	$this->previous_visit,
				'Date_of_Current_Visit__c' 	=>	$this->current_visit_started,
				'Times_Visited__c' 	=>	$this->times_visited,
				'Pages_Viewed_Current_Visit__c' 	=>	$this->pages_viewed/*,
				"JoomlaID__c"		=> $user_id*/
			);

		$rs = $sf->create_lead($fields,false);

		$update = $sf->update_lead_owner($rs[0]->id,$user->Id);
		var_dump ($rs);exit;
		
		
		$sf_dump = print_r($rs, TRUE);
		$req_dump = print_r($flds, true);
		$fp = fopen('linux_sf.log', 'a');
		fwrite($fp, "\n\r--------------------------\n\r".$req_dump."\n\r".$sf_dump);
		fclose($fp);
		
		if(isset($sf_dump[0]['errors'])){
			return $sf_dump[0]['errors'][0]['message'];
		} else {
			return "";
		}
		
		die();
		
		return "";
	}
	
	
	public function SaveUserData(){
	
	
	
		$post =	JRequest::get( 'post' );
		
		$post["password"] = trim($post["password"]);
		$post["retype_new_password"] = trim($post["retype_new_password"]);
		
	    if($post["password"] != "" && $post["password"]!=$post["retype_new_password"]){	
			echo("Password does not match the confirm password.");
		}
		else{
				if($post["password"] != ""){ //todo: trim / empty
					$post["password"] = md5($post["password"]);
				}
				else{
					unset ($post["password"]);
				}
				
				$this->UpdateUserProfile($post);
				
				
		}

	}
	
	
	private function CheckIfUserExist($data)
	{
	
		$database = JFactory::getDBO();

		$query = 'SELECT * FROM #__ibonlinestore_registe'
            .' WHERE user_name="'
            . $data["username"] .'"'
            .' AND password ="' . md5($data["password"])  .'"';

		$database->setQuery ( $query );
	    $result =  $database->query();
		$num_rows = $database->getNumRows();
		$rows = $database->loadObjectList();		
		
		if($num_rows==1){
			
			return $rows[0];
		}
		
		return 0;
			
	}
	
	private function UpdateUserProfile($data){
						
			$db = JFactory::getDBO();
			
			unset($data["save_x"]);
			unset($data["save_y"]);
			unset($data["option"]);
			unset($data["save"]);
			unset($data["retype_new_password"]);
			
			$id = IbonlinestoreHelper:: userInfo()->id;
			unset($data["id"]);
			

			// Insert columns.
			$columns = array_keys($data);
			 
			// Insert values.
			$values = array_values ($data);
			
			foreach($values as $key=>$val) $values[$key] = "'".IbonlinestoreHelper::escape($val)."'";
			foreach($columns as $key=>$val) $columns[$key] = "`".IbonlinestoreHelper::escape($val)."`";
					
			$user_details="";
			for ($i=0 ; $i<count($columns); $i++){
				$user_details .= $columns[$i] . "=" . $values[$i];
				if($i+1<count($columns)){
					$user_details .=",";
				}
			}
			
			$query = "UPDATE `bczfi_ibonlinestore_registe` SET " .$user_details . " WHERE id=" . $id ;			

			$db->setQuery($query);
			$db->query(); 
		
			
						
		}
	
	
	public function GetUserData()
	{
		
		//$info = IbonlinestoreHelper:: userInfo();
		//$user_id = $info->id;
		
		$database = JFactory::getDBO();

		$query = 'SELECT * FROM #__ibonlinestore_registe'
            .' WHERE id="'
            . "19" .'"';

		$database->setQuery ( $query );
	    $result =  $database->query();
		$num_rows = $database->getNumRows();
		$rows = $database->loadObjectList();		
		
		if($num_rows==1){
			return $rows[0];
		}
		
		return 0;
	}

	// private function addMlist($flds)
	// {
		// $data["email"] = $flds[email];
		// $data["FNAME"] =  $flds[first_name];
		// $data["LNAME"] =  $flds[last_name];
		// $data["COMPANY"] =  $flds[company];
		// $data["TELEPHONE"] =  $flds[telephone];
		// $data["COUNTRY"] =  $flds[country];
		// $data["sn"] =  "efc07ee28f";
		
		// $url = http_build_query($data);
		
		// file_get_contents("http://www.incredibuild.com/subscribe.php?".$url);
	
	// }
	
	
	public function loginUer()
	{
		$flds = JRequest::get( 'post' );
		$remember = $flds[remember];
		
		//echo IbonlinestoreHelper::escape($flds[user_name]);
		$info = IbonlinestoreHelper::checkExists(" user_name = '".IbonlinestoreHelper::escape($flds[user_name])."' and password = '".MD5(IbonlinestoreHelper::escape($flds[password]))."' ");
		
		if ($info)
		{
			
			
			$encrypt_info = IbonlinestoreHelper:: encrypt($info[0]->id."|".$info[0]->user_name."|".$info[0]->email);
			
			if ($flds['download'] != "true")
			{
				JFactory::getSession()->set('tmp_userlogin',$encrypt_info );
			}
			else
			{
				JFactory::getSession()->set('ib',$encrypt_info );
				
				$sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');
				
				if(strpos($info[0]->sf_id, "003") !== 0)
				{
					$sf->create_download($info[0]->sf_id,"1");
				}
				else
					$sf->create_download($info[0]->sf_id,"2");
			}
			
			JFactory::getSession()->set('new',"0");
			
			return 1;	
		}
		else
		{
			$info2 = IbonlinestoreHelper::checkExists(" user_name = '".IbonlinestoreHelper::escape($flds[user_name])."' ");
		
			if ($info2)
			{
			
				return "-1||Your password is incorrect, please try again or <a onclick='recover_pass();return false;' href='#'>click here</a> to request a new password";
			}
			else
				return "-1||The user name you are trying to access with is incorrect or this user is not registered with us yet. Please try again or register to our web site <a onclick=\"document.getElementById('tab2').click();return false;\" href='#'>here</a>";
				
		}

	
	}
}
?>
