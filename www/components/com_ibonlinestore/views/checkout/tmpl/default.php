<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');

$info = IbonlinestoreHelper:: userInfo();
$reg_info = explode("|",IbonlinestoreHelper::decrypt(JFactory::getSession()->get('ib')));

if (strtolower($info->country) == "israel")
{
?>
<script>
		txt = "<b>Please Note:</b> The IncrediBuild online purchase form is designed for International (non-Israeli) purchases only. A sales representative will be contacting you shortly to assist you with this purchase. For additional assistance, contact us at <a onclick='window.open(\"mailto:sales@incredibuild.com\",\"_parent\");return false;' href='#' style='text-decoration:underline'>sales@incredibuild.com</a>. ";
		new Messi(txt, {title: 'Dear Israeli buyer',titleClass: 'anim error', modal: true , buttons: [{id: 0, label: '<- Back to store', val: 'X'}], callback: function(val) { window.location = "https://www.incredibuild.com/ibonlinestore"; }});
		</script>
		<?

$this->israelyBuyerMail($info);		
		
}
else
{
?>
<script>
cart_type = "small"
q = "&edit=no"
</script>
<?}?>
      <div class="middlecontainer">
        <div class="left_pan">
		<h1>Checkout</h1>
		
<div class="notepan">

</div>		

<div id='checkoutClass'>
		<!--<iframe style='width: 100%;height: 459px;background-color:transparent;' allowTransparency='true' id='tranzFrame'
		src='https://direct.tranzila.com/xoreax/iframe.php?sum=<? echo JFactory::getSession()->get('total');?>&currency=1&TranzilaToken=asdf&trBgColor=EEEFF0&trTextColor=000000&trButtonColor=de257b&nologo=1&buttonLabel=Process%20my%20order'
		></iframe>
		
		<?
		if (strtolower($info->country) != "israel")
		{
		?>
		<iframe style='width: 100%;height: 459px;background-color:transparent;' allowTransparency='true' name='tranzFrame' id='tranzFrame'
		
		></iframe>
		<form target='tranzFrame' method='post' id='tranform' action='https://direct.tranzila.com/xoreax/iframe.php'>
		<input type="hidden" id="email" name="email" value="<?=$info->email?>" />
		<input type="hidden" id="regname" name="regname" value="<?=$reg_info[4]?>" />
		<input type="hidden" id="fname" name="fname" value="<?=$info->first_name?>" />
		<input type="hidden" id="lname" name="lname" value="<?=$info->last_name?>" />
		<input type="hidden" id="company" name="company" value="<?=$info->company?>" />
		<input type="hidden" id="phone" name="phone" value="<?=$info->telephone?>" />
		<input type="hidden" id="address" name="address" value="<?=$info->street?>" />
		<input type="hidden" id="city" name="city" value="<?=$info->city?>" />
		<input type="hidden" id="state" name="state" value="<?=$info->state?>" />
		<input type="hidden" id="country" name="country" value="<?=$info->country?>" />
		<input type="hidden" id="contact" name="contact" value="<?=$info->first_name?> <?=$info->last_name?>" />
		<input type="hidden" id="notes" name="notes" value="" />
		<input type="submit" id='submit_form' style='display:none'/></form>
		<?}?>-->
		
		<?
		if (strtolower($info->country) != "israel")
		{
		?>
		<div style=" padding-top: 41px; " align='center'>
		<a style='font-size:24px;color:#000;text-decoration:none;'>
		Please Wait...<br><br>
		<img src='https://www.incredibuild.com/components/com_ibonlinestore/assets/images/cc_loader.gif'>
		</a>
		</div><!-- https://direct.tranzila.com/xoreax/ -->
		<form  method='post' id='tranform' action='https://direct.tranzila.com/xoreax/'>
		<input type="hidden" id="email" name="email" value="<?=$info->email?>" />
		<input type="hidden" id="regname" name="regname" value="<?=$reg_info[4]?>" />
		<input type="hidden" id="fname" name="fname" value="<?=$info->first_name?>" />
		<input type="hidden" id="lname" name="lname" value="<?=$info->last_name?>" />
		<input type="hidden" id="company" name="company" value="<?=$info->company?>" />
		<input type="hidden" id="phone" name="phone" value="<?=$info->telephone?>" />
		<input type="hidden" id="address" name="address" value="<?=$info->street?>" />
		<input type="hidden" id="city" name="city" value="<?=$info->city?>" />
		<input type="hidden" id="state" name="state" value="<?=$info->state?>" />
		<input type="hidden" id="country" name="country" value="<?=$info->country?>" />
		<input type="hidden" id="contact" name="contact" value="<?=$info->first_name?> <?=$info->last_name?>" />
		<input type="hidden" id="ponum" name="ponum" value="<?php 
		$ponum = JRequest::getVar('ponum');
		echo $ponum;
		?>" />
		<input type="hidden" id="notes" name="notes" value="" /> 

		<input type="submit" id='submit_form' style='display:none'/>

		<input type='hidden' name='pdesc' value='IncrediBuild'>
        <input type='hidden' name='purchase_type' value='standard'>
        <input type='hidden' name='cred_type' value='1'>
        <input type='hidden' name='currency' value='2'>
		<input type='hidden' name='tran_type' value='01'>
        <input type='hidden' name='force' value='0'>
		
		 <input type='hidden' name='sum' value='<? echo JFactory::getSession()->get('total');?>'>
		 
		 <input type='hidden' name='tran_id' value='<? echo $this->tran_id;?>'>
		
		</form>
		
		<?}?>
		
		</div>
        </div>
        <div class="right_pan">
          <div id='cart_view'></div> 
			<div id='cart_view_h'></div>
          <div class="securepan"><a href="#"><img src="/components/com_ibonlinestore/assets/images/secure.png" alt="" /></a></div>
        </div>
        <div class="clear"></div>
      </div>

