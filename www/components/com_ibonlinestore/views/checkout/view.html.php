<?php
/*------------------------------------------------------------------------
# view.html.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

//require_once ('/var/www/incredl3/public_html/templates/yoo_air/PHPMailerAutoload.php');
		
function externalSendEmail($toAddress, $toName = '', $subject, $body, $from = '', $fromName = '') {
		require_once ('templates/yoo_air/PHPMailerAutoload.php');	

		
		$Mail = new PHPMailer();
		$Mail->IsSMTP(); // Use SMTP
		$Mail->Host        = "smtp.gmail.com"; // Sets SMTP server
		$Mail->SMTPDebug   = 0; // 2 to enable SMTP debug information
		$Mail->SMTPAuth    = TRUE; // enable SMTP authentication
		$Mail->SMTPSecure  = "tls"; //Secure conection
		$Mail->Port        = 587; // set the SMTP port
		$Mail->SMTPKeepAlive = FALSE;   
		$Mail->Username    = 'noreply@incredibuild.com'; // SMTP account username
		$Mail->Password    = 'xorPass77'; // SMTP account password
		$Mail->Priority    = 3; // Highest priority - Email priority (1 = High, 3 = Normal, 5 = low)
		$Mail->CharSet     = 'iso-8859-1';
		$Mail->Subject     = $subject;
		$Mail->ContentType = 'text/html; charset=utf-8\r\n';
		
		//$Mail->AddBCC('sales@incredibuild.com');
		
		if ($from != '') {
			$Mail->From        = $from;
		}
		
		$Mail->WordWrap    = 900; // RFC 2822 Compliant for Max 998 characters per line
		
		$Mail->AddAddress( $toAddress ); // To: //$toName
		
		$Mail->isHTML( TRUE );
		$Mail->Body = $body;
		
		//send the message, check for errors
		if (!$Mail->send()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return "1";
		}
		
		$Mail->SmtpClose();
	}

/**
 * HTML Car View class for the Ibonlinestore Component
 */
class IbonlinestoreViewcheckout extends JView
{
	
	
	
	// Overwriting JView display method
	function display($tpl = null)
	{
	
if (JRequest::getVar('ponum') != '' &&  JRequest::getVar('ponum')) {
							$ponum = JRequest::getVar('ponum');
							$session = & JFactory::getSession();
							/* this way unset data from the session store */
//							$session->clear('security_code');
							/* and now set the new value */
							$session->set('ponum', $ponum);
							
						}

						//IbonlinestoreHelper::checkMaintIssue();

		if (!JRequest::getVar('a'))
		{

		if (count(JFactory::getSession()->get("cart"))== 0)
				JFactory::getApplication()->redirect("index.php?option=com_ibonlinestore");
			
			
			if (!IbonlinestoreHelper:: userInfo())
				JFactory::getApplication()->redirect("index.php?option=com_ibonlinestore&view=register");
			
			$this->insertTranzillaParams();
			
			parent::display($tpl);
		}
		else
		{			
			/*print_r($_SESSION);
			echo "<br>POST:<br>";
			print_r (JRequest::get( 'post' ));
			echo "<br>GET:<br>";
			print_r (JRequest::get( 'get' ));
			die();*/
	 
			// if (JRequest::getVar('a')=="success" && (JRequest::getVar('Response')=="003" || JRequest::getVar('Response')=="000"))
			if (JRequest::getVar('a')=="success" && JRequest::getVar('Response')=="000" )
			{
				$this->placeOrder();
			}
			elseif(JRequest::getVar('a')=="success" && JRequest::getVar('Response')=="003" ){
				$this->unauthorizedResponse();
				$this->fail = $this->getFailText();
			}
			elseif(JRequest::getVar('a')=="success" && JRequest::getVar('Response')=="1337" ){
				$this->placeOrder();
			}
			elseif (JRequest::getVar('a')=="fail")
				$this->fail = $this->getFailText();
			
			parent::display(JRequest::getVar('a'));
		}

	}
	
	public function insertTranzillaParams()
	{
		
		$db = JFactory::getDbo();
		
		$ib = JFactory::getSession()->get('ib');
		$cart = IbonlinestoreHelper::encrypt(serialize (JFactory::getSession()->get('cart')));
		$q = JFactory::getSession()->get('q');
		$discountCode = JFactory::getSession()->get('discountCode');

		
		$query = "INSERT INTO `bczfi_tranzilla` (`ib`,`cart`,`q`,`discountCode`) VALUES ('".$ib."','".$cart."','".$q."','".$discountCode."')";
			
		$db->setQuery($query);
		$db->query(); 
			
		$this->tran_id = $db->insertid();
	
	}

	private function unauthorizedResponse(){
		
		$message = '<img src="https://www.incredibuild.com/invoiceLayout/images/logo.jpg" /><br/>';
		$message .= "<p>Hello,</p>\n\n";
		$message .= "<p>Thank you for submitting an IncrediBuild order. Please note your card charge request, was refused by your credit card company. In order to complete the credit card purchase, please do one of the following:</p>\n\n";
		$message .= "<p>1. Try again with a different credit card.<br>\n";
		$message .= "2. Call Your bank / Credit Card Company, instruct them to authorize the purchase, and then try again. Different limits are often set for international purchases and this usually works around this problem.</p>\n\n";
		$message .= "<p>If you have any questions, please contact us at <a href='mailto:sales@incredibuild.com'>sales@incredibuild.com</a>.</p>\n\n";
		$message .= "<p>Best Regards,<br/>\nThe IncrediBuild team</p>";
		
		
		// $mail =& JFactory::getMailer();
		// $config =& JFactory::getConfig(); 
		// $mail ->isHTML(true); 
		// $mail->addRecipient(JRequest::getVar("email"));
		// $mail->addBCC('sales@incredibuild.com');
		// $mail->setSubject( 'IncrediBuild - Unauthorized Payment' ); 
		// $mail->setBody( $message ); 
		// $mail->Send();
				
		$success = externalSendEmail(
							JRequest::getVar("email"), 
							'' , 
							'IncrediBuild - Unauthorized Payment', 
							$message, 
							'sales@incredibuild.com', 
							'IncrediBuild'
							);
	}
	
	private function placeOrder()
	{
		
		if (JRequest::getVar('upgmaint')!="")
		{
			$this->placeTmpMorder();
		}
		else
			$this->mailOrderDetails();
		
		JFactory::getSession()->set('ok',1);
		
	}
	
	private function placeTmpMorder()
	{
			
		
		// $req_dump = "placeTmpMorder:\n";
		// $fp = fopen('logan_test_log.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		
	$message = '<img src="https://www.incredibuild.com/invoiceLayout/images/logo.jpg" /><br/>';
   $message .= "<h2>Thank You for Your Order.</h2>\n";
  $message .= "We have accepted your online purchase order for an IncrediBuild license. Once processed, your electronic license file will be sent to this e-mail address (<a href=mailto:" . JRequest::getVar("email") . ">" . JRequest::getVar("email"). "</a>). Please allow 1 working day for your order to be processed.<p>\n";
  $message .= "Your order confirmation code is: <b>" . JRequest::getVar('index') . "</b>.<br>\n";
  $message .= "Please state this code when inquiring about your order.<p>\n";
  $message .= "<b>The following order details were accepted on " . date("F j, Y") . ":</b><p>\n";
  $message .= "<table border='0' bgcolor='dddddd' style='font-family:Verdana;font-size:12'><tr>\n";
  $message .= "<td>Registered name:</td><td><b>" . JRequest::getVar("regname") . "</b><br></td></tr>\n";

 
      $message .= "<tr><td>12-month Maintenance Service (no. Agents):</td><td><b>" . JRequest::getVar("upgmaint") . "</td></tr>\n";

			
		  $message .= "<tr><td>Order total:</td><td><b>$" . number_format(JRequest::getVar("sum"),2) . "</td></tr>\n";
		  $message .= "</table><p>\n"; 
		  $message .= "For inquiries, send mail to <a href=mailto:sales@incredibuild.com>sales@incredibuild.com</a>.<p>\n";
		  $message .= "Thank you for your business,<p>\n";
		  $message .= "IncrediBuild Software<br><a href=http://www.incredibuild.com>www.incredibuild.com</a>\n";
  
		
		
		// $mail =& JFactory::getMailer();
		// $config =& JFactory::getConfig(); 
		// $mail ->isHTML(true); 
		// $mail->addRecipient(JRequest::getVar("email"));
		// $mail->setSubject( 'IncrediBuild Order Confirmation' ); 
		// $mail->setBody( $message ); 
		// $mail->Send();
		
		$success = externalSendEmail(
							JRequest::getVar("email"), 
							'' , 
							'IncrediBuild Order Confirmation', 
							$message, 
							'sales@incredibuild.com', 
							'IncrediBuild'
							);
		
		
		
		sleep(5);
		
		$subject = "Someone ordered IncrediBuild! (";

		  if (JRequest::getVar("Response") == "000")
		  {
			if (JRequest::getVar("force") != 1)
			  $subject .= "transaction approved automatically)";
			else
			  $subject .= "transaction approved manually)";
		  }
		  else
			$subject .= "transaction pending, manual processing required)";

		$message .= "<div style='font-family:Verdana;font-size:12;background-color:#d6e4fe;border:2 #6290d5 solid;'>";
	  $message .= "<b><u>System Messages</u></b><br>";
	  if (JRequest::getVar("notes"))
		$message .= "<p><b>Notes:</b> " . JRequest::getVar("notes");
	  if (JRequest::getVar("upgrade"))
		$message .= "<p><b>This order upgrades an existing license.</b>";
	  if (JRequest::getVar("invite"))
		$message .= "<p><b>Invitation code used:</b> " . JRequest::getVar("invite");
	  if (JRequest::getVar("surcharge_total") > 0)
		$message .= "<p><b>Surcharge:</b> $" . number_format(JRequest::getVar("surcharge_total"), 2);
	  $message .= "</div>";
 
 
		// $mail2 =& JFactory::getMailer();
		// $config =& JFactory::getConfig(); 
		// $mail2 ->isHTML(true); 
		// $mail2->addRecipient("sales@incredibuild.com");
		// $mail2->setSubject( $subject );
		// $mail2->setBody( $message ); 
		// $mail2->Send();
		
		$success = externalSendEmail(
							'sales@incredibuild.com', 
							'' , 
							$subject, 
							$message, 
							'sales@incredibuild.com', 
							'IncrediBuild'
							);
	
		
		// $req_dump = "at the end.\n";
		// $req_dump .= print_r($mail, TRUE);
		// $req_dump .= print_r($mail2, TRUE);
		// $fp = fopen('logan_test_log.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		
		
		JFactory::getSession()->set('index',JRequest::getVar('index'));

	}
	
	
	
	private function mailOrderDetails()
	{
		$sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');
		
		
		// $req_dump = "mailOrderDetails:\n";
		// $fp = fopen('logan_test_log.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		
		$db = JFactory::getDbo();
		
		$tran_id = JRequest::getVar("tran_id");
		
		// if (!is_numeric($tran_id))
		// {
			// $req_dump = "tran_id not numeric.\n";
			// $fp = fopen('logan_test_log.txt', 'a');
			// fwrite($fp, $req_dump);
			// fclose($fp);
		
			// die();
		// }
		
		// $req_dump = "After tran_id numeric check.\n";
		// $fp = fopen('logan_test_log.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		
		$query = "select * from `bczfi_tranzilla` where id=".$tran_id;
			
		$db->setQuery($query);
		$results = $db->loadObjectList();

			
		$cart = unserialize (IbonlinestoreHelper::decrypt($results[0]->cart));
		$reg_info = explode("|",IbonlinestoreHelper::decrypt($results[0]->ib));
		
		$q = $results[0]->q;
		$discountCode = $results[0]->discountCode;
	

		//get text
		$message = $this->getMailText();
		$message .= "Your order confirmation code is: <b>" . JRequest::getVar('index') . "</b>.<br>\n";
		$message .= "Please state this code when inquiring about your order.<p>\n";
		$message .= "<b>The following order details were accepted on " . date("F j, Y") . ":</b><p>\n";
		$message .= "<table border='0' bgcolor='dddddd' style='font-family:Verdana;font-size:12'><tr>\n";
		$message .= "<td>License E-mail:</td><td><b>" . $reg_info[6] . "</b><br></td></tr>\n";
		$message .= "<td>License Registered name:</td><td><b>" . $reg_info[5] . "</b><br></td></tr>\n";
			
		
		$info = array();
		
//		print_r ($cart);
		
		foreach ($cart as $key=>$item)
		{

			$dicount = "";
			if (isset($item["sprice"]))
			{
				$price = $item[sprice];
				$dicount = ((1-($item[sprice]/$item[price])) * 100);
			}
			else
				$price = $item[price];
				
//			$message .= "<tr><td>".$item[subtitle].":</td><td><b>$".number_format($item[qty]*$price,2)." (" . $item[qty] . ")";
			
			if ($dicount!="")
			{
		//		$message .= "(".number_format($dicount)."% off)";
			}
			
			
			$info["itemDesc"][] = $item[subtitle];
			$info["itemPrice"][] = $price;
			$info["itemQty"][] = $item[qty];
			
			$message .="</td></tr>\n";
		}
 
			
		  $message .= "<tr><td>Order total:</td><td><b>$" . JRequest::getVar("sum") . "</td></tr>\n";
		  $message .= "</table><p>\n"; 
		  $message .= "For inquiries, send mail to <a href=mailto:sales@incredibuild.com>sales@incredibuild.com</a>.<p>\n";
		  $message .= "Thank you for your business,<p>\n";
		  $message .= "IncrediBuild Software<br><a href=http://www.incredibuild.com>www.incredibuild.com</a>\n";
  
		

		
		//sf
		$user_info = IbonlinestoreHelper::userInfo($reg_info);
		$ib_info = $reg_info;
		
		
		if ($q!="")
		{
				$sf->approveQuote(false,false,$reg_info);//add items to opportunity if quote
				$sf->update_quote_status($q,"Approved by Customer");//change quote status
		}
		


		$account = $sf->get_sf_account_ids($user_info->sf_id,$ib_info[0]);
		
		
		//print_r($account);
	//	echo "------<br>";
		$fields = array(
			'Name'	=> JRequest::getVar('index'),
			'Account__c' => $account['AccountId'],
			'Date__c'	=> date("Y-m-d"),
			'USD_Amount__c'	=> JRequest::getVar("sum"),
			'Type__c'	=> 'Credit Card'
		);
		

		$data = $sf->create_object("Incoming_Payment__c",$fields);
		
	///	print_r($data);
		//echo "------<br>";
		
		
		$sf->update_payment_opportunity($ib_info[3],$data[0]->id);
		
		//$opp = $sf->get_opportunity_info($ib_info[3]);
		//$Licensee_Email__c = $opp[0]->records[0]->Licensee_Email__c;
		
		// if(JRequest::getVar('Response')=="1337" ){
			// // echo "<br><br>";
			// // print_r($sf);
			// echo "<br/>End of the line.";
			// die();
		// }

		
		//echo "------<br>";

		// $mail =& JFactory::getMailer();
		// $config =& JFactory::getConfig(); 
		// $mail ->isHTML(true); 
		// $mail->addRecipient($user_info->email);
		// $mail->setSubject( 'IncrediBuild Order Confirmation' ); 
		// $mail->setBody( $message ); 
		// $mail->Send();
		
		// externalSendEmail(
							// 'joel.blankchtein@incredibuild.com', 
							// '' , 
							// 'IncrediBuild Order Confirmation  -  TEST', 
							// $message . '<br/><br/><br/><pre>'.print_r($opp,true).'</pre><br/><br/><br/><pre>'.print_r($Licensee_Email__c,true).'</pre>', 
							// '', 
							// ''
							// );
		// sleep(5);
		
		$success = externalSendEmail(
							$user_info->email, 
							'' , 
							'IncrediBuild Order Confirmation', 
							//$message . ' - ' . print_r($opp, true), 
							$message,
							'sales@incredibuild.com', 
							'IncrediBuild'
							);
							
		sleep(5);
		
		// $success = externalSendEmail(
							// $Licensee_Email__c, 
							// '' , 
							// 'IncrediBuild Order Confirmation', 
							// $message, 
							// 'sales@incredibuild.com', 
							// 'IncrediBuild'
							// );
		
		
							
		// // print_r($success);
		// // die();
		// sleep(5);
		
		$subject = "Someone ordered IncrediBuild! (";

		  if (JRequest::getVar("Response") == "000")
		  {
			if (JRequest::getVar("force") != 1)
			  $subject .= "transaction approved automatically)";
			else
			  $subject .= "transaction approved manually)";
		  }
		  else
			$subject .= "transaction pending, manual processing required)";

		$message .= "<div style='font-family:Verdana;font-size:12;background-color:#d6e4fe;border:2 #6290d5 solid;'>";
	  $message .= "<b><u>System Messages</u></b><br>";
	  if (JRequest::getVar("notes"))
		$message .= "<p><b>Notes:</b> " . JRequest::getVar("notes");
	  if (JRequest::getVar("upgrade"))
		$message .= "<p><b>This order upgrades an existing license.</b>";
	  if (JRequest::getVar("invite"))
		$message .= "<p><b>Invitation code used:</b> " . JRequest::getVar("invite");
	  if (JRequest::getVar("surcharge_total") > 0)
		$message .= "<p><b>Surcharge:</b> $" . number_format(JRequest::getVar("surcharge_total"), 2);
	  $message .= "</div>";
 
 
		// $mail2 =& JFactory::getMailer();
		// $config =& JFactory::getConfig(); 
		// $mail2 ->isHTML(true); 
		// $mail2->addRecipient("sales@incredibuild.com");
		// $mail2->setSubject( $subject );
		// $mail2->setBody( $message ); 
		// $mail2->Send();
		
		
		$success = externalSendEmail(
							'sales@incredibuild.com', 
							'' , 
							$subject, 
							$message, 
							'sales@incredibuild.com', 
							'IncrediBuild'
							);
		
		// $req_dump = "After mail 2.\n";
		// $fp = fopen('logan_test_log.txt', 'a');
		// fwrite($fp, $req_dump);
		// fclose($fp);
		
		//create invoice
		$info["sum"] = JRequest::getVar("sum");
		$this->sendInvoice($info);
		
		if ($discountCode != "")
		{

			$db = JFactory::getDbo();
			$query = "update bczfi_ibonlinestore_coupon set active=0 where one_time=1 and code='".$discountCode."'";
			$db->setQuery($query);
			$db->query(); 

		}
	}
	
	
	private function sendInvoice($info)
	{
	

		$intDollar = $info['sum'];
		
		$rate = simplexml_load_string(file_get_contents("http://www.boi.org.il/currency.xml"));
		$rate = $rate->CURRENCY[0]->RATE;
		
		$intNIS = $intDollar  * $rate;
											
		ini_set('set_time_limit', '5');  
		ini_set('max_execution_time', '5');  

		$fields = array(  
		'compID'=>'xoreax',  
		'user'=>'sales',  
		'pass'=>'828d66',
		'maampercent'=>'0',  
		'dateissued'=>date('Ymd'),
		'validity' => date('Y-m-d', strtotime('last day of next month')),
		'clientname'=>'xoreax',  //get client name frm IB(ron) 
		'docType'=>'invrec',
		'currency'=>'2', 
		'rate' => $rate,   
		'credit'=>'1',
		'totalvat'=>'0',
        'sendOrig'=>'ron.ginton@incredibuild.com',
		'totalwithvat'=> $intNIS,
        'cctotal'  => $intNIS,
        //'cc_validity_YY' => ,
        //'cc_cvv'  => ,
        //'cc_id' => ,
        'cc_numofpayments[0]'=>'1',        
        'ccfirstpayment' =>$intNIS,
		'desc'=>$info['itemDesc'],
		'unitprice'=>$info['itemPrice'],
		'quantity'=>$info['itemQty'],
		'totalsum'=>$intNIS,
		'show_response'=>'1',
		'debug'=>'no',
		'lang'=>'en',
		'hidenis'=>'1'
		);
   


	}
	
	public function israelyBuyerMail($info)
	{
		$subject = "Israely Buyer Attempt";
		//$data = get_object_vars($info);
		//print_r ($data);
		
		$message .= "First name: ".$info->first_name."<br>";
		$message .= "Last name: ".$info->last_name."<br>";
		$message .= "Email: ".$info->email."<br>";
		$message .= "User name: ".$info->user_name."<br>";
		$message .= "Company: ".$info->company."<br>";
		$message .= "Telephone: ".$info->telephone."<br>";
		$message .= "Registered name: ".$info->registered_name."<br>";
		$message .= "Street: ".$info->street."<br>";
		$message .= "Country: ".$info->country."<br>";
		$message .= "License email: ".$info->license_email."<br><br><br>";
		
		$message .= "Items:<br><table>";
		
		$cart = JFactory::getSession()->get('cart');

		foreach ($cart as $key=>$item)
		{

			$message .= "<tr><td>".$item[subtitle].":</td><td><b>$".($item[qty]*$item[price])."(" . $item[qty] . ")</td></tr>\n";
		}
		
		$message .= "</table>";
		
		$mail2 =& JFactory::getMailer();
		$config =& JFactory::getConfig(); 
		$mail2 ->isHTML(true); 
		$mail2->addRecipient("sales@incredibuild.com");
		$mail2->setSubject( $subject );
		$mail2->setBody( $message ); 
		$mail2->Send();
	}
	
	private function getMailText()
	{
		$db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__ibonlinestore_email');
        $db->setQuery((string)$query);
        $data = $db->loadObjectList();


		return $data[0]->order_conf;
			

	}
	
	
	private function getFailText()
	{
		$db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__ibonlinestore_thankyo');
        $db->setQuery((string)$query);
        $data = $db->loadObjectList();


		return $data[0]->fail;
			
	}
}
?>