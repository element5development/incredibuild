<?php
/*------------------------------------------------------------------------
# view.html.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla view library
jimport('joomla.application.component.view');
/**
 * HTML Ibonlinestore View class for the IB Online Store Component
 */
class IbonlinestoreViewrenew extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
	
		if (JFactory::getSession()->get('discountCode') == "IB404040p")
		{
				JFactory::getApplication()->redirect("index.php?option=com_ibonlinestore");
		}
		
		// Assign data to the view
		$this->items = $this->get('Steps');
		
		$this->renew = $this->get('Renew');
		$this->solutions = $this->get('Solutions');
		// Check for errors.
		if (count($errors = $this->get('Errors'))):
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		endif;
		
		
		setcookie("ib",$info,time()-36000000);
		JFactory::getSession()->clear('ib');
		
		// Display the view
		parent::display($tpl);
	}
}
?>