<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');

$item = $this->items;
?>
<script>
var cart_type = "small"
var mTermsText = "<div style='height:400px;overflow:auto'><? echo str_replace("\n","",str_replace("\r","",str_replace('"',"'",$item->maintenance_terms)));?></div>"
</script>
<div style='width:1012px;'>
<div class="left_pan">
<h1>IncrediBuild Online Maintenance Renewal</h1>

<div class="steppan"><span>Step 3</span></div>
<div class="clear"> </div>



<h3 class="bgnone">Please select the number of Agents in your license.</h3>
<div class="solutionspan">

<?php foreach($this->renew as $renew): ?>
<div class="solutionsteppan" id="<? echo IbonlinestoreHelper::encrypt($renew->id."|".$renew->type."|".$renew->title."|".$renew->subtitle."|".$renew->price."|".$renew->description);?>">
<div class="solutionarrowpan"><?php echo $renew->title; ?><span><?php echo $renew->subtitle; ?></span></div>
<div class="accleratespan"><?php echo str_replace("\n","<br>",$renew->description); ?></div>
<div class="pricepan"><span>$</span><?php echo $renew->price; ?></div>
<div class="addtocartpan">
<div class="pluspan" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/plussignimg.png" border="0" alt="" /></a></div>
<div class="minuspan" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/minussignimg.png" border="0" alt="" /></a></div>
<input type="text" name="" id="qty_<?php echo $i; ?>" value="<? echo IbonlinestoreHelper::itemQty($renew->id); ?>" /></div>
<div class="clear"> </div>
</div>
<?php 
$i++;
endforeach; ?>

</div>
<br>
<? echo $item->maintenance_page;?>
<br>
<h5 class='terms'>
<input type="checkbox" name="accept_terms" id="accept_terms" onchange="enable_disable_submit()">
I have read and agree to the above <a href='#' style='text-decoration:underline;color:#df167b' onclick='clickMterms();return false;'>Terms and Conditions</a>.</h5>
<br>

<div class="clickherepan2">
<div class="nextbt1"><a href="/cart?renew=1"><img src="/components/com_ibonlinestore/assets/images/nextbt.png" border="0" alt=""></a></div>
<div class="continuebt"><a href="/cart?renew=1"><img src="/components/com_ibonlinestore/assets/images/continuebt.png" border="0" alt=""></a></div>
<div class="nextbt2"><a href="/cart?renew=1"><img src="/components/com_ibonlinestore/assets/images/nextbt.png" border="0" alt=""></a></div>
</div>

</div>

<div class="right_pan">
	<div id='cart_view'></div> 
	<div id='cart_view_h'></div>
          <div class="securepan"><a href="#"><img src="/components/com_ibonlinestore/assets/images/secure.png" alt=""></a></div>
		<?php echo $item->discount; ?>
          <h1>IncrediBuild ROI Calculator </h1>
          <div class="graph_pan">
            <div class="graphpic"><img src="/components/com_ibonlinestore/assets/images/graphimg.png" alt=""></div>
            <div class="returnpop">Return your investment <br>
              in less than 3 months!</div>
            <div class="testitnow"><a href="http://www.incredibuild.com/purchase-assistant.html"><img src="/components/com_ibonlinestore/assets/images/testitnow.png" alt=""></a></div>
            <div class="clear"></div>
          </div>
        </div>
</div>