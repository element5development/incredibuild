<?php
/*------------------------------------------------------------------------
# view.html.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML Ibonlinestor View class for the Ibonlinestore Component
 */
		
  function externalSendEmail($toAddress, $toName = '', $subject, $body, $from = '', $fromName = '', $reply = '', $replyName = '') {
 
		require_once ('templates/yoo_air/PHPMailerAutoload.php');
			
		$Mail = new PHPMailer();
		$Mail->IsSMTP(); // Use SMTP
		$Mail->Host        = "smtp.gmail.com"; // Sets SMTP server
		$Mail->SMTPDebug   = 0; // 2 to enable SMTP debug information
		$Mail->SMTPAuth    = TRUE; // enable SMTP authentication
		$Mail->SMTPSecure  = "tls"; //Secure conection
		$Mail->Port        = 587; // set the SMTP port
		$Mail->SMTPKeepAlive = FALSE;   
		$Mail->Username    = 'noreply@incredibuild.com'; // SMTP account username
		$Mail->Password    = 'xorPass77'; // SMTP account password
		$Mail->Priority    = 3; // Highest priority - Email priority (1 = High, 3 = Normal, 5 = low)
		$Mail->CharSet     = 'iso-8859-1';
		$Mail->Subject     = $subject;
		$Mail->ContentType = 'text/html; charset=utf-8\r\n';
		
		if ($from != '') {
			$Mail->From        = $from;
			if ($fromName != '') {
				$Mail->FromName        = $fromName;
			}
		}
		
		if ($reply != '') {
			$Mail->AddReplyTo($reply, $replyName);
		}
		
		$Mail->WordWrap    = 900; // RFC 2822 Compliant for Max 998 characters per line
		$Mail->AddAddress( $toAddress ); // To: //$toName
		$Mail->isHTML( TRUE );
		$Mail->Body = $body;
		
		//send the message, check for errors
		if (!$Mail->send()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return "1";
		}
		
		$Mail->SmtpClose();
	}
	
class IbonlinestoreViewsalesforce extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		
		if (JRequest::getVar('action') == "update_standard_products")
		{
				$this->getModel()->update_standard_products();
		}
		
		if (JRequest::getVar('action') == "update_joomla_id_from_file")
		{

				$this->getModel()->update_joomla_id_from_file();
		}

		if (JRequest::getVar('action') == "approve_quote")
		{
			//ob_start();
			?>
				<center>
				<script>
				window.resizeTo(400, 250);
				</script>
				<br><Br>	
			<?php
			$q = JRequest::getVar('q');
			
			$opt_id = $this->getModel()->prepper_external_quote($q);
			
			$this->getModel()->approveQuote($opt_id,$q);//add items to opportunity if quote
			
			$this->getModel()->update_opportunity_status($opt_id,"Closed Won");//update oppotrunity to close
			$this->getModel()->update_quote_status($q,"Approved by Customer");//update quote status
	
			$html = "<h1>Thank You for Your Order.</h1>";

			$html .= "We have accepted your IncrediBuild purchase confirmation. Once your order is processed, a temporary license file as well as an invoice for payment will be sent to you. You could then submit payment via a wire transfer or check and once payment for the order is made (our payment terms are NET 30), a standard license file will be sent.<br>";
			$html .= "Please state this code when inquiring about your order.<br>";

			$html .= "<br><br>For inquiries, send mail to sales@incredibuild.com.";
			
			$html .= "<br><br>Thank you for your business,";

			$html .= "<br>Xoreax / IncrediBuild ";
			$html .= "<br>www.incredibuild.com";
			
			$html1 = "<h1>A new quoted order has been approved (without payment).</h1>";
			 
			$html1 .= "<a href='https://eu3.salesforce.com/".$q."'>https://eu3.salesforce.com/".$q."</a><br>";

			$html1 .= "<br>Xoreax / IncrediBuild ";
			$html1 .= "<br>www.incredibuild.com";

			// $mail =& JFactory::getMailer();
			// $config =& JFactory::getConfig(); 
			// $mail ->isHTML(true); 
			// $mail->addRecipient("sales@xoreax.com");
			// $mail->setSubject( 'A new quoted order has been approved' ); 
			// $mail->setBody( $html1 ); 
			// $mail->Send();	
			
			//$success = externalSendEmail('sales@incredibuild.com', '', 'A new quoted order has been approved', $html1);
			externalSendEmail('sales@incredibuild.com', '', 'A new quoted order has been approved', $html1);

			//print_r($success);
			//die();
			
			// /*
			// $mail2 =& JFactory::getMailer();
			// $config2 =& JFactory::getConfig(); 
			// $mail2->isHTML(true); 
			// $mail2->addRecipient($info->email);
			// $mail2->setSubject( 'Thank You for Your Order' ); 
			// $mail2->setBody( $html ); 
			// $mail2->Send();	
			// */
			
			
			// echo "approved";
			// echo "<script> window.opener.location.href = 'https://eu3.salesforce.com/".$opt_id."'; window.close(); </script>";
	
			
			
			// //new code to create i4u invoice - requires all data from SF
			
			$register = &JModel::getInstance('register','IbonlinestoreModel');
			$register->appriveQuoteInvoice($opt_id);
					
			// // Display the view
			//parent::display($tpl);
		}
		
		/*
		$data = array(
		  'FirstName' => "test opp1",
		  'LastName' => "opp1",
		  'Company' => "company opp1",
		  'Email' => "opp1@com.com",
		);
		
		$data = $this->getModel()->create_lead($data);
		
		print_r ($data);
		die();
	
	
		$data = $this->getModel()->convert_lead("00Q110000016eOvEAI");
		
		print_r($data);
		die();


		//print_r ($this->getModel("salesforce")->update_lead_opportunity("00611000002LGYyAAO","00111000004zobDAAQ"));
		
		$this->getModel("salesforce")->create_lineitem();
		*/
		
		//$this->getModel("salesforce")->get_licenses();
	}
}
?>