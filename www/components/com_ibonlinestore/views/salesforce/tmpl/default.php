<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

?><div id="ibonlinestore-content">
	<p><strong>Step1</strong>: <?php echo $this->item->step1; ?></p>
	<p><strong>Step2</strong>: <?php echo $this->item->step2; ?></p>
	<p><strong>Step3</strong>: <?php echo $this->item->step3; ?></p>
</div>