<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');

$item = $this->items;
?>
<input type="hidden" id="page_is_dirty" name="page_is_dirty" value="0" />
<script>
var cart_type = "small"
</script>
<div style='width:1012px;'>
<div class="left_pan">
<h1>Pricing and Terms</h1>
<h6>
<?php echo $item->subTitle; ?></h6>
<div class="steppan"><span>Step 1</span></div>
<div class="clear"></div>
<h3>Choose Your Agent Packages <span>( Please enter the number of agents of each type that you would like to purchase )</span></h3>
<div class="eachmachinepan">Each machine requires its own Agent.<br /> Prices are per-machine and according to the number of logical cores.</div>
<div class="stepdivoutpan">

<?php 

if (JFactory::getSession()->get('discountCode') == "IB404040p")
{
?>
<style>
.stepdiv
{
margin: 0px 79px 0px 0px
}
</style>

<?php

}


$i=0;
foreach($this->agents as $agent) {

if (JFactory::getSession()->get('discountCode') == "IB404040p" && $agent->title == "IB232")
{
	continue;
}
?>
<div class="stepdiv">
<div class="steptopcurvepan" id="<?php echo IbonlinestoreHelper::encrypt($agent->id."|".$agent->type."|".$agent->title."|".$agent->subtitle."|".$agent->price."|".$agent->description."|".$agent->sf_prciebookid);?>">
<p><?php echo $agent->title; ?></p>
<h5><?php echo $agent->subtitle; ?></h5>
<div class="stepdescreption"><?php echo str_replace("\n","<br>",$agent->description); ?></div>
<div class="pricecostpan"><span>$</span><?php echo $agent->price; ?></div>
<div class="agentpan">
<div class="agentnumberpan"><input type="text" name="" readonly id="qty_<?php echo $i; ?>" value="<?php echo IbonlinestoreHelper::itemQty($agent->id); ?>" />
<div class="agentxt">Agent</div>
<div class="agentplus" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/plussignimg.png" border="0" alt="" /></a></div>
<div class="agentminus"  id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/minussignimg.png" border="0" alt="" /></a></div>
</div>
</div>

</div>
</div>
<?php 
$i++;
} ?>

<div class="clear"> </div>
</div>
<div class="clickherepan">
<div class="clickherebt"><a href="#step2pan"><img src="/components/com_ibonlinestore/assets/images/clickherebt.png" border="0" alt="" /></a></div>
<div class="checkoutbt"><a href="/register"><img src="/components/com_ibonlinestore/assets/images/checkoutbt.png" border="0" alt="" /></a></div>
<div class="clear"> </div>
</div>

<?php echo $item->step1; 

if (JFactory::getSession()->get('discountCode') != "IB404040p") {
?>

<div class="steppan" id='step2pan'><span>Step 2</span></div>
<h3 class="bgnone">Choose Your Acceleration Solutions</h3>
<p>IncrediBuild accelerated various types of compilers, development environments, tools and applications including Visual Studio, Make &amp; Build tools. Please select the appropriate solutions relevant for your environment.</p>
<div class="solutionspan">

<?php foreach($this->solutions as $solution) { ?>
<div class="solutionsteppan" id="<?php echo IbonlinestoreHelper::encrypt($solution->id."|".$solution->type."|".$solution->title."|".$solution->subtitle."|".$solution->price."|".$solution->description."|".$solution->sf_prciebookid);?>">
<div class="solutionarrowpan"><?php echo $solution->title; ?><span><?php echo $solution->subtitle; ?></span></div>
<div class="accleratespan"><?php echo str_replace("\n","<br>",$solution->description); ?></div>
<div class="pricepan"><span>$</span><?php echo $solution->price; ?></div>
<div class="addtocartpan">
<div class="pluspan" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/plussignimg.png" border="0" alt="" /></a></div>
<div class="minuspan" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/minussignimg.png" border="0" alt="" /></a></div>
<input type="text" name="" id="qty_<?php echo $i; ?>" value="<?php echo IbonlinestoreHelper::itemQty($solution->id); ?>" /></div>
<div class="clear"> </div>
</div>
<?php 
$i++;
} ?>

<div class="clickherepan2">
<div class="nextbt1"><a href="/register"><img src="/components/com_ibonlinestore/assets/images/nextbt.png" border="0" alt="" /></a></div>
<div class="continuebt"><a href="/register"><img src="/components/com_ibonlinestore/assets/images/continuebt.png" border="0" alt="" /></a></div>
<div class="nextbt2"><a href="/register"><img src="/components/com_ibonlinestore/assets/images/nextbt.png" border="0" alt="" /></a></div>
</div>

<?php echo $item->step2; ?>
</div>

<br>
<div><a name="bundle_section"></a>
<div class="steppan"><span>Bundle</span></div>
<h3 class="bgnone">Purchasing for IncrediBuild FreeDev Premium</h3>
<p>The FreeDev Premium is offered for companies who had not purchased IncrediBuild before. It allows small teams to use a Bundle of Agents and solutions on a time limited basis.</p>
<p>Choose one of the following two options: </p>
<div class="solutionspan">

<?php foreach($this->bundles as $bundle) { ?>
<div class="solutionsteppan" id="<?php echo IbonlinestoreHelper::encrypt($bundle->id."|".$bundle->type."|".$bundle->title."|".$bundle->subtitle."|".$bundle->price."|".$bundle->description."|".$bundle->sf_prciebookid); ?>">
<div class="solutionarrowpan"><?php echo $bundle->title; ?><span><?php echo $bundle->subtitle; ?></span></div>
<div class="accleratespan"><?php echo str_replace("\n","<br>",$bundle->description); ?></div>
<div class="pricepan"><span>$</span><?php echo $bundle->price; ?></div>
<button class='bundleBtn'>Order Now</button>
<div class="clear"> </div>
</div>
<?php 
$i++;
} ?>
<br>
Note that the FreeDev Premium license:<br> 
1. Can NOT be split, added or combined to an existing license<br>
2. Can NOT be expanded –  it is for 5machines ONLY.<br>
3. Includes access to new versions and technical support.<br>
4. May be upgraded to an Enterprise license at any point, with full deduction of remaining license time.      <br>

</div>
</div>

<div class="steppan step3"><span>Step 3</span></div>
<?php echo $item->step3; ?>
<?php
 } 
?>
</div>

<div class="right_pan">
	<div id='cart_view'></div> 
	<div id='cart_view_h'></div>
          <div class="securepan"><a href="#"><img src="/components/com_ibonlinestore/assets/images/secure.png" alt=""></a></div>
		<?php echo $item->discount; ?>
          <h1>IncrediBuild ROI Calculator </h1>
          <div class="graph_pan">
            <div class="graphpic"><img src="/components/com_ibonlinestore/assets/images/graphimg.png" alt=""></div>
            <div class="returnpop">Return your investment <br>
              in less than 3 months!</div>
            <div class="testitnow"><a href="http://www.incredibuild.com/purchase-assistant.html"><img src="/components/com_ibonlinestore/assets/images/testitnow.png" alt=""></a></div>
            <div class="clear"></div>
          </div>
        </div>
</div>
