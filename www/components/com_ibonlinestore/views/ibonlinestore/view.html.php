<?php


/*------------------------------------------------------------------------
# view.html.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla view library
jimport('joomla.application.component.view');
/**
 * HTML Ibonlinestore View class for the IB Online Store Component
 */
class IbonlinestoreViewibonlinestore extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
	
		// Assign data to the view
		$this->items = $this->get('Steps');
		
		$this->agents = $this->get('Agents');
		$this->solutions = $this->get('Solutions');
		$this->bundles = $this->get('Bundles');
		// Check for errors.
		if (count($errors = $this->get('Errors'))):
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		endif;
		
		if ($_SERVER['REQUEST_URI'] != "/ibonlinestore")
			header ("location: https://www.incredibuild.com/ibonlinestore");
		
		setcookie("ib",$info,time()-36000000);
		JFactory::getSession()->clear('ib');
		setcookie("cart",$info,time()-36000000);
		JFactory::getSession()->clear('cart');
		setcookie("sf_cart_items",$info,time()-36000000);
		JFactory::getSession()->clear('sf_cart_items');
		setcookie("q",$info,time()-36000000);
		JFactory::getSession()->clear('q');
		
		// Display the view
		parent::display($tpl);
	}
}
?>