<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');
?>

<script>
var login_page = true;
var page_rout="<?php echo $this->from; ?>";
</script>

      <div class="middlecontainer">
        <div class="left_pan">

          <div class="clear"></div>
          <h1>Sign up / Log in</h1>
          <div class="signinnav">
            <ul>
              <li class="firsttab" id="tab1"><a href="javascript://">Registered User</a></li>
              <li class="secondtabactive" id="tab2"><a href="javascript://">New User</a></li>
            </ul>
          </div>
          <div id="form1">
            <div class="signupleftpan" style='background:none'>
                Please sign in to complete your purchase.</h6>
              <div class="signupformpan">
                <ul>
                  <li class="mandetory">[ <span class="star">*</span> ] Mandetory Fields </li>
                  <li><span>[ <span class="star">*</span> ]</span> User Name ::</li>
                  <li>
                    <div class="inputbox">
                      <input name="user_name" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Password : </li>
                  <li>
                    <div class="inputbox">
                      <input name="password" type="password" />
                    </div>
                  </li>
				  
                </ul>
              </div>
              <div class="forgetpassword"><a href="#">Forgot my password</a></div>
			  <div id='login_msg_error'></div>
			  <div class="submitbutton return2 loginpage">
                <input name="" type="image" src="/components/com_ibonlinestore/assets/images/submitpaymentbt2.png" alt="" />
              </div>
			  
            </div>

            <div class="clear"> </div>
          </div>
          <div id="form2">
            <div class="signupleftpan">
              <h6>Please register with us so you do not have to fill up your information every time you visit us.</h6>
              <div class="signupformpan">
                <ul>
                  <li class="mandetory">[ <span class="star">*</span> ] Mandetory Fields </li>
                  <li><span>[ <span class="star">*</span> ]</span> First Name :</li>
                  <li>
                    <div class="inputbox">
                      <input name="first_name" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Last Name :</li>
                  <li>
                    <div class="inputbox">
                      <input name="last_name" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Email Address : </li>
                  <li>
                    <div class="inputbox">
                      <input name="email" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Telephone Number:</li>
                  <li>
                    <div class="inputbox">
                      <input name="telephone" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> How did you hear about IncrediBuild? :</li>
                  <li>
				  <?
				  
				  $h = count($this->heardlist);
				  ?>
				  <select  name='Heard_from[]' multiple style=' width: 336px; height: <? echo ($h*18);?>px; '>
				  <?
				  
				  foreach ($this->heardlist as $it)
				  {
					
					echo '<option value="'.$it->text.'">'.$it->text.'</option>';
				  
				  }
				  print_r ($this->heardlist);
				  ?>
				  </select>
                      
                  </li>
                </ul>
              </div>
            </div>
            <div class="signuprightpan">
              <div class="signupformpan">
                <ul>
                  <li><span>[ <span class="star">*</span> ]</span> Company Name : </li>
                  <li>
                    <div class="inputbox">
                      <input name="company" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Mailing Address : </li>
                  <li>
                    <div class="inputbox">
                      <input name="street"  type="text" value="Street" onfocus="if(this.value=='Street')this.value='';" onblur="if(this.value=='')this.value='Street'" />
                    </div>
                  </li>
                  <li>
                    <div class="inputbox2">
                      <input name="city"  type="text" value="City" onfocus="if(this.value=='City')this.value='';" onblur="if(this.value=='')this.value='City'" />
                    </div>
                    <div class="inputbox2">
                      <input name="state"  type="text" value="State" class='no' onfocus="if(this.value=='State')this.value='';" onblur="if(this.value=='')this.value='State'" />
                    </div>
                    <div class="inputbox2 nomarginR">
                      <input name="country"  type="text" value="Country" onfocus="if(this.value=='Country')this.value='';" onblur="if(this.value=='')this.value='Country'" />
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> User name : </li>
                  <li>
                    <div class="inputbox">
                      <input name="user_name" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Password :</li>
                  <li>
                    <div class="inputbox">
                      <input name="password" type="password" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Re-enter Password :</li>
                  <li>
                    <div class="inputbox">
                      <input name="re_password" type="password" />
                    </div>
                  </li>
                  
                </ul>
              </div>
			  <div id='new_msg_error'></div>
              <div class="submitbutton new2 loginpage">
                <input name="" type="image" src="/components/com_ibonlinestore/assets/images/submitpaymentbt2.png" alt="" />
              </div>
              <div class="rememberpan">
                <ul>
                   <!--<li>
				 
                    <input name="remember" type="checkbox" value="" />
                    Remember me</li>-->
                  <li>
                    <input name="mlist" type="checkbox"  checked />
                    I would like to join your mailing list</li>
                </ul>
              </div>
            </div>
            <div class="clear"> </div>
          </div>
        </div>
        <div class="right_pan">
          <div id='cart_view'></div> 
			<div id='cart_view_h'></div>
          <div class="securepan"><a href="#"><img src="/components/com_ibonlinestore/assets/images/secure.png" alt="" /></a></div>
        </div>
        <div class="clear"></div>
      </div>

