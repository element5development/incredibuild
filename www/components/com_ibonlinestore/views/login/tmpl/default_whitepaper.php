<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filter.output');

?>

<script>
cart_type = "small"
q = "&edit=no"
</script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/jquery.js"></script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/jquery.loader.min.js"></script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/check_valid.js"></script>
<link href="components/com_ibonlinestore/assets/css/jquery.loader.min.css" rel="stylesheet" />
<link href="components/com_ibonlinestore/assets/css/ibonlinestore.css" rel="stylesheet" />

<style>

.redbox{

	border: 1px solid red;
}
.spacer{
	height:20px;
}
</style>


<?php 

//print_r($this->userData);
//echo $this->userData->id;

?>

  <div class="middlecontainer">
        <div class="left_pan">

          <div class="clear"></div>
          <h1>IncrediBuild White Paper</h1>
		
		<div class="spacer"></div>
		 <div class="brochureBtn">
		<a href="/extrafiles/whitepaper.pdf" class="" onclick="window.open(this.href, 'mywin',
		'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;">
		Click here to download</a>
		</div>
		  
        </div>
        <div class="clear"></div>
      </div>
	  
	  

     