<?php

/*------------------------------------------------------------------------

# view.html.php - IB Online Store Component

# ------------------------------------------------------------------------

# author    Daniel Krotoro

# copyright Copyright (C) 2013. All Rights Reserved

# license   GNU/GPL Version 2 or later - webmark.co.il

# website   www.webmark.co.il

-------------------------------------------------------------------------*/



// No direct access to this file

defined('_JEXEC') or die('Restricted access');



// import Joomla view library

jimport('joomla.application.component.view');



/**

 * HTML Thankyo View class for the Ibonlinestore Component

 */

class IbonlinestoreViewLogin extends JView

{

	// Overwriting JView display method

	function display($tpl = null)

	{

	
		$from = JRequest::getVar('f');

		$this->from = JRequest::getVar('f');

			

			if ($from!="" && IbonlinestoreHelper:: userInfo())

			{

				$this->sendMail2Office();

				//die();

				JFactory::getApplication()->redirect("?fnm=".$from);
				

			}

			elseif($from !="")

			{

				$this->heardlist = $this->getHeardList();

				parent::display($tpl);

			}

			elseif($url!="" && !IbonlinestoreHelper:: userInfo()){


				JFactory::getApplication()->redirect("login?f=".$url);

			}

			else{

				JFactory::getApplication()->redirect("index.php");

			}

			

	}

	private function sendMail2Office()

	{
		$user = IbonlinestoreHelper::userInfo();


		$message = "Full name: $user->first_name $user->last_name <br/>";
		$message .= "Email: $user->email <br/>";
		$message .= "User Name: $user->user_name <br/>";
		$message .= "Company: $user->company <br/>";
		$message .= "Phone: $user->telephone <br/>";
		$message .= "Street: $user->street <br/>";
		$message .= "City: $user->city <br/>";
		$message .= "State: $user->state <br/>";
		$message .= "Country: $user->country <br/>";

		$mail =& JFactory::getMailer();
		$config =& JFactory::getConfig(); 
		$mail->isHTML(true); 
		$mail->addRecipient('joel.blankchtein@incredibuild.com');
		$mail->setSubject( 'whitepaper.pdf download' ); 
		$mail->setBody( $message ); 
		$mail->Send();



	}
	

	private function getHeardList()

	{

		$db = JFactory::getDBO();

        $query = $db->getQuery(true);

        $query->select('*');

        $query->from('#__ibonlinestore_heardlist');

        $db->setQuery((string)$query);

        $data = $db->loadObjectList();





		return $data;

			



	}

}

?>