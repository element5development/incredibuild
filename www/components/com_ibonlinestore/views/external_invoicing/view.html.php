<?php
/*------------------------------------------------------------------------
# view.html.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Yaniv Shimony
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML Thankyo View class for the Ibonlinestore Component
 */
class IbonlinestoreViewexternal_invoicing extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
	   $sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');
       $opp_id = JRequest::getVar('OID');
       
        error_reporting(0);
        ini_set("display_errors", 1);
        $info = $res[1]->records[0];
        //print_r($res[1]->records[0]->Credit_Card_Company__c);
        if ($_REQUEST['doctype'] == 'invrec') {
        
        //get additional data
        
       $opp = $sf->get_opportunity_info($opp_id);

	   
	   if (count($opp[4]->records) > 1) {
			$poNumbers = '';
			$totalOpps = 0;
			foreach ($opp[4]->records as $oppertunity) {
		
		$info["itemDesc"][] = 'PO Number: '.$oppertunity->PO__c;
			$info["itemPrice"][] = $oppertunity->Amount;
			$info["itemQty"][] = '1';
			$poNumbers .= $oppertunity->PO__c.',';
			$totalOpps = $totalOpps + $oppertunity->Amount;
		}
$poNumbers = mb_substr($poNumbers, 0, -1);
		}
	   else {
	   $cart = $sf->get_newlineitems($opp_id);
       $cart = $cart->records;
       $info = array();
	
        foreach ($cart as $item) {
            $info["itemDesc"][] = $item->PricebookEntry->Product2->ProductCode.' - '.$item->PricebookEntry->Product2->Name;
			$info["itemPrice"][] = $item->UnitPrice;
			$info["itemQty"][] = $item->Quantity;
        
        }   
}
    	$poNum = $opp[0]->records[0]->PO__c;
		$type = $opp[1]->records[0]->Type__c;
		
		
                  
//            $_REQUEST['InvoiceItemPrice'] = str_replace('$','',$_REQUEST['InvoiceItemPrice']);
  //              $_REQUEST['CcAmount'] = str_replace('$', '', $_REQUEST['CcAmount']);
                
                
                $intDollar = str_replace(',','',$_REQUEST['CcAmount']);
                $intDollar = $opp[1]->records[0]->USD_Amount__c;
                $creditCardNumber = $opp[1]->records[0]->Credit_Card_number__c;
                $creditCardCompany = $opp[1]->records[0]->Credit_Card_Company__c;
                $payDate = $opp[1]->records[0]->Date__c;
                $payDate =  strtotime($payDate);
                //change date arrangment
                $payDate = date('d-m-Y',$payDate);

                $rate = simplexml_load_string(file_get_contents("http://www.boi.org.il/currency.xml"));
        		$rate = $rate->CURRENCY[0]->RATE;
        		$intDollar = floatval ($intDollar);
                $rate = floatval ($rate);
        		$intNIS = $intDollar * $rate;
                
				        $country = $opp[3]->records[0]->BillingAddress->country;
        $city = $opp[3]->records[0]->BillingAddress->city;
        $postalCode = $opp[3]->records[0]->BillingAddress->postalCode;
		$state = $opp[3]->records[0]->BillingAddress->state;
		$street = $opp[3]->records[0]->BillingAddress->street;
		$name = $opp[3]->records[0]->Name;
		$amount = $opp[0]->records[0]->Amount;

		
				
                $ccdate = explode('/',$_REQUEST['CcDate']);
                //$ccdate[2] = '20'.$ccdate[2];
                $ccdate = array_reverse($ccdate);
                $ccdate = implode('-',$ccdate);
                $_REQUEST['CcDate'] = $ccdate;
                $_REQUEST['CcAmount'] = str_replace('$','',$_REQUEST['CcAmount']);
                
                $invoiceSubject = 'IncrediBuild online order: '.$_REQUEST['InvoiceSubject'];
                
                $_REQUEST['CcAmount'] = str_replace(',','',$_REQUEST['CcAmount']);
                $_REQUEST['InvoiceItemPrice'] =	str_replace(',','',$_REQUEST['InvoiceItemPrice']);
                
                ini_set('set_time_limit', '5');  
                ini_set('max_execution_time', '5');  
		
		
                if ($type == 'Wire' || $type == 'Check') {
				$info = array();
				$info['itemDesc'][] ='Incredibuild';
        		$info['itemPrice'][] = $intDollar;
        		$info['itemQty'][] = '1';
				
				}
		
               	$fields = array(
                'compID'=>'xoreax',
				'sendOrig' => 'sales@incredibuild.com',				
                'user'=>'sales',  
                'pass'=>'828d66',
                'maampercent'=>'0',  
                'dateissued'=>date('Y-m-d'),  
                'clientname'=>$name,  
                'docType'=>'invrec',
                'currency'=>'2', 
                'rate' => $rate,   
                'totalvat'=>'0',
                'client_street' => $street,
				'client_country' => $country,
				'client_city' => $city,
				'desc'=>$info['itemDesc'],
        		'unitprice'=>$info['itemPrice'],
        		'quantity'=>$info['itemQty'],
        		'totalwithvat'=>$intNIS,  
                'taxexempt'=>'1',
                'paid'=>$intNIS,
                'totalsum'=>$intNIS,
                'totalpaid'=>$intNIS, 
                'show_response'=>'1',
                'debug'=>'yes',
                'hidenis'=>'1'
                );
                
              

				if ($type == 'Credit Card') {
				$fields['credit'] = '1';
                $fields['cc_validity_YY'] = '22';
                $fields['cc_validity_MM'] = '11';
                $fields['cc_cvv']  = '000';
                $fields['cc_id'] = '000000000';
                $fields['cc_cardtype'] = $creditCardCompany;
                $fields['cctotal'] = $intDollar;  
                $fields['cc_numofpayments'] = '1';
                $fields['cc_peraondate'] = $payDate;  
                $fields['cc_cardnumber'] = $creditCardNumber;  
				}

				if ($type == 'Wire') {
				$fields['banktransfer'] = 1;
				//$fields['depositedTo'] = 1;
				$fields['totalBT'] = $intDollar; 
				$fields['peraonDateBT'] = date('Y.m.d',time());
				
				}
                if ($type == 'Check') {
				
				$Formatted_Date_checks__c = $opp[1]->records[0]->Formatted_Date_checks__c;
				//echo $Formatted_Date_checks__c.'------333';
				
				$formDate = explode('/',$Formatted_Date_checks__c);
				
				$day = $formDate[1];
				$month = $formDate[0];
				$year = $formDate[2];
				$year = '20'.$year;
				if ($day[0] == '0') $day = str_replace('0','',$day);
				if ($month[0] == '0') $month = str_replace('0','',$month);
				
			
				$fields['cheque'] = 1;
				$fields['chequeTotal'][0] = $intDollar;
				$fields['chequeDateY'][0] = $year;
				$fields['chequeDateM'][0] = $month;
				$fields['chequeDateD'][0] = $day;
				$fields['chequeNumber'][0] = $opp[1]->records[0]->Check_Number__c;
				$fields['chequeSnif'][0] = $opp[1]->records[0]->Check_bank_branch_Snif__c;
				$fields['chequeAcc'][0] = $opp[1]->records[0]->Check__c;
				$fields['chequeBank'][0] = $opp[1]->records[0]->Check_Bank_name__c;
				
				}
				
								  $country = $opp[3]->records[0]->BillingAddress->country;

								  
                if ($country == 'Israel'  || $country == 'ישראל') {
                    $vatPercent = 0.18;
                    $vat = 18;
                    $fields['maampercent'] = $vat;
                    $fields['paidincvat']= $intNIS * (1 + $vatPercent);   
                    $fields['totalvat'] = $intNIS * $vatPercent;
                    $fields['totalwithvat']= $intNIS * (1 + $vatPercent);   
                    $fields['taxexempt'] = '0';
					if ($poNumbers) $poNum = $poNumbers;
                    $fields['hwc'] = 'עבור הזמנה מספר:: '.$poNum;
					$fields['es'] = 'חשבונית מס-קבל עבור הזמנה מספר: '.$poNum;
					
                }
                
                else {
            	
					$fields['lang'] = 'en';
					$fields['taxexempt'] = '1';
					if ($poNumbers) $poNum = $poNumbers;
                    $fields['hwc'] = 'INVOICE-RECEIPT FOR PURCHASE ORDER NUMBER : '.$poNum;
					$fields['es'] = 'Invoice-Receipt for Purchase Order number: '.$poNum;
					                 
                }

				
				$string = http_build_query($fields);
                $ch = curl_init();  
                curl_setopt($ch, CURLOPT_URL,"https://www.icount.co.il/api/create_doc.php");  
                curl_setopt($ch, CURLOPT_POST, 1);  
                curl_setopt($ch, CURLOPT_POSTFIELDS,"$string");  
                echo "-------------------------------------\n";  
                curl_exec ($ch);  
                echo "-------------------------------------\n";  
                curl_close ($ch);  
                die;  
				echo 'test';
//                exit('??? after die???'); 
                
                //*** End of New API                      

        }
	   	
           //start of tax_invoice
           	elseif ($_REQUEST['doctype'] == 'invoice') {

			
       $opp = $sf->get_opportunity_info($opp_id);
       $cart = $sf->get_newlineitems($opp_id);
       $cart = $cart->records;
       $poNum = $opp[0]->records[0]->PO__c;
		
       $info = array();
       
       


        foreach ($cart as $item) {
            $info["itemDesc"][] = $item->PricebookEntry->Product2->ProductCode.' - '.$item->PricebookEntry->Product2->Name;
			$info["itemPrice"][] = $item->UnitPrice;
			$info["itemQty"][] = $item->Quantity;
			$info["originalPrice"][] = $item->PricebookEntry->UnitPrice;	
        }   
        print_r($info);
		exit;
        
				$country = $opp[3]->records[0]->BillingAddress->country;
                
                $intDollar = str_replace(',','',$_REQUEST['CcAmount']);
                $intDollar = $opp[1]->records[0]->USD_Amount__c;
                $creditCardNumber = $opp[1]->records[0]->Credit_Card_number__c;
                $creditCardCompany = $opp[1]->records[0]->Credit_Card_Company__c;                                
                $payment_due = $opp[0]->records[0]->Payment_Due_Date__c;

				
                $payDate = date('d-m-Y',$payDate);

                $rate = simplexml_load_string(file_get_contents("http://www.boi.org.il/currency.xml"));
        		$rate = $rate->CURRENCY[0]->RATE;
        		$intDollar = floatval ($intDollar);
                $rate = floatval ($rate);
        		$intNIS = $intDollar * $rate;
                
                $ccdate = explode('/',$_REQUEST['CcDate']);

				
                $ccdate = array_reverse($ccdate);
                $ccdate = implode('-',$ccdate);
                $_REQUEST['CcDate'] = $ccdate;
                $_REQUEST['CcAmount'] = str_replace('$','',$_REQUEST['CcAmount']);
                
                $invoiceSubject = 'IncrediBuild online order: '.$_REQUEST['InvoiceSubject'];
                
                $_REQUEST['CcAmount'] = str_replace(',','',$_REQUEST['CcAmount']);
                $_REQUEST['InvoiceItemPrice'] =	str_replace(',','',$_REQUEST['InvoiceItemPrice']);
                
                ini_set('set_time_limit', '5');  
                ini_set('max_execution_time', '5');  
                echo $payDate;
                echo '<br/>';
                
 
		$country = $opp[3]->records[0]->BillingAddress->country;
        $city = $opp[3]->records[0]->BillingAddress->city;
        $postalCode = $opp[3]->records[0]->BillingAddress->postalCode;
		$state = $opp[3]->records[0]->BillingAddress->state;
		$street = $opp[3]->records[0]->BillingAddress->street;
		$name = $opp[3]->records[0]->Name;
		$amount = $opp[0]->records[0]->Amount;


		
                $fields = array(
                'compID'=>'xoreax',
				'sendOrig' => 'yaniv@webmark.co.il',				
                'user'=>'sales',  
                'pass'=>'828d66',
                'maampercent'=>'0',  
                'dateissued'=>date('Ymd'),  
                'clientname'=>$name,
               'client_street' => $street,
				'client_country' => $country,
				'client_city' => $city,
                'docType'=>'invoice',
                'currency'=>'2', 
                'rate' => $rate,   
                'paydate' => $payment_due,  
                'desc'=>$info['itemDesc'],
        		'unitprice'=>$info['itemPrice'],
        		'quantity'=>$info['itemQty'],
                'paid'=>$intNIS,
                'totalsum'=>$intNIS,
                'totalpaid'=>$intNIS, 
                'show_response'=>'1',
                'debug'=>'yes',
                'hidenis'=>'1'
                );
				  $country = $opp[3]->records[0]->BillingAddress->country;

                if ($country == 'Israel' || $country == 'ישראל') {
		
                    $vatPercent = 0.18;
                    $vat = 18;
                    $fields['maampercent'] = $vat;
                    $fields['paidincvat']= $intNIS * (1 + $vatPercent);   
                    $fields['totalvat'] = $intNIS * $vatPercent;
                    $fields['totalwithvat']= $intNIS * (1 + $vatPercent);   
                    $fields['taxexempt'] = '0';
                    $fields['hwc'] = 'עבור הזמנה מספר:: '.$poNum;
					$fields['es'] = 'חשבונית מס עבור הזמנה מספר: '.$poNum;
                }
                
                else {
            	
					$fields['lang'] = 'en';
					$fields['taxexempt'] = '1';
                    $fields['hwc'] = 'INVOICE FOR PURCHASE ORDER NUMBER : '.$poNum;
					$fields['es'] = 'Invoice for Purchase Order number: '.$poNum;
                }



                $string = http_build_query($fields);
                $ch = curl_init();  
                curl_setopt($ch, CURLOPT_URL,"https://www.icount.co.il/api/create_doc.php");  
                curl_setopt($ch, CURLOPT_POST, 1);  
                curl_setopt($ch, CURLOPT_POSTFIELDS,"$string");  
                echo "-------------------------------------\n";  
                curl_exec ($ch);  
                echo "-------------------------------------\n";  
                curl_close ($ch);  
                die;  
                 
                
                //*** End of New API                      

        
        
        
        }
	
        
                   	elseif ($_REQUEST['doctype'] == 'deal') {
           	    
                   $opp = $sf->get_opportunity_info($opp_id);					
				   $cart = $sf->get_newlineitems($opp_id);
				   $cart = $cart->records;
				   $poNum = $opp[0]->records[0]->PO_Confirmation_num__c;
	   
	   
        $info = array();
		$productDiscount = $opp[2]->VolumeDiscountOverride__c;
		$ResellerDiscount = $opp[3]->records[0]->VolumeDiscountOverride__c;		
		$invoiceProductsHTML = '';
		$netSumBeforeReseller = 0;
        foreach ($cart as $item) {
		
            $info["itemDesc"][] = $item->PricebookEntry->Product2->ProductCode.' - '.$item->PricebookEntry->Product2->Name;
			$info["itemPrice"][] = $item->UnitPrice;
			$info["itemQty"][] = $item->Quantity;
			$invoiceProductsHTML .= '<tr>
			<td>'.$item->PricebookEntry->Product2->ProductCode.'</td>
			<td><span class="bold">'.$item->PricebookEntry->Product2->Family.'</span><br/>
				'.$item->PricebookEntry->Product2->Name.'
			</td>
			<td>'.$item->Quantity.'</td>
			<td>$'.$item->PricebookEntry->UnitPrice.'</td>
			<!--<td>'.$productDiscount.'%</td>-->
			<td>'.$ResellerDiscount.'%</td>
			<td>$'.$item->UnitPrice.'</td>
			<td>$'.number_format($item->TotalPrice,2).'</td>';
			$netSumBeforeReseller = $netSumBeforeReseller + $item->TotalPrice;			
        }   
			
			$country = $opp[3]->records[0]->BillingAddress->country;
			$city = $opp[3]->records[0]->BillingAddress->city;
			$postalCode = $opp[3]->records[0]->BillingAddress->postalCode;
			$state = $opp[3]->records[0]->BillingAddress->state;
			$street = $opp[3]->records[0]->BillingAddress->street;
			$name = $opp[3]->records[0]->Name;
			$amount = $opp[0]->records[0]->Amount;
	
			$intDollar = str_replace(',','',$amount);
			$intDollar = $amount;
			$creditCardNumber = $opp[1]->records[0]->Credit_Card_number__c;
			$creditCardCompany = $opp[1]->records[0]->Credit_Card_Company__c;                                
			$payment_due = $opp[0]->records[0]->Payment_Due_Date__c;
                //$payment_due = strtotime($payment_due);
                //$payment_due = date('d-m-Y',$payment_due);
                //echo '<br/>'.$payment_due;
                //$payDate = $opp[1]->records[0]->Date__c;
                //$payDate =  strtotime($payDate);
                //change date arrangment
                $payDate = date('Y-m-d',$payDate);

                $rate = simplexml_load_string(file_get_contents("http://www.boi.org.il/currency.xml"));
        		$rate = $rate->CURRENCY[0]->RATE;
        		$intDollar = floatval ($intDollar);
                $rate = floatval ($rate);

        		$intNIS = $intDollar * $rate;
                

                $ccdate = explode('/',$_REQUEST['CcDate']);
                //$ccdate[2] = '20'.$ccdate[2];
                $ccdate = array_reverse($ccdate);
                $ccdate = implode('-',$ccdate);
                $_REQUEST['CcDate'] = $ccdate;
                $_REQUEST['CcAmount'] = str_replace('$','',$_REQUEST['CcAmount']);
                
                $invoiceSubject = 'IncrediBuild online order: '.$_REQUEST['InvoiceSubject'];
                
                $_REQUEST['CcAmount'] = str_replace(',','',$_REQUEST['CcAmount']);
                $_REQUEST['InvoiceItemPrice'] =	str_replace(',','',$_REQUEST['InvoiceItemPrice']);
                
                ini_set('set_time_limit', '5');  
                ini_set('max_execution_time', '5');                  
				$intNIS = intval ($intNIS);
				$fields = array(
                'compID'=>'xoreax',  
				//'sendOrig' => 'sales@incredibuild.com',
				'sendOrig' => 'sales@incredibuild.com',
                'user'=>'sales',  
                'pass'=>'828d66',
                'maampercent'=>'0',  
                'dateissued'=>date('Ymd'),  
                'clientname'=>$name,  
                'docType'=>'deal',
                'currency'=>'2', 
                'rate' => $rate,
				'client_street' => $street,
				'client_country' => $country,
				'client_city' => $city,
                  'validity' => $payment_due,  
                'desc'=>$info['itemDesc'],
        		'unitprice'=>$info['itemPrice'],
        		'quantity'=>$info['itemQty'],
                'paid'=>$intNIS,
                'totalsum'=>$intNIS,				
                'show_response'=>'1',
                'debug'=>'yes',
                'hidenis'=>'1'
                );
				$country = $opp[3]->records[0]->BillingAddress->country;
				$vatPercent = '0.00';
				
				
				if ($country == 'Israel'  || $country == 'ישראל') {
				
                    $vatPercent = 0.18;
                    $vat = 18;
                    $fields['maampercent'] = $vat;
                    $fields['paidincvat']= $intNIS * (1 + $vatPercent);   
                    $fields['totalvat'] = $intNIS * $vatPercent;
                    $fields['totalwithvat']= $intNIS * (1 + $vatPercent);   
                    $fields['taxexempt'] = '0';
					$fields['hwc'] = 'עבור הזמנה מספר:: '.$poNum;
					$fields['es'] = 'חשבון עסקה עבור הזמנה מספר: '.$poNum;
     			  
                }
                
                else {
					$fields['es'] = 'Invoice for Purchase Order number: '.$poNum;
				     $fields['hwc'] = 'FOR PURCHASE ORDER NUMBER: '.$poNum;
					$fields['lang'] = 'en';
					$fields['taxexempt'] = '1';
					$fields['totalpaid'] = $intNIS;
				};
				
		$invoiceNumber = $opp[0]->records[0]->Invoice_Number__c;
		$CloseDate =  $opp[0]->records[0]->CloseDate;
		$CloseDate = strtotime($CloseDate);
		$resellerID = $opp[0]->records[0]->ResellerLookup__c;
		$oppAccountID = $opp[0]->records[0]->AccountId;
		$firstAccountName = $opp[2]->Name;
		$secondAccountName = $opp[3]->records[0]->Name;
		$AccountNumber = $opp[2]->records[0]->AccountNumber;
		$AccountNumber = $opp[2]->AccountNumber;

		if ($VolumeDiscountOverride__c && $VolumeDiscountOverride__c != 0) $VolumeDiscountOverride__c = '<div>'.$VolumeDiscountOverride__c.'% Reseller Discount:</div>';
		else $VolumeDiscountOverride__c = '';
		if ($resellerID == $oppAccountID) $clientTitle = '<div class="bold">'.$firstAccountName.'</div>';
		else $clientTitle = '<div class="bold">'.$secondAccountName.'</div>			<div>For end user: <span class="bold">'.$firstAccountName.'</span></div>';

		if ($VolumeDiscountOverride__c) $totalBeforeTax = $netSumBeforeReseller - $totalResellerDiscount;
		else $totalBeforeTax = $netSumBeforeReseller;

		$VAT = $opp[0]->records[0]->VAT__c;
		if ($VAT) {
			$VATPercentage = 1 + $VAT / 100;
			$vatPercent	= '<div>'.$VAT.'% Tax:</div>';
			$netSumBeforeResellerAfterVAT = $totalBeforeTax * $VAT / 100;
			$vatTotal = '<div>$'.$netSumBeforeResellerAfterVAT.'</div>';
			$total = $totalBeforeTax - $netSumBeforeResellerAfterVAT;
			}
		else {
		$vatPercent = '';
		$vatTotal = '';
		$total = $totalBeforeTax;
		}
		$total = number_format($total);


$CloseDate = date('F d, Y',$CloseDate);
if (JFactory::getSession()->get('ponum') && JFactory::getSession()->get('ponum') != '') {
							$poNum = JFactory::getSession()->get('ponum');
							}
							
							$file = getcwd().'/invoiceLayout/index.html';
$invoiceLayout = file_get_contents($file);
$Comments_for_customer_invoice__c = $opp[0]->records[0]->Comments_for_customer_invoice__c;
$invoiceLayout = str_replace('##Comments_for_customer_invoice__c##',$$Comments_for_customer_invoice__c,$invoiceLayout);

$invoiceLayout = str_replace('##street##',$street,$invoiceLayout);
$invoiceLayout = str_replace('##city##',$city,$invoiceLayout);
$invoiceLayout = str_replace('##state##',$state,$invoiceLayout);
$invoiceLayout = str_replace('##postalCode##',$postalCode,$invoiceLayout);
$invoiceLayout = str_replace('##country##',$country,$invoiceLayout);
$invoiceLayout = str_replace('##invoiceNumber##',$invoiceNumber,$invoiceLayout);
$invoiceLayout = str_replace('##CloseDate##',$CloseDate,$invoiceLayout);
$invoiceLayout = str_replace('##$vatPercent##',$$vatPercent,$invoiceLayout);
$invoiceLayout = str_replace('##intDollar##',$intDollar,$invoiceLayout);
$invoiceLayout = str_replace('##invoiceProductsHTML##',$invoiceProductsHTML,$invoiceLayout);
$invoiceLayout = str_replace('##clientTitle##',$clientTitle,$invoiceLayout);
$invoiceLayout = str_replace('##poNum##',$poNum,$invoiceLayout);
//$invoiceLayout = str_replace('##netSumBeforeReseller##',$netSumBeforeReseller,$invoiceLayout);
$invoiceLayout = str_replace('##VolumeDiscountOverride__c##',$VolumeDiscountOverride__c,$invoiceLayout);
$invoiceLayout = str_replace('##totalResellerDiscount##',$totalResellerDiscount,$invoiceLayout);
$invoiceLayout = str_replace('##VAT##',$VAT,$invoiceLayout);
$invoiceLayout = str_replace('##vatPercent##',$vatPercent,$invoiceLayout);
$invoiceLayout = str_replace('##vatTotal##',$vatTotal,$invoiceLayout);
$invoiceLayout = str_replace('##total##',$total,$invoiceLayout);



echo $invoiceLayout;
exit ($string);

                $string = http_build_query($fields);
                $ch = curl_init();  
                curl_setopt($ch, CURLOPT_URL,"https://www.icount.co.il/api/create_doc.php");  
                curl_setopt($ch, CURLOPT_POST, 1);  
                curl_setopt($ch, CURLOPT_POSTFIELDS,"$string");  
                echo "-------------------------------------\n";  
                curl_exec ($ch);  
                echo "-------------------------------------\n";  
                curl_close ($ch);  
                die;  
                 
                        
        }


    }
}
?>
