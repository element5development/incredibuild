<?php
/*------------------------------------------------------------------------
# view.html.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla view library
jimport('joomla.application.component.view');
/**
 * HTML Thankyou View class for the IB Online Store Component
 */
class IbonlinestoreViewthankyou extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		// Assign data to the view
		$this->items = $this->get('Items');
		// Check for errors.
		if (count($errors = $this->get('Errors'))):
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		endif;
		
		if (JFactory::getSession()->get('ib') && JFactory::getSession()->get('cart'))
		{
			//mark special code as used
			if (JFactory::getSession()->get('discountCode') == 'VSP20OFF') {
				$sf_id = 0;
				$session = JFactory::getSession();
				$ib = $session->get('tmp_userlogin');
				$tmp  = explode("|",IbonlinestoreHelper::decrypt($ib));
				$info = IbonlinestoreHelper::checkExists(" id = '".IbonlinestoreHelper::escape($tmp[0])."'");
					
				if ($info[0])
				{
					$salesforce = &JModel::getInstance('salesforce','IbonlinestoreModel');
					$sf_id = $info[0]->sf_id;
					if ($sf_id != 0) {
						//$eligibility = $salesforce->check_special_promotion_eligibility($sf_id);
						$salesforce->mark_special_promotion_eligibility_redeemed($sf_id);
					}
				}
			}
			
			JFactory::getSession()->clear('cart');
			JFactory::getSession()->clear('ok');
			JFactory::getSession()->clear('discountType');
			JFactory::getSession()->clear('discountVal');
			JFactory::getSession()->clear('discountCode');
			JFactory::getSession()->clear('discountCond');
			
			setcookie("ib",$info,time()-36000000);
			JFactory::getSession()->clear('ib');
			setcookie("cart",$info,time()-36000000);
			JFactory::getSession()->clear('cart');
			setcookie("sf_cart_items",$info,time()-36000000);
			JFactory::getSession()->clear('sf_cart_items');
			setcookie("q",$info,time()-36000000);
			JFactory::getSession()->clear('q');
			JFactory::getSession()->set('index',JRequest::getVar('index'));
		
			parent::display($tpl);
		}
		else
		{	
			JFactory::getApplication()->redirect("index.php?option=com_ibonlinestore");
		}
	}
	


	
}
?>