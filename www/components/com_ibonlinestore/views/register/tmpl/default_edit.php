<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filter.output');

?>

<script>
cart_type = "small"
q = "&edit=no"
</script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/jquery.js"></script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/jquery.loader.min.js"></script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/check_valid.js"></script>
<link href="components/com_ibonlinestore/assets/css/jquery.loader.min.css" rel="stylesheet" />
<link href="components/com_ibonlinestore/assets/css/ibonlinestore.css" rel="stylesheet" />

<style>

.redbox{

	border: 1px solid red;
}

</style>


<?php 

//print_r($this->userData);
//echo $this->userData->id;

?>

      <div class="edit_profile signupformpan">
		<form method="post" class="form-validate" id="edit_profile" >
		 <div style="overflow-y:scroll;height:390px;width:100%;">
		  <ul>
		  <li class="mandetory">[ <span class="star">*</span> ] Mandetory Fields </li>
		  <br>
		  <li style="margin-left:3px;">Email Address : <?php echo $this->userData->email; ?></li>

				<li style="margin-left:3px;">User name : <?php echo $this->userData->user_name; ?></li>
				
		  <br>
		  <li><span>[ <span class="star">*</span> ]</span> First Name :</li>
		  <li>
			<div class="inputbox">
			  <input type="text" name="first_name" value="<?php echo $this->userData->first_name; ?>" >
			</div>
		  </li>
		   <li><span>[ <span class="star">*</span> ]</span> Last Name :</li>
			  <li>
				<div class="inputbox">
				  <input type="text" name="last_name" value="<?php echo $this->userData->last_name; ?>" >
				</div>
			  </li>


				<li><span>[ <span class="star">*</span> ]</span> Telephone Number:</li>
                  <li>
                    <div class="inputbox">
                      <input type="text" name="telephone" value="<?php echo $this->userData->telephone; ?>">
                    </div>
                  </li>
				<li><span>[ <span class="star">*</span> ]</span> Company Name : </li>
                  <li>
                    <div class="inputbox">
                      <input type="text" name="company" value="<?php echo $this->userData->company; ?>">
                    </div>
                  </li>
				<li><span>[ <span class="star">*</span> ]</span> Mailing Address : </li>
                  <li>
                    <div class="inputbox">
                      <input name="street" type="text" value="<?php echo $this->userData->street; ?>" onfocus="if(this.value=='Street')this.value='';" onblur="if(this.value=='')this.value='Street'">
                    </div>
                  </li>
                  <li>
                    <div class="inputbox2">
                      <input name="city" type="text" value="<?php echo $this->userData->city; ?>" onfocus="if(this.value=='City')this.value='';" onblur="if(this.value=='')this.value='City'">
                    </div>
                    <div class="inputbox2">
                      <input name="state" type="text" value="<?php echo $this->userData->state; ?>" class="no" onfocus="if(this.value=='State')this.value='';" onblur="if(this.value=='')this.value='State'">
                    </div>
                    <div class="inputbox2 nomarginR">
                      <input name="country" type="text" value="<?php echo $this->userData->country; ?>" onfocus="if(this.value=='Country')this.value='';" onblur="if(this.value=='')this.value='Country'">
                    </div>
                    <div class="clear"></div>
                  </li>
				
				  
				</ul>
				<div id='reset_txt'>
					<a href='#' style='color:#000' onclick='$("#reset_txt").hide();$("#reset_password").show();'>Reset Password</a>
				</div>
				<div id='reset_password' style='display:none'>
				<ul>
				<li> Password :</li>
                  <li>
                    <div class="inputbox">
						<input type="password" name="password" id="password" >
                    </div>
                  </li>
                  <li> Re-enter Password :</li>
                  <li>
                    <div class="inputbox">
                      <input type="password" name="retype_new_password" id="password2" name="password2" >
                    </div>
                  </li>
				</ul>
				</div>
				 <div class="submitbutton new">
	                <input type="image" id="submit" class="validate" onClick="return CheckValid();" name="save" src="/components/com_ibonlinestore/assets/images/submitedit.png" alt=""  value="submit"/>
                 </div>
				</div>
		</form>
	</div>
