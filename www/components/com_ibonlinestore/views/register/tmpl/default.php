<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');
if ($_REQUEST['test'] == 1) {
jimport('joomla.application.component.model');
JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_ibonlinestore/models');
$sf = JModelLegacy::getInstance( 'salesforce', 'IbonlinestoreModel' );
$opp_id = JRequest::getVar('OID');      
ini_set("display_errors", 1);
$opp = $sf->get_opportunity_info($opp_id);
print_r($opp[5]);
exit('test');
}

?>

<script>
cart_type = "small"
q = "&edit=no"
</script>
      <div class="middlecontainer">
        <div class="left_pan">

          <div class="clear"></div>
          <h1>Sign up / Log in</h1>
          <div class="signinnav">
            <ul>
              <li class="firsttab" id="tab1"><a href="javascript://">Returning customer</a></li>
              <li class="secondtabactive" id="tab2"><a href="javascript://">New customer</a></li>
            </ul>
          </div>
          <div id="form1">
            <div class="signupleftpan">
              <h6>Welcome back. <br />
                Please sign in to complete your purchase.</h6>
              <div class="signupformpan">
                <ul>
                  <li class="mandetory">[ <span class="star">*</span> ] Mandatory Fields </li>
                  <li><span>[ <span class="star">*</span> ]</span> User Name :</li>
                  <li>
                    <div class="inputbox">
                      <input name="user_name" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Password : </li>
                  <li>
                    <div class="inputbox">
                      <input name="password" type="password" />
                    </div>
                  </li>
                </ul>
              </div>
              <div class="forgetpassword"><a href="#">Forgot my password</a></div>
			  <br>
			             <div id='login_msg_error'></div>
              <div class="submitbutton return">
                <input name="" type="image" src="/components/com_ibonlinestore/assets/images/submitpaymentbt.png" alt="" />
              </div>
            </div>

  
          </div>
          <div id="form2">
            <div class="signupleftpan">
              <h6>Please register with us so you do not have to fill up your information every time you visit us.</h6>
              <div class="signupformpan">
                <ul>
                  <li class="mandetory">[ <span class="star">*</span> ] Mandatory Fields </li>
                  <li><span>[ <span class="star">*</span> ]</span> First Name :</li>
                  <li>
                    <div class="inputbox">
                      <input name="first_name" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Last Name :</li>
                  <li>
                    <div class="inputbox">
                      <input name="last_name" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Email Address : </li>
                  <li>
                    <div class="inputbox">
                      <input name="email" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Telephone Number:</li>
                  <li>
                    <div class="inputbox">
                      <input name="telephone" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Company Name : </li>
                  <li>
                    <div class="inputbox">
                      <input name="company" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Mailing Address : </li>
                  <li>
                    <div class="inputbox">
                      <input name="street"  type="text" value="Street" onfocus="if(this.value=='Street')this.value='';" onblur="if(this.value=='')this.value='Street'" />
                    </div>
                  </li>
                  <li>
                    <div class="inputbox2">
                      <input name="city"  type="text" value="City" onfocus="if(this.value=='City')this.value='';" onblur="if(this.value=='')this.value='City'" />
                    </div>
                    <div class="inputbox2">
                      <input name="state"  type="text" value="State" class='no' onfocus="if(this.value=='State')this.value='';" onblur="if(this.value=='')this.value='State'" />
                    </div>
                    <div class="inputbox2 nomarginR">
                      <input name="country"  type="text" value="Country" onfocus="if(this.value=='Country')this.value='';" onblur="if(this.value=='')this.value='Country'" />
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> User Name : </li>
                  <li>
                    <div class="inputbox">
                      <input name="user_name" type="text" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Password :</li>
                  <li>
                    <div class="inputbox">
                      <input name="password" type="password" />
                    </div>
                  </li>
                  <li><span>[ <span class="star">*</span> ]</span> Re-enter Password :</li>
                  <li>
                    <div class="inputbox">
                      <input name="re_password" type="password" />
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="signuprightpan">
              <h6>Important</h6>
              <p>The license file (as well as the purchase confirmation email)
                will be sent to the  following email :</p>
              <div class="signupformpan">
                <ul>
                  <li><span>[ <span class="star">*</span> ]</span> Licensee e-mail  :</li>
                  <li>
                    <div class="inputbox">
                      <input name="license_email" type="text" />
                    </div>
                  </li>
				  <?php
				  if (!JFactory::getSession()->get('q')){
				  ?>
                  <li><span>[ <span class="star">*</span> ]</span> License Registered Name :</li>
                  <li>
                    <div class="inputbox">
                      <input name="registered_name" type="text" />
                    </div>
                  </li>
				  <?php } ?>
                  <!--  <li class="submitpay">
                  <input name="" type="image" src="/components/com_ibonlinestore/assets/images/submitpaymentbt.png" alt="" />
                </li>-->
                </ul>
              </div>
              <div class="termscondition">
                <ul>
                  <li>The "Registered Name" field will be encoded into your license file and will be displayed as part of your license information.</li>
                  <li>This will typically contain the organization, project or development team name.</li>
                  <li>Make sure you have spelled this field correctly.</li>
                  <li>In case you order upgrades to an existing license, please check your license's registered name by right click the IncrediBuild Tray-Icon -> <a href="#">Help|About</a>.</li>
                </ul>
              </div>
			  <div id='new_msg_error'></div>
              <div class="submitbutton new">
                <input name="" type="image" src="/components/com_ibonlinestore/assets/images/submitpaymentbt.png" alt="" />
              </div>
              <div class="rememberpan">
                <ul>
                   <!--<li>
				 
                    <input name="remember" type="checkbox" value="" />
                    Remember me</li>-->
                  <li>
                    <input name="mlist" type="checkbox"  checked />
                    I would like to join your mailing list</li>
                </ul>
              </div>
			  
			  <input type="hidden" name="page_referer" id="page_referer" value="<?php echo htmlspecialchars($_SERVER['HTTP_REFERER']); ?>" />
			  
			  
            </div>
            <div class="clear"> </div>
          </div>
        </div>
        <div class="right_pan">
          <div id='cart_view'></div> 
			<div id='cart_view_h'></div>
          <div class="securepan"><a href="#"><img src="/components/com_ibonlinestore/assets/images/secure.png" alt="" /></a></div>
        </div>
        <div class="clear"></div>
      </div>
