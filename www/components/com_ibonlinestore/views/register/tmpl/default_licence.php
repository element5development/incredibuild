<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

if (JRequest::get( 'post' ))
{
?>
<script>
window.parent.location = "/cart"
</script>
<?php
die();
}
?>
<html>
<body>
  <script src="/components/com_ibonlinestore/assets/js/jquery.js" type="text/javascript"></script>
<style>
.licence_div
{
	display: block;
	width: 100%;
	height: 45px;
}

body,html
{
	font-family: myriad pro, "Lucida Grande","Lucida Sans Unicode",Arial,Verdana,sans-serif;
	text-decoration: none;
}

.btn_cont
{
padding: 10px;
overflow: hidden;
font: 1.3em bold helvetica, arial;
color: #fff;
text-shadow: 0 -2px 1px rgba(0, 0, 0, 0.25);
background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0.25, #3b3b3b), color-stop(0.75, #575757));
background-image: linear-gradient(bottom, #3b3b3b 25%, #575757 75%);
background-image: -moz-linear-gradient(bottom, #3b3b3b 25%, #575757 75%);
background-image: -o-linear-gradient(bottom, #3b3b3b 25%, #575757 75%);
background-image: -webkit-linear-gradient(bottom, #de257b 25%, #DE258F 75%);
background-image: -ms-linear-gradient(bottom, #3b3b3b 25%, #575757 75%);
-webkit-border-radius: 5px 5px 0 0px;
border-radius: 5px 5px 5px 5px;
-moz-border-radius-topright: 5px;
-moz-border-radius-bottomright: 5px;
-moz-border-radius-bottomleft: 5px;
-moz-border-radius-topleft: 5px;
border: 0px;
height: 39px;
padding: 9px;
margin: 0px;
cursor: pointer;
}
</style>
<script>

$(document).ready(function()
{
	function validateEmail(email) 
	{
		var re = /\S+@\S+\.\S+/;
		return re.test(email);
	}

	$("#btn_cont").click(function()
	{
		var msg = "";
		
		 if ($("[name='licence_select']:checked").val() == "new")
		{
			if ($("#license_n").val().trim() == "")
				msg = "Please enter license name"
		}
		
		if (!validateEmail($("#license_email").val()))
			msg = "Please enter a valid licensee e-mail"
			
		$("#msg").html(msg)
		
		if (msg=="")	
			return true;
		else
		{
			return false;
		}
	})
	
	$("#license_n,#license_l").click(function()
	{
		$(this).parent().parent().find("input").first().click();
		
	})
	
	$(".messi-title" , window.parent.document).html("Licensee email")

})


</script>
<form method='post' >
<?php

if (!JFactory::getSession()->get('q'))
{
			
			
if ($this->licences->records):
?>
<div class='licence_div' >
<label>
<div style='float:left'>
<input type='radio' name='licence_select' value='list' checked>
</div>
<div style='float:left'>
Select existing licence : 
&emsp;
<select style='width: 190px;' name='license_l' id='license_l'>

<?php

foreach ($this->licences->records as $entry):
?>
	<option value='<?php echo $entry->Id?>|<?php echo $entry->Name?>'><?php echo $entry->Name?>
<?php
endforeach;

?>
</select>
</div>
</label>
</div>
<?php endif; ?>

<div class='licence_div' >
<label>
<div style='float:left'>
<input type='radio' name='licence_select' value='new' <?php echo (!$this->licences->records) ? "checked" : ""; ?> >
</div>
<div style='float:left'>
Create a new license : 
&emsp;&nbsp;&nbsp;
<input type='text' name='license_n' id='license_n'>
</div>
</label>
</div>

<?php }
else
{
	echo "<br><Br>";
} ?>

<div class='licence_div' >

<center>
	<font color='#e2107c'>[*]</font>Enter licensee e-mail : <br>
	<input name='license_email' id='license_email'>
	
</center>

</div>
<br>
<div align=right>
<span id='msg' style='color:red'></span>
<input type='submit' class='btn_cont' id='btn_cont' value='Continue ->'>
</form>
</div>
</body>
</html>

