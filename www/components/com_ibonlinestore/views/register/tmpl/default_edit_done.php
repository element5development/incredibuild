<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filter.output');

?>

<script>
cart_type = "small"
q = "&edit=no"
</script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/jquery.js"></script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/jquery.loader.min.js"></script>
<script type="text/javascript" src="components/com_ibonlinestore/assets/js/check_valid.js"></script>
<link href="components/com_ibonlinestore/assets/css/jquery.loader.min.css" rel="stylesheet" />
<link href="components/com_ibonlinestore/assets/css/ibonlinestore.css" rel="stylesheet" />

<center>
Your changes has been saved<br><Br>
<a href='#' onclick='$(".messi-closebtn", parent.document.body).click()'><u>Close Window</u></a>
</center>