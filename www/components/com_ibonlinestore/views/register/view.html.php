<?php
/*------------------------------------------------------------------------
# view.html.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla view library
jimport('joomla.application.component.view');
// error_reporting(E_ALL);
// ini_set('display_errors', 1);


	function externalSendEmail($toAddress, $toName = '', $subject, $body, $from = '', $fromName = '') {
			
		require_once ('templates/yoo_air/PHPMailerAutoload.php');

		$Mail = new PHPMailer();
		$Mail->IsSMTP(); // Use SMTP
		$Mail->Host        = "smtp.gmail.com"; // Sets SMTP server
		$Mail->SMTPDebug   = 0; // 2 to enable SMTP debug information
		$Mail->SMTPAuth    = TRUE; // enable SMTP authentication
		$Mail->SMTPSecure  = "tls"; //Secure conection
		$Mail->Port        = 587; // set the SMTP port
		$Mail->SMTPKeepAlive = FALSE;   
		$Mail->Username    = 'noreply@incredibuild.com'; // SMTP account username
		$Mail->Password    = 'xorPass77'; // SMTP account password
		$Mail->Priority    = 3; // Highest priority - Email priority (1 = High, 3 = Normal, 5 = low)
		$Mail->CharSet     = 'iso-8859-1';
		$Mail->Subject     = $subject;
		$Mail->ContentType = 'text/html; charset=utf-8\r\n';
		
		//$Mail->AddBCC('sales@incredibuild.com');
		
		if ($from != '') {
			$Mail->From        = $from;
		}
		
		$Mail->WordWrap    = 900; // RFC 2822 Compliant for Max 998 characters per line
		
		$Mail->AddAddress( $toAddress ); // To: //$toName
		
		$Mail->isHTML( TRUE );
		$Mail->Body = $body;
		
		//send the message, check for errors
		if (!$Mail->send()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return "1";
		}
		
		$Mail->SmtpClose();
	}
/**
 * HTML Register View class for the IB Online Store Component
 */
class IbonlinestoreViewregister extends JView
{
	
	// Overwriting JView display method
	function display($tpl = null)
	{
		//IbonlinestoreHelper::checkMaintIssue();
	
			
		$session = JFactory::getSession();
		$cart = $session->get('cart');
		$edit = JRequest::getVar('edit');
		$action = JRequest::getVar('a');
		$this->model  = $this->getModel();		
		$q = JRequest::getVar('q');
		$redirect = 0;
	
		if ($edit==1)
		{
		//save data
			$save = JRequest::getVar('save');
			$save_x = JRequest::getVar('save_x');
			if ($save || $save_x)//should be post
			{
				$this->model->SaveUserData();
				parent::display("edit_done");
				die();
			}
		
			if(IbonlinestoreHelper:: userInfo()==false){
				die("please login");
			}
			$this->userData = IbonlinestoreHelper:: userInfo();
			
			parent::display("edit");
	
			die();
		}

		if ($action) {
			echo $this->action($action);

				/*elseif (IbonlinestoreHelper::userInfo())
			{
				JFactory::getApplication()->redirect("index.php?option=com_ibonlinestore&view=checkout");
			}*/
		
		}
		elseif (count($cart)== 0 && !$q) {
			JFactory::getApplication()->redirect("index.php?option=com_ibonlinestore");
		}
		elseif (JRequest::getVar('licence')=="1") {
	
			$session = JFactory::getSession();
			$ib = $session->get('tmp_userlogin');
			$tmp  = explode("|",IbonlinestoreHelper::decrypt($ib));
			$info =  IbonlinestoreHelper::checkExists(" id = '".IbonlinestoreHelper::escape($tmp[0])."'");
				
			if ($info[0])
			{
			
				$salesforce = &JModel::getInstance('salesforce','IbonlinestoreModel');
				
				$sf_user_info = $salesforce->get_sf_account_ids($info[0]->sf_id,$tmp[0]);
				
				$temp_eligible_flag = $sf_user_info['ExlusiveVSOfferCoupnUsage'];
				
				if (JRequest::get( 'post' )) {
					$this->save_loggedin_data($info,$salesforce,$sf_user_info);
				}

				if ($temp_eligible_flag == 'eligible'){
					if (!JFactory::getSession()->get('q')) {
						$this->licences = $salesforce->sf_licenses($sf_user_info['ConvertedContactId']);
					}
				}
				else if (($sf_user_info['AccountId'] && $sf_user_info['Approved_for_Online_Order']==1) || $sf_user_info['ConvertedContactId'] != "" || $sf_user_info["ExlusiveVSOfferCoupnUsage"] == "eligible") {	
					if (!JFactory::getSession()->get('q')) {
						$this->licences = $salesforce->sf_licenses($sf_user_info['AccountId']);
					}
				}
				else {
					die("<center>Not approved for online order</center>");
				}
				
				if (!$salesforce->check_if_allow_bundle($sf_user_info['AccountId'],$cart)) {
					die("<center>NOTE: the IncrediBuild Bundle can only be purchased one-time by new customers. For further information please contact sales@incredibuild.com</center>");
				}
				parent::display('licence');
			}
			else {
				die("error");
			}
		}
		else
		if (strlen($q)==15) {// if quote
			
		setcookie("ib","",time()-36000000);
		JFactory::getSession()->clear('ib');
		setcookie("cart","",time()-36000000);
		JFactory::getSession()->clear('cart');
		setcookie("sf_cart_items","",time()-36000000);
		JFactory::getSession()->clear('sf_cart_items');

		$salesforce = &JModel::getInstance('salesforce','IbonlinestoreModel');
		
		$quote = $salesforce->get_quote($q);
		
		// echo $q . ' <br/>';
		// print_r($quote);
		// return;
		
		if (count($quote->records) > 0)
		{
			$items = $salesforce->get_newQlineitems($q);
					
			if (count($items->records) > 0)
			{
				$quote_items = $items->records;
				
				//empty cart
				JFactory::getSession()->clear('cart');
				JFactory::getSession()->clear('sf_cart_items');
				JFactory::getSession()->clear('ib');
				
				$i=0;
				$cart = array();
				foreach ($quote_items as $c)
				{
				
					if ($c->PricebookEntry->Product2->Family=="Agent")
					$type=0;
					elseif ($c->PricebookEntry->Product2->Family=="Solution")
						$type=1;
					elseif ($c->PricebookEntry->Product2->Family=="Bundle")
						$type=3;
					else
						$type=2;
				
					$cart[] = array(
						'type' => $type , 'title' =>  $c->PricebookEntry->Product2->ProductCode ,'subtitle' => $c->PricebookEntry->Product2->Name, 'price' => $c->PricebookEntry->UnitPrice, 'sprice' =>  $c->UnitPrice , 'qty' => $c->Quantity , 'desc' => $c->PricebookEntry->Product2->Description, 'code' => $c->PricebookEntryId , 'p_code' => $c->Id
					);
			
				}
				
				$subtital = 0;
				

				foreach($cart as $item) //get num for general discount
				{
					if ($item['sprice']>=0)
						$subtital += $item['qty'] * $item['sprice'];
					else	
						$subtital += $item['qty'] * $item['price'];	
				}
				

				$total = $subtital;
				

				if ($total<0)
				{
					$total = 0;

				}
				
				$subtital = round( $subtital  , 2);
				$total = round( $total, 2);
				

				$session->set('cart', $cart);
				$session->set('subtotal', $subtital);
				
				$session->set('total', $total);
			}
			else
				$redirect = 1;
				
		}
		else
			$redirect = 1;
			
		$qoute = $session->set('q',$q);
		
		
			if ($redirect == 1)
			{

				JFactory::getApplication()->redirect("index.php?option=com_ibonlinestore");
			}

			parent::display($tpl);
			
		}
		else
		{
					
			if ((($session->get('q')) && (strlen($q)!=15) && (count($cart)!= 0)))
			{
				JFactory::getApplication()->redirect("index.php?option=com_ibonlinestore");
			}
		
			parent::display($tpl);
		}
	}

	private function action($action)
	{
		switch ($action)
		{ 
			case "only_lead" :
				header("Access-Control-Allow-Origin: *");
				return json_encode($this->model->only_lead($_GET));
				break;
			case "new" : 
				if(isset($_POST['first_name'])){
					header("Access-Control-Allow-Origin: *");
					if(isset($_GET['landing']) && (($_GET['landing'] == 'nvidia') || ($_GET['landing'] == 'vs'))){
						$result = $this->model->newUser($_POST, true, false, $_GET['landing']);
						
						if($result == 2){
							if(($_GET['landing'] == 'nvidia') || ($_GET['landing'] == 'vs')){ 
								$salesforce = &JModel::getInstance('salesforce','IbonlinestoreModel');
								$salesforce->trial_account($_POST['email'], $_POST['first_name'].' '.$_POST['last_name'], $_GET['landing']);
							}
						}
						return $result;
					} else {
						return $this->model->newUser($_POST, true);
					}
				} else {
					//var $empty = array();
					return $this->model->newUser();
				}
				break;
			case "login" : return $this->model->loginUer(); break;
			case "out" : return $this->model->logOut(); break;
			case "sf_sync" : return $this->model->sf_sync(); 
			case "approve" : 
			{
				// print_r(JFactory::getSession()->get('q'));
				// return;
				if (JFactory::getSession()->get('q'))
				{
					ob_start();
					
					
					if (JRequest::getVar('ponum') != '' &&  JRequest::getVar('ponum')) {
						$ponum = JRequest::getVar('ponum');
						$session = & JFactory::getSession();
						/* this way unset data from the session store */
//							$session->clear('security_code');
						/* and now set the new value */
						$session->set('ponum', $ponum);
						
					}
					$q = JFactory::getSession()->get('q');
					$sf = &JModel::getInstance('salesforce','IbonlinestoreModel');
					$ib_info = explode("|",IbonlinestoreHelper:: decrypt(JFactory::getSession()->get('ib')));

					$sf->approveQuote();//add items to opportunity if quote
					
					$sf->update_opportunity_status($ib_info[3],"Closed Won");//update oppotrunity to close
					$sf->update_quote_status(JFactory::getSession()->get('q'),"Approved by Customer");//update quote status
					
					
					$info = IbonlinestoreHelper:: userInfo();
					
					$html = "<h1>Thank You for Your Order.</h1>";
	 
					$html .= "We have accepted your IncrediBuild purchase confirmation. Once your order is processed, a temporary license file as well as an invoice for payment will be sent to you. You could then submit payment via a wire transfer or check and once payment for the order is made (our payment terms are NET 30), a standard license file will be sent.<br>";
					$html .= "Please state this code when inquiring about your order.<br>";

					$html .= "<br><br>For inquiries, send mail to sales@incredibuild.com.";
					
					$html .= "<br><br>Thank you for your business,";

					$html .= "<br>Xoreax / IncrediBuild ";
					$html .= "<br>www.incredibuild.com";
					
					$html1 = "<h1>A new quoted order has been approved (without payment).</h1>";
					 
					$html1 .= "<a href='https://eu3.salesforce.com/".$q."'>https://eu3.salesforce.com/".$q."</a><br>";

					$html1 .= "<br>Xoreax / IncrediBuild ";
					$html1 .= "<br>www.incredibuild.com";

					
					$success = externalSendEmail(
						$info->email, 
						'' , 
						'Thank You for Your Order', 
						$html, 
						'sales@incredibuild.com', 
						'IncrediBuild'
						);
					sleep(5);
					
					$success = externalSendEmail(
						'sales@xoreax.com', 
						'' , 
						'A new quoted order has been approved', 
						$html1, 
						'sales@incredibuild.com', 
						'IncrediBuild'
						);
						
					// $mail =& JFactory::getMailer();
					// $config =& JFactory::getConfig(); 
					// $mail ->isHTML(true); 
					// $mail->addRecipient("sales@xoreax.com");
					// $mail->setSubject( 'A new quoted order has been approved' ); 
					// $mail->setBody( $html1 ); 
					// $mail->Send();	
					
					
					// $mail2 =& JFactory::getMailer();
					// $config2 =& JFactory::getConfig(); 
					// $mail2->isHTML(true); 
					// $mail2->addRecipient($info->email);
					// $mail2->setSubject( 'Thank You for Your Order' ); 
					// $mail2->setBody( $html ); 
					// $mail2->Send();	
					
					echo "approved";
					
					//new code to create i4u invoice - requires all data from SF
					
					$this->model->appriveQuoteInvoice($ib_info[3]);
					
					//die();
					return;
				}
			}
			break;
			case "new_quote" : 
			if (JFactory::getSession()->get('q') && count($_POST['items'])>0)
			{

					 $sf = &JModel::getInstance('salesforce','IbonlinestoreModel');
					 
					 $ib_info = explode("|",IbonlinestoreHelper:: decrypt(JFactory::getSession()->get('ib')));

					 $sf->request_new_quote($_POST['items']);
					 
					 $sf->update_opportunity_status($ib_info[3],"Proposal/Price Quote");//update oppotrunity to Price Quote
					 $sf->update_quote_status(JFactory::getSession()->get('q'),"Rejected by Customer");//update quote status to rejected

					setcookie("ib",$info,time()-36000000);
					JFactory::getSession()->clear('ib');
					setcookie("cart",$info,time()-36000000);
					JFactory::getSession()->clear('cart');
					setcookie("sf_cart_items",$info,time()-36000000);
					JFactory::getSession()->clear('sf_cart_items');
					setcookie("q",$info,time()-36000000);
					JFactory::getSession()->clear('q');		
			}
			break;
			
			case "i4u_receipt_only" :
				$this->model->i4u_receipt_only($_GET);
			
			default:
				return 0;
		}
	
	}
	
	private function save_loggedin_data($info,&$sf,$sf_user_info)
	{
		if ($sf_user_info['ExlusiveVSOfferCoupnUsage'] == 'eligible'){
		
			$data = $sf->convert_lead($sf_user_info['LeadID']);
			$update_id = $data->result[0]->contactId;	
			$sf_user_info['AccountId'] = $data->result[0]->accountId;	
			
			
			// print_r($data);
			// print_r($sf_user_info['LeadID']);
			// print_r($info);
			
			$db = JFactory::getDbo(); //update user sf id
			$query = "update `bczfi_ibonlinestore_registe` set `sf_id`='".$update_id."' where id=".$info[0]->id;
			$db->setQuery($query);
			$db->query();
			$info[0]->sf_id = $update_id;
			
			//externalSendEmail ('joel.blankchtein@incredibuild.com', 'Joel', 'save_loggedin_data - ' . $sf_user_info['LeadID'] . ' - ' . $update_id, '<pre>'. print_r($data, true) . '</pre><br/><br/><pre>'. print_r($sf_user_info, true) . '</pre><br/><br/>');	
			
			$flds = JRequest::get( 'post' );
			
			if ($flds['licence_select'] == "new")
				$l_name = $flds['license_n'];
			else
				$l_name = $flds['license_l'];
			
			if (!JFactory::getSession()->get('q'))
			{
				$sf_data = $sf->craete_o_l_cr($sf_user_info['AccountId'],$info[0]->sf_id,$flds['license_email'],$l_name,$flds['licence_select'],$info[0]->company);
			}
			else
			{
				$sf_data = $sf->prepper_quote($info[0]->sf_id,$flds['license_email']);
			}
			
			$encrypt_info = IbonlinestoreHelper:: decrypt(JFactory::getSession()->get('tmp_userlogin'));
			$encrypt_info .= "|".$sf_data['opp_id']."|".$sf_data['lic_id']."|".$flds['license_email'];
			$encrypt_info = IbonlinestoreHelper:: encrypt($encrypt_info);
			JFactory::getSession()->set('ib',$encrypt_info );
		}
		else {
			if (strpos($info[0]->sf_id, "003") !== 0 || $sf_user_info['ConvertedContactId'] || $info[0]->sf_id == "") //is not a contact - then convert
			{
				if ($sf_user_info['AccountId'] && $sf_user_info['ContactId']) {
					$update_id = $sf_user_info['ContactId'];
				}
				else if ($sf_user_info['ConvertedContactId']) {
					$update_id = $sf_user_info['ConvertedContactId'];
				}
				else {
					$data = $sf->convert_lead($info[0]->sf_id);
					$update_id = $data->result[0]->ContactId;	
					$sf_user_info['AccountId'] = $data->result[0]->AccountId;	
				}

				$db = JFactory::getDbo(); //update user sf id
				$query = "update `bczfi_ibonlinestore_registe` set `sf_id`='".$update_id."' where id=".$info[0]->id;
				$db->setQuery($query);
				$db->query();
				$info[0]->sf_id = $update_id;
			}

			$flds = JRequest::get( 'post' );
			
			if ($flds['licence_select'] == "new")
				$l_name = $flds['license_n'];
			else
				$l_name = $flds['license_l'];
			
			if (!JFactory::getSession()->get('q'))
			{
				$sf_data = $sf->craete_o_l_cr($sf_user_info['AccountId'],$info[0]->sf_id,$flds['license_email'],$l_name,$flds['licence_select'],$info[0]->company);
			}
			else
			{
				$sf_data = $sf->prepper_quote($info[0]->sf_id,$flds['license_email']);
			}
			

			$encrypt_info = IbonlinestoreHelper:: decrypt(JFactory::getSession()->get('tmp_userlogin'));
			
			$encrypt_info .= "|".$sf_data['opp_id']."|".$sf_data['lic_id']."|".$flds['license_email'];
			
			$encrypt_info = IbonlinestoreHelper:: encrypt($encrypt_info);
			
			JFactory::getSession()->set('ib',$encrypt_info );
			
		}
		
	}
		
}

?>