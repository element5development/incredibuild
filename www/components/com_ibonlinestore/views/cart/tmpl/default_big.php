<?
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');

if (count($this->cart)==0 ) die();

//check maintnance again issue
$agentsNum = 0;
$maintNum = 0;
$soluNum = 0;
$maintErr = "";
foreach ($this->cart as $key=>$item)
{

	switch ($item[type])
	{

		case 0: $agentsNum += $item[qty]; break;
		case 1: $soluNum +=$item[qty]; break;
		case 2: $maintNum +=$item[qty]; break;
	}

}

if ($agentsNum>$maintNum && $maintNum>0)
	$maintErr = " maintErr";
	
?>

          <div class="carttablepan">
            <ul>
              <li class="delete">&nbsp;</li>
              <li class="name">Name</li>
              <li class="description">Description</li>
              <li class="price">Price</li>
              <li class="quantity">Quantity</li>
              <li class="total">Total</li>
              <div class="clear"></div>
            </ul>
			
			<? 
			  $i=1;
			  foreach ($this->cart as $key=>$item) :
			  ?>
			  
            <ul class="tablecontent<? if($item[type] == 2) echo $maintErr; ?>"  id="<? echo IbonlinestoreHelper::encrypt($key."|".$item[type]."|".$item[title]."|".$item[subtitle]."|".$item[price]."|".$item[desc]."|".$item[code]);?>">
              
			  <li class="delete"><a href="#">
			  
			  <?
			  if ($item['auto_maint']=="")
			  {
			  ?>
			  <img src="/components/com_ibonlinestore/assets/images/deletebt2.png" alt="" />
			  <?}?>
			  </a></li>
              
			  <li class="name"><? echo $item[title]; ?><span><? echo $item[subtitle]; ?></span></li>
              <li class="description"><? echo $item[desc]; ?></li>
              <li class="price">$<? echo $item[price]; ?>
			  <? if ($item[sprice]<$item[price]){?>
			  <br>
				<span style='font-size:12px;color:#d60a6d'>(%<? echo round(100-(($item[sprice]/$item[price])*100),1);?> off)</span>
			  <?}?>
			  </li>
              <li class="quantity">
                <div class="quantitypan">
				<? if ($item[type] != 3) : ?>
                  <div class="plusimg" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/plussignimg.png" alt="" /></a></div>
                  <div class="minusimg" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/minussignimg.png" alt="" /></a></div>
				  <? endif; ?>
                  <input name="textbox"  type="text" id="sqty_<?php echo $i; ?>" value="<? echo $item[qty]; ?>" readonly />
                </div>
              </li>
              <li class="total">
			  <? if ($item[sprice]<$item[price]){?>
			  <strike>$<? echo $item[qty]*$item[price]; ?></strike><br>
			  <span style='color:#d60a6d'>$<? echo round( $item[qty]*$item[sprice] , 2); ?></span>
			  <?}else{?>
			  $<? echo $item[qty]*$item[price]; ?>
			  <?}?>
			  
			  
			  </li>
            </ul>
			<? 
				$i++;
				endforeach; ?>
                
             
 
            <ul class="discount_pan">
			
			<li class="subtotal"><span style="
    float: rigt;
">discount: </span>
			 
			  			  
			  </li>
			  
		
			  </li>
              <li class="discount"><? echo round($this->discount*100,1); ?>%</li>
         
			  
              <div class="clear"></div>
            </ul>
			
 <ul class="discount_pan2">
	
<?
 if (JFactory::getSession()->get('discountType')!="")
	{
?>	
			<li class="subtotal"><span style="
    float: rigt;
">final discount: </span>

			  			  <span style="
    float: right;
    padding-top: 22px;
"><form method="post" style="
    padding: 0px;
    margin: 0px;
    width: 30px;
	">
<input type='hidden' name='coupon' value='no'/>
			<input type="submit" style="border: 0px;margin-left: 10px;text-decoration: underline;cursor: pointer;" value="[X]">
		  </form></span>
			  			  
			  </li>
			  
		
			  </li>
              <li class="discount2"><? echo JFactory::getSession()->get('discountName'); ?></li>
         
			  
              <div class="clear"></div>
            </ul>
		<?}?>	
            <ul class="grandtotalpan">
              <li class="subtotal">total: </li>
              <li class="subtotalnubber">$<? echo $this->total; ?></li>
              <div class="clear"></div>
            </ul>
          </div>
		  <? 
		  if (JFactory::getSession()->get('discountType')=="")
		  {
		  ?>
		  <form method='post' action="/cart">
		  <div class='cupon_div' >
		  	<div style='float:right'>
				<input type='submit' class='cupon_submit_btn' value='Apply Coupon' />
		  </div>
          <div class='inputbox_coupon'>
			<input type='text' name='coupon' autocomplete='off' id='coupon'/>
			<?php
			if (JFactory::getSession()->get('showSpecialPromotionMessage')=="show")
			{
			?>
			<script type="text/javascript">
				$('document').ready(function(){
					var couponTxt = "<b>Please Note:</b> The exclusive Visual Studio promotion coupon is intended for 1 use only.<br/>Please contact a sales representative to assist you with this purchase. For additional assistance, contact us at <a onclick='window.open(\"mailto:sales@incredibuild.com\",\"_parent\");return false;' href='#' style='text-decoration:underline'>sales@incredibuild.com</a>. ";
					new Messi(couponTxt, {title: 'Dear buyer',titleClass: 'anim error', modal: true , buttons: [{id: 0, label: '<- Back to store', val: 'X'}], callback: function(val) { $('#coupon').val(''); }});
				});
			</script>
			<?php
			}
			// <script type="text/javascript">
				// $('document').ready(function(){
					// $('#coupon').change(function(e){
						// console.log(e);
						// $.ajax({
								// url: 'https://www.incredibuild.com/ajaxRequestForThis',
								// data: data,
								// type: 'POST',
								// success: function(res) {
									// console.log(res);
									// if (res == '1') {
										
									// }
									// else {
										// var couponTxt = "<b>Please Note:</b> The exclusive Visual Studio promotion coupon is intended for 1 use only.<br/>Please contact a sales representative to assist you with this purchase. For additional assistance, contact us at <a onclick='window.open(\"mailto:sales@incredibuild.com\",\"_parent\");return false;' href='#' style='text-decoration:underline'>sales@incredibuild.com</a>. ";
										// new Messi(couponTxt, {title: 'Dear buyer',titleClass: 'anim error', modal: true , buttons: [{id: 0, label: '<- Back to store', val: 'X'}], callback: function(val) { $('#coupon').val(''); }});
									// }
								// }
						// });
					// });
				// });
			// </script>
			?>
			</div>
		<span style="height: 30px; line-height: 30px; font-size: 20px;">Do you have a coupon? </span><br />
		  </div>
		  </form>
		  <?}?>