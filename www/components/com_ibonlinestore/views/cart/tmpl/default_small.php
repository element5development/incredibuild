<?
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');

if (count($this->cart)==0 ) die();


//check maintnance again issue
$agentsNum = 0;
$maintNum = 0;
$maintErr = "";
foreach ($this->cart as $key=>$item)
{

	switch ($item[type])
	{

		case 0: $agentsNum += $item[qty]; break;
		case 2: $maintNum +=$item[qty]; break;
	}

}


if ($agentsNum>$maintNum && $maintNum>0)
	$maintErr = "class='maintErr'";
else
	$maintErr = "class='maintClass'";

if (!JFactory::getSession()->get('q')) {
?>
<h1 class="cart" id='cart_title'>Your Cart</h1>
<?
}
else{
?>
<h1 class="cart" id='cart_title'>Your Quote</h1>
<?}?>
          <div class="cartdetailpan">
            <div class="carttoppan"></div>
            <div class="cartmiddlepan">
              <ul>
			  
			  <? 
			  $i=1;
			  $total = 0;
			  foreach ($this->cart as $key=>$item) :
			  ?>
                <li <? if($item[type] == 2) echo $maintErr; ?> id="<? echo IbonlinestoreHelper::encrypt($key."|".$item[type]."|".$item[title]."|".$item[subtitle]."|".$item[price]."|".$item[desc]."|".$item[code]);?>" <? echo ($i%2 == 0 )? 'class="alt flrow"' : 'class="flrow"'; ?> >
				  		
				<? if ($this->edit != "no") :?>		
				  <div class="deletebt" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/deletebt.png" alt=""></a></div>
				<? endif; ?>
				  <div class="numberpan2" >
                    <input name="textbox" type="text" itemId='<?echo $item[title]; ?>' class='itemInput' readonly id="sqty_<?php echo $i; ?>" value="<? echo $item[qty]; ?>" readonly>
                    <div class="solution"><?  if ($item[type] == 0) echo "agent"; elseif($item[type] == 1) echo "solution"; elseif($item[type] == 3) echo "Bundle"; else echo "Mainte . ."; ?></div>
					<?
					if (!JFactory::getSession()->get('q') && $item[type]!=3) {
					?>
                    <div class="plussign" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/plussign.png" alt=""></a></div>
                    <div class="minussign" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/minussign.png" alt=""></a></div>
					<?}?>
                  </div>
			 
                  <div class="cartcontentrightpan2">
				  <? if ($item[type] == 1) {?>
						<p><? echo $item[subtitle]; ?>
					
					<?}else{?>
					
						<p><? echo $item[title]; ?><br>
						<? echo $item[subtitle]; ?>
				
					<?}?>
					<span>
					<? 
					if (isset($item['sprice']) ){?>
			  <strike style='float: left;padding-right: 5px;color: #566675;'>$<? echo round( $item[qty]*$item[price] , 1); ?></strike>
			  <span style='color:#d60a6d'>$<? echo round( $item[qty]*$item[sprice] , 1); ?></span>
			  <?
			  $total += ($item[qty]*$item[sprice]);
			  
			  
			  }else{?>
			  $<? echo $item[qty]*$item[price]; ?>
			  <?
			  $total += ($item[qty]*$item[price]);
			  }
			  
			  $all_total += ($item[qty]*$item[price]);
			  ?>
			  </span></p>
                  </div>
                  <div class="clear"></div>
                </li>
				<? 
				$i++;
				endforeach; 
				
				$discount = (($all_total-$total)/$all_total) * 100;
				?>
                
              </ul>
          <?php
		if (JFactory::getSession()->get('q')!=''):
		?>
<div class="subtotalpan smallcartinfo"> <span>Discount:    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span> <? echo round($discount,2); ?>% </div>
			  <?php else:?>
			  
<div class="subtotalpan smallcartinfo msg"><span><i>Discount (if available) will be applied on checkout</i></span></div>			  
	  			  
			  </li>
			  
			  
			  <?php endif;?>
              
			  
						  <?
			 if (JFactory::getSession()->get('discountType')!="")
				{
			?>
			  <div class="subtotalpan2 smallcartinfo"> <span>F.Discount:     </span> <span class='span2'> <? echo JFactory::getSession()->get('discountName'); ?> </span></div>
			  <?}?>
			  
              <div class="totalpan smallcartinfo"> <span>total: </span> $<? echo number_format($total,2); ?> </div>
			<input type='hidden' id='cartSum' value='<? echo $total; ?>'/>
			<? /* if ($this->cust) :?>
			<div class="custtxt">
			Not <? echo $this->cust->user_name;?>? <a href='/register?a=out'><u>SignOut</u></a><br>
			</div>
			<? endif;*/ 
			
			?>
			
			<? if ($this->edit != "no") :?>	
			  <ul>
			  
                <li>
				<?
				if (strpos($_SERVER['HTTP_REFERER'],"http://www.incredibuild.com/renew") !== false){
				?>
                  <div class="continuecheckout"><a href="/register?renew=1"><img src="/components/com_ibonlinestore/assets/images/continuebt2.png" alt=""></a></div>
                <?}else{?>
				             <div class="continuecheckout"><a href="/register"><img src="/components/com_ibonlinestore/assets/images/continuebt2.png" alt=""></a></div>	
				<?}?>
				</li>
				<!--
                <li class="ordertxt">If you shop with us regularly and 
                  have a disacount. We need copy here<a href="#"><img src="/components/com_ibonlinestore/assets/images/orderbt.png" alt=""></a>
                  <div class="clear"></div>
                </li>-->
              </ul>
			<? endif; ?>
			
            </div>
            <div class="cartbottompan"></div>
          </div>
		  
  