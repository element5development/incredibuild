<?php
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');

if (count($this->cart)==0 ) die();

//check maintnance again issue
$agentsNum = 0;
$maintNum = 0;
$soluNum = 0;
$maintErr = "";
foreach ($this->cart as $key=>$item)
{

	switch ($item[type])
	{
		case 0: 
			$agentsNum += $item[qty]; 
			break;
		case 1: 
			$soluNum += $item[qty]; 
			break;
		case 2: 
			$maintNum += $item[qty]; 
			break;
	}

}


if ($agentsNum>$maintNum && $maintNum>0)
	$maintErr = " maintErr";
	
?>

          <div class="carttablepan">
            <ul>
              <li class="delete">&nbsp;</li>
              <li class="name">Name</li>
              <li class="description">Description</li>
              <li class="price">Price</li>
              <li class="quantity">Quantity</li>
              <li class="total">Total</li>
              <div class="clear"></div>
            </ul>
			
			<?php 
			  $i=1;
			  foreach ($this->cart as $key=>$item) :
			  ?>
			  
            <ul class="tablecontent<?php if($item[type] == 2) echo $maintErr; ?>"  id="<?php echo IbonlinestoreHelper::encrypt($key."|".$item[type]."|".$item[title]."|".$item[subtitle]."|".$item[price]."|".$item[desc]."|".$item[code]."|".$item[p_code]);?>">
              
			  <li class="deleteq"><a href="#">
			  
			  <?php
			  if ($item['auto_maint']=="")
			  {
			  ?>
			  <img src="/components/com_ibonlinestore/assets/images/deletebt2.png" alt="" />
			  <?php } ?>
			  </a></li>
              
			  <li class="name"><?php echo $item[title]; ?><span><?php echo $item[subtitle]; ?></span></li>
              <li class="description"><?php echo $item[desc]; ?></li>
              <li class="price priceq">$<?php echo $item[price]; ?>
			  <?php if ($item[sprice]<$item[price]){ ?>
			  <br>
				<span style='font-size:12px;color:#d60a6d'>(%<?php echo round(100-(($item[sprice]/$item[price])*100),1);?> off)</span>
			  <?php } ?>
			  </li>
              <li class="quantity">
                <div class="quantitypan">
				  <?php if ($item[title] != "BUNDLE") : ?>
					<div class="plusimgq" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/plussignimg.png" alt="" /></a></div>
					<div class="minusimgq" id="<?php echo $i; ?>"><a href="#"><img src="/components/com_ibonlinestore/assets/images/minussignimg.png" alt="" /></a></div>
                  <?php endif; ?>
				  <input name="textbox"  type="text" id="sqty_<?php echo $i; ?>" value="<?php echo $item[qty]; ?>" readonly />
                </div>
              </li>
              <li class="total totalItem">
			  <?php if ($item[sprice] != $item[price]) { ?>
			  <strike>$<?php echo $item[qty]*$item[price]; ?></strike><br>
			  <span style='color:#d60a6d'>$<?php echo round( $item[qty]*$item[sprice] , 2); ?></span>
			  <?php
			  $total += $item[qty]*$item[sprice];
			  
			  }else{?>
			  $<?php echo $item[qty]*$item[price]; ?>
			  <?php
			  $total += $item[qty]*$item[price];
			  }
			  
			  $all_total += ($item[qty]*$item[price]);
			  ?>
			  
			  
			  </li>
            </ul>
			<?php 
				$i++;
				endforeach; 
				
				$discount = (($all_total-$total)/$all_total) * 100;
				?>
                
             
 
            <ul class="discount_pan">
			
			<li class="subtotal"><span style="
    float: rigt;
">discount: </span>
			 
			  			  
			  </li>
			  
		
			  </li>
              <li class="discount"><?php echo round($discount,2); ?>%</li>
         
			  
              <div class="clear"></div>
            </ul>
			
 <ul class="discount_pan2">
	
<?php
 if (JFactory::getSession()->get('discountType')!="")
	{
?>	
			<li class="subtotal"><span style="
    float: rigt;
">final discount: </span>

			  			  <span style="
    float: right;
    padding-top: 22px;
"><form method="post" style="
    padding: 0px;
    margin: 0px;
    width: 30px;
	">
<input type='hidden' name='coupon' value='no'/>
			<input type="submit" style="border: 0px;margin-left: 10px;text-decoration: underline;cursor: pointer;" value="[X]">
		  </form></span>
			  			  
			  </li>
			  
		
			  </li>
              <li class="discount2"><?php echo JFactory::getSession()->get('discountName'); ?></li>
         
			  
              <div class="clear"></div>
            </ul>
		<?php } ?>	
            <ul class="grandtotalpan">
              <li class="subtotal">total: </li>
              <li class="subtotalnubber">$<?php echo $this->total; ?></li>
              <div class="clear"></div>
            </ul>
          </div>
		  <br><br>