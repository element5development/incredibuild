<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');

?>
<!--
<div id="ibonlinestore-cart">
	<?php foreach($this->items as $item): ?>
		<?php
		if(empty($item->alias)):
			$item->alias = $item->title;
		endif;
		$item->alias = JFilterOutput::stringURLSafe($item->alias);
		$item->linkURL = JRoute::_('index.php?option=com_ibonlinestore&view=car&id='.$item->id.':'.$item->alias);
		?>
		<p><strong>Title</strong>: <a href="<?php echo $item->linkURL; ?>"><?php echo $item->title; ?></a></p>
		<p><strong>Description</strong>: <?php echo $item->description; ?></p>
		<p><strong>Price</strong>: <?php echo $item->price; ?></p>
		<p><strong>Type</strong>: <?php echo $item->type; ?></p>
		<p><strong>Link URL</strong>: <a href="<?php echo $item->linkURL; ?>">Go to page</a> - <?php echo $item->linkURL; ?></p>
		<br /><br />
	<?php endforeach; ?>
</div>-->

<script>
var cart_type = "big"
</script>

<div class="middlecontainer">
        <div class="shopingcartleftpan">
          <div class="clear"></div>
<?  
if (!JFactory::getSession()->get('q')) {
?>
<h1><span class="cart">Your Cart</span></h1>
<?
}
else{
?>
<h1><span class="cart">Your Quote</span></h1>
<?}?>

		  <div id='cart_view'></div> 
	<? if (!$_GET['renew']) : ?>
	
	<div id="ponumContainer" style="
    text-align: center;
">
  <label style="
    font-size: 20px;
">Your Purchase Order number (if available) - for your invoice</label><br>
  <input type="text" name="ponum" maxlength="60" value="" id="ponum" style="width: 200px; height: 30px; line-height: 30px; border-radius: 8px; margin-top: 5px; text-align: center;">
</div>
          <label class="termspan">
		  
            <input name="" type="checkbox" id='agree_box' /> Confirm that I read and agree to the terms and condition of the End User License Agreement</label>
			<? endif; ?>
			<br><br>
          <div class="securitybtpan">
				<?  
			if (JFactory::getSession()->get('q')) {
			?>
		   <a href="#" id='scur_pmnt_without_btn'><img src="/components/com_ibonlinestore/assets/images/Approve-Quote-(pay-later)_btn.png" alt="" /></a>
		   <?}?>
		   &nbsp;&nbsp;
		   <a href="#" id='scur_pmnt_btn'><img src="/components/com_ibonlinestore/assets/images/securepayment.png" alt="" /></a>
			  
			 
			  <br>
              <img src="/components/com_ibonlinestore/assets/images/securityimg.png" alt="" />
          </div>
		  <?  
		   if (JFactory::getSession()->get('q')) {
		   ?>
		   
		   <div class='securitybtpan quoteChange' style='display:none'>
		   <br>
			<a href="#" id='scur_pmnt_quote_change_btn'><img src="/components/com_ibonlinestore/assets/images/Request-New-Quote_btn2.png" alt="" /></a>
		   </div>
		   
		   
		   <?}?>
		  <br><br>
          <div class="clear"> </div>
        </div>
        <div class="clear"></div>
      </div>
		


