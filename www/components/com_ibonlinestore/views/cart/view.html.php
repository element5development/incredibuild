<?php
/*------------------------------------------------------------------------
# view.html.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla view library
jimport('joomla.application.component.view');
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
/**
 * HTML Cart View class for the IB Online Store Component
 */

	function externalSendEmail($toAddress, $toName = '', $subject, $body, $from = '', $fromName = '', $reply = '', $replyName = '', $bcc = '', $secondaryAddress = '') {
			
		require_once (getcwd().'/templates/yoo_air/PHPMailerAutoload.php');

		$Mail = new PHPMailer();
		$Mail->IsSMTP(); // Use SMTP
		$Mail->Host        = "smtp.gmail.com"; // Sets SMTP server
		$Mail->SMTPDebug   = 0; // 2 to enable SMTP debug information
		$Mail->SMTPAuth    = TRUE; // enable SMTP authentication
		$Mail->SMTPSecure  = "tls"; //Secure conection
		$Mail->Port        = 587; // set the SMTP port
		$Mail->SMTPKeepAlive = FALSE;   
		$Mail->Username    = 'noreply@incredibuild.com'; // SMTP account username
		$Mail->Password    = 'xorPass77'; // SMTP account password
		$Mail->Priority    = 3; // Highest priority - Email priority (1 = High, 3 = Normal, 5 = low)
		$Mail->CharSet     = 'iso-8859-1';
		$Mail->Subject     = $subject;
		$Mail->ContentType = 'text/html; charset=utf-8\r\n';
		
		if ($bcc != '') {
			$Mail->AddBCC($bcc);
		}
		
		if ($from != '') {
			$Mail->From        = $from;
			if ($fromName != '') {
				$Mail->FromName        = $fromName;
			}
		}
		
		if ($reply != '') {
			$Mail->AddReplyTo($reply, $replyName);
		}
		
		$Mail->WordWrap    = 900; // RFC 2822 Compliant for Max 998 characters per line
		
		$Mail->AddAddress( $toAddress ); // To: //$toName
		
		
		// $Mail->AddCC('ron.ginton@incredibuild.com');
		
		if ($secondaryAddress != '') {
			$Mail->AddAddress( $secondaryAddress ); 
		}
		
		$Mail->isHTML( TRUE );
		$Mail->Body = $body;
		
		//send the message, check for errors
		if (!$Mail->send()) {
			return "Mailer Error: " . $mail->ErrorInfo;
		} else {
			return "1";
		}
		
		$Mail->SmtpClose();
	}
 
class IbonlinestoreViewcart extends JView
{
	
	
	// Overwriting JView display method
	function display($tpl = null)
	{
		/*
		// Assign data to the view
		$this->items = $this->get('Items');
		
		// Check for errors.
		if (count($errors = $this->get('Errors'))):
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		endif;
		// Display the view*/
		
		$this->coupons();
		$this->modifyItems();

	
		if (JRequest::getVar('t')=="small")
			parent::display("small");
		elseif(JFactory::getSession()->get('q')!='' && JRequest::getVar('t')=="big")
		{
			parent::display("quote");
		}	
		elseif(JRequest::getVar('t')=="big")
		{
			parent::display("big");
		}
		else
		{
			$session = JFactory::getSession();
			
			//externalSendEmail ('joel.blankchtein@incredibuild.com', 'Joel', 'seesion check', print_r(IbonlinestoreHelper::userInfo(), true) . '<br/><br/>' . print_r($session->get('cart'), true));	
			
			if (count($session->get('cart'))!=0 && IbonlinestoreHelper:: userInfo())
				parent::display($tpl);
			else
			{
				if ($_SERVER['HTTP_REFERER']=="http://www.incredibuild.com/renew" || $_SERVER['HTTP_REFERER']=="https://www.incredibuild.com/renew")
					JFactory::getApplication()->redirect("index.php?option=com_ibonlinestore&view=renew");
				else
					JFactory::getApplication()->redirect("index.php?option=com_ibonlinestore");
			}
		}
	}
	
	private function coupons()
	{
	$code = JRequest::getVar('coupon','','post');
	

	if ($code)	
	{
		$eligibility = 'not_eligible';
		if ($code === 'VSP15OFF') {
			$sf_id = 0;
			$session = JFactory::getSession();
			$ib = $session->get('tmp_userlogin');
			$tmp  = explode("|",IbonlinestoreHelper::decrypt($ib));
			$user_info = IbonlinestoreHelper::checkExists(" id = '".IbonlinestoreHelper::escape($tmp[0])."'");
				
			if ($user_info[0])
			{
				$salesforce = &JModel::getInstance('salesforce','IbonlinestoreModel');
				$sf_id = $user_info[0]->sf_id;
				if ($sf_id != 0) {
					$eligibility = $salesforce->check_special_promotion_eligibility($sf_id);
					
				}
				else {
					$eligibility = 'not_eligible';
				}
			}
			else {
				$eligibility = 'not_eligible';
			}
			
			//externalSendEmail ('joel.blankchtein@incredibuild.com', 'Joel', 'Coupon Check - ' . $code, '<h1>' . print_r($eligibility, true) . '</h1><br/><br/><pre>'. print_r($user_info, true) . '</pre><br/><br/><pre>'. print_r($session, true) . '</pre><br/><br/>');	
				
		}
			
		$info = IbonlinestoreHelper::get_coupon_details($code);
		
		$session = JFactory::getSession();
		
		if (($info && $code !== 'VSP15OFF') || ($info && $code === 'VSP15OFF' && $eligibility === 'eligible'))
		{
			$session->set('discountName', $info[0]->name);
			$session->set('discountType', $info[0]->type);
			$session->set('discountVal', $info[0]->value);
			$session->set('discountCode', $info[0]->code);
			$session->set('discountCond', $info[0]->cond);
		}
		else
		{
			$session->set('discountName', "");
			$session->set('discountType', "");
			$session->set('discountVal', "");
			$session->set('discountCode', "");
			$session->set('discountCond', "");
			
			if ($eligibility == 'used') {
				$session->set('showSpecialPromotionMessage', "show");
			}
		}
		
		JFactory::getApplication()->redirect(JURI::base() . "cart" );
	
	}
	
	
	}
	
	
	
	
	private function modifyItems()
	{
	
	$qty = JRequest::getVar('qty','','post');
	$info = explode("|",IbonlinestoreHelper:: decrypt(JRequest::getVar('id','','post')));
	$this->show = JRequest::getVar('show','','post');
	
	if ((is_numeric($info[0]) && is_numeric($qty) && $qty>=0) || $this->show==1)
	{
			$session = JFactory::getSession();
			$cart = $session->get('cart');
			$id = $info[0];
			$type = $info[1];
			$title = $info[2];
			$subtitle = $info[3];
			$price = $info[4];
			$desc = $info[5];
			$code = $info[6];
			$discount_cond = JFactory::getSession()->get('discountCond');
			$discount_name = JFactory::getSession()->get('discountName');
			
			
			
			
			if (JRequest::getVar('t')=="big" && JFactory::getSession()->get('q')=='')
				$this->deleteSFitems();

			if ($type==3)
			{
				//remove all items in cart
				$session->clear("cart");
				$cart = array();
				$cart[$id] = array( 'type' => $type , 'title' => $title ,'subtitle' => $subtitle , 'price' => $price , 'qty' => 1 , 'desc' => $desc, 'code' => $code );
			}
			else
			{

				if ($id)
				{
					foreach($cart as $item) //if bundle exists delete
					{
						if ($item['type'] ==3)
						{
							$session->clear("cart");
							$cart = array();
							break;
						}
					}
			
					if ($qty>0)
							$cart[$id] = array( 'type' => $type , 'title' => $title ,'subtitle' => $subtitle , 'price' => $price , 'qty' => $qty , 'desc' => $desc, 'code' => $code );
						else
							unset($cart[$id]);
				}
			
			}
			
			

			
			if (count($cart)>0)
			{
					
			
			if (JRequest::getVar('t')=="big" && JFactory::getSession()->get('q')=='')
			{
				$cart = $this->calcDiscountCond($cart,$discount_cond);
				$cart = $this->getGeneralDiscounts($cart);
				$cart = $this->getSFitems($cart);
			}
			
			
			
			//calc subtotal
			$subtital = 0;
	
			
			//if ($session->get('discountVal')=="" || $session->get('discountVal')=="0")
				
			//else
			//	$discount = $this->getDiscountsFromCode($subtital);
			
			foreach($cart as $item) //get num for general discount
			{
				if ($item['sprice'] != $item['price'])
					$subtital += round( $item[qty]*$item[sprice] , 2);
				else	
				{
					$subtital += $item['qty'] * $item['price'];	
				}
			}
			

			$total = $subtital;
			

			if ($total<0)
			{
				$total = 0;
				$discount = 1;
			}
			
			$subtital = $subtital  ;
			$total = $total;
			}
			
			$session->set('cart', $cart);
			$session->set('subtotal', $subtital);
			
			$session->set('total', $total);
			
			$this->cart = $cart;
			$this->subtital = $subtital;

			$this->total = $total;
			
			$this->edit = JRequest::getVar('edit');
			
			

			$this->cust = IbonlinestoreHelper:: userInfo();
					
			
	}

	
	}
	
	private function deleteSFitems()
	{

		$session = JFactory::getSession();
		
		if ($session->get('sf_cart_items'))
		{
		
			$sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');
			$sf->delete_objects($session->get('sf_cart_items'));

			$session->set('sf_cart_items','');
		}
		
	}
	
	private function getSFitems($cart)
	{
			
			$user_info = IbonlinestoreHelper:: userInfo();
			$ib_info = explode("|",IbonlinestoreHelper:: decrypt(JFactory::getSession()->get('ib')));
			
			$sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');

			foreach ($cart as $key=>$item)
			{
				if ($item['auto_maint']=="")
				{
				
					$price = ($item['sprice']) ? $item['sprice'] : $item['price'] ;
					
					$data[] = array(
						"OpportunityId" 	=> 	$ib_info[3],
						"License__c"		=> $ib_info[4],
						"Description"		=> $item['desc'],
						"PricebookEntryId"	=>	$item['code'],
						"Quantity"			=>	$item['qty'],
						"UnitPrice"			=>  $price
					);
				}
			}
			
			
			$sf_info = $sf->create_lineitems($data);

			foreach ($sf_info as $item)
			{
				$tmp[] = $item->id;
			}

			JFactory::getSession()->set("sf_cart_items",$tmp);
	
			$new_items = $sf->get_newlineitems($ib_info[3]);
			
			$new_cart = array();
			
			foreach ($new_items->records as $item)
			{
				if ($item->PricebookEntry->Product2->Family=="Agent")
				$type=0;
				elseif ($item->PricebookEntry->Product2->Family=="Solution")
					$type=1;
				elseif ($item->PricebookEntry->Product2->Family=="Bundle")
					$type=3;
				else
					$type=2;
				
				
				$new_cart[] = array(
					'type' => $type , 'title' =>  $item->PricebookEntry->Product2->ProductCode ,'subtitle' => $item->PricebookEntry->Product2->Name, 'price' => $item->PricebookEntry->UnitPrice, 'sprice' =>  $item->UnitPrice , 'qty' => $item->Quantity , 'desc' => $item->PricebookEntry->Product2->Description, 'code' => $item->PricebookEntryId 
					,'auto_maint' =>$item->Maintenancefor__c
				);
			}
			
			return $new_cart;
	}
	
	private function calcDiscountCond($cart,$discount_cond)
	{
	

	foreach($cart as $key=>$item) //unset items
	{	
			unset($cart[$key]['sprice']);					
	}
				
	$disc_val = JFactory::getSession()->get('discountVal');			
	//for all items
	if ($disc_val>0)
	{
		foreach($cart as $key=>$item)
		{

				$cart[$key]['sprice'] = (1-($disc_val / 100)) * $cart[$key]['price'];
				
		}
	}
	
				
	$disc_groups = explode("|",$discount_cond);
	
	
	
	foreach($disc_groups as $disc_group)
	{
		$vals = explode(",",$disc_group);
		
		
		
		$exists = $this->condExsist($cart,$vals);
		
			
			
			if ($exists && $disc_group!="")
			{
				
				
				foreach($vals as $val)
				{
					
						$v = explode(":",$val);
			
						if ($v[0] == "a")
						{
							
							foreach($cart as $key=>$item)
							{
								
								if ($item['type'] == $v[1])
								{	
									$cart[$key]['sprice'] = (1-($v[2] / 100)) * $cart[$key]['price'];
								}
								
							}
						
						}
						else
						{
							foreach($cart as $key=>$item)
							{
								
								if ($key == $v[0])
								{	
									$cart[$key]['sprice'] = (1-($v[1] / 100)) * $cart[$key]['price'];
								}
								
							}
						}
					
				}

				return $cart;

			
			}
		
		
	
	}
	
	return $cart;
	
	}
	
	private function condExsist($cart,$vals)
	{
		
		foreach($vals as $val)
		{
			
			$v = explode(":",$val);
			
		if ($val!="")
		{		
			
			
			if ($v[0] == "a")
			{
				$exists =0;
				foreach ($cart as $c)
				{
					if ($c['type'] == $v[1])
					{
						$exists = 1;
					}
						
				}
				
				if ($exists == 0)
				{
					return false;
				}
					
			}
			else
			{
				
				if (!array_key_exists($v[0],$cart))
				{
					return false;
				}
					
			}
		
		}
		}
		
		return true;
		
	}
	
	private function getDiscountsFromCode($subtotal)
	{	
		$type = JFactory::getSession()->get('discountType');

		if ($type=="")
		{
			return 0;
		}
		
		switch ($type)
		{
			case 0: return (JFactory::getSession()->get('discountVal') / 100 ); break;
			case 1: return 1/ ($subtotal/JFactory::getSession()->get('discountVal'));break;
		}
		
		
	}
	
	private function approve_quote()
	{
		$sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');
		
		$sf->approve_quote();
	}
	
	private function getGeneralDiscounts($cart)
	{

	$agentsNum = 0;
	$maintNum = 0;
	foreach($cart as $item) //get num for general discount
	{
			if ($item[type] == 0)
				$agentsNum += $item[qty];
					
			if ($item[type] == 2)
				$maintNum += $item[qty];	
	}
		
		$dicsountP = $this->getDiscountsP($agentsNum,$maintNum);
		
		JFactory::getSession()->set('discount', $dicsountP);
		
		$this->discount = $dicsountP;
		
		foreach($cart as $key=>$item)
		{
			
			$currentP = 0;
			
			if ($cart[$key]['sprice'] != "")
			{
				$currentP = 1-($cart[$key]['sprice']/$cart[$key]['price']);
				
			}
			
			if ($currentP < $dicsountP)
			{	
			
				$cart[$key]['sprice'] = (1-($dicsountP)) * $cart[$key]['price'];
			}
								
		}
	
		
		return $cart;
	
	}
	
	private function getDiscountsP($agentsNum,$maintNum)
	{
		return 0;
		/*

		if ($maintNum!=0)
		{
			
			switch ($maintNum)
			{
				case ($maintNum>=30): return 0.15; break;
				case ($maintNum>=15): return 0.1;break;
				case ($maintNum>=5): return 0.05;break;

			}
			
		}
		else
		{
			if ($agentsNum==0)	
				return 0;
				
			switch ($agentsNum)
			{
				case ($agentsNum>=30): return 0.15; break;
				case ($agentsNum>=15): return 0.1;break;
				case ($agentsNum>=5): return 0.05;break;

			}
		}
		*/
	}
}
?>