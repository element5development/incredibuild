<?php
/*------------------------------------------------------------------------
# view.html.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML Thankyo View class for the Ibonlinestore Component
 */
class IbonlinestoreViewtrial_download extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		$app =& JFactory::getApplication();
		
		if (JRequest::getVar('sf')=="1" && JRequest::getVar('sf_email') && JRequest::getVar('sf_name'))
		{
			$sf = & JModel::getInstance('salesforce', 'IbonlinestoreModel');
			
			$sf->trial_account(JRequest::getVar('sf_email'),JRequest::getVar('sf_name'));
			
			die();
		}
		
		
		if (JRequest::getVar('t')=="1" && IbonlinestoreHelper:: userInfo())
		{
			parent::display("download");
		}
		else
		{
		
			setcookie("ib",$info,time()-36000000);
			JFactory::getSession()->clear('ib');
			$this->heardlist = $this->getHeardList();
			parent::display($tpl);
		}
			
	}
	
	private function getHeardList()
	{
		$db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__ibonlinestore_heardlist');
        $db->setQuery((string)$query);
        $data = $db->loadObjectList();


		return $data;
			

	}
}
?>