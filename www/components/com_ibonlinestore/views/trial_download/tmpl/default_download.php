<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filter.output');

$info = IbonlinestoreHelper::userInfo();

$arr['first_name'] = $info->first_name;
$arr['last_name'] = $info->last_name;
$arr['company'] = $info->company;
$arr['email'] = $info->email;
$arr['telephone'] = $info->telephone;
$arr['country'] = $info->country;

$data = IbonlinestoreHelper:: encrypt(join("|",$arr));

$new = JFactory::getSession()->get('new');

if ($new=="1")
	$heard = JFactory::getSession()->get('headrd_from');
	
$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
	
?>
      <div class="middlecontainer">
        <div class="left_pan">

          <div class="clear"></div>
          <h1>IncrediBuild Download Center</h1>
          
		  <iframe onload="iFrameHeight()"	id="blockrandom"
		name=""
		src="<?php echo 'https://'.$_SERVER['SERVER_NAME']; ?>/download_center_impl.php?t=<? echo $data; ?>&n=<? echo $new; ?>&h=<? echo $heard; ?>&a=<?php echo $_REQUEST['a'];?>"
		width="100%"
		height="1000"
		scrolling="auto"
		frameborder="0"
		class="wrapper" >
		No Iframes</iframe>
		  
        </div>
        <div class="clear"></div>
      </div>
