<?php
/*------------------------------------------------------------------------
# view.html.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML Registe View class for the Ibonlinestore Component
 */
class IbonlinestoreViewrecovery extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		$email = JRequest::getVar('e','','post');
		
		$this->revover = $this->recover($email);
		
		parent::display($tpl);
	}
	
	
	private function recover($email)
	{
	

		if ($email)
		{
			$info = IbonlinestoreHelper::checkExists("email = '".IbonlinestoreHelper::escape($email)."'");

			if ($info)
			{
					$newpass = rand(1000000,99999999);

					$db = JFactory::getDbo();
					$query = "update `bczfi_ibonlinestore_registe` set password ='".md5($newpass)."' where email='".$info[0]->email."'";
					$db->setQuery($query);
					$db->query(); 
					
					
					$mail =& JFactory::getMailer();
					$config =& JFactory::getConfig(); 
					$mail->addRecipient($info[0]->email);
					$mail ->isHTML(true); 
					$mail->setSubject( 'Password recovery' ); 
					$mail->setBody( "Your new password for is - ".$newpass.". Your username is - ".$info[0]->user_name ); 
					$mail->Send();
					
					return "1";
				
			}
			else
			{
				return "-1";
			}			

		}
			
	}
}
?>