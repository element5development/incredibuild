<?php
/*------------------------------------------------------------------------
# default.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');


if ($_POST['e']!="")
{
	if ($this->revover == "1")
	{
		echo "<br><center><font >Thank you - your login information was sent to your email.</font></center>";
		die();
	}
	else
	{
		echo "<br><center><font color=red>Email not found - please try again.</font></center><br><a href='#' onclick='history.back(-1);return false;' style='text-decoration:underline'><- Back</a>";
		die();
	}
	
}
	
?>
<html>
<body style='background-color:#EEEFF0 '>
<style>

body,html
{
	font-family: myriad pro, "Lucida Grande","Lucida Sans Unicode",Arial,Verdana,sans-serif;
	text-decoration: none;
}
</style>
<center>
<form method='post'>

Please type your email address:
<br>
<input type='text' name='e' style='width:90%'/><br>
*The email you've used when you registered to the site.
<br>
<input type='submit' value='Recover my password'>
</form>
</center>
</body>
</html>