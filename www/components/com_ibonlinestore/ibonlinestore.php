<?php
/*------------------------------------------------------------------------
# ibonlinestore.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// error_reporting(E_ALL);
// ini_set('display_errors', 1);


// Set the component css/js
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_ibonlinestore/assets/css/ibonlinestore.css');
$document->addStyleSheet('components/com_ibonlinestore/assets/css/jquery-ui-1.9.2.custom.css');
$document->addStyleSheet('components/com_ibonlinestore/assets/css/mesi.css');
$document->addScript('components/com_ibonlinestore/assets/js/jquery.js');
$document->addScript('components/com_ibonlinestore/assets/js/mesi.js');
$document->addScript('components/com_ibonlinestore/assets/js/ibonlinestore.js');

// Require helper file
JLoader::register('IbonlinestoreHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'ibonlinestore.php');

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Ibonlinestore
$controller = JController::getInstance('Ibonlinestore');

// Perform the request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
?>