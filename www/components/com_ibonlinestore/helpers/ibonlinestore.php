<?php
/*------------------------------------------------------------------------
# ibonlinestore.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Ibonlinestore component helper
 */
abstract class IbonlinestoreHelper
{

	public static function encrypt($str)
	{
			  for($i=0; $i<5;$i++)
			  {
				$str=strrev(base64_encode($str));
			  }
			  return $str;
	}
	
	public static function small_encrypt($str)
	{
			  for($i=0; $i<1;$i++)
			  {
				$str=strrev(base64_encode($str));
			  }
			  return $str;
	}
		 
	public static function decrypt($str)
	{
			for($i=0; $i<5;$i++)
			  {
				$str=base64_decode(strrev($str));
			  }
			  return $str;
	}
	
	public static function small_decrypt($str)
	{
			for($i=0; $i<1;$i++)
			  {
				$str=base64_decode(strrev($str));
			  }
			  return $str;
	}
	
	public static function itemQty($id)
	{
		$session = JFactory::getSession();
		$cart = $session->get('cart');
		
		return ($cart[$id]['qty'] !="") ? $cart[$id]['qty'] : 0;
			
	}
	
	public static function escape($data) {
        if ( !isset($data) or empty($data) ) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ( $non_displayables as $regex )
            $data = preg_replace( $regex, '', $data );
        $data = str_replace("'", "''", $data );
        return $data;
    }  
	
	public static function checkExists($where)
	{
	
		$db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__ibonlinestore_registe');
		$query->where($where);
        $db->setQuery((string)$query);
        $data = $db->loadObjectList();

		
		if (count($data)!=0)
			return $data;
			
		return false;
	}
	
	public static function get_coupon_details($code)
	{
		$db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('bczfi_ibonlinestore_coupon');
		$query->where("code = '".IbonlinestoreHelper::escape($code)."' and active=1");
        $db->setQuery((string)$query);
        $data = $db->loadObjectList();

		
		if (count($data)!=0)
			return $data;

		return false;
	
	}
	
	public static function userInfo($ib)
	{
	
	if (!$ib)
	{
		$session = JFactory::getSession();
		$ib = $session->get('ib');
	
	
	if ($_COOKIE['ib'])
	{
		$ib = $_COOKIE['ib'];
		$session->set('ib',$ib);
	}
	
	$tmp  = explode("|",IbonlinestoreHelper::decrypt($ib));
	
	}
	else
		$tmp  = $ib;
	
	$info =  IbonlinestoreHelper::checkExists(" id = '".IbonlinestoreHelper::escape($tmp[0])."'");
			
	return ($info) ? $info[0] : false;  
			
	}
	
	public function checkMaintIssue()
	{
	
	
	$cart = JFactory::getSession()->get('cart');


	$maintNum = 0;
	$agentsNum = 0;
	if (count($cart)!=0){	
		foreach ($cart as $key=>$item)
		{

			switch ($item[type])
			{

				case 0: $agentsNum += $item[qty]; break;
				case 2: $maintNum +=$item[qty]; break;
			}

		}

	}


	if ($agentsNum>$maintNum && $maintNum>0)
	{
		JFactory::getApplication()->redirect("renew?maintIssue=1");

		die();
	}
		
	
	
	}

}
?>