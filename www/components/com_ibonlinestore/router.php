<?php
/*------------------------------------------------------------------------
# router.php - IB Online Store Component
# ------------------------------------------------------------------------
# author    Daniel Krotoro
# copyright Copyright (C) 2013. All Rights Reserved
# license   GNU/GPL Version 2 or later - webmark.co.il
# website   www.webmark.co.il
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

function IbonlinestoreBuildRoute(&$query)
{
	$segments = array();

	if(isset($query['view'])):
		$segments[] = $query['view'];
		unset($query['view']);
	endif;

	if(isset($query['id'])):
		$segments[] = $query['id'];
		unset($query['id']);
	endif;

	return $segments;
}

function IbonlinestoreParseRoute($segments)
{
	$vars = array();
	// Count segments
	$count = count($segments);
	//Handle View and Identifier
	switch($segments[0])
	{
		case 'external_invoicing':
			$id = explode(':', $segments[$count-1]);
			$vars['id'] = (int) $id[0];
			$vars['view'] = 'external_invoicing';
			break;

        case 'ibonlinestore':
			$id = explode(':', $segments[$count-1]);
			$vars['id'] = (int) $id[0];
			$vars['view'] = 'ibonlinestore';
			break;

		case 'cart':
			$id = explode(':', $segments[$count-1]);
			$vars['id'] = (int) $id[0];
			$vars['view'] = 'cart';
			break;


		case 'register':
			$id = explode(':', $segments[$count-1]);
			$vars['id'] = (int) $id[0];
			$vars['view'] = 'register';
			break;

		case 'registe':
			$id = explode(':', $segments[$count-1]);
			$vars['id'] = (int) $id[0];
			$vars['view'] = 'registe';
			break;
		case 'thankyou':
			$id = explode(':', $segments[$count-1]);
			$vars['id'] = (int) $id[0];
			$vars['view'] = 'thankyou';
			break;

		case 'trial_download':
			$id = explode(':', $segments[$count-1]);
			$vars['id'] = (int) $id[0];
			$vars['view'] = 'trial_download';
			break;
			
		case 'login':
			$id = explode(':', $segments[$count-1]);
			$vars['id'] = (int) $id[0];
			$vars['view'] = 'login';
			break;
			
	}

	return $vars;
}
?>