<?
$new_security = false;
if (get_param('new_security'))
	$new_security = true;

// Download keys
require_once 'php_inc/dlkeys.inc.php';

function get_param($name)
{
  $ret = $_REQUEST[$name];
  $ret = str_replace("\\", "", $ret);
  if (!$ret)
    return "";
  else
    return $ret;
}
/*
if (!array_key_exists('key',$_REQUEST))
	$_REQUEST['key']='';
else
{


}
*/
	
$key = explode ("?key=",$_SERVER['HTTP_REFERER']);
	
$dlkey=new DlKey($key[1]);
$dlkey_success=$dlkey->ConsumeOne();
if ($dlkey_success)
{
	$dlkey->SetActivationSourceHost($_SERVER['REMOTE_ADDR']);
}
/*if (!$dlkey_success) {
	header('Location: /downloads/',true,301);
	exit();
}*/
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
/*
if ($dlkey_success && $dlkey->UseCount()==1 && $dlkey->GenerateLeadOnFirstUse())
{
	?>
	<form action='https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8' method='post'>
	<input type="hidden" name="retURL" value="<?php echo htmlspecialchars('http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);?>" />
	<input type="hidden" name="oid" value="00D200000000a6H" />
	<input type="hidden" name="sfga" value="00D200000000a6H" />
	<?php foreach($dlkey->LeadGenerationVars() as $key=>$value) { ?>
	  <input type="hidden" name="<? echo htmlspecialchars($key); ?>" value="<? echo htmlspecialchars($value); ?>" />
	<?php } ?>
	</form>
	<script type="text/javascript" language="JavaScript">document.forms[0].submit();</script>
	<?
	exit();
}
*/
?>
<!doctype html public '-//W3C//DTD HTML 3.2 Final//EN'>
<html>
  <head>
    <meta name='Description' content=''>
    <meta name='Keywords' content='grid engine, grid computing, parallel compilation, distributed compilation, parallel make, distributed make, compilation time, compilation speed, slow compilation, long compilation, slow project builds, long project builds, project build time, header optimization, precompiled headers, PCH'>
    <meta http-equiv='content-type' content='text/html; charset=UTF-8'>
    <link type='text/css' href='xoreax_css.css' rel='stylesheet' />
    <link type='application/rss+xml' href='feed/incredibuild.xml' rel='alternate' title='Xoreax Software News Feed' />
    <script type='text/javascript' src='xoerax_js.js'></script>
    <title>IncrediBuild Download Center - IncrediBuild by Xoreax Software</title>
  </head>
  <body onload='begin_download()'>
    <center>
              <table cellpadding='0' cellspacing='0' width='100%'>
<!-- Content Cell Begin-->
            <tr>
              <td class='simple_text'>
			  <?php if ($dlkey_success) { ?>
<p>
                  Your download will begin momentarily.
                </p>
				
                <p>
                  If download does not start, click <a href="/downloads<?=$new_security?'_strict':''?>/<? if ($new_security)
					{
						echo htmlspecialchars($dlkey->Key() . '/');
					}
					echo htmlspecialchars($dlkey->PHPResource()->exe_path); ?>">here</a> to download the file.<br>

                  A zipped version can be downloaded <a href='/downloads<?=$new_security?'_strict':''?>/<? if ($new_security)
					{
						echo htmlspecialchars($dlkey->Key() . '/');
					}
					echo htmlspecialchars($dlkey->PHPResource()->zip_path); ?>'>here</a>.
				</p>
				
				<p>
                  <div class='xuh2'>Installation</div>
                  For detailed installation instructions, please visit the <a href='/webhelp/webframe.htm#getting_started1.html' target='_blank'>
                  IncrediBuild Online User Manual</a>.
                </p>
                <p>
                  <div class='xuh2'>Support Resources</div>
                  The following resources are available for obtaining technical information:
                  <ul>
                    <li>
                      <a href='/webhelp/webframe.htm' target='_blank'>The IncrediBuild User Manual</a> - The user manual
                      fully covers every aspect of the product. The user manual is available both in an
                      <a href='/webhelp/webframe.htm' target='_blank'>online version</a>
                      and in .chm format (to open, right-click the IncrediBuild Agent tray-icon display and select "Help->Contents").
                    </li>
                    <li>
                      <a href='http://xoreax.helpserve.com/default_import/Knowledgebase/List' target='_blank'>The IncrediBuild Helpdesk Knowledgebase</a>
                      - This growing knowledgebase contains a continuously-updated database of answers and topics.
                    </li>
                    <li><a href='support_history.htm' target="_parent">The Version History Page</a></b> - A list of the changes made in each IncrediBuild version.</li>
                    <li><a href='support_faq.htm' target="_parent">The IncrediBuild FAQ Page</a></b> - Answers to frequently asked technical questions.</li>
                  </ul>
                </p>
				<?php } 
				else if ($dlkey->ExistsInDB())
				{
				?>
				<p>The download link you used has expired. You can get a new one <a href="incredibuild-download-center.htm" target="_parent">here</a>.</p>
				<?php
				}
				else
				{
				?>
				<p>We're sorry, but we could not process your download link. Get a new one <a href="incredibuild-download-center.htm" target="_parent">here</a>, or <a href="mailto:support@xoreax.com?Subject=Problem with Download Center link">contact support</a> if you're still experiencing issues.</p>
				<?php
				}
				?>				
                <script type='text/javascript'>
                  function begin_download()
                  {
				  <?php if ($dlkey_success) { ?>
                    document.location='/downloads<?=$new_security?'_strict':''?>/<? 
					if ($new_security)
					{
						echo addslashes($dlkey->Key() . '/');
					}
					echo addslashes($dlkey->PHPResource()->exe_path); ?>' ;
				  <?php } ?>
                  }
                </script>				
</td>
</tr><!-- Content Cell End-->
              </table>
            </td>
          </tr>
        </table>
        <!-- End Main Content Table -->
        
        <script type='text/javascript'>
          try {
            PlaceAdCookie();
          } catch(err) {}
        </script>
        <script type='text/javascript'>
          var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
          document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type='text/javascript' src="https://lct.salesforce.com/sfga.js"></script>
        <script type='text/javascript'>__sfga();</script>
		<!-- Google Code for Download Thank You Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1072692427;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "aVhlCLaJjgIQy_m__wM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1072692427/?value=0&amp;label=aVhlCLaJjgIQy_m__wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Google Code for Download Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 990059648;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "4DseCIjH9QQQgLmM2AM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>

<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/990059648/?value=0&amp;label=4DseCIjH9QQQgLmM2AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

    </center>
  </body>
</html>
